﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;

namespace Arkira.Core.ApplicationActivities
{
    //todo:remove direct access to actions and reviewers
    //todo:add correlationid to handle several instances of the save application and activity
    public class ApplicationActivity : Entity<Guid>
    {
        public virtual ICollection<string> Actions { get; internal set; } = new HashSet<string>();
        public virtual string ActivityId { get; internal set; }
        public string ApplicationId { get; set; }
        public virtual ICollection<string> Reviewers { get; private set; } = new HashSet<string>();
        public bool IsCompleted { get; set; }
        public string Outcome { get; set; } = default!;

        public static readonly string EntityName = "Application Activity";

        protected ApplicationActivity()
        {
        }

        public ApplicationActivity(string activityId, string applicationId, ICollection<string> reviewerIds, ICollection<string> actions)
        {
            ValidateParameters(activityId, applicationId, reviewerIds, actions);

            ActivityId = activityId;
            ApplicationId = applicationId;
            IsCompleted = false;

            AddReviewers(reviewerIds);
            AddActions(actions);
        }

        public void AddActions(ICollection<string> actions)
        {
            Actions.Clear();
            foreach (var action in actions)
                Actions.Add(action);
        }

        public void AddReviewers(ICollection<string> reviewerIds)
        {
            Reviewers.Clear();
            foreach (var id in reviewerIds)
                Reviewers.Add(id);
        }

        public void RemoveReviewer(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return;
            Reviewers.Remove(id);
        }

        public override string ToString() => EntityName;

        private void ValidateParameters(string activityId, string applicationId, ICollection<string> reviewerIds, ICollection<string> actions)
        {
            if (string.IsNullOrWhiteSpace(activityId) || string.IsNullOrWhiteSpace(applicationId) || reviewerIds == null || reviewerIds.Count < 1 || actions == null || actions.Count < 1)
                throw new ApplicationActivityException("Error while recording application activity", new ArgumentException($"One or more arguments is invalid: {applicationId} ,{activityId},{reviewerIds},{actions}"));
        }

        internal void CompleteActivity(string outcome)
        {
            Outcome = outcome;
            IsCompleted = true;
        }
    }
}