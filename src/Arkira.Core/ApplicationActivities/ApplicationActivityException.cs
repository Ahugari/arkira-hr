﻿using System;

namespace Arkira.Core.ApplicationActivities
{
    public class ApplicationActivityException : Exception
    {
        public ApplicationActivityException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}