﻿using Arkira.Core.Repositories;
using System.Threading.Tasks;

namespace Arkira.Core.ApplicationActivities
{
    public interface IApplicationActivityRepository : IRead<ApplicationActivity>, ISave<ApplicationActivity>, IRepository<ApplicationActivity>
    {
        public Task<ApplicationActivity> ReadByActivityIdAsync(string activityId);
    }
}