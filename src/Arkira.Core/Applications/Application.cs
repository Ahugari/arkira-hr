﻿using Arkira.Core.Entities;
using Arkira.Core.Enums;
using System;
using System.Collections.Generic;

namespace Arkira.Core.Applications
{
    public abstract class Application : Entity<Guid>
    {
        public virtual Guid ApplicantId { get; protected set; }
        public ApplicationStatus Status { get; protected set; }
        public Guid? InitiatorId { get; set; }
        public Dictionary<string, HashSet<string>> Actions { get; internal set; } = new Dictionary<string, HashSet<string>>();

        public virtual string OngoingActivityId { get; set; }

        public abstract string GetName();

        public abstract override string ToString();

        public virtual IEnumerable<Dictionary<string, HashSet<string>>> GetAllActions()
        {
            throw new NotImplementedException();
        }

        public virtual void HandleApplicationReview(IApplicationReviewAction action)
        {
            action.Process(this);
        }

        public bool ProcessReview(string action)
        {
            switch (action.Trim())
            {
                case ApplicationReviewActions.Recommend:
                    RecommendApplication();
                    break;

                case ApplicationReviewActions.Approve:
                    ApproveApplication();
                    break;

                case ApplicationReviewActions.Withdraw:
                    WithdrawApplication();
                    break;

                case ApplicationReviewActions.Archive:
                    ArchiveApplication();
                    break;

                case ApplicationReviewActions.Reject:
                    RejectApplication();
                    break;

                default:
                    throw new ApplicationFormException("Error while processing application review.", new ArgumentException($"Invalid action {action}"));
            }

            CompleteOngoingActivity();
            return true;
        }

        public bool ProcessCreation(string action)
        {
            switch (action.Trim())
            {
                case ApplicationCreationActions.Draft:
                    return DraftApplication();

                case ApplicationCreationActions.Submit:
                    return SubmitApplication();

                default:
                    return false;
            }
        }
        public void UpdateOngoingActivity(string activityId)
        {
            OngoingActivityId = activityId;
        }

        protected bool RecommendApplication()
        {
            Status = ApplicationStatus.Pending;

            return true;
        }

        protected bool SubmitApplication()
        {
            Status = ApplicationStatus.Submitted;

            return true;
        }

        protected bool DraftApplication()
        {
            Status = ApplicationStatus.Drafted;
            return true;
        }

        protected bool ApproveApplication()
        {
            Status = ApplicationStatus.Approved;
            return true;
        }

        protected bool RejectApplication()
        {
            Status = ApplicationStatus.Rejected;
            return true;
        }

        protected bool WithdrawApplication()
        {
            Status = ApplicationStatus.Withdrawn;
            return true;
        }

        protected bool ArchiveApplication()
        {
            Status = ApplicationStatus.Archived;
            return true;
        }

        private void CompleteOngoingActivity()
        {
            OngoingActivityId = string.Empty;
        }
    }
}