﻿namespace Arkira.Core.Applications
{
    public static class ApplicationReviewActions
    {
        public const string Recommend = "recommend";
        public const string Approve = "approve";
        public const string Reject = "reject";
        public const string Withdraw = "withdraw";
        public const string Archive = "archive";

        public static bool Contains(string action)
        {
            return action.Trim().ToLower() switch
            {
                Recommend => true,
                Approve => true,
                Reject => true,
                Withdraw => true,
                Archive => true,
                _ => false,
            };
        }
    }

    public static class ApplicationCreationActions
    {
        public const string Draft = "draft";
        public const string Submit = "submit";
    }
}