﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.ApplicationsHistory;
using Arkira.Core.Employees;
using Arkira.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Applications
{
    public struct ShortCode
    {
        public ShortCode(string name, string identifier)
        {
            Name = name;
            Identifier = identifier;
        }

        public string Identifier { get; set; }
        public string Name { get; set; }
    }

    public class ApplicationDomainManager : IApplicationDomainManager
    {
        private readonly IApplicationActivityRepository _applicationActivity;
        private readonly IApplicationHistoryRepository _applicationHistory;
        private readonly IApplicationHistoryDomainService _applicationHistoryDomainService;
        private readonly IEmployeeDomainService _employeeDomainService;

        public ApplicationDomainManager(IEmployeeDomainService employeeDomainService,
                                        IApplicationHistoryRepository applicationHistory,
                                        IApplicationActivityRepository applicationActivity,
                                        IApplicationHistoryDomainService applicationHistoryDomainService)
        {
            _employeeDomainService = employeeDomainService;
            _applicationHistory = applicationHistory;
            _applicationActivity = applicationActivity;
            _applicationHistoryDomainService = applicationHistoryDomainService;
        }

        public async Task<Employee> GetApplicantAsync(Application application, CancellationToken cancellationToken)
        {
            ValidateApplication(application);

            return await _employeeDomainService.GetEmployeeByIdAsync(application.ApplicantId.ToString(), cancellationToken);
        }

        public async Task<HashSet<Reviewer>> GetReviewersByTagAsync(string tag, string applicantId, CancellationToken cancellationToken = default)
        {
            //target: {reviewerName:"John Doe", id:"839hwfd8-hd98f-e8hff-ew8fh23ehd9" }
            ValidateArguments(tag, applicantId);
            ShortCode shortCode = TryDecodeTag(tag);
            HashSet<Reviewer> reviewers = new();

            switch (shortCode.Name)
            {
                case "job title":
                    reviewers.UnionWith(await GetReviewersByJobTitle(shortCode.Identifier, cancellationToken));
                    break;

                case "employee":
                    reviewers.Add(await GetReviewerByEmployeeId(shortCode.Identifier, cancellationToken));
                    break;

                case "manager":
                    reviewers.Add(await GetReviewerByManagerIdAsync(applicantId, cancellationToken));
                    break;

                case "supervisor":
                    reviewers.Add(await GetReviewerBySupervisorIdAsync(applicantId, cancellationToken));
                    break;

                default:
                    throw new ApplicationFormException("Error while getting reviwers.", new ArgumentException("Invalid tag."));
            }

            return reviewers;
        }

        public async Task<ApplicationProcessResult> ProcessAsync(Application application, string reviewerId, ICollection<Reviewer> reviewerIds, string reviewerAction, string reviewActivityId, CancellationToken cancellationToken = default)
        {
            //while processing an application, find out whether they are more one reviews required at this stage and find the right algo to handle it
            //if its only one review required, pass it to the concerned algo
            var applicationResult = new ApplicationProcessResult();

            try
            {
                ValidateApplication(application);
                ValidateReviewersList(reviewerIds);
                await ValidateReviewerActionAsync(application, reviewerAction);
                CheckIfReviewerExistsInList(application, reviewerId, reviewerIds);

                var reviewer = await _employeeDomainService.GetEmployeeByIdAsync(reviewerId, cancellationToken);

                if (reviewerIds.Count > 1)
                    applicationResult.ReviewCompleted = await ProcessAsMultiReviewAsync(application, reviewer, reviewerAction, reviewActivityId);
                else if (reviewerIds.Count == 1)
                    applicationResult.ReviewCompleted = await ProcessAsSingleReviewAsync(application, reviewer, reviewerAction, reviewActivityId);
            }
            catch (ApplicationFormException ex)
            {
                applicationResult.ReviewCompleted = false;
                applicationResult.Error = ex;
            }
            finally
            {
                applicationResult.ProcessCompleted = true;
            }

            return applicationResult;
        }

        private static void CheckIfReviewerExistsInList(Application application, string reviewerId, ICollection<Reviewer> reviewerIds)
        {
            if (!reviewerIds.Any(x => x.Id == reviewerId))
                throw new ApplicationFormException("Error while processing application review.", new InvalidOperationException($"Invalid reviewer ({reviewerId}) for application ({application.Id})."));
        }

        private static ShortCode TryDecodeTag(string tag)
        {
            ShortCode shortCode;
            try
            {
                shortCode = IdTagService.DecodeTag(tag);
            }
            catch (TagException ex)
            {
                throw new ApplicationFormException("Error while getting reviewers.", ex);
            }

            return shortCode;
        }

        private static void ValidateApplication(Application application)
        {
            if (application == null)
                throw new ApplicationFormException("Error while processing application review.", new ArgumentNullException(nameof(application), "Argument cannot be null."));
        }

        private static void ValidateArguments(string tag, string applicantId)
        {
            if (string.IsNullOrWhiteSpace(tag) || string.IsNullOrWhiteSpace(applicantId))
                throw new ApplicationFormException("Error while getting reviewers.", new ArgumentException("Invalid tag."));
        }

        private static void ValidateReviewersList(ICollection<Reviewer> reviewers)
        {
            if (reviewers == null || reviewers.Count < 1)
                throw new ApplicationFormException("Error while processing application review.", new InvalidOperationException("Reviewers list cannot be empty."));
        }

        private async Task<ApplicationHistory> GetExistingReviewerApplicationActivity(Application application, Employee reviewer)
        {
            return await _applicationHistory.GetApplicationReviewerActivity(application.Id.ToString(), application.OngoingActivityId, reviewer.Id.ToString());
        }

        private async Task<Reviewer> GetReviewerByEmployeeId(string identifier, CancellationToken cancellationToken)
        {
            Employee employee = await _employeeDomainService.GetEmployeeByIdAsync(identifier, cancellationToken);

            return new Reviewer(employee.Id.ToString(), employee.GetFullName());
        }

        private async Task<Reviewer> GetReviewerByManagerIdAsync(string applicantId, CancellationToken cancellationToken)
        {
            Employee employee = await _employeeDomainService.GetManagerByIdAsync(applicantId, cancellationToken);

            return new Reviewer(employee.Id.ToString(), employee.GetFullName());
        }

        private async Task<Reviewer> GetReviewerBySupervisorIdAsync(string applicantId, CancellationToken cancellationToken)
        {
            Employee employee = await _employeeDomainService.GetSupervisorByIdAsync(applicantId, cancellationToken);

            return new Reviewer(employee.Id.ToString(), employee.GetFullName());
        }

        private async Task<ISet<Reviewer>> GetReviewersByJobTitle(string identifier, CancellationToken cancellationToken)
        {
            var employees = (await _employeeDomainService.GetEmployeesByJobTitleAsync(identifier, cancellationToken));
            HashSet<Reviewer> reviewers = new();

            if (employees == null || !employees.Any())
                return reviewers;

            foreach (var employee in employees)
                reviewers.Add(new Reviewer { Id = employee.Id.ToString(), FullName = employee.GetFullName() });

            return reviewers;
        }

        private async Task<bool> ProcessAsMultiReviewAsync(Application application, Employee reviewer, string reviewerAction, string reviewActivityId)
        {
            if ((await GetExistingReviewerApplicationActivity(application, reviewer)) != null)
                return false;

            bool activityCompleted = await TryCompleteActivity(application, reviewerAction);

            await RecordApplicationActivity(application, reviewer, reviewerAction, activityCompleted, reviewActivityId);

            return activityCompleted;
        }

        private async Task<bool> ProcessAsSingleReviewAsync(Application application, Employee reviewer, string reviewerAction, string reviewActivityId)
        {
            if ((await GetExistingReviewerApplicationActivity(application, reviewer)) != null)
                return false;

            await RecordApplicationActivity(application, reviewer, reviewerAction, true, reviewActivityId);
            return application.ProcessReview(reviewerAction);
        }

        private async Task RecordApplicationActivity(Application application, Employee reviewer, string reviewerAction, bool activityCompleted, string reviewActivityId)
        {
            if (activityCompleted)
            {
                var applicationActivity = await _applicationActivity.ReadByActivityIdAsync(reviewActivityId);
                applicationActivity.CompleteActivity(reviewerAction);
                await _applicationActivity.SaveAsync(applicationActivity);
            }

            await _applicationHistoryDomainService.AddAsync(application.Id.ToString(), application.ToString(), application.OngoingActivityId, reviewerAction, reviewer.GetFullName(), reviewer.Id.ToString());
        }

        private async Task<bool> TryCompleteActivity(Application application, string reviewerAction)
        {
            var applicationActivity = await _applicationActivity.ReadOneAsync(Transform.ToGuid(application.OngoingActivityId));
            var totalNumberOfReviewers = applicationActivity.Reviewers.Count;
            var completedSimilarReviews = await _applicationHistory.GetAllByActionAndApplicationActivityId(application.OngoingActivityId, reviewerAction);

            bool activityCompleted = false;
            decimal overallActionCompletion = (decimal)completedSimilarReviews.Count() / totalNumberOfReviewers * 100;
            if (overallActionCompletion >= 50)
                activityCompleted = application.ProcessReview(reviewerAction);

            return activityCompleted;
        }

        private async Task ValidateReviewerActionAsync(Application application, string reviewerAction)
        {
            var activity = await _applicationActivity.ReadByActivityIdAsync(application.OngoingActivityId);

            if (string.IsNullOrWhiteSpace(reviewerAction) || !ApplicationReviewActions.Contains(reviewerAction.ToLower()))
                throw new ApplicationFormException("Error while processing application review.", new ArgumentException("Invalid review action."));

            if (!activity.Actions.Any(x => x.Equals(reviewerAction, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationFormException("Error while processing application review.", new ArgumentException("Review action not allowed for application."));
        }
    }
}