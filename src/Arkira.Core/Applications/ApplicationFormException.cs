﻿using System;

namespace Arkira.Core.Applications
{
    public class ApplicationFormException : Exception
    {
        public ApplicationFormException(string message) : base(message)
        {
        }

        public ApplicationFormException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}