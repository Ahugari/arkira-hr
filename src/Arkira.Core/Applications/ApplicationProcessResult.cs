﻿using System;

namespace Arkira.Core.Applications
{
    public class ApplicationProcessResult
    {
        public bool ReviewCompleted { get; set; }
        public bool ProcessCompleted { get; set; }
        public Exception Error { get; set; } = default;
    }
}