﻿using Arkira.Core.Services;

namespace Arkira.Core.Applications.Events
{
    public record ApplicationPrepared<T>(Application Application, string ActionName) : IAppNotification<T> where T : Application;
}