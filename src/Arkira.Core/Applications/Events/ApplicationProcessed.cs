﻿using Arkira.Core.Services;

namespace Arkira.Core.Applications.Events
{
    public record ApplicationProcessed<T>(string ApplicationId, string ApplicationAction, string ActionPerformedById, string ApplicationName, string ActivityId) : IAppNotification<T> where T : Application;
}