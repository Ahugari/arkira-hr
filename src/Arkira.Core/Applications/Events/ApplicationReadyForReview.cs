﻿using Arkira.Core.Services;
using System.Collections.Generic;

namespace Arkira.Core.Applications.Events
{
    public record ApplicationReadyForReview<T>(Application Application, string ReviewerId, string ReviewerName, HashSet<string> Actions) : IAppNotification<T> where T : Application;
}