﻿using Arkira.Core.Services;

namespace Arkira.Core.Applications.Events
{
    public record ApplicationReadyForReviewSend<T>(WorkflowReviewApplication WorkflowReviewApplication) : IAppNotification<T> where T : Application;
}