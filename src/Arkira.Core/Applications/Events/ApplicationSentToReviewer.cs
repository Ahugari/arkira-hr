﻿using Arkira.Core.Services;
using System.Collections.Generic;

namespace Arkira.Core.Applications.Events
{
    public record ApplicationSentToReviewer<T>(Application Application, string ApplicantName, HashSet<Reviewer> Reviewers, HashSet<string> ApplicationWatchers) : IAppNotification<T> where T : Application;
}