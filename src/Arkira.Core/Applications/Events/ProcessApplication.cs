﻿using MediatR;

namespace Arkira.Core.Applications.Events
{
    public record ProcessApplication(string ApplicationTypeName, string ApplicationAction, string ActionPerformedById, string ApplicationId) : INotification;
}