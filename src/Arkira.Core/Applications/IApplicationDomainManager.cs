﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Applications
{
    public interface IApplicationDomainManager
    {
        Task<HashSet<Reviewer>> GetReviewersByTagAsync(string tag, string applicantId, CancellationToken cancellationToken = default);

        Task<ApplicationProcessResult> ProcessAsync(Application application, string reviewerId, ICollection<Reviewer> reviewerIds, string reviewerAction, string reviewActivityId, CancellationToken cancellationToken = default);
    }
}