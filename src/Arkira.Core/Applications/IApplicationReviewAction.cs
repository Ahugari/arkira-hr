﻿namespace Arkira.Core.Applications
{
    public interface IApplicationReviewAction
    {
        void GetName();

        void Process(Application application);
    }
}