﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Arkira.Core.Applications
{
    public static class IdTagService
    {
        private static Dictionary<string, string> ShortCodeNames = new()
        {
            { "job title", "jbt" },
            { "supervisor", "spr" },
            { "manager", "mgr" },
            { "employee", "emp" }
        };

        public static ImmutableDictionary<string, string> GetTagCodes()
        {
            return ShortCodeNames.ToImmutableDictionary();
        }

        public static ShortCode DecodeTag(string tag)
        {
            //target: {name: "job title", identifier: "id0f903-sdf46f-f58nfi-23rj89fje2re2"}

            if (string.IsNullOrWhiteSpace(tag))
                throw new TagException("Error while decoding tag.", new ArgumentException("Tag cannot be empty or null"));

            var splitTag = tag.ToLower().Split(':');
            var shortCodeName = splitTag[0];

            if (!ShortCodeNames.ContainsValue(shortCodeName))
                throw new TagException("Error while decoding tag.", new ArgumentException("Invalid tag."));

            var shortCodeFullName = ShortCodeNames.FirstOrDefault(x => x.Value.Equals(shortCodeName, StringComparison.OrdinalIgnoreCase)).Key;

            return new(shortCodeFullName, splitTag[1]);
        }

        public static string EncodeAsTag(string typeName)
        {
            //target - "spr"

            var appShortCodeBuilder = new StringBuilder();
            typeName = typeName.ToLower().Trim();

            if (!ShortCodeNames.ContainsKey(typeName))
                throw new TagException("Error while encoding tag.", new ArgumentException("Invalid type name."));

            return appShortCodeBuilder.Append(ShortCodeNames[typeName]).ToString();
        }

        public static string EncodeAsTag(string typeName, string value)
        {
            //target - "jbt:asdf902-sdif2-2390f-2389hfsdio"

            var appShortCodeBuilder = new StringBuilder();
            typeName = typeName.ToLower().Trim();

            if (!ShortCodeNames.ContainsKey(typeName))
                throw new TagException("Error while validating tag.", new ArgumentException("Invalid type name."));

            appShortCodeBuilder.Append(ShortCodeNames[typeName]);
            appShortCodeBuilder.Append(':');
            appShortCodeBuilder.Append(value.ToLower().Trim());

            return appShortCodeBuilder.ToString();
        }
    }
}