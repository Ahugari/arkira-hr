﻿namespace Arkira.Core.Applications
{
    public struct Reviewer
    {
        public Reviewer(string id, string fullName)
        {
            Id = id;
            FullName = fullName;
        }

        public string Id { get; set; }
        public string FullName { get; set; }
    }
}