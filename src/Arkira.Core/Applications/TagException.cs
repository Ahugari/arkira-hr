﻿using System;

namespace Arkira.Core.Applications
{
    public class TagException : Exception
    {
        public TagException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}