﻿using System.Collections.Generic;

namespace Arkira.Core.Applications
{
    public record WorkflowReviewApplication(string ApplicationId,
                                            HashSet<Reviewer> Reviewers,
                                            HashSet<string> ReviewActions,
                                            HashSet<string> SubscribedJobTitleIds,
                                            string WorkflowActivityId);
}