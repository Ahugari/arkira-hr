﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.ApplicationsHistory
{
    public class ApplicationHistory : Entity<Guid>, IHasCreationDate
    {
        public virtual string ApplicationId { get; private set; }
        public string ApplicationType { get; private set; }
        public string ActionPerformed { get; private set; }
        public string ActionPerformerId { get; private set; }
        public string ActionPerformer { get; private set; }
        public string ReviewActivityId { get; private set; }
        public bool UserActionsRequired { get; private set; }
        public DateTime DateCreated { get; private set; }

        protected ApplicationHistory()
        {
        }

        public ApplicationHistory(string applicationId, string applicationTypeName, string actionPerformed, string actionPerformer, string actionPerformerId, string reviewActivityId = null)
        {
            ApplicationId = applicationId;
            ApplicationType = applicationTypeName;
            ActionPerformed = actionPerformed;
            ActionPerformer = actionPerformer;
            ActionPerformerId = actionPerformerId;
            UserActionsRequired = string.IsNullOrWhiteSpace(reviewActivityId);
            ReviewActivityId = reviewActivityId;

            DateCreated = DateTime.Now;
        }

        public static string EntityName => "Application History";

        public override string ToString() => EntityName;
    }
}