﻿using Arkira.Core.Applications;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.ApplicationsHistory
{
    public interface IApplicationHistoryDomainService
    {
        Task AddAsync(string applicationId, string applicationTypeName, string reviewActivityId, string reviewerAction, string reviewerName, string reviewerId);
    }

    public class ApplicationHistoryDomainService : IApplicationHistoryDomainService
    {
        private readonly IApplicationHistoryRepository _repository;

        public ApplicationHistoryDomainService(IApplicationHistoryRepository repository)
        {
            _repository = repository;
        }

        public async Task AddAsync(string applicationId, string applicationTypeName, string reviewActivityId, string reviewerAction, string reviewerName, string reviewerId)
        {
            ValidateParameters(applicationId, applicationTypeName, reviewActivityId, reviewerAction, reviewerId);

            var applicationHistory = new ApplicationHistory(applicationId, applicationTypeName, reviewerAction, reviewerName, reviewerId, reviewActivityId);

            await _repository.SaveAsync(applicationHistory);
        }

        private void ValidateParameters(string applicationId, string applicationTypeName, string reviewActivityId, string reviewerAction, string reviewerId)
        {
            if (string.IsNullOrWhiteSpace(applicationId) || string.IsNullOrWhiteSpace(applicationTypeName) || string.IsNullOrWhiteSpace(reviewerAction) || string.IsNullOrWhiteSpace(reviewerId))
                throw new ApplicationHistoryException("Error while recording application history.", new ArgumentException("Invalid arguments provided."));

            if (!ApplicationReviewActions.Contains(reviewerAction))
                throw new ApplicationHistoryException("Error while recording application history.", new ArgumentException($"Invalid reviewer action provided: {reviewerAction}"));
        }
    }
}