﻿using System;

namespace Arkira.Core.ApplicationsHistory
{
    public class ApplicationHistoryException : Exception
    {
        public ApplicationHistoryException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}