﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.LeaveApplications;
using Arkira.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.ApplicationsHistory.Handlers
{
    //TODO:change this to only be called by application types and nothing else
    internal class RecordApplicationHistory : IAppNotificationHandler<ApplicationPrepared<LeaveApplication>>
    {
        private readonly IApplicationHistoryRepository _historyRepository;
        private readonly IApplicationActivityRepository _activityRepository;

        public RecordApplicationHistory(IApplicationHistoryRepository repository, IApplicationActivityRepository activityRepository)
        {
            _historyRepository = repository;
            _activityRepository = activityRepository;
        }

        public async Task Handle(ApplicationPrepared<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            //TODO:replace initiatorId with actual name of initiator
            var action = ReformApplicationAction(notification.ActionName);
            await _historyRepository.SaveAsync(new ApplicationHistory(notification.Application.Id.ToString(), notification.Application.GetName(), action, notification.Application.InitiatorId.ToString(), notification.Application.InitiatorId.ToString()));
        }

        private string ReformApplicationAction(string actionName)
        {
            return actionName.ToLower().Contains(ApplicationCreationActions.Submit)
                ? ApplicationCreationActions.Submit
                : ApplicationCreationActions.Draft;
        }
    }
}