﻿using Arkira.Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arkira.Core.ApplicationsHistory
{
    public interface IApplicationHistoryRepository : ISave<ApplicationHistory>, IRead<ApplicationHistory>, IRepository<ApplicationHistory>
    {
        Task<ApplicationHistory> GetApplicationReviewerActivity(string applicationId, string activityId, string reviewerId);

        Task<IEnumerable<ApplicationHistory>> GetAllByActionAndApplicationActivityId(string activityId, string action);
    }
}