﻿namespace Arkira.Core
{
    public struct ArkiraConsts
    {
        public const int MIN_NUMBER_OF_YEARS_ALLOWED = 16;
        public const int MAX_NUMBER_OF_YEARS_ALLOWED = 95;
        public const int MIN_ENTITLEMENT_CAP_ALLOWED = 1;
        public const int MAX_ENTITLEMENT_CAP_ALLOWED = 365;
        public const int MIN_CARRY_OVER_DAYS_ALLOWED = 1;
        public const int MAX_CARRY_OVER_DAYS_ALLOWED = 50;
        public const int MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED = -50;
        public const int MAX_NEGATIVE_CARRY_OVER_DAYS_ALLOWED = -1;
        public const int MIN_MONTHS_PERIOD_IN_FUTURE = 0;
        public const int MAX_MONTHS_PERIOD_IN_FUTURE = 60;
        public const int MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION = 1;
        public const int MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION = 4;
        public const int MIN_MONTHS_FOR_CARRY_OVER_EXPIRY = 1;
        public const int MAX_MONTHS_FOR_CARRY_OVER_EXPIRY = 12;
        public const int MIN_NEGATIVE_ENTITLEMENT = -60;
        public const int MAX_NEGATIVE_ENTITLEMENT = -1;
        public const int FUTURE_EFFECTIVE_DATE_YEARS_LIMIT = 1;
        public const int NUMBER_OF_MONTHS_IN_YEAR = 12;
        public const int MIN_DAYS_TO_ENROLLMENT = 0;
        public const int MAX_DAYS_TO_ENROLLMENT = 1825;
        public const int MIN_DAYS_FOR_LEAVE_APPLICATION_ALLOWED = 1;
        public const int MIN_PERCENTAGE_FOR_SPLIT_YEAR_LEAVE_DEDUCTION = 25;

        public const string DB_PREFIX = "Arkira";
        public const string LEAVE_APPLICATIONS_WORKFLOW_TAG = "Leave Application";
        public const string APPLICATIONS_WORKFLOW_TAG = "Application Id";
        public const string APPLICATIONS_TYPE_WORKFLOW_TAG = "Application Type Id";
        public const string APPLICATIONS_WORKFLOW_REVIEWER_TAG = "Application Reviewer Id";
        public const string APPLICATIONS_WORKFLOW_REVIEWERS_LIST = "Reviewers List";
        public const string APPLICATIONS_WORKFLOW_REVIEWER_ACTIONS_LIST = "Reviewer Actions List";

        public static readonly char[] specialCharacters = new char[] {'!','@','#','$','%','^','&','*','(',')',
                '_','-','+','=','[','{',']','}',':',';','\'','"',',','<','.','>','/',
                '?','|','\\','`','~'};
    }
}