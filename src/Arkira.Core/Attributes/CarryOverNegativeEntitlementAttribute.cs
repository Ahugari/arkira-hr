﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class CarryOverNegativeEntitlementAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.AllowNegativeEntitlement && leaveType.CarryOverNegativeEntitlement)
                return new ValidationResult("Enable Negative Leave Entitlement before enabling Carry Over Negative Leave Entitlement.");

            return ValidationResult.Success;
        }
    }
}