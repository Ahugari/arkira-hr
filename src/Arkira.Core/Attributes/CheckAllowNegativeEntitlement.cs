﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class CheckAllowNegativeEntitlement : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.AllowNegativeEntitlement && leaveType.MaxNegativeEntitlementBelowZero != null)
                return new ValidationResult("Enable Negative Leave Entitlement before setting Maximum Negative Leave Entitlement Below Zero.");

            if (leaveType.AllowNegativeEntitlement
                && (leaveType.MaxNegativeEntitlementBelowZero == null
                || leaveType.MaxNegativeEntitlementBelowZero > ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED))
                return new ValidationResult("Provide a valid number of days below Zero.");

            return ValidationResult.Success;
        }
    }
}