﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class CheckCarryOverLeaveYearStartAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (leaveType.CarryOverUnusedEntitlement
                && leaveType.CarryOverLeaveYearStarts == Enums.CarryOverLeaveYearStarts.AnniversaryOfHireDate
                && leaveType.CarryOverLeaveYearStartDate != null)
                return new ValidationResult("Cannot use Carry Over Leave Start Year Date with selected Carry Over Leave Year Starts option.");

            if (leaveType.CarryOverUnusedEntitlement
                && leaveType.CarryOverLeaveYearStarts == Enums.CarryOverLeaveYearStarts.GivenDayAndMonth
                && leaveType.CarryOverLeaveYearStartDate == null)
                return new ValidationResult("Provide valid Carry Over Leave Start Year date");

            return ValidationResult.Success;
        }
    }
}