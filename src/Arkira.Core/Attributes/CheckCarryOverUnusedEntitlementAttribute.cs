﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class CheckCarryOverUnusedEntitlementAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.CarryOverUnusedEntitlement
                && leaveType.CarryOverLeaveYearStarts != null)
                return new ValidationResult("Enable Carry Over Unused Entitlement before picking Leave Year Start options.");

            return ValidationResult.Success;
        }
    }
}