﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class CheckLeaveRequestWindowAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (leaveType.LeaveRequestWindow == Enums.LeaveRequestWindow.SomePeriodInFuture
                && (leaveType.MaxMonthsPeriodInFuture < ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE
                || leaveType.MaxMonthsPeriodInFuture > ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE))
                return new ValidationResult($"Provide valid number of Months in future. Should be more than {ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE - 1} and less than {ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1} months");

            return ValidationResult.Success;
        }
    }
}