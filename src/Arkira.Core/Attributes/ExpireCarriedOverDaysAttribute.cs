﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class ExpireCarriedOverDaysAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.CarryOverNegativeEntitlement && leaveType.ExpireCarriedOverDays)
                return new ValidationResult($"Enable Carry Over Negative entitlement before enabling Expire Carried Over days.");

            return ValidationResult.Success;
        }
    }
}