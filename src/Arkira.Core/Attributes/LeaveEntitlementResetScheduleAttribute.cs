﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class LeaveEntitlementResetScheduleAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (leaveType.LeaveEntitlementReset == Enums.LeaveEntitlementReset.ResetOnSchedule && leaveType.LeaveEntitlementResetScheduleDate == null)
                return new ValidationResult("Provide leave reset schedule date.");

            return ValidationResult.Success;
        }
    }
}