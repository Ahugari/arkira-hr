﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class LeaveRequestBalanceDeductionAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if ((LeaveRequestWindowRequiresDeductions(leaveType))
                && leaveType.NewYearLeaveRequestDeductionOptions == null)
                return new ValidationResult("Select suitable New year Leave Request Deduction option to continue.");

            return ValidationResult.Success;
        }

        private static bool LeaveRequestWindowRequiresDeductions(LeaveType leaveType)
        {
            return leaveType.LeaveRequestWindow == Enums.LeaveRequestWindow.CurrentAndFollowingYear ^ leaveType.LeaveRequestWindow == Enums.LeaveRequestWindow.SomePeriodInFuture;
        }
    }
}