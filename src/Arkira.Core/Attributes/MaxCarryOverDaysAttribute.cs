﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class MaxCarryOverDaysAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (leaveType.CarryOverUnusedEntitlement && (leaveType.MaxCarryOverDays == null || leaveType.MaxCarryOverDays < 1))
                return new ValidationResult("Provide valid number of carry over days.");

            return ValidationResult.Success;
        }
    }
}