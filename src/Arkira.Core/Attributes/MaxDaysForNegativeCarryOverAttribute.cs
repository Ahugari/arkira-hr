﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class MaxDaysForNegativeCarryOverAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.AllowNegativeEntitlement && leaveType.CarryOverNegativeEntitlement)
                return new ValidationResult("Enable Negative Leave Entitlement to set maximum days to carry over.");

            if (leaveType.AllowNegativeEntitlement && leaveType.CarryOverNegativeEntitlement
                && (leaveType.MaxNegativeCarryOverDays == null || leaveType.MaxNegativeCarryOverDays < 1))
                return new ValidationResult("Provide valid days for Maximum Negative Carry Over Entitlement.");

            return ValidationResult.Success;
        }
    }
}