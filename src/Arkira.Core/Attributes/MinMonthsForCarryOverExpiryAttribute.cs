﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class MinMonthsForCarryOverExpiryAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (!leaveType.AllowNegativeEntitlement && leaveType.CarryOverNegativeEntitlement && leaveType.ExpireCarriedOverDays)
                return new ValidationResult("Enable negative leave entitlement to set months for carry over expiry.");

            if (leaveType.ExpireCarriedOverDays
                && (leaveType.MonthsForCarryOverExpiry == null
                || leaveType.MonthsForCarryOverExpiry < ArkiraConsts.MIN_MONTHS_FOR_CARRY_OVER_EXPIRY
                || leaveType.MonthsForCarryOverExpiry > ArkiraConsts.MAX_MONTHS_FOR_CARRY_OVER_EXPIRY))
                return new ValidationResult("Provide valid months for carry over expiry.");

            return ValidationResult.Success;
        }
    }
}