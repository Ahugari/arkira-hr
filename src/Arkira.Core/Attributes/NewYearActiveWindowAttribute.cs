﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class NewYearActiveWindowAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;

            if (leaveType.LeaveRequestWindow == Enums.LeaveRequestWindow.CurrentAndFollowingYear
                && (leaveType.NewYearActiveWindow == null || leaveType.NewYearActiveWindow < ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION
                || leaveType.NewYearActiveWindow > ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION))
                return new ValidationResult($"Provide valid number of months to activate New Leave Year. Must be more than {ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION - 1} and less than {ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION + 1} month(s).");

            return ValidationResult.Success;
        }
    }
}