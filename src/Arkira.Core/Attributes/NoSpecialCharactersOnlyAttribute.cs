﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Arkira.Core.Attributes
{
    internal class NoSpecialCharactersOnlyAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;
            if (leaveType.Name.ToCharArray().All(x => ArkiraConsts.specialCharacters.Contains(x)))
                return new ValidationResult("Name cannot contain only special characters.");

            return ValidationResult.Success;
        }
    }
}