﻿using Arkira.Core.LeaveTypes;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.Attributes
{
    internal class StandardEntitlementCapAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            LeaveType leaveType = (LeaveType)validationContext.ObjectInstance;
            if (leaveType.EntitlementType == Enums.EntitlementType.Unlimited)
                return ValidationResult.Success;

            if (leaveType.EntitlementType == Enums.EntitlementType.Standard
                && (leaveType.EntitlementCap == null
                || leaveType.EntitlementCap < ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED
                || leaveType.EntitlementCap > ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED))
                return new ValidationResult("Provide a valid Leave Entitlement Cap.");

            return ValidationResult.Success;
        }
    }
}