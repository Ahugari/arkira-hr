﻿using Arkira.Core.Repositories;

namespace Arkira.Core.EmployeeFields.JobTitles
{
    public interface IJobTitleRepository : ISave<JobTitle>, IRead<JobTitle>, IRepository<JobTitle>
    {
    }
}