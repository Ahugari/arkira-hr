﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.EmployeeFields.JobTitles
{
    public class JobTitle : Entity<Guid>, IHasCreationDate, IHasModifiedDate
    {
        public DateTime? DateModified { get; private set; }

        public DateTime DateCreated { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public JobTitle FullUpdate(JobTitleFullUpdateDto update)
        {
            if (IsTransient())
            {
                Id = update.Id;
                DateCreated = DateTime.Today;
            }
            else
                DateModified = DateTime.Today;

            Name = update.Name.Trim();
            Description = update.Description.Trim();

            return this;
        }

        public static string EntityName => "Job Title";

        public override string ToString() => EntityName;
    }
}