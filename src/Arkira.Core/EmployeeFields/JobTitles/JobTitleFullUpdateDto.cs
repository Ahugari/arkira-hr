﻿using System;

namespace Arkira.Core.EmployeeFields.JobTitles
{
    public class JobTitleFullUpdateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}