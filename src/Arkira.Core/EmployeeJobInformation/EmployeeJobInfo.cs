﻿using Arkira.Core.Employees;
using Arkira.Core.Entities;
using System;

namespace Arkira.Core.EmployeeJobInformation
{
    public class EmployeeJobInfo : Entity<Guid>, IHasCreationDate, IHasModifiedDate
    {
        public Guid EmployeeId { get; internal set; }

        public Guid JobTitleId { get; internal set; }

        public Guid ReportsTo { get; internal set; }

        public DateTime? DateModified { get; private set; }

        public DateTime DateCreated { get; private set; }

        public Employee Employee { get; internal set; }

        private EmployeeJobInfo()
        {
        }

        public EmployeeJobInfo(Guid employeeId, Guid jobTitleId, Guid reportsTo) : base()
        {
            if (IsTransient())
            {
                DateCreated = DateTime.Now;
                EmployeeId = employeeId;
            }
            else
            {
                DateModified = DateTime.Now;
            }

            JobTitleId = jobTitleId;
            ReportsTo = reportsTo;
        }

        public static readonly string EntityName = "Job Information";

        public override string ToString() => EntityName;
    }
}