﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.EmployeeJobInformation
{
    public class EmployeeJobInfoManager
    {
        private readonly IJobTitleRepository _jobTitleRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeJobInfoRepository _empJobInfoRepository;

        public EmployeeJobInfoManager(IEmployeeJobInfoRepository empJobInfoRepository, IJobTitleRepository jobTitleRepository, IEmployeeRepository employeeRepository)
        {
            _empJobInfoRepository = empJobInfoRepository;
            _jobTitleRepository = jobTitleRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<EmployeeJobInfo> CreateAsync(Guid employeeId, Guid jobTitleId, Guid reportsToId)
        {
            await ValidateEmployeeId(employeeId);
            await ValidateJobTitleId(jobTitleId);
            await ValidateEmployeeId(reportsToId);

            return new EmployeeJobInfo(employeeId, jobTitleId, reportsToId);
        }

        public async Task<EmployeeJobInfo> UpdateAsync(Guid id, Guid jobTitleId)
        {
            await ValidateJobTitleId(jobTitleId);
            var exisitingJobInfo = await ValidateEmployeeJobInfo(id);

            exisitingJobInfo.JobTitleId = jobTitleId;

            await _empJobInfoRepository.SaveAsync(exisitingJobInfo);

            return exisitingJobInfo;
        }

        private async Task<EmployeeJobInfo> ValidateEmployeeJobInfo(Guid id)
        {
            var exisitingJobInfo = await _empJobInfoRepository.ReadOneAsync(id);
            if (exisitingJobInfo == null)
                throw new EmployeeJobInfoStateException("Employee Job Information doesn't exist.");
            return exisitingJobInfo;
        }

        private async Task ValidateJobTitleId(Guid jobTitleId)
        {
            var existingJobTitle = await _jobTitleRepository.ReadOneAsync(jobTitleId);
            if (existingJobTitle == null)
                throw new EmployeeJobInfoStateException("Job title is not valid.");
        }

        private async Task ValidateEmployeeId(Guid employeeId)
        {
            var existingEmployee = await _employeeRepository.FindAsync(employeeId);
            if (existingEmployee == null)
                throw new EmployeeJobInfoStateException("Employee Id is not valid.");
        }
    }
}