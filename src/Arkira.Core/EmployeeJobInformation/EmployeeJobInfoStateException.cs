﻿using System;

namespace Arkira.Core.EmployeeJobInformation
{
    public class EmployeeJobInfoStateException : Exception
    {
        public EmployeeJobInfoStateException(string message) : base(message)
        {
        }
    }
}