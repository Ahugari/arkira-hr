﻿using System;

namespace Arkira.Core.EmployeeJobInformation
{
    public class EmployeeJobInformationDto
    {
        public Guid Id { get; set; }
        public Guid EmployeeId { get; set; }
        public Guid JobTitleId { get; set; }
    }
}