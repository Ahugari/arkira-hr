﻿using Arkira.Core.Repositories;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Core.EmployeeJobInformation
{
    public interface IEmployeeJobInfoRepository : IGenericCreateUpdateDeleteRepository<EmployeeJobInfo>
    {
        Task<EmployeeJobInfo> ReadOneWithEmployeeAsync(Expression<Func<EmployeeJobInfo, bool>> predicate);
    }
}