﻿using Arkira.Core.Employees;
using Arkira.Core.Entities;
using Arkira.Core.LeavePolicies;
using System;

namespace Arkira.Core.EmployeeLeavePolicies
{
    public class EmployeeLeavePolicy : IHasCreationDate
    {
        public Guid EmployeeId { get; set; }
        public Guid LeavePolicyId { get; set; }
        public DateTime DateCreated { get; set; }
        public Employee Employee { get; set; }
        public LeavePolicy LeavePolicy { get; set; }

        private EmployeeLeavePolicy()
        {
        }

        public EmployeeLeavePolicy(Guid employeeId, Guid leavePolicyId)
        {
            EmployeeId = employeeId;
            LeavePolicyId = leavePolicyId;
            DateCreated = DateTime.Now;
        }
    }
}