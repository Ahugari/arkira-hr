﻿using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.EmployeeLeavePolicies;
using Arkira.Core.Entities;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicyAccounts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Arkira.Core.Employees
{
    public class Employee : Entity<Guid>, IHasVersionId, IHasCreationDate
    {
        public string EmployeeNumber { get; set; }
        public EmployeeStatus EmployeeStatus { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MartitalStatus { get; set; }
        public string NIN { get; set; }
        public int? NSSF { get; set; }
        public int? TIN { get; set; }
        public ShirtSize? ShirtSize { get; set; }
        public DateTime BirthDate { get; internal set; }

        [ConcurrencyCheck]
        public int Version { get; internal set; }

        public DateTime DateCreated { get; private set; }

        public DateTime HireDate { get; set; }

        public LeavePolicyAccount LeavePolicyAccount { get; set; }

        public ICollection<EmployeeLeavePolicy> LeavePolicyEntitlements { get; internal set; }
        public ICollection<EmployeeJobInfo> EmployeeJobInformation { get; internal set; }

        protected Employee()
        {
        }

        public Employee(string employeeNumber, EmployeeStatus employeeStatus, string firstName, string middleName, string lastName, string preferredName, Gender gender, MaritalStatus martitalStatus, string nIN, int? nSSF, int? tIN, ShirtSize? shirtSize, DateTime birthDate, DateTime hireDate) : base()
        {
            EmployeeNumber = employeeNumber.Trim();
            EmployeeStatus = employeeStatus;
            FirstName = firstName.Trim();
            MiddleName = !string.IsNullOrWhiteSpace(middleName) ? middleName.Trim() : string.Empty;
            LastName = lastName.Trim();
            PreferredName = !string.IsNullOrWhiteSpace(preferredName) ? preferredName.Trim() : string.Empty;
            Gender = gender;
            MartitalStatus = martitalStatus;
            NIN = nIN.Trim();
            NSSF = nSSF;
            TIN = tIN;
            ShirtSize = shirtSize;
            BirthDate = birthDate;
            HireDate = hireDate;
            DateCreated = DateTime.Now;
            LeavePolicyEntitlements = new HashSet<EmployeeLeavePolicy>();
        }

        public virtual string GetFullName()
        {
            StringBuilder @string = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(LastName))
                @string.Append(LastName);
            if (!string.IsNullOrWhiteSpace(MiddleName))
                @string.Append($" {MiddleName[0]}.");
            if (!string.IsNullOrWhiteSpace(FirstName))
                @string.Append($" {FirstName}");
            return @string.ToString();
        }

        public string GetLatestJobTitleId()
        {
            if (EmployeeJobInformation.Count < 1)
                return "";

            return EmployeeJobInformation.OrderByDescending(x => x.DateCreated).FirstOrDefault().JobTitleId.ToString();
        }

        public static readonly string EntityName = "Employee";

        public override string ToString() => EntityName;
    }
}