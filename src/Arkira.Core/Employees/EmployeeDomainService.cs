﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.EmployeeLeavePolicies;
using Arkira.Core.Enums;
using Arkira.Core.Helpers;
using Arkira.Core.LeavePolicies;
using Arkira.Core.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Employees
{
    public class EmployeeDomainService : IEmployeeDomainService
    {
        private readonly ISystemClock _clock;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IJobTitleRepository _jobTitleRepository;
        private readonly IEmployeeJobInfoRepository _employeeJobInfoRepository;

        public EmployeeDomainService(ISystemClock clock, IEmployeeRepository repository, IJobTitleRepository jobTitleRepository)
        {
            //TODO: Remove the dependency to the system clock.
            _clock = clock;
            _employeeRepository = repository;
            _jobTitleRepository = jobTitleRepository;
        }

        public async Task<Employee> CreateAsync(
            [NotNull] string employeeNumber,
            [NotNull] string firstName,
            string middleName,
            [NotNull] string lastName,
            string preferredName,
            Gender gender,
            MaritalStatus martitalStatus,
            [NotNull] string nIN,
            int? nSSF,
            int? tIN,
            ShirtSize? shirtSize,
            [NotNull] DateTime birthDate,
            [NotNull] DateTime hireDate)
        {
            CheckEmptyInputs(employeeNumber, firstName, lastName, nIN);

            Employee employee = new Employee
            (
                await SetEmployeeNumber(employeeNumber),
                EmployeeStatus.Active,
                firstName,
                middleName,
                lastName,
                preferredName,
                gender,
                martitalStatus,
                nIN,
                nSSF,
                tIN,
                shirtSize,
                SetBirthDate(birthDate),
                hireDate
            );

            //publish event

            await _employeeRepository.InsertAsync(employee);

            return employee;
        }

        public async Task AddLeavePolicyEntitlement(Employee employee, LeavePolicy leavePolicy)
        {
            var employeeInDb = await _employeeRepository.FindAsync(employee.Id);
            var existingLeavePolicy = employeeInDb.LeavePolicyEntitlements.FirstOrDefault(x => x.LeavePolicyId == leavePolicy.Id);

            if (existingLeavePolicy != null)
                return;

            employeeInDb.LeavePolicyEntitlements.Add(new EmployeeLeavePolicy(employee.Id, leavePolicy.Id));

            //TODO: Publish LeavePolicy added event and handlers include notification and leave policy history
        }

        public async Task AddJobInformation(Employee employee, EmployeeJobInfo employeeJobInfo, Employee reportsTo)
        {
            var employeeInDb = await _employeeRepository.FindAsync(employee.Id);
            var existingJobInfo = employeeInDb.EmployeeJobInformation.FirstOrDefault(x => x.JobTitleId == employeeJobInfo.Id);

            if (existingJobInfo != null)
                return;

            employeeInDb.EmployeeJobInformation.Add(new EmployeeJobInfo(employee.Id, employeeJobInfo.JobTitleId, reportsTo.Id));
        }

        public async Task<string> GetLatestJobTitle(Employee employee)
        {
            var jobTitleId = Validate.AsGuid(employee.GetLatestJobTitleId());

            return (await _jobTitleRepository.ReadOneAsync(jobTitleId)).Name;
        }

        public async Task<Employee> GetEmployeeByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            return await _employeeRepository.GetByIdAsync(id, cancellationToken);
        }

        public async Task<Employee> GetSupervisorByIdAsync(string employeeId, CancellationToken cancellationToken)
        {
            var empId = Transform.ToGuid(employeeId);
            var empJobInfo = await _employeeJobInfoRepository.ReadOneAsync(empId);

            return await _employeeRepository.GetByIdAsync(empJobInfo.ReportsTo.ToString(), cancellationToken);
        }

        public async Task<Employee> GetManagerByIdAsync(string employeeId, CancellationToken cancellationToken)
        {
            var employeeSupervisor = await GetSupervisorByIdAsync(employeeId, cancellationToken);

            return await GetSupervisorByIdAsync(employeeSupervisor.Id.ToString(), cancellationToken);
        }

        public async Task<IEnumerable<Employee>> GetEmployeesByJobTitleAsync(string jobTitleId, CancellationToken cancellationToken)
        {
            return await _employeeRepository.GetAllByJobTitle(jobTitleId, cancellationToken);
        }

        private async Task<IOrderedEnumerable<EmployeeLeavePolicy>> GetLeavePolicyEntitlements(Employee employee, LeavePolicy leavePolicy)
        {
            //TODO: Move this into employee application service
            var employeeInDb = await _employeeRepository.FindAsync(employee.Id);

            return employeeInDb.LeavePolicyEntitlements
                .Where(x => x.EmployeeId == employee.Id && x.LeavePolicyId == leavePolicy.Id && x.LeavePolicy.IsLatest)
                .Select(x => x)
                .OrderByDescending(x => x.DateCreated);
        }

        private static void CheckEmptyInputs(string employeeNumber, string firstName, string lastName, string nIN)
        {
            if (string.IsNullOrWhiteSpace(employeeNumber))
                throw new ArgumentException($"'{nameof(employeeNumber)}' cannot be null or whitespace.", nameof(employeeNumber));

            if (string.IsNullOrWhiteSpace(firstName))
                throw new ArgumentException($"'{nameof(firstName)}' cannot be null or whitespace.", nameof(firstName));

            if (string.IsNullOrWhiteSpace(lastName))
                throw new ArgumentException($"'{nameof(lastName)}' cannot be null or whitespace.", nameof(lastName));

            if (string.IsNullOrWhiteSpace(nIN))
                throw new ArgumentException($"'{nameof(nIN)}' cannot be null or whitespace.", nameof(nIN));
        }

        private async Task<string> SetEmployeeNumber(string employeeNumber)
        {
            await ValidateEmployeeNumber(employeeNumber);

            return employeeNumber;
        }

        private async Task ValidateEmployeeNumber(string employeeNumber)
        {
            var existingEmployee = await _employeeRepository.FindAsync(x => x.EmployeeNumber.ToLower().Trim() == employeeNumber.ToLower().Trim());

            if (existingEmployee != null)
                throw new InvalidOperationException($"The {nameof(employeeNumber)} '{employeeNumber}' already exists.");
        }

        private DateTime SetBirthDate(DateTime date)
        {
            ValidateBirthDate(_clock, date);

            return date;
        }

        private void ValidateBirthDate(ISystemClock clock, DateTime input)
        {
            if (clock.UtcNow.Year < input.Year)
                throw new InvalidBirthDateException("Birthdate cannot be a future date.");

            if (clock.UtcNow.AddYears(-ArkiraConsts.MIN_NUMBER_OF_YEARS_ALLOWED).Year <= input.Year
                && input.Year <= clock.UtcNow.Year)
                throw new InvalidBirthDateException($"Birthdate should be older than the minimum number of years allowed({ArkiraConsts.MIN_NUMBER_OF_YEARS_ALLOWED}).");

            if (clock.UtcNow.AddYears(-ArkiraConsts.MAX_NUMBER_OF_YEARS_ALLOWED).Year >= input.Year)
                throw new InvalidBirthDateException($"Birthdate should less than the maximum number of years allowed({ArkiraConsts.MAX_NUMBER_OF_YEARS_ALLOWED}).");
        }
    }
}