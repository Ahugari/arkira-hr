﻿using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Employees
{
    public interface IEmployeeDomainService
    {
        Task AddLeavePolicyEntitlement(Employee employee, LeavePolicy leavePolicy);

        Task<Employee> CreateAsync([NotNull] string employeeNumber, [NotNull] string firstName, string middleName, [NotNull] string lastName, string preferredName, Gender gender, MaritalStatus martitalStatus, [NotNull] string nIN, int? nSSF, int? tIN, ShirtSize? shirtSize, [NotNull] DateTime birthDate, [NotNull] DateTime hireDate);

        Task<Employee> GetEmployeeByIdAsync(string id, CancellationToken cancellationToken = default);

        Task<IEnumerable<Employee>> GetEmployeesByJobTitleAsync(string jobTitleId, CancellationToken cancellationToken);

        Task<Employee> GetManagerByIdAsync(string employeeId, CancellationToken cancellationToken);

        Task<Employee> GetSupervisorByIdAsync(string employeeId, CancellationToken cancellationToken);
    }
}