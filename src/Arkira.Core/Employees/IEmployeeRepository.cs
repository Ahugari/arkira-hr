﻿using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Employees
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {
        void Add(Employee employee);

        Task<Employee> GetByIdAsync(string id, CancellationToken cancellationToken);

        Task<IEnumerable<Employee>> GetAllByJobTitle(string jobTitleId, CancellationToken cancellationToken);

        Task<Employee> GetWithJobInformationAsync(Expression<Func<Employee, bool>> predicate, CancellationToken cancellationToken);

        Task<IEnumerable<Employee>> GetAllWithJobInformationAsync(CancellationToken cancellationToken);

        Task<Employee> GetWithLeavePolicyEntitlements(Expression<Func<Employee, bool>> predicate);
    }
}