﻿using System;

namespace Arkira.Core.Employees
{
    public class InvalidBirthDateException : Exception
    {
        public InvalidBirthDateException(string message) : base(message)
        {
        }
    }
}