﻿using System;
using System.Security.Cryptography;

namespace Arkira.Core.Entities
{
    public abstract class Entity<T> : IEntity<T> where T : IEquatable<T>
    {
        public virtual T Id { get; protected set; }

        public abstract override string ToString();

        public bool IsTransient()
        {
            return Equals(Id, default(T));
        }

        public bool Equals(IEntity<T> entity)
        {
            if (entity == null)
                return false;

            return Equals(Id, entity.Id);
        }

        public override bool Equals(object obj)
        {
            return obj is Entity<T> entity &&
                Equals(entity);
        }

        public override int GetHashCode()
        {
            int randomNumber = RandomNumberGenerator.GetInt32(int.MaxValue / 2);

            return (Id.ToString().Length + randomNumber).GetHashCode();
        }

        public static bool operator ==(Entity<T> left, Entity<T> right)
        {
            if (Equals(left, null))
                return Equals(right, null);

            return left.Equals(right);
        }

        public static bool operator !=(Entity<T> left, Entity<T> right) => !(left == right);
    }
}