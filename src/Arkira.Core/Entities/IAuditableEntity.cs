﻿using System;

namespace Arkira.Core.Entities
{
    public interface IAuditableEntity
    {
        public Guid CreatedBy { get; }
        public DateTime CreatedOn { get; }
        public Guid ModifiedBy { get; }
        public DateTime ModifiedOn { get; }
    }
}