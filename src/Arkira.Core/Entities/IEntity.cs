﻿using System;

namespace Arkira.Core.Entities
{
    public interface IEntity<T> : IEquatable<IEntity<T>> where T : IEquatable<T>
    {
        public T Id { get; }
    }
}