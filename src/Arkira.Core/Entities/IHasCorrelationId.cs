﻿using System;

namespace Arkira.Core.Entities
{
    public interface IHasCorrelationId
    {
        public Guid CorrelationId { get; }
    }
}