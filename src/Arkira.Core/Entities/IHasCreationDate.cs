﻿using System;

namespace Arkira.Core.Entities
{
    public interface IHasCreationDate
    {
        public DateTime DateCreated { get; }
    }
}