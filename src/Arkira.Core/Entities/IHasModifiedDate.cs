﻿using System;

namespace Arkira.Core.Entities
{
    public interface IHasModifiedDate
    {
        public DateTime? DateModified { get; }
    }
}