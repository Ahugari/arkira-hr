﻿namespace Arkira.Core.Entities
{
    public interface IHasVersionId
    {
        public int Version { get; }
    }
}