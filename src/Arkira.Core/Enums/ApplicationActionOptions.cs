﻿namespace Arkira.Core.Enums
{
    public enum ApplicationActionOptions
    {
        Comment,
        Submit,
        Deny,
        Recommend,
        Approve,
        Archive,
        Cancel
    }

}