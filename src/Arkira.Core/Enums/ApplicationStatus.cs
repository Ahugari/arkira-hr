﻿namespace Arkira.Core.Enums
{
    public enum ApplicationStatus
    {
        Drafted,
        Pending,
        Approved,
        Rejected,
        Archived,
        Withdrawn,
        Reviewed,
        Submitted
    }
}