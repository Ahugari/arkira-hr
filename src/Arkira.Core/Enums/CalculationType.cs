﻿namespace Arkira.Core.Enums
{
    public enum CalculationType
    {
        EstimateFutureEntitlement,
        CalculateAsAtCurrentDay
    }
}