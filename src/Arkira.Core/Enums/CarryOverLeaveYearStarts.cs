﻿namespace Arkira.Core.Enums
{
    public enum CarryOverLeaveYearStarts
    {
        GivenDayAndMonth,
        AnniversaryOfHireDate
    }
}