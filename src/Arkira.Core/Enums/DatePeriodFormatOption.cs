﻿namespace Arkira.Core.Enums
{
    public enum DatePeriodFormatOption
    {
        Days,
        Months
    }
}