﻿namespace Arkira.Core.Enums
{
    public enum EmployeeStatus
    {
        Inactive,
        Active
    }
}