﻿namespace Arkira.Core.Enums
{
    public enum EntitlementType
    {
        Standard,
        Unlimited
    }
}