﻿namespace Arkira.Core.Enums
{
    public enum LeaveEntitlementReset
    {
        ResetNever,
        ResetOnSchedule,
        ResetOnWorkAnniversary
    }
}