﻿namespace Arkira.Core.Enums
{
    public enum LeavePolicyAccountAction
    {
        Add,
        Deduct,
        Enrolled,
        Withdraw,
        EntitlementReset,
        Accrued,
        CarriedOver
    }
}