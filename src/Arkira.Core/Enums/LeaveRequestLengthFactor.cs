﻿namespace Arkira.Core.Enums
{
    public enum LeaveRequestLengthFactor
    {
        EveryWorkingDay,
        EveryCalendarDay
    }
}