﻿namespace Arkira.Core.Enums
{
    public enum LeaveRequestWindow
    {
        OnlyInCurrentYear,
        CurrentAndFollowingYear,
        SomePeriodInFuture
    }
}