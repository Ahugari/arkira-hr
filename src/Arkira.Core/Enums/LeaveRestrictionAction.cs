﻿namespace Arkira.Core.Enums
{
    public enum LeaveRestrictionAction
    {
        DisplayWarning,
        BlockRequest
    }
}