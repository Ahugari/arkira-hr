﻿namespace Arkira.Core.Enums
{
    public enum LeaveRestrictionOption
    {
        LeaveMustNotBeLongerThanNDays,
        LeaveMustNotBeShorterThanNDays,
        LeaveMustBeRequestedNDaysIfLongerThanNDays,
        LeaveCanBeRequestedNDaysInThePast,
        EmployeeCanMakeRequestAfterNDaysFromHire
    }
}