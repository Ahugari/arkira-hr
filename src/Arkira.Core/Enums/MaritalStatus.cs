﻿namespace Arkira.Core.Enums
{
    public enum MaritalStatus
    {
        Married,
        Single
    }
}