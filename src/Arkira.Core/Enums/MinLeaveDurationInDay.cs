﻿namespace Arkira.Core.Enums
{
    public enum MinLeaveDurationInDay
    {
        WholeDay,
        QuarterDay,
        HalfDay
    }
}