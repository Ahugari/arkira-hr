﻿namespace Arkira.Core.Enums
{
    public enum NewYearLeaveRequestDeductionOptions
    {
        AtFirstFromTheCurrentYear,
        ProportionalFromEachYear
    }
}