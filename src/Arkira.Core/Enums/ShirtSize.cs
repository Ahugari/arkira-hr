﻿namespace Arkira.Core.Enums
{
    public enum ShirtSize
    {
        SM,
        S,
        X,
        XL,
        XXL
    }
}