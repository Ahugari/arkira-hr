﻿namespace Arkira.Core.Enums
{
    public enum WorkType
    {
        Application,
        Task,
        Document
    }
}