﻿using System;

namespace Arkira.Core.Helpers
{
    public static class Check
    {
        public static bool IsValidDate(DateTime effectiveDate, int yearsInFutureLimit)
        {
            if (effectiveDate < DateTime.Now)
                throw new ArgumentException($"{nameof(effectiveDate)} cannot be earlier than today.");

            if (effectiveDate > DateTime.Now.AddYears(yearsInFutureLimit))
                throw new ArgumentException($"Provided date is out of range.");

            return true;
        }
    }
}