﻿using System;

namespace Arkira.Core.Helpers
{
    public static class Transform
    {
        public static Guid ToGuid(string rawValue)
        {
            Guid transformed;

            if (!Guid.TryParse(rawValue, out transformed))
                throw new InvalidOperationException("Cannot convert string to Guid.");

            return transformed;
        }
    }
}