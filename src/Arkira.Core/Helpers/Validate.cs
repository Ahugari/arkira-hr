﻿using System;

namespace Arkira.Core.Helpers
{
    public static class Validate
    {
        public static Guid AsGuid(string value)
        {
            var guid = Guid.NewGuid();
            try
            {
                guid = Transform.ToGuid(value);
            }
            catch (InvalidOperationException ex)
            {
                //ignore
            }
            return guid;
        }
    }
}