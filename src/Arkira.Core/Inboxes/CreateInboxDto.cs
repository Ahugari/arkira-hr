﻿using System.Collections.Generic;

namespace Arkira.Core.Inboxes
{
    public class CreateInboxDto
    {
        public string TargetId { get; set; } = default!;
        public string ItemId { get; set; } = default!;
        public string ItemName { get; set; } = default!;
        public string Description { get; set; } = default!;
        public string Section { get; set; } = default!;
        public string Category { get; set; } = default!;
        public Dictionary<string, HashSet<string>> Actions { get; set; } = new Dictionary<string, HashSet<string>>();
    }
}