﻿using MediatR;

namespace Arkira.Core.Inboxes.Events
{
    public record NewInboxAdded(Inbox Inbox) : INotification;
}