﻿using Arkira.Core.Applications.Events;
using Arkira.Core.LeaveApplications;
using Arkira.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Inboxes.Handlers
{
    //TODO:change this to handle application type but not subtypes
    public class AddApplicationToInbox : IAppNotificationHandler<ApplicationReadyForReview<LeaveApplication>>, IAppNotificationHandler<ApplicationPrepared<LeaveApplication>>
    {
        private readonly IInboxRepository _inboxRepository;

        public AddApplicationToInbox(IInboxRepository inboxRepository)
        {
            _inboxRepository = inboxRepository;
        }

        public async Task Handle(ApplicationReadyForReview<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddTarget(notification.ReviewerId)
                .AddItemDetails(notification.Application.Id.ToString(), notification.Application.GetName())
                .AddItemActions(notification.Application.Actions)
                .Build();

            await _inboxRepository.SaveAsync(inbox);
        }

        public async Task Handle(ApplicationPrepared<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Sent)
                .AddTarget(notification.Application.ApplicantId.ToString())
                .AddItemDetails(notification.Application.Id.ToString(), notification.Application.GetName())
                .Build();

            await _inboxRepository.SaveAsync(inbox);
        }
    }
}