﻿using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.Inboxes.Handlers
{
    public class UpdateApplicationInbox : IAppNotificationHandler<ApplicationProcessed<Application>>
    {
        private readonly IInboxRepository _inboxRepository;

        public UpdateApplicationInbox(IInboxRepository inboxRepository)
        {
            _inboxRepository = inboxRepository;
        }

        public async Task Handle(ApplicationProcessed<Application> notification, CancellationToken cancellationToken)
        {
            var existingInbox = await _inboxRepository.ReadOneAsync(x => x.ItemId == notification.ApplicationId && x.TargetId == notification.ActionPerformedById);

            existingInbox.CompleteItem();

            await _inboxRepository.SaveAsync(existingInbox);
        }
    }
}