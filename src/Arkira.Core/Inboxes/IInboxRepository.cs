﻿using Arkira.Core.Repositories;

namespace Arkira.Core.Inboxes
{
    public interface IInboxRepository : ISave<Inbox>, IRead<Inbox>, IRepository<Inbox>
    {
    }
}