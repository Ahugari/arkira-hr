﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;

namespace Arkira.Core.Inboxes
{
    public class Inbox : Entity<Guid>, IHasCreationDate, IHasModifiedDate
    {
        public string TargetId { get; private set; } = default!;
        public string ItemId { get; private set; } = default!;
        public string ItemName { get; private set; } = default!;
        public string Description { get; internal set; } = default!;
        public string Section { get; internal set; } = default!;
        public string Category { get; internal set; } = default!;
        public Dictionary<string, HashSet<string>> Actions { get; set; } = new Dictionary<string, HashSet<string>>();

        public DateTime DateCreated { get; internal set; }

        public DateTime? DateModified { get; private set; }

        internal Inbox()
        {
        }

        public void CompleteItem()
        {
            Section = InboxSections.Completed;
            DateModified = DateTime.Now;
        }

        internal void SetItemCategory(string itemCategory)
        {
            var cleaneItemCategory = itemCategory.ToLower().Trim();
            if (!InboxItemTypes.Approvals.ToLower().Contains(cleaneItemCategory))
                throw new InboxException("Invalid Item Category!", new ArgumentException("Parameter: Category"));

            Category = itemCategory.Trim();
        }

        internal void SetItemSection(string itemSection)
        {
            var cleanItemSection = itemSection.ToLower().Trim();
            if (!InboxSections.Inbox.ToLower().Contains(cleanItemSection)
                && !InboxSections.Completed.ToLower().Contains(cleanItemSection)
                && !InboxSections.Sent.ToLower().Contains(cleanItemSection))
                throw new InboxException("Invalid Item Section!", new ArgumentException("Parameter: Section"));

            Section = itemSection.Trim();
        }

        internal void SetActions(KeyValuePair<string, HashSet<string>> actions)
        {
            if (string.IsNullOrWhiteSpace(Section))
                throw new InboxException("Section cannot be null or empty.");

            if (InboxSections.Inbox.ToLower().Contains(Section.ToLower().Trim())
                && (actions.Key == null || actions.Value == null))
                throw new InboxException("Actions cannot be empty!", new ArgumentException("Parameter: Actions"));

            Actions.Add(actions.Key, actions.Value);
        }

        internal void SetItemName(string itemName)
        {
            ItemName = string.IsNullOrWhiteSpace(itemName.Trim()) ? throw new InboxException("Item Name is required.", new ArgumentException("Parameter: itemName")) : itemName.Trim();
        }

        internal void SetItemId(string itemId)
        {
            ItemId = string.IsNullOrWhiteSpace(itemId.Trim()) ? throw new InboxException("Item Id is required.", new ArgumentException("Parameter: itemId")) : itemId.Trim();
        }

        internal void SetTargetId(string targetId)
        {
            TargetId = string.IsNullOrWhiteSpace(targetId.Trim()) ? throw new InboxException("Target Id is required.", new ArgumentException("Parameter: targetId")) : targetId.Trim();
        }

        public static readonly string EntityName = "Inbox";

        public override string ToString() => EntityName;
    }
}