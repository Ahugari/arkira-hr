﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Arkira.Core.Inboxes
{
    public abstract class InboxBuilder
    {
        protected Inbox _inbox;

        public InboxBuilder PrepareInbox(string section)
        {
            ValidateItemSection(section);
            _inbox = new();
            _inbox.Section = section;
            _inbox.DateCreated = DateTime.Now;

            return this;
        }

        public InboxBuilder AddTarget(string id)
        {
            _inbox.SetTargetId(id);

            return this;
        }

        public InboxBuilder AddItemDetails(string itemId, string itemName)
        {
            _inbox.SetItemId(itemId);
            _inbox.SetItemName(itemName);

            return this;
        }

        public InboxBuilder AddItemActions(Dictionary<string, HashSet<string>> itemActions)
        {
            if (itemActions == null || itemActions.Count == 0)
                throw new InboxException("Error while creating new inbox item.", new InvalidOperationException("Item actions cannot be empty or null."));

            KeyValuePair<string, HashSet<string>> action = new();
            action = itemActions.Count == 1 ? itemActions.FirstOrDefault() : itemActions.LastOrDefault();

            _inbox.SetActions(action);

            return this;
        }

        public virtual InboxBuilder AddDescription(string description)
        {
            _inbox.Description = description.Trim();

            return this;
        }

        public abstract Inbox Build();

        protected virtual void ValidateItemSection(string itemSection)
        {
            if ((!InboxSections.Sent.ToLower().Contains(itemSection.ToLower().Trim()) && !InboxSections.Inbox.ToLower().Contains(itemSection.ToLower().Trim())) || string.IsNullOrWhiteSpace(itemSection))
                throw new InboxException("Invalid Item Section.", new ArgumentException("Parameter: Item Section"));
        }
    }

    public class ApprovalItem : InboxBuilder
    {
        public override Inbox Build()
        {
            if (!InboxSections.Sent.ToLower().Contains(_inbox.Section.ToLower()) && (_inbox.Actions == null || _inbox.Actions.Count < 1))
                throw new InboxException("Actions cannot be empty.");

            _inbox.Category = InboxItemTypes.Approvals;

            return _inbox;
        }
    }
}