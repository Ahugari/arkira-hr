﻿using System;

namespace Arkira.Core.Inboxes
{
    public class InboxException : Exception
    {
        public InboxException(string message) : base(message)
        {
        }

        public InboxException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}