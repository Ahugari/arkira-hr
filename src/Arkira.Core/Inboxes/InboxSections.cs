﻿namespace Arkira.Core.Inboxes
{
    public static class InboxSections
    {
        public const string Inbox = "Inbox";
        public const string Completed = "Completed";
        public const string Sent = "Sent";
    }
}