﻿using System;
using System.Collections.Generic;

namespace Arkira.Core.Inboxes
{
    public abstract class InboxService
    {
        public abstract Inbox Create(string targetId, string itemSection, string itemCategory, string itemId, string itemName, Dictionary<string, HashSet<string>> actions, string description = "");

        protected void ValidateItemCategory(string itemCategory)
        {
            if (!InboxItemTypes.Approvals.ToLower().Contains(itemCategory.ToLower().Trim()) || string.IsNullOrWhiteSpace(itemCategory))
                throw new InboxException("Invalid Item Category!", new ArgumentException("Parameter: Item Category"));
        }

        protected void ValidateItemSection(string itemSection)
        {
            if (!InboxSections.Inbox.ToLower().Contains(itemSection.ToLower().Trim()) || string.IsNullOrWhiteSpace(itemSection))
                throw new InboxException("Invalid Item Section.", new ArgumentException("Parameter: Item Section"));
        }
    }
}