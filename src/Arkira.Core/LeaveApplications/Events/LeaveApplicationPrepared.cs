﻿using MediatR;

namespace Arkira.Core.LeaveApplications.Events
{
    public record LeaveApplicationPrepared(LeaveApplication LeaveApplication, int NumberOfDaysToDeduct) : INotification;
}