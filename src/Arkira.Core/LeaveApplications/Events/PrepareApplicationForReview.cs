﻿using MediatR;

namespace Arkira.Core.LeaveApplications.Events
{
    public record PrepareApplicationForReview(string ApplicationId, string NextReviewerTitle) : INotification
    {
    }
}