﻿using MediatR;
using System.Collections.Generic;

namespace Arkira.Core.LeaveApplications.Events
{
    public record StoreLeaveApplicationActions(string ApplicationId, HashSet<string> Actions, string WorkflowInstanceId) : INotification;
}