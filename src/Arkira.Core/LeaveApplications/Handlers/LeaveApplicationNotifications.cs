﻿using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications.Handlers
{
    internal class LeaveApplicationNotifications : IAppNotificationHandler<ApplicationSentToReviewer<LeaveApplication>>
    {
        private readonly IAppMediator _mediator;

        public LeaveApplicationNotifications(IAppMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Handle(ApplicationSentToReviewer<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            await _mediator.Publish(new ApplicationSentToReviewer<Application>(notification.Application, notification.ApplicantName, notification.Reviewers, notification.ApplicationWatchers), cancellationToken);
        }
    }
}