﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.LeaveApplications.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications.Handlers
{
    public class PersistApplicationActions : INotificationHandler<StoreLeaveApplicationActions>
    {
        private readonly ILeaveApplicationRepository _leaveApplicationRepository;
        private readonly IApplicationActivityRepository _applicationActivityRepository;

        public PersistApplicationActions(ILeaveApplicationRepository leaveApplicationRepository)
        {
            _leaveApplicationRepository = leaveApplicationRepository;
        }

        public async Task Handle(StoreLeaveApplicationActions notification, CancellationToken cancellationToken)
        {
            Guid applicationIdAsGuid;
            if (!Guid.TryParse(notification.ApplicationId, out applicationIdAsGuid))
                throw new InvalidOperationException("Invalid Application Id!");

            var existingApplication = await _leaveApplicationRepository.FindAsync(applicationIdAsGuid);
            if (existingApplication == null)
                throw new InvalidOperationException("Invalid Application!");

            var applicationActions = new Dictionary<string, HashSet<string>>();
            applicationActions.Add(notification.WorkflowInstanceId, notification.Actions);


            existingApplication.Actions = applicationActions;

            await _leaveApplicationRepository.UpdateAsync(existingApplication);

            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();
        }
    }
}