﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.Employees;
using Arkira.Core.Helpers;
using Arkira.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications.Handlers
{
    public class PrepareApplicationReviewSend : IAppNotificationHandler<ApplicationReadyForReviewSend<LeaveApplication>>
    {
        private readonly IEmployeeJobInfoRepository jobInfoRepository;
        private readonly IJobTitleRepository jobTitleRepository;
        private readonly ILeaveApplicationRepository _leaveApplicationRepository;
        private readonly IAppMediator _mediator;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IApplicationActivityRepository _activityRepository;

        public PrepareApplicationReviewSend(IEmployeeJobInfoRepository jobInfoRepository, IJobTitleRepository jobTitleRepository, IAppMediator mediator, ILeaveApplicationRepository leaveApplicationRepository, IEmployeeRepository employeeRepository, IApplicationActivityRepository activityRepository)
        {
            this.jobInfoRepository = jobInfoRepository;
            this.jobTitleRepository = jobTitleRepository;
            _mediator = mediator;
            _leaveApplicationRepository = leaveApplicationRepository;
            _employeeRepository = employeeRepository;
            _activityRepository = activityRepository;
        }

        public async Task Handle(ApplicationReadyForReviewSend<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            //receive group of reviewers
            if (notification.WorkflowReviewApplication.Reviewers == null || !notification.WorkflowReviewApplication.Reviewers.Any())
                return;

            Guid applicationIdAsGuid = GetApplicationIdAsGiud(notification.WorkflowReviewApplication.ApplicationId);
            LeaveApplication existingApplication = await GetApplicationById(applicationIdAsGuid);

            List<string> reviewerIds = notification.WorkflowReviewApplication.Reviewers.Select(x => x.Id).ToList();
            await _activityRepository.SaveAsync(new ApplicationActivity(notification.WorkflowReviewApplication.WorkflowActivityId, notification.WorkflowReviewApplication.ApplicationId, reviewerIds, notification.WorkflowReviewApplication.ReviewActions));

            await SendToReviewers(notification.WorkflowReviewApplication.Reviewers, existingApplication, notification.WorkflowReviewApplication.ReviewActions, cancellationToken);

            var applicant = await GetEmployeeById(existingApplication.ApplicantId.ToString());
            var applicationWatchers = await GetApplicationWatchers(notification.WorkflowReviewApplication.SubscribedJobTitleIds);
            var reviewersList = notification.WorkflowReviewApplication.Reviewers;
            await _mediator.Publish(new ApplicationSentToReviewer<LeaveApplication>(existingApplication, applicant.GetFullName(), reviewersList, applicationWatchers), cancellationToken);
        }

        private async Task SendToReviewers(HashSet<Reviewer> reviewers, LeaveApplication existingApplication, HashSet<string> actions, CancellationToken cancellationToken)
        {
            if (reviewers.Count == 1)
                await SendToReviewer(reviewers.FirstOrDefault().Id, existingApplication, actions, cancellationToken);
            else
                foreach (var reviewer in reviewers)
                    await SendToReviewer(reviewer.Id, existingApplication, actions, cancellationToken);
        }

        private async Task SendToReviewer(string id, LeaveApplication existingApplication, HashSet<string> actions, CancellationToken cancellationToken)
        {
            Employee reviewer = await GetEmployeeById(id);
            await _mediator.Publish(new ApplicationReadyForReview<LeaveApplication>(existingApplication, reviewer.Id.ToString(), reviewer.GetFullName(), actions), cancellationToken);
        }

        private async Task<HashSet<string>> GetApplicationWatchers(HashSet<string> subscribedJobTitleIds)
        {
            HashSet<string> applicationWatcherIds = new();
            if (subscribedJobTitleIds.Count < 1)
                return applicationWatcherIds;

            foreach (var jobTitleId in subscribedJobTitleIds)
            {
                var jobTitleInDb = await jobTitleRepository.ReadOneAsync(x => x.Id.ToString() == jobTitleId);

                if (jobTitleInDb == null)
                    continue;

                var employeeJobInfoInDb = await jobInfoRepository.ReadOneAsync(x => x.JobTitleId == jobTitleInDb.Id);

                if (employeeJobInfoInDb != null)
                    applicationWatcherIds.Add(employeeJobInfoInDb.EmployeeId.ToString());
            }

            return applicationWatcherIds;
        }

        private async Task<LeaveApplication> GetApplicationById(Guid applicationIdAsGuid)
        {
            var existingApplication = await _leaveApplicationRepository.FindAsync(applicationIdAsGuid);
            if (existingApplication == null)
                throw new InvalidOperationException("Invalid Application!");
            return existingApplication;
        }

        private async Task<Employee> GetEmployeeById(string id)
        {
            var employeeId = Transform.ToGuid(id);
            var existingEmployee = await _employeeRepository.FindAsync(x => x.Id == employeeId);

            if (existingEmployee == null)
                throw new InvalidOperationException("Invalid employee Id.");
            return existingEmployee;
        }

        private static Guid GetApplicationIdAsGiud(string applicationId)
        {
            Guid applicationIdAsGuid;
            if (!Guid.TryParse(applicationId, out applicationIdAsGuid))
                throw new InvalidOperationException("Invalid Application Id!");
            return applicationIdAsGuid;
        }
    }
}