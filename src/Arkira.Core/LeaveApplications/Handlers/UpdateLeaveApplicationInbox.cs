﻿using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications.Handlers
{
    public class UpdateLeaveApplicationInbox : IAppNotificationHandler<ApplicationProcessed<LeaveApplication>>
    {
        private readonly IAppMediator _mediator;

        public UpdateLeaveApplicationInbox(IAppMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Handle(ApplicationProcessed<LeaveApplication> notification, CancellationToken cancellationToken)
        {
            await _mediator.Publish(new ApplicationProcessed<Application>(notification.ApplicationId, notification.ApplicationAction, notification.ActionPerformedById, notification.ApplicationName, notification.ActivityId));
        }
    }
}