﻿using Arkira.Core.Employees;
using Arkira.Core.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications
{
    public interface ILeaveApplicationDomainService
    {
        Task<LeaveApplicationCreationResult> CreateAsync([NotNull] Guid leavePolicyId, [NotNull] DateTime startsOn, [NotNull] DateTime endsOn, [NotNull] Guid coveringEmployee, [NotNull] Guid applicantId, string notes, string hrNotes, [NotNull] ICollection<LeaveAttachment> attachments, Guid? initiatorId, ApplicationStatus status = ApplicationStatus.Drafted, bool isInitiatedByHr = false, int version = 1, Guid? correlationId = null);

        Task<Employee> GetApplicantAsync(LeaveApplication leaveApplication);

        Task<Employee> GetApplicantByApplicationIdAsync(string id, CancellationToken cancellationToken = default);

        DateTime GetDateCreated(LeaveApplication leaveApplication);

        Task<int> GetLengthOfApplicationPeriod(LeaveApplication leaveApplication);

        DateTime GetStartDate(LeaveApplication leaveApplication);
    }
}