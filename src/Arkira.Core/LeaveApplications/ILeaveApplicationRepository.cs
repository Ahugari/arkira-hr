﻿using Arkira.Core.Repositories;

namespace Arkira.Core.LeaveApplications
{
    public interface ILeaveApplicationRepository : IGenericRepository<LeaveApplication>
    {
    }
}