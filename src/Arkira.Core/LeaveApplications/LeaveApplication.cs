﻿using Arkira.Core.Applications;
using Arkira.Core.Entities;
using Arkira.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Arkira.Core.LeaveApplications
{
    public class LeaveApplication : Application, IHasCorrelationId, IHasVersionId, IHasCreationDate
    {
        public Guid LeavePolicyId { get; set; }
        public DateTime StartsOn { get; set; }
        public DateTime EndsOn { get; set; }
        public Guid CoveringEmployee { get; set; }
        public string Notes { get; set; }
        public string HrNotes { get; set; } = default;
        public Guid CorrelationId { get; internal set; }

        [ConcurrencyCheck]
        public int Version { get; internal set; }

        public ICollection<LeaveAttachment> Attachments { get; internal set; }
        public bool IsInitiatedByHr { get; set; }
        public DateTime DateCreated { get; private set; }

        protected LeaveApplication()
        {
        }

        public LeaveApplication(
            Guid leavPolicyId,
            DateTime startsOn,
            DateTime endsOn,
            Guid coveringEmployee,
            Guid applicantId,
            string notes,
            string hrNotes,
            Guid correlationId,
            int version,
            Guid? InitiatorId,
            DateTime dateCreated,
            bool isInitiatedByHr = false,
            ApplicationStatus status = ApplicationStatus.Drafted) : base()
        {
            LeavePolicyId = leavPolicyId;
            StartsOn = startsOn.Date;
            EndsOn = endsOn.Date;
            CoveringEmployee = coveringEmployee;
            ApplicantId = applicantId;
            Notes = notes;
            HrNotes = hrNotes;
            CorrelationId = correlationId;
            Version = version;
            this.InitiatorId = InitiatorId;
            IsInitiatedByHr = isInitiatedByHr;
            Status = status;
            Attachments = new HashSet<LeaveAttachment>();
            DateCreated = dateCreated;
        }

        public override string GetName() => "Leave Application";

        public override string ToString() => GetName();


    }
}