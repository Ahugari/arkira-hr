﻿using Arkira.Core.Applications.Events;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.Helpers;
using Arkira.Core.LeaveApplications.Events;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.PublicHolidays;
using Arkira.Core.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveApplications
{
    public struct LengthOfApplication
    {
        public readonly IEnumerable<DateTime> Dates;
        public readonly int TotalNumberOfDays;

        public LengthOfApplication(List<DateTime> validDates, int count)
        {
            Dates = validDates;
            TotalNumberOfDays = count;
        }
    }

    public struct LeaveApplicationCreationResult
    {
        public readonly LeaveApplication Application;
        public readonly HashSet<string> ApplicationWarnings;

        public LeaveApplicationCreationResult(LeaveApplication application, HashSet<string> warnings)
        {
            Application = application;
            ApplicationWarnings = warnings;
        }
    }

    public class LeaveApplicationDomainService : ILeaveApplicationDomainService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILeaveApplicationRepository _leaveApplicationRepository;
        private readonly LeavePolicyAccountHistoryDomainService _leavePolicyAccountHistoryDomainService;
        private readonly ILeavePolicyRepository _leavePolicyRepository;
        private readonly ILeaveTypeRepository _leaveTypeRepository;
        private readonly IAppMediator _mediator;
        private readonly IPublicHolidayRepository _publicHolidayRepository;
        private readonly ISystemClock _systemClock;

        public LeaveApplicationDomainService(ILeavePolicyRepository leavePolicyRepository,
                                             LeavePolicyAccountHistoryDomainService leavePolicyAccountHistoryDomainService,
                                             IEmployeeRepository employeeRepository, ILeaveTypeRepository leaveTypeRepository,
                                             ILeaveApplicationRepository leaveApplicationRepository, IPublicHolidayRepository publicHolidayRepository,
                                             ISystemClock systemClock, IAppMediator mediator)
        {
            _leavePolicyRepository = leavePolicyRepository;
            _leavePolicyAccountHistoryDomainService = leavePolicyAccountHistoryDomainService;
            _employeeRepository = employeeRepository;
            _leaveTypeRepository = leaveTypeRepository;
            _systemClock = systemClock;
            _mediator = mediator;
            _leaveApplicationRepository = leaveApplicationRepository;
            _publicHolidayRepository = publicHolidayRepository;
        }

        public async Task<LeaveApplicationCreationResult> CreateAsync(
            [NotNull] Guid leavePolicyId,
            [NotNull] DateTime startsOn,
            [NotNull] DateTime endsOn,
            [NotNull] Guid coveringEmployee,
            [NotNull] Guid applicantId,
            string notes,
            string hrNotes,
            [NotNull] ICollection<LeaveAttachment> attachments,
            Guid? initiatorId,
            ApplicationStatus status = ApplicationStatus.Drafted,
            bool isInitiatedByHr = false,
            int version = 1,
            Guid? correlationId = null
            )
        {
            var dateCreated = _systemClock.UtcNow;
            var leavePolicy = await _leavePolicyRepository.FindAsync(leavePolicyId);
            await ValidateInput(startsOn, endsOn, coveringEmployee, applicantId, hrNotes, attachments, initiatorId, isInitiatedByHr, leavePolicy);
            await ValidateAgainstLeavePolicy(leavePolicyId, startsOn, endsOn, applicantId, dateCreated);

            var leaveApplication = new LeaveApplication(
                leavePolicyId,
                startsOn,
                endsOn,
                coveringEmployee,
                applicantId,
                notes,
                hrNotes,
                correlationId ?? Guid.NewGuid(),
                version,
                initiatorId,
                dateCreated,
                isInitiatedByHr,
                status);

            var warnings = await ValidateAgainstRestrictionProfileAsync(leavePolicy.LeaveRestrictions, leaveApplication);

            AddAttachments(leaveApplication, attachments, leavePolicy);

            //calculate the leave balance and update leavepolicy history
            var numberOfDaysToDeduct = await CalculateLeaveDays(startsOn, endsOn, leavePolicy);

            _leaveApplicationRepository.Add(leaveApplication);

            await _mediator.Publish(new LeaveApplicationPrepared(leaveApplication, numberOfDaysToDeduct));

            await _mediator.Publish(new ApplicationPrepared<LeaveApplication>(leaveApplication, status.ToString()));

            return new LeaveApplicationCreationResult(leaveApplication, warnings);
        }

        public async Task<int> GetLengthOfApplicationPeriod(LeaveApplication leaveApplication)
        {
            var leavePolicy = await _leavePolicyRepository.FindAsync(leaveApplication.LeavePolicyId);

            return await CalculateLeaveDays(leaveApplication.StartsOn, leaveApplication.EndsOn, leavePolicy);
        }

        public DateTime GetDateCreated(LeaveApplication leaveApplication)
        {
            return leaveApplication.DateCreated;
        }

        public DateTime GetStartDate(LeaveApplication leaveApplication)
        {
            return leaveApplication.StartsOn.Date;
        }

        public async Task<Employee> GetApplicantByApplicationIdAsync(string id, CancellationToken cancellationToken = default)
        {
            var applicationId = Transform.ToGuid(id);

            var application = await _leaveApplicationRepository.FindAsync(applicationId);

            return await GetApplicantAsync(application);
        }

        public async Task<Employee> GetApplicantAsync(LeaveApplication leaveApplication)
        {
            return await _employeeRepository.FindAsync(leaveApplication.ApplicantId);
        }

        private async Task<HashSet<string>> ValidateAgainstRestrictionProfileAsync(ICollection<LeaveRestriction> leaveRestrictions, LeaveApplication leaveApplication)
        {
            HashSet<string> warningsList = new();

            foreach (var restriction in leaveRestrictions)
            {
                var validationResult = await (restriction as ILeaveRestrictionService).ValidateAsync(leaveApplication, this);

                if (validationResult.RestrictionValidationResult is LeaveRestrictionAction.BlockRequest && !validationResult.SuccessResult)
                    throw new InvalidOperationException(validationResult.ValidationMessage);

                if (validationResult.RestrictionValidationResult is LeaveRestrictionAction.DisplayWarning && !validationResult.SuccessResult)
                    warningsList.Add(validationResult.ValidationMessage);
            }
            return warningsList;
        }

        private static bool EmployeeIsEntitledToLeavePolicy(LeavePolicy leavePolicy, Employee employee)
        {
            return employee.LeavePolicyEntitlements.Any(x => x.EmployeeId == employee.Id && x.LeavePolicyId == leavePolicy.Id);
        }

        private static void ValidateAttachments(ICollection<LeaveAttachment> attachments, LeavePolicy leavePolicy)
        {
            if (leavePolicy.EnableAttachments && attachments == null)
                throw new InvalidOperationException("Attachments are required for the selected Leave Policy.");

            if (leavePolicy.EnableAttachments && attachments.Count < 1)
                throw new InvalidOperationException("Attachments are required for the selected Leave Policy.");

            if (!leavePolicy.EnableAttachments && attachments != null)
                throw new InvalidOperationException("Attachments are not required for the selected Leave Policy.");
        }

        private static void ValidateHrInitiation(string hrNotes, Guid? initiatorId, bool isInitiatedByHr, Guid applicantId)
        {
            if (isInitiatedByHr && !initiatorId.HasValue)
                throw new InvalidOperationException("Initiator cannot be empty while InitiationByHr is enabled.");
            if (!isInitiatedByHr && initiatorId.HasValue)
                throw new InvalidOperationException("Cannot set Initiator while InitiationByHr is disabled.");
            if (initiatorId.HasValue && initiatorId.Value == applicantId)
                throw new InvalidOperationException("Initiator cannot be the same as applicant.");
            ValidateHrNotes(hrNotes, isInitiatedByHr);
        }

        private static void ValidateHrNotes(string hrNotes, bool isInitiatedByHr)
        {
            if (isInitiatedByHr && string.IsNullOrWhiteSpace(hrNotes))
                throw new InvalidOperationException("Notes by HR should be provided.");
            if (!string.IsNullOrWhiteSpace(hrNotes) && !isInitiatedByHr)
                throw new InvalidOperationException("Notes by HR not required while InitiationByHr is disabled.");
        }

        private void AddAttachments(LeaveApplication leaveApplication, ICollection<LeaveAttachment> attachments, LeavePolicy leavePolicy)
        {
            if (!leavePolicy.EnableAttachments)
                return;

            foreach (var attachment in attachments)
                if (!leaveApplication.Attachments.Contains(attachment))
                    leaveApplication.Attachments = attachments;
        }

        private async Task<int> CalculateLeaveDays(DateTime startsOn, DateTime endsOn, LeavePolicy leavePolicy)
        {
            LengthOfApplication applicationPeriod = await CalculateLengthOfApplicationPeriodAsync(startsOn, endsOn, leavePolicy.LeaveType.LeaveRequestLengthFactor, leavePolicy.LeaveType.IncludePublicHolidays);
            return applicationPeriod.TotalNumberOfDays;
        }

        private async Task<LengthOfApplication> CalculateLengthOfApplicationPeriodAsync(DateTime startsOn, DateTime endsOn, LeaveRequestLengthFactor leaveRequestLengthFactor, bool includePublicHolidays)
        {
            var rawDuration = (endsOn - startsOn);
            var validDates = new List<DateTime>();
            var duration = rawDuration.Days + 1;

            if (duration == 1 && startsOn == endsOn && await IsPublicHolidayAsync(endsOn) && !includePublicHolidays)
                throw new InvalidOperationException("Leave Policy selected doesn't count public holidays. Select another Leave Policy or change your date selection.");

            if (leaveRequestLengthFactor is LeaveRequestLengthFactor.EveryWorkingDay
                && duration < 3
                && startsOn.DayOfWeek is DayOfWeek.Saturday
                && endsOn.DayOfWeek is DayOfWeek.Sunday)
                throw new InvalidOperationException("Leave Policy selected doesn't allow your current date selection. Select another Leave Policy or change your date selection.");

            var currentDay = startsOn;
            for (int day = 0; day < duration; day++)
            {
                if ((leaveRequestLengthFactor == LeaveRequestLengthFactor.EveryWorkingDay
                    && (currentDay.AddDays(day).DayOfWeek == DayOfWeek.Sunday) ^ (currentDay.AddDays(day).DayOfWeek == DayOfWeek.Saturday))
                    || (await IsPublicHolidayAsync(currentDay) && !includePublicHolidays))
                    continue;

                validDates.Add(currentDay.AddDays(day).Date);
            }

            return new LengthOfApplication(validDates, validDates.Count);
        }

        private async Task<int> GetBalanceAfterApplicationPeriodAsync(LeavePolicy leavePolicy, int numberOfDays, Employee employee)
        {
            var currentBalance = await _leavePolicyAccountHistoryDomainService.GetCurrentBalanceAsync(employee, leavePolicy);
            return currentBalance - numberOfDays;
        }

        private async Task<int?> GetEntitlementCapAsync(LeavePolicy leavePolicy)
        {
            if (leavePolicy.HasOwnEntitlement)
                return leavePolicy.EntitlementCap.Value;

            var leaveType = await _leaveTypeRepository.FindAsync(leavePolicy.EntitlementFromTypeId.Value);

            return leaveType.EntitlementCap;
        }

        private async Task<bool> HasUsableLeavePolicyAccountBalance(Guid leavePolicyId, Guid applicantId, int balanceAfterRequest, DateTime dateCreated, DateTime startsOn)
        {
            var employeeInDb = await _employeeRepository.FindAsync(applicantId);
            if (employeeInDb == null)
                throw new InvalidOperationException("Employee doesn't exist.");

            var leavePolicyInDb = await _leavePolicyRepository.FindAsync(leavePolicyId);
            if (leavePolicyInDb == null)
                throw new InvalidOperationException("Leave Policy doesn't exist.");

            var existingApprovedApplications = await _leaveApplicationRepository.GetManyAsync(x => x.ApplicantId == applicantId
                                                                                                && x.Status == ApplicationStatus.Approved
                                                                                                && x.EndsOn < startsOn);
            var dateFilter = _systemClock.UtcNow;
            if (existingApprovedApplications.Any())
                dateFilter = existingApprovedApplications.OrderByDescending(x => x.EndsOn).FirstOrDefault().EndsOn;

            var leavePolicyAccountBalance = await _leavePolicyAccountHistoryDomainService.GetCurrentBalanceAsAtAsync(employeeInDb, leavePolicyInDb, dateFilter);

            if (!leavePolicyInDb.LeaveType.AllowNegativeEntitlement && !leavePolicyInDb.LeaveType.NewYearActiveWindow.HasValue)
                return balanceAfterRequest > -1;

            if (leavePolicyInDb.LeaveType.AllowNegativeEntitlement && balanceAfterRequest > leavePolicyInDb.LeaveType.MaxNegativeEntitlementBelowZero)
                return true;

            if (leavePolicyInDb.LeaveType.NewYearActiveWindow.HasValue)
            {
                DateTime newYearActiveStartDate = DateTime.Today.AddMonths(leavePolicyInDb.LeaveType.NewYearActiveWindow.Value);
                if (leavePolicyInDb.LeaveType.NewYearActiveWindow.HasValue && leavePolicyAccountBalance >= 0 && dateCreated >= newYearActiveStartDate)
                    return true;
            }

            return false;
        }

        private async Task<bool> IsPublicHolidayAsync(DateTime day)
        {
            return await _publicHolidayRepository.FindAsync(x => x.Day.Date == day.Date) != null;
        }

        private bool IsWithinNewYearActiveWindow(int? newYearActiveWindow, DateTime dateCreated)
        {
            if (newYearActiveWindow == null)
                throw new ArgumentNullException(nameof(newYearActiveWindow));

            DateTime newYearWindowStartDate = DateTime.Now.AddMonths(newYearActiveWindow.Value);
            return dateCreated > newYearWindowStartDate;
        }

        private void ValidateAgainstLeaveRequestWindow(DateTime startsOn, DateTime endsOn, LeaveType leaveType)
        {
            var startOfCurrentYear = new DateTime(_systemClock.UtcNow.Year, 1, 1);

            if (leaveType.LeaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture)
            {
                var requestWindowStartDate = startOfCurrentYear.AddMonths(ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE);
                var requestWindowEndDate = requestWindowStartDate.AddMonths(leaveType.MaxMonthsPeriodInFuture.Value);

                if (startsOn < requestWindowStartDate || endsOn < requestWindowStartDate
                    ^ startsOn > requestWindowEndDate || endsOn > requestWindowEndDate)
                    throw new InvalidOperationException($"Requested period do not fall between acceptable Leave Policy Application period window. Acceptable period is '{requestWindowStartDate}' to '{requestWindowEndDate}'.");
            }
            else if (leaveType.LeaveRequestWindow == LeaveRequestWindow.CurrentAndFollowingYear)
            {
                var endOfNextYear = new DateTime(_systemClock.UtcNow.AddYears(1).Year, 12, 31);
                if (startsOn < startOfCurrentYear || endsOn < startOfCurrentYear || startsOn > endOfNextYear || endsOn > endOfNextYear)
                    throw new InvalidOperationException($"Requested period do not fall between acceptable Leave Policy Application period window. Acceptable period is '{startOfCurrentYear}' to '{endOfNextYear}'.");
            }
            else if (leaveType.LeaveRequestWindow == LeaveRequestWindow.OnlyInCurrentYear)
            {
                var endOfCurrentYear = new DateTime(_systemClock.UtcNow.Year, 12, 31);

                if ((startsOn < startOfCurrentYear || endsOn < startOfCurrentYear) || (startsOn > endOfCurrentYear || endsOn > endOfCurrentYear))
                    throw new InvalidOperationException($"Requested period do not fall between acceptable Leave Policy Application period window. Acceptable period is '{startOfCurrentYear}' to '{endOfCurrentYear}'.");
            }
        }

        private async Task ValidateApplicationPeriodDates(DateTime startsOn, DateTime endsOn, Guid applicantId)
        {
            if (endsOn.Date < startsOn.Date)
                throw new InvalidOperationException("Selected date period is invalid.");

            if (await OverlapingApplicationExists(startsOn, endsOn, applicantId))
                throw new InvalidOperationException("Selected date period overlaps with existing Leave Applications.");
        }

        private async Task<bool> OverlapingApplicationExists(DateTime startsOn, DateTime endsOn, Guid applicantId)
        {
            return (await _leaveApplicationRepository.FindAsync(x =>
            x.ApplicantId == applicantId && x.Status != ApplicationStatus.Withdrawn
            && startsOn.Date >= x.StartsOn.Date && endsOn.Date <= x.EndsOn.Date
            || (startsOn.Date <= x.StartsOn.Date && endsOn.Date >= x.EndsOn.Date)
            || (startsOn.Date > x.StartsOn.Date && endsOn.Date > x.EndsOn.Date)
            || (startsOn.Date < x.StartsOn.Date && endsOn.Date < x.EndsOn.Date)
            || (startsOn.Date < x.StartsOn.Date && endsOn.Date > x.EndsOn.Date))) != null;
        }

        private async Task ValidateCoveringEmployee(Guid employee, Guid applicantId)
        {
            await ValidateEmployee(employee);

            if (employee == applicantId)
                throw new InvalidOperationException("Applicant cannot be the same as covering employee.");

            //TODO: validate that employee isn't on leave, issue a warning message if possible
            //employee should covering, if on leave, should return atleast two days before the
            //start of leave for the employee
        }

        private async Task ValidateDuplicateLeaveApplicationAsync(DateTime startsOn, DateTime endsOn, Guid applicantId)
        {
            if (await _leaveApplicationRepository.FindAsync(x => x.StartsOn.Date == startsOn.Date && x.EndsOn.Date == endsOn.Date && x.ApplicantId == applicantId) != null)
                throw new InvalidOperationException("Application with specified date period already exists.");
        }

        private async Task ValidateEmployee(Guid employee)
        {
            var existingEmployee = await _employeeRepository.FindAsync(employee);
            if (existingEmployee == null)
                throw new InvalidOperationException("Invalid employee.");
        }

        private async Task ValidateInput(DateTime startsOn, DateTime endsOn, Guid coveringEmployee, Guid applicantId, string hrNotes, ICollection<LeaveAttachment> attachments, Guid? initiatorId, bool isInitiatedByHr, LeavePolicy leavePolicy)
        {
            await ValidateApplicationPeriodDates(startsOn, endsOn, applicantId);
            await ValidateDuplicateLeaveApplicationAsync(startsOn, endsOn, applicantId);
            ValidateLeavePolicy(leavePolicy);
            await ValidateEmployee(applicantId);
            await ValidateCoveringEmployee(coveringEmployee, applicantId);
            ValidateHrInitiation(hrNotes, initiatorId, isInitiatedByHr, applicantId);
            ValidateAttachments(attachments, leavePolicy);
        }

        private void ValidateLeavePolicy(LeavePolicy leavePolicy)
        {
            if (leavePolicy == null)
                throw new InvalidOperationException("Leave Policy doesn't exist");
        }

        private async Task ValidateAgainstLeavePolicy(Guid leavePolicyId, DateTime startsOn, DateTime endsOn, Guid applicantId, DateTime dateCreated)
        {
            var leavePolicy = await _leavePolicyRepository.FindAsync(leavePolicyId);
            var leaveType = await _leaveTypeRepository.FindAsync(leavePolicy.LeaveTypeId);
            var employee = await _employeeRepository.GetWithLeavePolicyEntitlements(x => x.Id == applicantId);

            if (!EmployeeIsEntitledToLeavePolicy(leavePolicy, employee))
                throw new InvalidOperationException("Leave Policy is not available for the current employee.");

            ValidateAgainstLeaveRequestWindow(startsOn, endsOn, leaveType);

            var entitlementCap = await GetEntitlementCapAsync(leavePolicy);
            bool isUnlimited = entitlementCap == null;
            //TODO: arrange the unlimited days validation so it doesn't skip other required validations
            if (isUnlimited)
                return;

            var datesAndDays = await CalculateLengthOfApplicationPeriodAsync(startsOn, endsOn, leaveType.LeaveRequestLengthFactor, leaveType.IncludePublicHolidays);
            int numberOfDays = datesAndDays.TotalNumberOfDays;

            var balanceAfter = await GetBalanceAfterApplicationPeriodAsync(leavePolicy, numberOfDays, employee);
            if (!await HasUsableLeavePolicyAccountBalance(leavePolicyId, applicantId, balanceAfterRequest: balanceAfter, dateCreated, startsOn))
                throw new InvalidOperationException("Account balance too low to continue.");
        }
    }
}