﻿using System;

namespace Arkira.Core.LeaveApplications
{
    public class LeaveApplicationException : Exception
    {
        public LeaveApplicationException(string message) : base(message)
        {
        }

        public LeaveApplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}