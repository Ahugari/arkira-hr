﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.LeaveApplications
{
    public class LeaveAttachment : Entity<Guid>
    {
        public string Path { get; internal set; }
        public Guid UploadedBy { get; internal set; }
        public LeaveApplication LeaveApplication { get; internal set; }

        private LeaveAttachment()
        {
        }

        public LeaveAttachment(string path, Guid uploadedBy)
        {
            Path = path;
            UploadedBy = uploadedBy;
        }

        public static readonly string EntityName = "Leave Attachment";

        public override string ToString() => EntityName;
    }
}