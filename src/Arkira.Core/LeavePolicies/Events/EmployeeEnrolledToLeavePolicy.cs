﻿using Arkira.Core.Employees;
using MediatR;

namespace Arkira.Core.LeavePolicies.Events
{
    public record EmployeeEnrolledToLeavePolicy(Employee Employee, LeavePolicy LeavePolicy) : INotification;
}