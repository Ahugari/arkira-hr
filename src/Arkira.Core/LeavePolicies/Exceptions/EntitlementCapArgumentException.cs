﻿using System;

namespace Arkira.Core.LeavePolicies.Exceptions
{
    public class EntitlementCapArgumentException : Exception
    {
        public EntitlementCapArgumentException(string message) : base(message)
        {
        }
    }
}