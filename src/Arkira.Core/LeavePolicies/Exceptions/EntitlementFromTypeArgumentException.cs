﻿using System;

namespace Arkira.Core.LeavePolicies.Exceptions
{
    public class EntitlementFromTypeArgumentException : Exception
    {
        public EntitlementFromTypeArgumentException(string message) : base(message)
        {
        }
    }
}