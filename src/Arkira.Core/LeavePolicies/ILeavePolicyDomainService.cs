﻿using Arkira.Core.Enums;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicies
{
    public interface ILeavePolicyDomainService
    {
        Task<LeavePolicy> CreateAsync(string name, Guid leaveTypeId, bool hasOwnEntitlement, bool paidLeave, Guid? entitlementFromTypeId, MinLeaveDurationInDay minLeaveDurationInDay, int daysToEnrollment, bool enableAttachments, DateTime effectiveDate, int? entitlementCap, int? version = null, Guid? correlationId = null, Guid? oldVersion = null, bool isLatest = true);

        Task<int> GetEntitlementCapAsync(LeavePolicy leavePolicy);

        Task RemoveAsync(Guid id);

        Task<LeavePolicy> UpdatePolicy(Guid id, string name, Guid leaveTypeId, bool hasOwnEntitlement, bool paidLeave, Guid? entitlementFromTypeId, MinLeaveDurationInDay minLeaveDurationInDay, int daysToEnrollment, bool enableAttachments, int? entitlementCap);

        Task<DateTime> GetLeavePolicyYearStartDateAsync(Guid leavePolicyId);
    }
}