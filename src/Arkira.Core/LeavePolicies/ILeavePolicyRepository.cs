﻿using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicies
{
    public interface ILeavePolicyRepository : IGenericRepository<LeavePolicy>
    {
        void Add(LeavePolicy entity);

        Task<IEnumerable<LeavePolicy>> GetAllWithRestrictions();

        Task<IEnumerable<LeavePolicy>> GetAsync();

        LeavePolicy GetOneWithRestrictions(Expression<Func<LeavePolicy, bool>> predicate);

        Task RemoveAsync(LeavePolicy leavePolicy);
    }
}