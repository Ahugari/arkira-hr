﻿using Arkira.Core.EmployeeLeavePolicies;
using Arkira.Core.Entities;
using Arkira.Core.Enums;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Arkira.Core.LeavePolicies
{
    public class LeavePolicy : Entity<Guid>, IHasVersionId, IHasCorrelationId, IHasCreationDate
    {
        public string Name { get; set; }
        public Guid LeaveTypeId { get; set; }
        public bool HasOwnEntitlement { get; set; }
        public bool PaidLeave { get; set; }
        public Guid? EntitlementFromTypeId { get; set; }
        public MinLeaveDurationInDay MinLeaveDurationInDay { get; set; }
        public bool EnableAttachments { get; set; }
        public int? EntitlementCap { get; set; }

        public LeaveType LeaveType { get; set; }

        public Guid CorrelationId { get; set; }

        [ConcurrencyCheck]
        public int Version { get; set; }

        public int DaysToEnrollment { get; set; }
        public DateTime DateCreated { get; private set; }
        public DateTime EffectiveDate { get; set; }

        public bool IsLatest { get; set; }

        public ICollection<EmployeeLeavePolicy> EntitledEmployees { get; internal set; }
        public ICollection<LeaveRestriction> LeaveRestrictions { get; internal set; }

        private LeavePolicy()
        {
        }

        public LeavePolicy(
            string name,
            Guid leaveTypeId,
            bool hasOwnEntitlement,
            bool paidLeave,
            Guid? entitlementFromTypeId,
            MinLeaveDurationInDay minLeaveDurationInDay,
            bool enableAttachments,
            [NotNull] DateTime effectiveDate,
            int? entitlementCap,
            Guid correlationId,
            int version,
            int daysToEnrollment,
            bool isLatest) : base()
        {
            Name = name.Trim();
            LeaveTypeId = leaveTypeId;
            HasOwnEntitlement = hasOwnEntitlement;
            PaidLeave = paidLeave;
            EntitlementFromTypeId = entitlementFromTypeId;
            MinLeaveDurationInDay = minLeaveDurationInDay;
            EnableAttachments = enableAttachments;
            EffectiveDate = effectiveDate;
            EntitlementCap = entitlementCap;
            CorrelationId = correlationId;
            Version = version;
            DaysToEnrollment = daysToEnrollment;
            DateCreated = DateTime.Now;
            IsLatest = isLatest;
            EntitledEmployees = new HashSet<EmployeeLeavePolicy>();
            LeaveRestrictions = new HashSet<LeaveRestriction>();
        }

        public static readonly string EntityName = "Leave Policy";

        public override string ToString() => EntityName;

        //TODO: Implement remove
        //TODO: while implementing update, carefully perform an update on days to enrollment
        //since we would have to remove scheduled enrollments (if schedules have been created already)
        //TODO: Remove the entitledemployees as a navigation property since employees already have it.
    }
}