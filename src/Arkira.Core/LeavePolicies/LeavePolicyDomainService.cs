﻿using Arkira.Core.EmployeeLeavePolicies;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies.Events;
using Arkira.Core.LeavePolicies.Exceptions;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicies
{
    public class LeavePolicyDomainService : ILeavePolicyDomainService
    {
        private readonly ILeavePolicyRepository _leavePolicyRepository;
        private readonly ILeaveTypeRepository _leaveTypeRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMediator _mediator;

        public LeavePolicyDomainService(ILeavePolicyRepository leavePolicyRepository,
            ILeaveTypeRepository leaveTypeRepository, IEmployeeRepository employeeRepository, IMediator mediator)
        {
            _leavePolicyRepository = leavePolicyRepository;
            _leaveTypeRepository = leaveTypeRepository;
            _employeeRepository = employeeRepository;
            _mediator = mediator;
        }

        public async Task<int> GetEntitlementCapAsync(LeavePolicy leavePolicy)
        {
            var existingLeavePolicy = await _leavePolicyRepository.FindAsync(leavePolicy.Id);
            if (existingLeavePolicy == null)
                throw new InvalidOperationException("Leave Policy doesn't exist.");

            if (!existingLeavePolicy.HasOwnEntitlement)
                return (await _leaveTypeRepository.FindAsync(leavePolicy.EntitlementFromTypeId.Value)).EntitlementCap.Value;

            return existingLeavePolicy.EntitlementCap.Value;
        }

        public async Task<LeavePolicy> CreateAsync(
            string name,
            Guid leaveTypeId,
            bool hasOwnEntitlement,
            bool paidLeave,
            Guid? entitlementFromTypeId,
            MinLeaveDurationInDay minLeaveDurationInDay,
            int daysToEnrollment,
            bool enableAttachments,
            DateTime effectiveDate,
            int? entitlementCap,
            int? version = null,
            Guid? correlationId = null,
            Guid? oldVersion = null,
            bool isLatest = true)
        {
            await ValidateInput(hasOwnEntitlement, entitlementFromTypeId, entitlementCap, effectiveDate);

            if (await NameAlreadyUsed(name, correlationId))
                throw new InvalidOperationException($"Leave Policy '{name}' already exists.");

            var leavePolicy = new LeavePolicy(
                name,
                leaveTypeId,
                hasOwnEntitlement,
                paidLeave,
                entitlementFromTypeId,
                minLeaveDurationInDay,
                enableAttachments,
                effectiveDate,
                entitlementCap,
                correlationId ?? Guid.NewGuid(),
                version ?? 1,
                daysToEnrollment,
                isLatest);

            //TODO:Publish event for leave policy account history and more
            //use mediator pattern

            _leavePolicyRepository.Add(leavePolicy);

            await AddPolicyToEmployeesEntitlementListsAsync(leavePolicy);
            ScheduleEntitlementReset(leavePolicy);

            return leavePolicy;
        }

        public async Task<LeavePolicy> UpdatePolicy(
            Guid id,
            string name,
            Guid leaveTypeId,
            bool hasOwnEntitlement,
            bool paidLeave,
            Guid? entitlementFromTypeId,
            MinLeaveDurationInDay minLeaveDurationInDay,
            int daysToEnrollment,
            bool enableAttachments,
            int? entitlementCap
            )
        {
            var existingLeavePolicy = await _leavePolicyRepository.FindAsync(id);
            if (existingLeavePolicy == null)
                throw new InvalidOperationException("Leave Policy doesn't exist.");

            existingLeavePolicy.IsLatest = false;

            var updatedLeavepolicy = await CreateAsync(
                name, leaveTypeId, hasOwnEntitlement, paidLeave, entitlementFromTypeId, minLeaveDurationInDay,
                daysToEnrollment, enableAttachments, new SystemClock().UtcNow, entitlementCap, existingLeavePolicy.Version + 1,
                existingLeavePolicy.CorrelationId, existingLeavePolicy.Id);

            return updatedLeavepolicy;
        }

        public async Task RemoveAsync(Guid id)
        {
            var existingLeavePolicy = await _leavePolicyRepository.FindAsync(id);
            if (existingLeavePolicy == null)
                throw new InvalidOperationException("Leave Policy doesn't exists or already deleted.");

            //TODO: Verify attached leave applications and raise exception if found any.
            //TODO: Remove any entitlements that have been added to employees.

            _leavePolicyRepository.RemoveAsync(existingLeavePolicy);
        }

        //public async Task AddLeaveRestrictions(LeavePolicy leavePolicy, params LeaveRestriction[] leaveRestrictions)
        //{
        //    var leavePolicyInDb = await _leavePolicyRepository.FindAsync(leavePolicy.Id);
        //    if (leavePolicyInDb == null)
        //        return;
        //    var validLeaveRestrictions = await GetValidLeaveRestrictions(leaveRestrictions, leavePolicy.Id);

        //    foreach (var restriction in validLeaveRestrictions)
        //        if (!leavePolicy.LeaveRestrictions.Contains(restriction))
        //            leavePolicy.LeaveRestrictions.Add(restriction);

        //    //TODO: Publish LeavePolicy added event and handlers include notification and leave policy history
        //}

        //public async Task RemoveLeaveRestrictions(LeavePolicy leavePolicy, params LeaveRestriction[] leaveRestrictions)
        //{
        //    var leavePolicyInDb = await _leavePolicyRepository.FindAsync(leavePolicy.Id);
        //    if (leavePolicyInDb == null)
        //        return;
        //    var validLeaveRestrictions = await GetValidLeaveRestrictions(leaveRestrictions, leavePolicy.Id);

        //    foreach (var restriction in validLeaveRestrictions)
        //        if (!leavePolicy.LeaveRestrictions.Contains(restriction))
        //            leavePolicy.LeaveRestrictions.Remove(restriction);

        //    //TODO: Publish LeavePolicy removed event and handlers include notification and leave policy history
        //}

        public DateTime GetCarryOverLeaveYearStartDate(Guid leaveTypeId)
        {
            throw new NotImplementedException();
        }

        public IOrderedEnumerable<EmployeeLeavePolicy> GetEntitledEmployees(LeavePolicy leavePolicy)
        {
            return leavePolicy.EntitledEmployees
                .Where(x => x.LeavePolicyId == leavePolicy.Id)
                .Select(x => x)
                .OrderByDescending(x => x.DateCreated);
        }

        public async Task<DateTime> GetLeavePolicyYearStartDateAsync(Guid leavePolicyId)
        {
            var leavePolicyInDb = await _leavePolicyRepository.FindAsync(leavePolicyId);
            if (leavePolicyInDb == null)
                throw new InvalidOperationException("Leave Policy doesn't exist.");

            return leavePolicyInDb.EffectiveDate;
        }

        private static void AddLeaveRestrictions(List<LeaveRestriction> leaveRestrictions, LeavePolicy leavePolicy)
        {
            if (leaveRestrictions != null)
                leavePolicy.LeaveRestrictions = leaveRestrictions;
        }

        private void ScheduleEntitlementReset(LeavePolicy leavePolicy)
        {
            if (leavePolicy.LeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnSchedule)
            {
                //TODO:create a schedule for the specifed date
            }
            else if (leavePolicy.LeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnWorkAnniversary)
            {
                //TODO: create a daily background job that will check for the employees work anniversary
                //and reset the entitlement by adding it to their account history
            }

            //TODO: once a reset has been performed, their current balance should reflect the new entitlement and carry over days
            //idealy this should be the work of the get current balance method
        }

        private async Task AddPolicyToEmployeesEntitlementListsAsync(LeavePolicy leavePolicy)
        {
            await AddToEligibleEmployeesAsync(leavePolicy);
            await ScheduleEmployeesNotEligibleAsync(leavePolicy);
        }

        //private async Task<List<LeaveRestriction>> GetValidLeaveRestrictions(LeaveRestriction[] leaveRestrictionProfiles, Guid leaveTypeId)
        //{
        //    var validRestrictions = new List<LeaveRestriction>();

        //    foreach (var restriction in leaveRestrictionProfiles)
        //    {
        //        var existingRestriction = await _leaveRestrictionProfileRepository.FindAsync(x => x.Id == restriction.Id
        //                                                                                        && x.LeavePolicyId == leaveTypeId);
        //        if (existingRestriction == null)
        //            continue;
        //        validRestrictions.Add(existingRestriction);
        //    }

        //    return validRestrictions;
        //}

        private async Task ScheduleEmployeesNotEligibleAsync(LeavePolicy leavePolicy)
        {
            //TODO:schedule background daily jobs to add leave policy to their entitlement list.
            //this should be run daily and checked accross all employees.
            //the scheduled job will have to call a method that will search for all employees now eligble for the policy
            //AND do not currently have it on their entitlements list
            //the called method will have to access the eligble employees list and perform checks accordingly

            _ = await GetEmployeesNotEligibleAsync(leavePolicy);
        }

        private async Task<IEnumerable<Employee>> GetEmployeesNotEligibleAsync(LeavePolicy leavePolicy)
        {
            return await _employeeRepository.GetManyAsync(x => x.HireDate.AddDays(leavePolicy.DaysToEnrollment).Date > DateTime.Today.Date);
        }

        private async Task AddToEligibleEmployeesAsync(LeavePolicy leavePolicy)
        {
            IEnumerable<Employee> eligbleEmployees = await GetEligbleEmployeesAsync(leavePolicy);

            foreach (var employee in eligbleEmployees)
            {
                EmployeeLeavePolicy employeeLeavePolicy = new EmployeeLeavePolicy(employee.Id, leavePolicy.Id);
                if (!leavePolicy.EntitledEmployees.Contains(employeeLeavePolicy))
                    leavePolicy.EntitledEmployees.Add(employeeLeavePolicy);

                await _mediator.Publish(new EmployeeEnrolledToLeavePolicy(employee, leavePolicy));
            }
        }

        private async Task<IEnumerable<Employee>> GetEligbleEmployeesAsync(LeavePolicy leavePolicy)
        {
            return await _employeeRepository.GetManyAsync(x => x.HireDate.AddDays(leavePolicy.DaysToEnrollment).Date <= DateTime.Today.Date);
        }

        private async Task<bool> NameAlreadyUsed(string name, Guid? correlationId)
        {
            return await _leavePolicyRepository.FindAsync(x => x.Name.ToLower()
            .Trim() == name.ToLower()
            .Trim() && x.CorrelationId != correlationId) != null;
        }

        private async Task ValidateInput(bool hasOwnEntitlement, Guid? entitlementFromTypeId, int? entitlementCap, DateTime? effectiveDate)
        {
            ValidateEffectiveDate(effectiveDate);
            await ValidateEntitlementFromType(hasOwnEntitlement, entitlementFromTypeId);
            ValidateEntitlementCap(hasOwnEntitlement, entitlementCap);
        }

        private void ValidateEffectiveDate(DateTime? effectiveDate)
        {
            if (effectiveDate == null || effectiveDate == DateTime.MinValue)
                throw new InvalidOperationException("Effective date must be provided.");
        }

        private void ValidateEntitlementCap(bool hasOwnEntitlement, int? entitlementCap)
        {
            if (hasOwnEntitlement && entitlementCap == null)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be null or empty while {nameof(hasOwnEntitlement)} is enabled.");

            if (!hasOwnEntitlement && entitlementCap.HasValue)
                throw new EntitlementCapArgumentException($"Cannot set {nameof(entitlementCap)} while {nameof(hasOwnEntitlement)} is disabled.");

            if (hasOwnEntitlement && entitlementCap > ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be higher than {ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED}.");

            if (hasOwnEntitlement && entitlementCap < ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be lower than {ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED}.");
        }

        private async Task ValidateEntitlementFromType(bool hasOwnEntitlement, Guid? entitlementFromTypeId)
        {
            if (hasOwnEntitlement && entitlementFromTypeId.HasValue)
                throw new EntitlementFromTypeArgumentException($"Cannot set {nameof(entitlementFromTypeId)} while {nameof(hasOwnEntitlement)} is enabled.");

            if (!hasOwnEntitlement && entitlementFromTypeId == null)
                throw new EntitlementFromTypeArgumentException($"{nameof(entitlementFromTypeId)} cannot be null or empty while {nameof(hasOwnEntitlement)} is disabled.");

            if (!hasOwnEntitlement && !await LeaveTypeExists(entitlementFromTypeId.Value))
                throw new EntitlementFromTypeArgumentException("Leave type doesn't exist.");
        }

        private async Task<bool> LeaveTypeExists(Guid entitlementFromTypeId)
        {
            try
            {
                return await _leaveTypeRepository.FindAsync(entitlementFromTypeId) != null;
            }
            catch (InvalidOperationException ex)
            {
                return false;
            }
        }
    }
}