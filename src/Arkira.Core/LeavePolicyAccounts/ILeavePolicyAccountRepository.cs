﻿using Arkira.Core.Repositories;

namespace Arkira.Core.LeavePolicyAccounts
{
    public interface ILeavePolicyAccountRepository : IGenericRepository<LeavePolicyAccount>
    {
    }
}