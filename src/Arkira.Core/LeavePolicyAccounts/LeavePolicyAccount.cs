﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.LeavePolicyAccounts
{
    public class LeavePolicyAccount : Entity<Guid>, IAuditableEntity
    {
        public Guid LeavePolicyId { get; set; }
        public int Balance { get; set; }
        public Guid EmployeeId { get; set; }

        public bool IsUsable { get; set; }
        public Guid CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public static readonly string EntityName = "Leave Policy Account";

        public override string ToString() => EntityName;
    }
}