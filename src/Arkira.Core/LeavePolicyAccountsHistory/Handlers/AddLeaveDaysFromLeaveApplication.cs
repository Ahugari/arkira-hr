﻿using Arkira.Core.LeaveApplications.Events;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicyAccountsHistory.Handlers
{
    public class AddLeaveDaysFromLeaveApplication : INotificationHandler<LeaveApplicationPrepared>
    {
        private readonly LeavePolicyAccountHistoryDomainService _accountHistoryDomainService;

        public AddLeaveDaysFromLeaveApplication(LeavePolicyAccountHistoryDomainService accountHistoryDomainService)
        {
            _accountHistoryDomainService = accountHistoryDomainService;
        }

        public Task Handle(LeaveApplicationPrepared notification, CancellationToken cancellationToken)
        {
            //TODO: this handler should instead handle the leave application approved event
            _accountHistoryDomainService.Create(notification.LeaveApplication.ApplicantId,
                                                Enums.LeavePolicyAccountAction.Deduct,
                                                notification.NumberOfDaysToDeduct,
                                                notification.LeaveApplication.EndsOn,
                                                notification.LeaveApplication.LeavePolicyId);

            return Task.CompletedTask;
        }
    }
}