﻿using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicies.Events;
using Arkira.Core.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicyAccountsHistory.Handlers
{
    public class AddLeavePolicyAccountHistory : INotificationHandler<EmployeeEnrolledToLeavePolicy>
    {
        private readonly LeavePolicyAccountHistoryDomainService _accountHistoryDomainService;
        private readonly LeavePolicyDomainService _leavePolicyDomainService;

        public AddLeavePolicyAccountHistory(LeavePolicyAccountHistoryDomainService leavePolicyAccountHistoryDomainService,
            LeavePolicyDomainService leavePolicyDomainService)
        {
            _accountHistoryDomainService = leavePolicyAccountHistoryDomainService;
            _leavePolicyDomainService = leavePolicyDomainService;
        }

        public async Task Handle(EmployeeEnrolledToLeavePolicy notification, CancellationToken cancellationToken)
        {
            foreach (var employeeLeavePolicy in notification.LeavePolicy.EntitledEmployees)
            {
                _accountHistoryDomainService.Create(employeeLeavePolicy.EmployeeId, LeavePolicyAccountAction.Enrolled,
                    await _leavePolicyDomainService.GetEntitlementCapAsync(notification.LeavePolicy), new SystemClock().UtcNow,
                    notification.LeavePolicy.Id);
            }
        }
    }
}