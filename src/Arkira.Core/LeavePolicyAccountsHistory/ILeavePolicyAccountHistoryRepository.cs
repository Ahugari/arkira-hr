﻿using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.Repositories;

namespace Arkira.Core.LeavePolicyAccounts
{
    public interface ILeavePolicyAccountHistoryRepository : IGenericRepository<LeavePolicyAccountHistory>
    {
        public void Add(LeavePolicyAccountHistory leavePolicyAccountHistory);
    }
}