﻿using Arkira.Core.Entities;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using System;

namespace Arkira.Core.LeavePolicyAccountsHistory
{
    public class LeavePolicyAccountHistory : Entity<Guid>, IHasCreationDate
    {
        public Guid EmployeeId { get; set; }
        public LeavePolicyAccountAction ActionTaken { get; set; }
        public int? Amount { get; set; }
        public DateTime ActionDate { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid LeavePolicyId { get; set; }
        public LeavePolicy LeavePolicy { get; set; }

        private LeavePolicyAccountHistory()
        {
        }

        public LeavePolicyAccountHistory(Guid employeeId, LeavePolicyAccountAction actionTaken, int? amount,
                                         DateTime actionDate, Guid leavePolicyId)
        {
            EmployeeId = employeeId;
            ActionTaken = actionTaken;
            Amount = amount;
            ActionDate = actionDate;
            LeavePolicyId = leavePolicyId;
            DateCreated = DateTime.Now;
        }

        public static readonly string EntityName = "Leave Policy Account Log";

        public override string ToString() => EntityName;
    }
}