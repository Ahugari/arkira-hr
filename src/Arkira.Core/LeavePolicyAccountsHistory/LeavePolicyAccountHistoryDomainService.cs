﻿using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arkira.Core.LeavePolicyAccountsHistory
{
    public class LeavePolicyAccountHistoryDomainService
    {
        private readonly ILeavePolicyAccountHistoryRepository _leavePolicyAccountHistoryRepository;
        private readonly ILeavePolicyRepository _leavePolicyRepository;
        private readonly ILeaveTypeDomainService _leaveTypeDomainService;
        private readonly ILeavePolicyDomainService _leavePolicyDomainService;
        private readonly ISystemClock _systemClock;

        public LeavePolicyAccountHistoryDomainService(
            ILeavePolicyAccountHistoryRepository leavePolicyAccountHistoryRepository,
            ILeavePolicyRepository leavePolicyRepository, ILeaveTypeDomainService leaveTypeDomainService,
            ILeavePolicyDomainService leavePolicyDomainService, ISystemClock systemClock = null)
        {
            _leavePolicyAccountHistoryRepository = leavePolicyAccountHistoryRepository;
            _leavePolicyRepository = leavePolicyRepository;
            _leaveTypeDomainService = leaveTypeDomainService;
            _leavePolicyDomainService = leavePolicyDomainService;
            _systemClock = systemClock ?? new SystemClock();
        }

        public LeavePolicyAccountHistory Create(Guid employeeId, LeavePolicyAccountAction actionTaken, int amount,
                                         DateTime actionDate, Guid leavePolicyId)
        {
            var leavePolicyAccountHistory = new LeavePolicyAccountHistory(employeeId,
                                                 actionTaken,
                                                 amount,
                                                 actionDate.Date,
                                                 leavePolicyId);
            _leavePolicyAccountHistoryRepository.Add(leavePolicyAccountHistory);

            return leavePolicyAccountHistory;
        }

        public async Task<int> GetCurrentBalanceAsync(Employee employee, LeavePolicy leavePolicy)
        {
            IOrderedEnumerable<LeavePolicyAccountHistory> policyHistory = await GetLeavePolicyHistoryAsync(employee, leavePolicy);
            return await CalculateLeavePolicyAccountBalanceAsync(policyHistory);
        }

        public async Task<int> GetCurrentBalanceAsAtAsync(Employee employee, LeavePolicy leavePolicy, DateTime asOfDate)
        {
            IOrderedEnumerable<LeavePolicyAccountHistory> policyHistory = await GetLeavePolicyHistoryAsAt(employee, leavePolicy, asOfDate);
            return await CalculateLeavePolicyAccountBalanceAsync(policyHistory);
        }

        public async Task<int> GetBalanceAsAt(Employee employee, LeavePolicy leavePolicy, DateTime asOfDate)
        {
            var policyHistory = await GetLeavePolicyHistoryAsAt(employee, leavePolicy, asOfDate);

            return await CalculateLeavePolicyAccountBalanceAsync(policyHistory);
        }

        public async Task<IOrderedEnumerable<LeavePolicyAccountHistory>> GetLeavePolicyHistoryAsAt(Employee employee, LeavePolicy leavePolicy, DateTime asOfDate)
        {
            var policyHistory = (await GetLeavePolicyHistoryAsync(employee, leavePolicy)).ToList();
            var leavePolicyInDb = await _leavePolicyRepository.FindAsync(leavePolicy.Id);
            var entitlementResetDate = await _leaveTypeDomainService.GetEntitlementResetDate(leavePolicy.LeaveTypeId, employee);
            var entitlementStartDate = policyHistory.LastOrDefault()?.ActionDate;
            var entitlementResetsOccured = policyHistory.AsEnumerable()
                                                       .Select(x => x.Amount != null
                                                                    && x.ActionTaken is LeavePolicyAccountAction.EntitlementReset)
                                                       .Count();

            if (!entitlementResetDate.HasValue || !entitlementStartDate.HasValue)
                return policyHistory.OrderByDescending(x => x.ActionDate);

            int allEntitlementResetOccurances = GetNumberOfOccurancesOfResetDate(entitlementResetDate.Value, entitlementStartDate.Value, asOfDate);

            var futureEntitlementResets = allEntitlementResetOccurances - entitlementResetsOccured;

            for (int i = 0; i < futureEntitlementResets; i++)
            {
                var entitlementCap = await _leavePolicyDomainService.GetEntitlementCapAsync(leavePolicy);
                policyHistory.Add(Create(employee.Id,
                                             LeavePolicyAccountAction.EntitlementReset,
                                             entitlementCap,
                                             entitlementResetDate.Value.Date,
                                             leavePolicyInDb.Id));
            }

            return policyHistory.OrderByDescending(x => x.ActionDate);
        }

        public async Task<IOrderedEnumerable<LeavePolicyAccountHistory>> GetLeavePolicyHistoryAsync(Employee employee,
            LeavePolicy leavePolicy, bool orderByAsc = true)
        {
            var result = await _leavePolicyAccountHistoryRepository.GetManyAsync(x => x.EmployeeId == employee.Id && x.LeavePolicyId == leavePolicy.Id);

            return orderByAsc ? result.OrderBy(x => x.ActionDate) : result.OrderByDescending(x => x.ActionDate);
        }

        private static int GetNumberOfOccurancesOfResetDate(DateTime resetDate, DateTime startDate, DateTime endDate)
        {
            var numberOfYearsBetweenPeriod = endDate.Year - startDate.Year;
            var invalidYears = 0;

            if (resetDate.Month < startDate.Month)
                invalidYears++;
            if (resetDate.Month > endDate.Month)
                invalidYears++;
            if (resetDate.Month == startDate.Month && resetDate.Day < startDate.Day)
                invalidYears++;
            if (resetDate.Month == endDate.Month && resetDate.Day > endDate.Day)
                invalidYears++;

            numberOfYearsBetweenPeriod -= invalidYears;

            return numberOfYearsBetweenPeriod;
        }

        private async Task<int> CalculateLeavePolicyAccountBalanceAsync(IOrderedEnumerable<LeavePolicyAccountHistory> policyHistory)
        {
            var currentBalance = 0;
            var policyHistoryAsOfCurrentDate = policyHistory.Where(x => x.ActionDate.Date <= _systemClock.UtcNow.Date)
                .OrderByDescending(x => x.ActionDate)
                .ToList();

            foreach (var item in policyHistoryAsOfCurrentDate)
            {
                if (await ActionDateIsLaterThanLeaveTypeYearStartDateAsync(item.LeavePolicy, item.EmployeeId, item.ActionDate))
                {
                    if (item.ActionTaken is LeavePolicyAccountAction.Add
                        or LeavePolicyAccountAction.EntitlementReset
                        or LeavePolicyAccountAction.Accrued
                        or LeavePolicyAccountAction.Enrolled)
                        currentBalance += item.Amount.Value;

                    if (item.ActionTaken is LeavePolicyAccountAction.Deduct or LeavePolicyAccountAction.Withdraw)
                        currentBalance -= item.Amount.Value;

                    if (item.ActionTaken is LeavePolicyAccountAction.CarriedOver)
                        currentBalance = await AddCarriedOverLeaveDaysAsync(policyHistory: item, currentBalance);
                }
            }

            return currentBalance;
        }

        private async Task<bool> ActionDateIsLaterThanLeaveTypeYearStartDateAsync(LeavePolicy leavePolicy, Guid employeeId, DateTime actionDate)
        {
            var startDate = await _leaveTypeDomainService.GetLeaveTypeYearStartDateAsync(leavePolicy.LeaveTypeId, employeeId) ?? await _leavePolicyDomainService.GetLeavePolicyYearStartDateAsync(leavePolicy.Id);
            return actionDate.Date >= startDate.Date;
        }

        private async Task<int> AddCarriedOverLeaveDaysAsync(LeavePolicyAccountHistory policyHistory, int currentBalance)
        {
            int monthsToExpiry = await _leaveTypeDomainService.GetMonthsForCarryOverExpiry(policyHistory.LeavePolicy.LeaveTypeId);
            if (monthsToExpiry! < 0 && !await CarriedOverDaysAreExpiredAsync(monthsToExpiry, policyHistory.LeavePolicy.LeaveTypeId, policyHistory.EmployeeId))
                currentBalance += policyHistory.Amount.Value;

            return currentBalance;
        }

        private async Task<bool> CarriedOverDaysAreExpiredAsync(int monthsToExpiry, Guid leaveTypeId, Guid employeeId)
        {
            var currentDate = _systemClock.UtcNow;
            DateTime startOfYear = await _leaveTypeDomainService.GetCarryOverLeaveYearStartDateAsync(leaveTypeId, employeeId);
            DateTime dateOfExpiry = startOfYear.AddMonths(monthsToExpiry);

            return dateOfExpiry > currentDate;
        }
    }
}