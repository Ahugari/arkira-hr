﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class EmployeeCanMakeRequestAfterNDaysFromHire : LeaveRestriction, ILeaveRestrictionService
    {
        public EmployeeCanMakeRequestAfterNDaysFromHire(int? dayLimit, LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy) : base(leaveRestrictionAction, leavePolicy)
        {
            if (dayLimit is null)
                throw new ArgumentNullException(nameof(dayLimit));

            LeaveRestrictionOption = LeaveRestrictionOption.EmployeeCanMakeRequestAfterNDaysFromHire;
            DaysLimit = dayLimit;
        }

        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository)
        {
            DaysLimit = restriction.DaysLimit;
            LeaveRestrictionAction = restriction.LeaveRestrictionAction;

            restrictionRepository.Update(this);

            return this;
        }

        public async Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService)
        {
            DateTime dateOfEligibility = (await applicationDomainService.GetApplicantAsync(application)).HireDate.AddDays(Convert.ToInt32(DaysLimit));
            if (applicationDomainService.GetDateCreated(application) < dateOfEligibility)
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Applicant is not eligible to apply for leave until {dateOfEligibility}.");

            return new LeaveRestrictionValidationResult().IsValid();
        }
    }
}