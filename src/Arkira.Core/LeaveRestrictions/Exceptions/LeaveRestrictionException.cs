﻿using System;

namespace Arkira.Core.LeaveRestrictions.Exceptions
{
    public class LeaveRestrictionException : Exception
    {
        public LeaveRestrictionException(string message) : base(message)
        {
        }
    }
}