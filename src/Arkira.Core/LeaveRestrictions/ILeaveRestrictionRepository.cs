﻿using Arkira.Core.Enums;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public interface ILeaveRestrictionRepository
    {
        public IUnitOfWork UnitOfWork { get; }

        public Task<LeaveRestriction> CreateAsync(LeaveRestriction newLeaveRestriction);

        public Task<LeaveRestriction> FindAsync(LeaveRestrictionOption leaveRestrictionOption, Guid leavePolicyId);

        public Task<IEnumerable<LeaveRestriction>> GetAllAsync(LeaveRestrictionOption leaveRestrictionOption, Guid leavePolicyId);

        public Task<int> GetCountAsync();

        public Task RemoveAsync(LeaveRestriction restriction);

        public LeaveRestriction Update(LeaveRestriction entity);
    }
}