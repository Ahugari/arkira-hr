using Arkira.Core.LeaveApplications;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public interface ILeaveRestrictionService
    {
        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository);

        public Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService);
    }
}