﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveCanBeRequestedNDaysInThePast : LeaveRestriction, ILeaveRestrictionService
    {
        public LeaveCanBeRequestedNDaysInThePast(int? daysLimit, LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy) : base(leaveRestrictionAction, leavePolicy)
        {
            if (daysLimit is null)
                throw new ArgumentNullException(nameof(daysLimit));

            LeaveRestrictionOption = LeaveRestrictionOption.LeaveCanBeRequestedNDaysInThePast;
            DaysLimit = daysLimit;
        }

        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository)
        {
            DaysLimit = restriction.DaysLimit;
            LeaveRestrictionAction = restriction.LeaveRestrictionAction;

            return restrictionRepository.Update(this);
        }

        public async Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService)
        {
            DateTime acceptableLeaveStartDate = DateTime.Now.AddDays(-Convert.ToInt32(DaysLimit)).Date;
            if (applicationDomainService.GetStartDate(application) < acceptableLeaveStartDate)
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Leave Start date should not be older than {acceptableLeaveStartDate}.");

            return new LeaveRestrictionValidationResult().IsValid();
        }
    }
}