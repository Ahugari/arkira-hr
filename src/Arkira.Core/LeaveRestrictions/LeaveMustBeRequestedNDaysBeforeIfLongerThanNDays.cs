﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays : LeaveRestriction, ILeaveRestrictionService
    {
        public LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(int? applicationLengthCheck, DatePeriodFormatOption? periodFormat, int? periodBeforeApplication, LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy) : base(leaveRestrictionAction, leavePolicy)
        {
            if (applicationLengthCheck is null)
                throw new ArgumentNullException(nameof(applicationLengthCheck));

            if (periodFormat is null)
                throw new ArgumentNullException(nameof(periodFormat));

            if (periodBeforeApplication is null)
                throw new ArgumentNullException(nameof(periodBeforeApplication));

            LeaveRestrictionOption = LeaveRestrictionOption.LeaveMustBeRequestedNDaysIfLongerThanNDays;
            ApplicationLengthConstraint = applicationLengthCheck;
            DatePeriodFormat = periodFormat;
            PeriodBeforeApplicationAcceptance = periodBeforeApplication;
        }

        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository)
        {
            ApplicationLengthConstraint = restriction.ApplicationLengthConstraint;
            DatePeriodFormat = restriction.DatePeriodFormat;
            PeriodBeforeApplicationAcceptance = restriction.PeriodBeforeApplicationAcceptance;
            LeaveRestrictionAction = restriction.LeaveRestrictionAction;

            return restrictionRepository.Update(this);
        }

        public async Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService)
        {
            if (DatePeriodFormat.Value is DatePeriodFormatOption.Days
                && (await applicationDomainService.GetLengthOfApplicationPeriod(application)) > ApplicationLengthConstraint
                && applicationDomainService.GetDateCreated(application) > DateTime.Now.AddDays(-PeriodBeforeApplicationAcceptance.Value))
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Leave application must be submitted {PeriodBeforeApplicationAcceptance.Value} day(s) before start date.");

            if (DatePeriodFormat.Value is DatePeriodFormatOption.Months
                && (await applicationDomainService.GetLengthOfApplicationPeriod(application)) > ApplicationLengthConstraint
                && applicationDomainService.GetDateCreated(application) > DateTime.Now.AddMonths(-PeriodBeforeApplicationAcceptance.Value))
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Leave application must be submitted {PeriodBeforeApplicationAcceptance.Value} month(s) before start date.");

            return new LeaveRestrictionValidationResult().IsValid();
        }
    }
}