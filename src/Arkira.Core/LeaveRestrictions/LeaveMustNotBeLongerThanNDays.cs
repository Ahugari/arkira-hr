﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveMustNotBeLongerThanNDays : LeaveRestriction, ILeaveRestrictionService
    {
        public LeaveMustNotBeLongerThanNDays(int daysLimit, LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy) : base(leaveRestrictionAction, leavePolicy)
        {
            LeaveRestrictionOption = LeaveRestrictionOption.LeaveMustNotBeLongerThanNDays;
            DaysLimit = daysLimit;
        }

        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository)
        {
            LeaveRestrictionAction = restriction.LeaveRestrictionAction;
            DaysLimit = restriction.DaysLimit;

            return restrictionRepository.Update(this);
        }

        public async Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService)
        {
            if (await applicationDomainService.GetLengthOfApplicationPeriod(application) > DaysLimit)
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Leave period exceeds the set limit({DaysLimit}).");

            return new LeaveRestrictionValidationResult().IsValid();
        }
    }
}