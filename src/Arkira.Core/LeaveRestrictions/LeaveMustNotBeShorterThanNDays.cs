﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveMustNotBeShorterThanNDays : LeaveRestriction, ILeaveRestrictionService
    {
        public LeaveMustNotBeShorterThanNDays(int? daysLimit, LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy) : base(leaveRestrictionAction, leavePolicy)
        {
            if (daysLimit is null)
                throw new ArgumentNullException(nameof(daysLimit));

            DaysLimit = daysLimit;
            LeaveRestrictionOption = LeaveRestrictionOption.LeaveMustNotBeLongerThanNDays;
        }

        public LeaveRestriction Update(LeaveRestriction restriction, ILeaveRestrictionRepository restrictionRepository)
        {
            DaysLimit = restriction.DaysLimit;
            LeaveRestrictionAction = restriction.LeaveRestrictionAction;

            return restrictionRepository.Update(this);
        }

        public async Task<LeaveRestrictionValidationResult> ValidateAsync(LeaveApplication application, ILeaveApplicationDomainService applicationDomainService)
        {
            if (await applicationDomainService.GetLengthOfApplicationPeriod(application) < DaysLimit)
                return new LeaveRestrictionValidationResult().Invalid(LeaveRestrictionAction, $"Minimum leave period allowed is {DaysLimit}.");

            return new LeaveRestrictionValidationResult().IsValid();
        }
    }
}