﻿using Arkira.Core.Entities;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using System;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveRestriction : IHasCreationDate
    {
        public LeaveRestrictionOption LeaveRestrictionOption { get; protected set; }
        public LeaveRestrictionAction LeaveRestrictionAction { get; protected set; }
        public int? DaysLimit { get; protected set; }
        public int? ApplicationLengthConstraint { get; protected set; }
        public int? PeriodBeforeApplicationAcceptance { get; protected set; }
        public DatePeriodFormatOption? DatePeriodFormat { get; protected set; }
        public Guid LeavePolicyId { get; protected set; }
        public LeavePolicy LeavePolicy { get; protected set; }

        public DateTime DateCreated { get; protected set; }

        private LeaveRestriction()
        {
        }

        protected LeaveRestriction(LeaveRestrictionAction leaveRestrictionAction, LeavePolicy leavePolicy)
        {
            LeaveRestrictionAction = leaveRestrictionAction;
            LeavePolicyId = leavePolicy.Id;
            DateCreated = DateTime.Now;
        }
    }
}