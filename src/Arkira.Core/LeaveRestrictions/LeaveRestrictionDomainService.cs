﻿using System;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveRestrictionDomainService
    {
        private readonly ILeaveRestrictionRepository _restrictionRepository;

        public LeaveRestrictionDomainService(ILeaveRestrictionRepository restrictionRepository)
        {
            _restrictionRepository = restrictionRepository;
        }

        public async Task<LeaveRestriction> UpdateAsync(LeaveRestriction updatedRestriction, LeaveRestriction oldRestriction)
        {
            if (updatedRestriction.LeaveRestrictionOption != oldRestriction.LeaveRestrictionOption)
                throw new InvalidOperationException("Restriction should match old restriction.");

            var restrictionInDb = await GetLeaveRestrictionAsync(oldRestriction);

            return (restrictionInDb as ILeaveRestrictionService).Update(updatedRestriction, _restrictionRepository);
        }

        private async Task<LeaveRestriction> GetLeaveRestrictionAsync(LeaveRestriction restriction)
        {
            var existingRestriction = await _restrictionRepository.FindAsync(restriction.LeaveRestrictionOption, restriction.LeavePolicyId);

            if (existingRestriction == null)
                throw new InvalidOperationException("Restriction doesn't exist.");

            return existingRestriction;
        }
    }
}