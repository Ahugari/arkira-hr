﻿using Arkira.Core.Enums;
using System;

namespace Arkira.Core.LeaveRestrictions
{
    public class LeaveRestrictionValidationResult
    {
        public bool SuccessResult { get; private set; }
        public string ValidationMessage { get; private set; }
        public LeaveRestrictionAction RestrictionValidationResult { get; private set; }

        public LeaveRestrictionValidationResult IsValid()
        {
            SuccessResult = true;
            return this;
        }

        public LeaveRestrictionValidationResult Invalid(LeaveRestrictionAction validationResult, string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message));

            ValidationMessage = message;
            RestrictionValidationResult = validationResult;

            return this;
        }
    }
}