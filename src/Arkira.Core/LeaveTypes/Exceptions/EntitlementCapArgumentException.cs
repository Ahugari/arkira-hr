﻿using System;

namespace Arkira.Core.LeaveTypes.Exceptions
{
    public class EntitlementCapArgumentException : Exception
    {
        public EntitlementCapArgumentException(string message) : base(message)
        {
        }
    }

    public class LeaveEntitlementResetScheduleDateArgumentException : Exception
    {
        public LeaveEntitlementResetScheduleDateArgumentException(string message) : base(message)
        {
        }
    }

    public class MaxCarryOverDaysArgumentException : Exception
    {
        public MaxCarryOverDaysArgumentException(string message) : base(message)
        {
        }
    }

    public class CarryOverNegativeEntitlementArgumentException : Exception
    {
        public CarryOverNegativeEntitlementArgumentException(string message) : base(message)
        {
        }
    }

    public class MaxNegativeCarryOverDaysException : Exception
    {
        public MaxNegativeCarryOverDaysException(string message) : base(message)
        {
        }
    }

    public class MaxMonthsPeriodInFutureArgumentException : Exception
    {
        public MaxMonthsPeriodInFutureArgumentException(string message) : base(message)
        {
        }
    }

    public class NewYearActiveWindowArgumentException : Exception
    {
        public NewYearActiveWindowArgumentException(string message) : base(message)
        {
        }
    }

    public class ExpireCarriedOverDaysArgumentException : Exception
    {
        public ExpireCarriedOverDaysArgumentException(string message) : base(message)
        {
        }
    }

    public class MonthsForCarryOverExpiryArgumentException : Exception
    {
        public MonthsForCarryOverExpiryArgumentException(string message) : base(message)
        {
        }
    }

    public class CarryOverLeaveYearStartDateArgumentException : Exception
    {
        public CarryOverLeaveYearStartDateArgumentException(string message) : base(message)
        {
        }
    }

    public class InvalidNegativeEntitlementBelowZeroArgumentException : Exception
    {
        public InvalidNegativeEntitlementBelowZeroArgumentException(string message) : base(message)
        {
        }
    }

    public class CarryOverLeaveYearStartsArgumentException : Exception
    {
        public CarryOverLeaveYearStartsArgumentException(string message) : base(message)
        {
        }
    }

    public class NewYearDeductionOptionsArgumentException : Exception
    {
        public NewYearDeductionOptionsArgumentException(string message) : base(message)
        {
        }
    }
}