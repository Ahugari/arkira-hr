﻿using Arkira.Core.Employees;
using Arkira.Core.Enums;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveTypes
{
    public interface ILeaveTypeDomainService
    {
        Task<LeaveType> CreateAsync([NotNull] string name,
                                    [NotNull] EntitlementType entitlementType,
                                    int? entitlementCap,
                                    LeaveEntitlementReset leaveEntitlementReset,
                                    DateTime? leaveEntitlementResetScheduleDate,
                                    bool carryOverUnusedEntitlement,
                                    int? maxCarryOverDays,
                                    bool carryOverNegativeEntitlement,
                                    int? maxNegativeCarryOverDays,
                                    bool expireCarriedOverDays,
                                    int? monthsForCarryOverExpiry,
                                    CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
                                    DateTime? carryOverLeaveYearStartDate,
                                    [NotNull] LeaveRequestLengthFactor leaveRequestLengthFactor,
                                    bool includePublicHolidays,
                                    bool allowNegativeEntitlement,
                                    int? maxNegativeEntitlementBelowZero,
                                    [NotNull] LeaveRequestWindow leaveRequestWindow,
                                    int? maxMonthsPeriodInFuture,
                                    int? newYearActiveWindow,
                                    NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
                                    [NotNull] DateTime effectiveDate,
                                    Guid? correlationId,
                                    int version = 1,
                                    Guid? oldVersion = null);

        Task<DateTime?> GetEntitlementResetDate(Guid leaveTypeId, Employee employee);

        Task RemoveAsync(Guid id);

        Task<LeaveType> UpdateAsync([NotNull] Guid id,
                                    [NotNull] string name,
                                    [NotNull] EntitlementType entitlementType,
                                    int? entitlementCap,
                                    LeaveEntitlementReset leaveEntitlementReset,
                                    DateTime? leaveEntitlementResetScheduleDate,
                                    bool carryOverUnusedEntitlement,
                                    int? maxCarryOverDays,
                                    bool carryOverNegativeEntitlement,
                                    int? maxNegativeCarryOverDays,
                                    bool expireCarriedOverDays,
                                    int? monthsForCarryOverExpiry,
                                    CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
                                    DateTime? carryOverLeaveYearStartDate,
                                    [NotNull] LeaveRequestLengthFactor leaveRequestLengthFactor,
                                    bool includePublicHolidays,
                                    bool allowNegativeEntitlement,
                                    int? maxNegativeEntitlementBelowZero,
                                    [NotNull] LeaveRequestWindow leaveRequestWindow,
                                    int? maxMonthsPeriodInFuture,
                                    int? newYearActiveWindow,
                                    NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
                                    [NotNull] DateTime effectiveDate,
                                    Guid correlationId);

        Task<int> GetMonthsForCarryOverExpiry(Guid leaveTypeId);

        Task<DateTime> GetCarryOverLeaveYearStartDateAsync(Guid leaveTypeId, Guid employeeId);

        Task<DateTime?> GetLeaveTypeYearStartDateAsync(Guid leaveTypeId, Guid employeeId);
    }
}