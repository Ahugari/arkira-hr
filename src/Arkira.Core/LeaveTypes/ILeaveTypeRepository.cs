﻿using Arkira.Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveTypes
{
    public interface ILeaveTypeRepository : IGenericRepository<LeaveType>
    {
        Task<List<LeaveType>> GetAsync();

        void Add(LeaveType leaveType);

        Task RemoveAsync(LeaveType leaveType);
    }
}