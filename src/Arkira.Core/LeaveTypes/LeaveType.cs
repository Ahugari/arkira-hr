﻿using Arkira.Core.Attributes;
using Arkira.Core.Entities;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Arkira.Core.LeaveTypes
{
    /*
     * <summary>
     * The Effective Date will be used when the EntitlementReset is not set and Carryforward is not enabled.
     * </summary>
     */

    public class LeaveType : Entity<Guid>, IHasCorrelationId, IHasVersionId, IHasCreationDate
    {
        [NoSpecialCharactersOnly]
        public string Name { get; set; }

        public EntitlementType EntitlementType { get; set; }

        [StandardEntitlementCap]
        public int? EntitlementCap { get; set; }

        //TODO: provide friendly messages since we are dealing with validation messages.

        public LeaveEntitlementReset LeaveEntitlementReset { get; set; }

        [LeaveEntitlementResetSchedule]
        public DateTime? LeaveEntitlementResetScheduleDate { get; set; }

        public bool CarryOverUnusedEntitlement { get; set; }

        [MaxCarryOverDays]
        public int? MaxCarryOverDays { get; set; }

        [CarryOverNegativeEntitlement]
        public bool CarryOverNegativeEntitlement { get; set; }

        [MaxDaysForNegativeCarryOver]
        public int? MaxNegativeCarryOverDays { get; set; }

        [ExpireCarriedOverDays]
        public bool ExpireCarriedOverDays { get; set; }

        [MinMonthsForCarryOverExpiry]
        public int? MonthsForCarryOverExpiry { get; set; }

        [CheckCarryOverUnusedEntitlement]
        public CarryOverLeaveYearStarts? CarryOverLeaveYearStarts { get; set; }

        [CheckCarryOverLeaveYearStart]
        public DateTime? CarryOverLeaveYearStartDate { get; set; }

        public LeaveRequestLengthFactor LeaveRequestLengthFactor { get; set; }
        public bool IncludePublicHolidays { get; set; }

        public bool AllowNegativeEntitlement { get; set; }

        [CheckAllowNegativeEntitlement]
        public int? MaxNegativeEntitlementBelowZero { get; set; }

        public LeaveRequestWindow LeaveRequestWindow { get; set; }

        [CheckLeaveRequestWindow]
        public int? MaxMonthsPeriodInFuture { get; set; }

        [NewYearActiveWindow]
        public int? NewYearActiveWindow { get; set; }

        [LeaveRequestBalanceDeduction]
        public NewYearLeaveRequestDeductionOptions? NewYearLeaveRequestDeductionOptions { get; set; }

        public DateTime EffectiveDate { get; set; }

        public ICollection<LeavePolicy> LeavePolicies { get; set; }

        [ConcurrencyCheck]
        public int Version { get; internal set; }

        public Guid CorrelationId { get; internal set; }

        public DateTime DateCreated { get; private set; }

        private LeaveType()
        {
        }

        public LeaveType(
            [NotNull] string name,
            [NotNull] EntitlementType entitlementType,
            int? entitlementCap,
            LeaveEntitlementReset leaveEntitlementReset,
            DateTime? leaveEntitlementResetScheduleDate,
            bool carryOverUnusedEntitlement,
            int? maxCarryOverDays,
            bool carryOverNegativeEntitlement,
            int? maxNegativeCarryOverDays,
            bool expireCarriedOverDays,
            int? monthsForCarryOverExpiry,
            CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
            DateTime? carryOverLeaveYearStartDate,
            [NotNull] LeaveRequestLengthFactor leaveRequestLengthFactor,
            bool includePublicHolidays,
            bool allowNegativeEntitlement,
            int? maxNegativeEntitlementBelowZero,
            [NotNull] LeaveRequestWindow leaveRequestWindow,
            int? maxMonthsPeriodInFuture,
            int? newYearActiveWindow,
            NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
            [NotNull] DateTime effectiveDate,
            Guid correlationId,
            int version = 1) : base()
        {
            Name = name.Trim();
            EntitlementType = entitlementType;
            EntitlementCap = entitlementCap;
            LeaveEntitlementReset = leaveEntitlementReset;
            LeaveEntitlementResetScheduleDate = leaveEntitlementResetScheduleDate;
            CarryOverUnusedEntitlement = carryOverUnusedEntitlement;
            MaxCarryOverDays = maxCarryOverDays;
            CarryOverNegativeEntitlement = carryOverNegativeEntitlement;
            MaxNegativeCarryOverDays = maxNegativeCarryOverDays;
            ExpireCarriedOverDays = expireCarriedOverDays;
            MonthsForCarryOverExpiry = monthsForCarryOverExpiry;
            CarryOverLeaveYearStarts = carryOverLeaveYearStarts;
            CarryOverLeaveYearStartDate = carryOverLeaveYearStartDate;
            LeaveRequestLengthFactor = leaveRequestLengthFactor;
            IncludePublicHolidays = includePublicHolidays;
            AllowNegativeEntitlement = allowNegativeEntitlement;
            MaxNegativeEntitlementBelowZero = maxNegativeEntitlementBelowZero;
            LeaveRequestWindow = leaveRequestWindow;
            MaxMonthsPeriodInFuture = maxMonthsPeriodInFuture;
            NewYearActiveWindow = newYearActiveWindow;
            NewYearLeaveRequestDeductionOptions = newYearLeaveRequestDeductionOptions;
            EffectiveDate = effectiveDate;
            CorrelationId = correlationId;
            Version = version;
            LeavePolicies = new HashSet<LeavePolicy>();
            DateCreated = DateTime.Now;
        }

        public static readonly string EntityName = "Leave Type";

        public override string ToString() => EntityName;

        //TODO:while implementing update operations, updating entitlement reset should be done carefully since we have to remove scheduled resets for employees.
        //TODO:while implementing update operations,
    }
}