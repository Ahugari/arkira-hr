﻿using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.Helpers;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveTypes.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Arkira.Core.LeaveTypes
{
    public class LeaveTypeDomainService : ILeaveTypeDomainService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILeavePolicyRepository _leavePolicyRepository;
        private readonly ILeaveTypeRepository _leaveTypeRepository;

        public LeaveTypeDomainService(ILeaveTypeRepository leaveTypeRepository, ILeavePolicyRepository leavePolicyRepository, IEmployeeRepository employeeRepository)
        {
            _leaveTypeRepository = leaveTypeRepository;
            _leavePolicyRepository = leavePolicyRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<LeaveType> CreateAsync(
            [NotNull] string name,
            [NotNull] EntitlementType entitlementType,
            int? entitlementCap,
            LeaveEntitlementReset leaveEntitlementReset,
            DateTime? leaveEntitlementResetScheduleDate,
            bool carryOverUnusedEntitlement,
            int? maxCarryOverDays,
            bool carryOverNegativeEntitlement,
            int? maxNegativeCarryOverDays,
            bool expireCarriedOverDays,
            int? monthsForCarryOverExpiry,
            CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
            DateTime? carryOverLeaveYearStartDate,
            [NotNull] LeaveRequestLengthFactor leaveRequestLengthFactor,
            bool includePublicHolidays,
            bool allowNegativeEntitlement,
            int? maxNegativeEntitlementBelowZero,
            [NotNull] LeaveRequestWindow leaveRequestWindow,
            int? maxMonthsPeriodInFuture,
            int? newYearActiveWindow,
            NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
            [NotNull] DateTime effectiveDate,
            Guid? correlationId,
            int version = 1,
            Guid? oldVersion = null
            )
        {
            await ValidateInput(name, entitlementType, entitlementCap, leaveEntitlementReset,
                          leaveEntitlementResetScheduleDate, carryOverUnusedEntitlement, maxCarryOverDays,
                          carryOverNegativeEntitlement, maxNegativeCarryOverDays, expireCarriedOverDays,
                          monthsForCarryOverExpiry, carryOverLeaveYearStarts, carryOverLeaveYearStartDate,
                          allowNegativeEntitlement, maxNegativeEntitlementBelowZero, leaveRequestWindow,
                          maxMonthsPeriodInFuture, newYearActiveWindow, newYearLeaveRequestDeductionOptions,
                          effectiveDate, oldVersion);

            var leaveType = new LeaveType(name, entitlementType, entitlementCap, leaveEntitlementReset,
                                          leaveEntitlementResetScheduleDate, carryOverUnusedEntitlement,
                                          maxCarryOverDays, carryOverNegativeEntitlement, maxNegativeCarryOverDays,
                                          expireCarriedOverDays, monthsForCarryOverExpiry, carryOverLeaveYearStarts,
                                          carryOverLeaveYearStartDate, leaveRequestLengthFactor, includePublicHolidays,
                                          allowNegativeEntitlement, maxNegativeEntitlementBelowZero, leaveRequestWindow,
                                          maxMonthsPeriodInFuture, newYearActiveWindow,
                                          newYearLeaveRequestDeductionOptions, effectiveDate, correlationId ?? Guid.NewGuid());

            leaveType.Version = version;

            //publish event

            _leaveTypeRepository.Add(leaveType);

            return leaveType;
        }

        public async Task<DateTime> GetCarryOverLeaveYearStartDateAsync(Guid leaveTypeId, Guid employeeId)
        {
            LeaveType existingLeaveType = await GetExistingLeaveType(leaveTypeId);
            if (existingLeaveType.CarryOverLeaveYearStarts is CarryOverLeaveYearStarts.GivenDayAndMonth)
                return existingLeaveType.CarryOverLeaveYearStartDate.Value;

            var existingEmployee = await _employeeRepository.FindAsync(employeeId);
            if (existingEmployee == null)
                throw new InvalidOperationException("Employee doesn't exist");

            return existingEmployee.HireDate;
        }

        public async Task<DateTime?> GetLeaveTypeYearStartDateAsync(Guid leaveTypeId, Guid employeeId)
        {
            LeaveType existingLeaveType = await GetExistingLeaveType(leaveTypeId);
            if (existingLeaveType.CarryOverLeaveYearStarts is CarryOverLeaveYearStarts.GivenDayAndMonth)
                return existingLeaveType.CarryOverLeaveYearStartDate.Value.Date;

            if (existingLeaveType.CarryOverLeaveYearStarts is CarryOverLeaveYearStarts.AnniversaryOfHireDate
                || (existingLeaveType.CarryOverUnusedEntitlement
                    && existingLeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnWorkAnniversary))
            {
                var existingEmployee = await _employeeRepository.FindAsync(employeeId);
                if (existingEmployee == null)
                    throw new InvalidOperationException("Employee doesn't exist");

                return existingEmployee.HireDate.Date;
            }

            if (!existingLeaveType.CarryOverUnusedEntitlement && existingLeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnSchedule)
                return existingLeaveType.LeaveEntitlementResetScheduleDate.Value.Date;

            return null;
        }

        public async Task<DateTime?> GetEntitlementResetDate(Guid leaveTypeId, Employee employee)
        {
            LeaveType existingLeaveType = await GetExistingLeaveType(leaveTypeId);

            if (existingLeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnWorkAnniversary)
                return (await _employeeRepository.FindAsync(employee.Id)).HireDate;

            if (existingLeaveType.LeaveEntitlementReset is LeaveEntitlementReset.ResetOnSchedule)
                return existingLeaveType.LeaveEntitlementResetScheduleDate;

            return null;
        }

        public async Task<int> GetMonthsForCarryOverExpiry(Guid leaveTypeId)
        {
            LeaveType existingLeaveType = await GetExistingLeaveType(leaveTypeId);

            if (!existingLeaveType.ExpireCarriedOverDays)
                return -1;

            return existingLeaveType.MonthsForCarryOverExpiry.Value;
        }

        public async Task RemoveAsync(Guid id)
        {
            var exsitingLeaveType = await _leaveTypeRepository.FindAsync(id);
            if (exsitingLeaveType == null)
                throw new InvalidOperationException("Leave Type doesn't exist or was already deleted.");

            var leavePoliciesAttached = await _leavePolicyRepository.FindAsync(x => x.LeaveTypeId == id || x.EntitlementFromTypeId == id);
            if (leavePoliciesAttached != null)
                throw new InvalidOperationException("Leave Type is currently being used in one or more Leave Policies.");

            _leaveTypeRepository.RemoveAsync(exsitingLeaveType);
        }

        public async Task<LeaveType> UpdateAsync(
            [NotNull] Guid id,
            [NotNull] string name,
            [NotNull] EntitlementType entitlementType,
            int? entitlementCap,
            LeaveEntitlementReset leaveEntitlementReset,
            DateTime? leaveEntitlementResetScheduleDate,
            bool carryOverUnusedEntitlement,
            int? maxCarryOverDays,
            bool carryOverNegativeEntitlement,
            int? maxNegativeCarryOverDays,
            bool expireCarriedOverDays,
            int? monthsForCarryOverExpiry,
            CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
            DateTime? carryOverLeaveYearStartDate,
            [NotNull] LeaveRequestLengthFactor leaveRequestLengthFactor,
            bool includePublicHolidays,
            bool allowNegativeEntitlement,
            int? maxNegativeEntitlementBelowZero,
            [NotNull] LeaveRequestWindow leaveRequestWindow,
            int? maxMonthsPeriodInFuture,
            int? newYearActiveWindow,
            NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
            [NotNull] DateTime effectiveDate,
            Guid correlationId)
        {
            //TODO:Should handle cases where the update will update the references to this leave type.
            // Can publish event after leave type update that will inform concerned observers that they should update leave type references. (i.e. leave policy observer).
            // This means that updateasync should have a parameter where the user specifies if all existing leave policies should be updated or handle only future references.
            LeaveType existingLeaveType = await GetExistingLeaveType(id, "Cannot update non existend Leave Type.");

            return await CreateAsync(name, entitlementType, entitlementCap, leaveEntitlementReset,
                leaveEntitlementResetScheduleDate, carryOverUnusedEntitlement, maxCarryOverDays,
                carryOverNegativeEntitlement, maxNegativeCarryOverDays, expireCarriedOverDays, monthsForCarryOverExpiry,
                carryOverLeaveYearStarts, carryOverLeaveYearStartDate, leaveRequestLengthFactor, includePublicHolidays,
                allowNegativeEntitlement, maxNegativeEntitlementBelowZero, leaveRequestWindow, maxMonthsPeriodInFuture,
                newYearActiveWindow, newYearLeaveRequestDeductionOptions, effectiveDate, correlationId,
                existingLeaveType.Version + 1, existingLeaveType.CorrelationId);
        }

        private static bool IsValidName(string name)
        {
            var nameCharArray = name.ToCharArray();
            return !string.IsNullOrWhiteSpace(name) && !nameCharArray.Any(x => ArkiraConsts.specialCharacters.Contains(x));
        }

        private async Task EnsureNonExistentName(string name, Guid? id)
        {
            var existingLeaveType = await _leaveTypeRepository.FindAsync(x => x.Name.ToLower().Trim() == name.ToLower().Trim() && x.CorrelationId != id);

            if (existingLeaveType != null)
                throw new InvalidOperationException($"The Leave Type '{name}' is already used.");
        }

        private async Task<LeaveType> GetExistingLeaveType(Guid leaveTypeId, string exceptionMessage = "Leave type doesn't exist.")
        {
            var existingLeaveType = await _leaveTypeRepository.FindAsync(leaveTypeId);
            if (existingLeaveType == null)
                throw new InvalidOperationException(exceptionMessage);
            return existingLeaveType;
        }

        private void ValidateCarryOverLeaveYearStartDate(CarryOverLeaveYearStarts? carryOverLeaveYearStarts, DateTime? carryOverLeaveYearStartDate)
        {
            if (carryOverLeaveYearStarts != CarryOverLeaveYearStarts.GivenDayAndMonth && carryOverLeaveYearStartDate.HasValue)
                throw new CarryOverLeaveYearStartDateArgumentException($"Enable {nameof(carryOverLeaveYearStarts)} before setting {nameof(carryOverLeaveYearStartDate)}.");

            if (carryOverLeaveYearStarts == CarryOverLeaveYearStarts.GivenDayAndMonth && carryOverLeaveYearStartDate == null)
                throw new CarryOverLeaveYearStartDateArgumentException($"{nameof(carryOverLeaveYearStartDate)} cannot be null or empty with selected {nameof(carryOverLeaveYearStarts)} option.");
        }

        private void ValidateCarryOverLeaveYearStarts(bool carryOverUnusedEntitlement, CarryOverLeaveYearStarts? carryOverLeaveYearStarts)
        {
            if (!carryOverUnusedEntitlement && carryOverLeaveYearStarts.HasValue)
                throw new CarryOverLeaveYearStartsArgumentException($"{nameof(carryOverUnusedEntitlement)} cannot be disabled with selected {nameof(carryOverLeaveYearStarts)} option.");

            if (carryOverUnusedEntitlement && carryOverLeaveYearStarts == null)
                throw new CarryOverLeaveYearStartsArgumentException($"{nameof(carryOverLeaveYearStarts)} cannot be null or empty while {nameof(carryOverUnusedEntitlement)} is enabled.");
        }

        private void ValidateCarryOverNegativeEntitlement(bool allowNegativeEntitlement, bool carryOverNegativeEntitlement)
        {
            if (!allowNegativeEntitlement && carryOverNegativeEntitlement)
                throw new CarryOverNegativeEntitlementArgumentException($"Enable {nameof(allowNegativeEntitlement)} before enabling {nameof(carryOverNegativeEntitlement)}.");
        }

        private void ValidateEntitlementCap(EntitlementType entitlementType, int? entitlementCap)
        {
            if (entitlementType == EntitlementType.Standard && entitlementCap == null)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be null or empty with the selected {nameof(entitlementType)} option.");

            if (entitlementType == EntitlementType.Standard && entitlementCap < ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be less than {ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED} days.");

            if (entitlementType == EntitlementType.Standard && entitlementCap > ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED)
                throw new EntitlementCapArgumentException($"{nameof(entitlementCap)} cannot be more than {ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED} days.");
        }

        private void ValidateEntitlementType(EntitlementType entitlementType, int? entitlementCap, LeaveEntitlementReset leaveEntitlementReset, bool carryOverUnusedEntitlement, bool allowNegativeEntitlement, LeaveRequestWindow leaveRequestWindow)
        {
            //TODO:Tests
            if (entitlementType == EntitlementType.Unlimited
                && entitlementCap.HasValue)
                throw new InvalidOperationException($"Cannot set Entitlement Cap with the selected Entitlement Type: {entitlementType}.");

            if (entitlementType == EntitlementType.Unlimited
                && carryOverUnusedEntitlement)
                throw new InvalidOperationException($"Cannot enable Carry Over Unsed Entitlement with the selected Entitlement Type: {entitlementType}.");

            if (entitlementType == EntitlementType.Unlimited
                && allowNegativeEntitlement)
                throw new InvalidOperationException($"Cannot enable Allow Negative Entitlement with the selected Entitlement Type: {entitlementType}.");

            if (entitlementType == EntitlementType.Unlimited
                && leaveEntitlementReset != LeaveEntitlementReset.ResetNever)
                throw new InvalidOperationException($"Cannot set selected Leave Entitlement Reset option with the selected Entitlement Type: {entitlementType}.");
        }

        private void ValidateExpireCarryOverDays(bool carryOverNegativeEntitlement, bool expireCarriedOverDays)
        {
            if (!carryOverNegativeEntitlement && expireCarriedOverDays)
                throw new ExpireCarriedOverDaysArgumentException($"Enable {nameof(carryOverNegativeEntitlement)} before enabling {nameof(expireCarriedOverDays)}.");
        }

        private async Task ValidateInput(
            string name,
            EntitlementType entitlementType,
            int? entitlementCap,
            LeaveEntitlementReset leaveEntitlementReset,
            DateTime? leaveEntitlementResetScheduleDate,
            bool carryOverUnusedEntitlement,
            int? maxCarryOverDays,
            bool carryOverNegativeEntitlement,
            int? maxNegativeCarryOverDays,
            bool expireCarriedOverDays,
            int? monthsForCarryOverExpiry,
            CarryOverLeaveYearStarts? carryOverLeaveYearStarts,
            DateTime? carryOverLeaveYearStartDate,
            bool allowNegativeEntitlement,
            int? maxNegativeEntitlementBelowZero,
            LeaveRequestWindow leaveRequestWindow,
            int? maxMonthsPeriodInFuture,
            int? newYearActiveWindow,
            NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions,
            DateTime effectiveDate,
            Guid? oldVersion = null)
        {
            ValidateName(name);
            await EnsureNonExistentName(name, oldVersion);

            _ = Check.IsValidDate(effectiveDate, ArkiraConsts.FUTURE_EFFECTIVE_DATE_YEARS_LIMIT);

            ValidateEntitlementCap(entitlementType, entitlementCap);
            ValidateEntitlementType(entitlementType, entitlementCap, leaveEntitlementReset, carryOverUnusedEntitlement, allowNegativeEntitlement, leaveRequestWindow);
            ValidateLeaveEntitlementReset(leaveEntitlementReset, entitlementType, leaveRequestWindow);
            ValidateLeaveEntitlementResetScheduleDate(leaveEntitlementReset, leaveEntitlementResetScheduleDate);
            ValidateMaxCarryOverDays(carryOverUnusedEntitlement, maxCarryOverDays);
            ValidateCarryOverNegativeEntitlement(allowNegativeEntitlement, carryOverNegativeEntitlement);
            ValidateMaxNegativeCarryOverDays(carryOverNegativeEntitlement, maxNegativeCarryOverDays);
            ValidateExpireCarryOverDays(carryOverNegativeEntitlement, expireCarriedOverDays);
            ValidateMonthsForCarryOverExpiry(expireCarriedOverDays, monthsForCarryOverExpiry);
            ValidateCarryOverLeaveYearStarts(carryOverUnusedEntitlement, carryOverLeaveYearStarts);
            ValidateCarryOverLeaveYearStartDate(carryOverLeaveYearStarts, carryOverLeaveYearStartDate);
            ValidateMaxNegativeEntitlementBelowZero(allowNegativeEntitlement, maxNegativeEntitlementBelowZero);
            ValidateMaxMonthsPeriodInFuture(leaveRequestWindow, maxMonthsPeriodInFuture);
            ValidateNewYearActiveWindow(leaveRequestWindow, newYearActiveWindow, entitlementType);
            ValidateNewYearRequestDeductionOptions(leaveRequestWindow, newYearLeaveRequestDeductionOptions, entitlementType, allowNegativeEntitlement, carryOverNegativeEntitlement);
        }

        private void ValidateLeaveEntitlementReset(LeaveEntitlementReset leaveEntitlementReset, EntitlementType entitlementType, LeaveRequestWindow leaveRequestWindow)
        {
            if (entitlementType is EntitlementType.Unlimited && leaveRequestWindow is LeaveRequestWindow.SomePeriodInFuture && leaveEntitlementReset != LeaveEntitlementReset.ResetNever)
                throw new InvalidOperationException("Cannot set selected Entitlement Reset option with the selected Entitlement Type and Request Window options.");
        }

        private void ValidateLeaveEntitlementResetScheduleDate(LeaveEntitlementReset leaveEntitlementReset, DateTime? leaveEntitlementResetScheduleDate)
        {
            if (leaveEntitlementReset != LeaveEntitlementReset.ResetOnSchedule && leaveEntitlementResetScheduleDate.HasValue)
                throw new LeaveEntitlementResetScheduleDateArgumentException($"Cannot set {nameof(leaveEntitlementResetScheduleDate)} with the selected {nameof(leaveEntitlementReset)} option.");

            if (leaveEntitlementReset == LeaveEntitlementReset.ResetOnSchedule && leaveEntitlementResetScheduleDate == null)
                throw new LeaveEntitlementResetScheduleDateArgumentException($"{nameof(leaveEntitlementResetScheduleDate)} cannot be null or empty with the selected {nameof(leaveEntitlementReset)} option.");
        }

        private void ValidateMaxCarryOverDays(bool carryOverUnusedEntitlement, int? maxCarryOverDays)
        {
            if (!carryOverUnusedEntitlement && maxCarryOverDays.HasValue)
                throw new MaxCarryOverDaysArgumentException($"Cannot set {nameof(maxCarryOverDays)} while {nameof(carryOverUnusedEntitlement)} is disabled.");

            if (carryOverUnusedEntitlement && maxCarryOverDays == null)
                throw new MaxCarryOverDaysArgumentException($"{nameof(maxCarryOverDays)} cannot be null or empty while {nameof(carryOverUnusedEntitlement)} is enabled.");

            if (carryOverUnusedEntitlement && maxCarryOverDays < ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED)
                throw new MaxCarryOverDaysArgumentException($"{nameof(maxCarryOverDays)} cannot be less than {ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED} days.");

            if (carryOverUnusedEntitlement && maxCarryOverDays > ArkiraConsts.MAX_CARRY_OVER_DAYS_ALLOWED)
                throw new MaxCarryOverDaysArgumentException($"{nameof(maxCarryOverDays)} cannot be more than {ArkiraConsts.MAX_CARRY_OVER_DAYS_ALLOWED} days.");
        }

        private void ValidateMaxMonthsPeriodInFuture(LeaveRequestWindow leaveRequestWindow, int? maxMonthsPeriodInFuture)
        {
            if (leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture && maxMonthsPeriodInFuture == null)
                throw new MaxMonthsPeriodInFutureArgumentException($"{nameof(maxMonthsPeriodInFuture)} cannot be null or empty with the selected {nameof(leaveRequestWindow)} option.");

            if (leaveRequestWindow != LeaveRequestWindow.SomePeriodInFuture && maxMonthsPeriodInFuture.HasValue)
                throw new MaxMonthsPeriodInFutureArgumentException($"Cannot set {nameof(maxMonthsPeriodInFuture)} with the selected {nameof(leaveRequestWindow)} option.");

            if (leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture && maxMonthsPeriodInFuture < ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE)
                throw new MaxMonthsPeriodInFutureArgumentException($"{nameof(maxMonthsPeriodInFuture)} cannot be less than {nameof(ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE)} months.");

            if (leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture && maxMonthsPeriodInFuture > ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE)
                throw new MaxMonthsPeriodInFutureArgumentException($"{nameof(maxMonthsPeriodInFuture)} cannot be more than {nameof(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE)} months.");
        }

        private void ValidateMaxNegativeCarryOverDays(bool carryOverNegativeEntitlement, int? maxNegativeCarryOverDays)
        {
            if (!carryOverNegativeEntitlement && maxNegativeCarryOverDays.HasValue)
                throw new MaxNegativeCarryOverDaysException($"Enable {nameof(carryOverNegativeEntitlement)} before setting maximum negative Carry Over days.");

            if (carryOverNegativeEntitlement && maxNegativeCarryOverDays == null)
                throw new MaxNegativeCarryOverDaysException($"{nameof(maxNegativeCarryOverDays)} cannot be null or empty while {nameof(carryOverNegativeEntitlement)} is enabled.");

            if (carryOverNegativeEntitlement && maxNegativeCarryOverDays < ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED)
                throw new MaxNegativeCarryOverDaysException($"{nameof(maxNegativeCarryOverDays)} cannot be less than {ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED } days.");

            if (carryOverNegativeEntitlement && maxNegativeCarryOverDays > ArkiraConsts.MAX_NEGATIVE_CARRY_OVER_DAYS_ALLOWED)
                throw new MaxNegativeCarryOverDaysException($"{nameof(maxNegativeCarryOverDays)} cannot be more than {ArkiraConsts.MAX_NEGATIVE_CARRY_OVER_DAYS_ALLOWED } days.");
        }

        private void ValidateMaxNegativeEntitlementBelowZero(bool allowNegativeEntitlement, int? maxNegativeEntitlementBelowZero)
        {
            if (!allowNegativeEntitlement && maxNegativeEntitlementBelowZero.HasValue)
                throw new InvalidNegativeEntitlementBelowZeroArgumentException($"Enable {nameof(allowNegativeEntitlement)} before setting {nameof(maxNegativeEntitlementBelowZero)}.");

            if (allowNegativeEntitlement && maxNegativeEntitlementBelowZero == null)
                throw new InvalidNegativeEntitlementBelowZeroArgumentException($"{nameof(maxNegativeEntitlementBelowZero)} cannot be null or empty while {nameof(allowNegativeEntitlement)} is enabled.");

            if (allowNegativeEntitlement && maxNegativeEntitlementBelowZero < ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT)
                throw new InvalidNegativeEntitlementBelowZeroArgumentException($"{nameof(maxNegativeEntitlementBelowZero)} cannot be less than {ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT} day(s).");

            if (allowNegativeEntitlement && maxNegativeEntitlementBelowZero > ArkiraConsts.MAX_NEGATIVE_ENTITLEMENT)
                throw new InvalidNegativeEntitlementBelowZeroArgumentException($"{nameof(maxNegativeEntitlementBelowZero)} cannot be more than {ArkiraConsts.MAX_NEGATIVE_ENTITLEMENT} day(s).");
        }

        private void ValidateMonthsForCarryOverExpiry(bool expireCarriedOverDays, int? monthsForCarryOverExpiry)
        {
            if (!expireCarriedOverDays && monthsForCarryOverExpiry.HasValue)
                throw new MonthsForCarryOverExpiryArgumentException($"Enable {nameof(expireCarriedOverDays)} before setting months for Carry Over expiry.");

            if (expireCarriedOverDays && monthsForCarryOverExpiry == null)
                throw new MonthsForCarryOverExpiryArgumentException($"{nameof(monthsForCarryOverExpiry)} cannot be null or empty while {nameof(expireCarriedOverDays)} is enabled.");

            if (expireCarriedOverDays && monthsForCarryOverExpiry < ArkiraConsts.MIN_MONTHS_FOR_CARRY_OVER_EXPIRY)
                throw new MonthsForCarryOverExpiryArgumentException($"{nameof(monthsForCarryOverExpiry)} cannot be less than {ArkiraConsts.MIN_MONTHS_FOR_CARRY_OVER_EXPIRY}");

            if (expireCarriedOverDays && monthsForCarryOverExpiry > ArkiraConsts.MAX_MONTHS_FOR_CARRY_OVER_EXPIRY)
                throw new MonthsForCarryOverExpiryArgumentException($"{nameof(monthsForCarryOverExpiry)} cannot be more than {ArkiraConsts.MAX_MONTHS_FOR_CARRY_OVER_EXPIRY}");
        }

        private void ValidateName(string name)
        {
            if (!IsValidName(name))
                throw new ArgumentException($"'{nameof(name)}' cannot be null or whitespace.", nameof(name));
        }

        private void ValidateNewYearActiveWindow(LeaveRequestWindow leaveRequestWindow, int? newYearActiveWindow, EntitlementType entitlementType)
        {
            if (leaveRequestWindow != LeaveRequestWindow.CurrentAndFollowingYear
                && leaveRequestWindow != LeaveRequestWindow.SomePeriodInFuture
                && newYearActiveWindow.HasValue
                )
                throw new NewYearActiveWindowArgumentException($"Cannot set New Year activation window with the selected {nameof(leaveRequestWindow)} option.");

            if ((leaveRequestWindow == LeaveRequestWindow.CurrentAndFollowingYear ^ leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture)
                && entitlementType != EntitlementType.Unlimited
                && newYearActiveWindow == null
                )
                throw new NewYearActiveWindowArgumentException($"{nameof(newYearActiveWindow)} cannot be null or empty with the selected {nameof(leaveRequestWindow)} option.");

            if (leaveRequestWindow == LeaveRequestWindow.CurrentAndFollowingYear ^ leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture
                && newYearActiveWindow < ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION
                )
                throw new NewYearActiveWindowArgumentException($"{nameof(newYearActiveWindow)} cannot be less than {ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION} month(s).");

            if (leaveRequestWindow == LeaveRequestWindow.CurrentAndFollowingYear ^ leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture
                && newYearActiveWindow > ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION)
                throw new NewYearActiveWindowArgumentException($"{nameof(newYearActiveWindow)} cannot be more than {ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION} month(s).");
        }

        private void ValidateNewYearRequestDeductionOptions(LeaveRequestWindow leaveRequestWindow, NewYearLeaveRequestDeductionOptions? newYearLeaveRequestDeductionOptions, EntitlementType entitlementType, bool allowNegativeEntitlement, bool carryOverNegativeEntitlement)
        {
            if (leaveRequestWindow != LeaveRequestWindow.CurrentAndFollowingYear && leaveRequestWindow != LeaveRequestWindow.SomePeriodInFuture
                && newYearLeaveRequestDeductionOptions.HasValue && entitlementType != EntitlementType.Unlimited)
                throw new NewYearDeductionOptionsArgumentException($"Cannot set Leave Request Deduction options with the selected {nameof(leaveRequestWindow)} option.");

            if (leaveRequestWindow == LeaveRequestWindow.CurrentAndFollowingYear ^ leaveRequestWindow == LeaveRequestWindow.SomePeriodInFuture
                && newYearLeaveRequestDeductionOptions == null && entitlementType != EntitlementType.Unlimited)
                throw new NewYearDeductionOptionsArgumentException($"{nameof(newYearLeaveRequestDeductionOptions)} cannot be null or empty with the selected {nameof(leaveRequestWindow)} option.");

            if (leaveRequestWindow != LeaveRequestWindow.OnlyInCurrentYear
                && allowNegativeEntitlement
                && newYearLeaveRequestDeductionOptions != NewYearLeaveRequestDeductionOptions.ProportionalFromEachYear
                && !carryOverNegativeEntitlement)
                throw new NewYearActiveWindowArgumentException("Cannot set the selected Leave Request Deduction option while Negative Entitlement is enabled.");
        }
    }
}