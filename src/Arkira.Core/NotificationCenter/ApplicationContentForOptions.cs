﻿namespace Arkira.Core.NotificationCenter
{
    public static class ApplicationContentForOptions
    {
        public const string Reviewer = "reviewer";
        public const string Owner = "owner";
        public const string Watcher = "watcher";
    }
}