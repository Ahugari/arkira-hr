﻿using System;
using System.Collections.Generic;

namespace Arkira.Core.NotificationCenter
{
    public class ApplicationNotificationContentBuilder : NotificationContentBuilder
    {
        private const string Message = "Specify a valid type of receipient for the application notification type.";

        public ApplicationNotificationContentBuilder() : base()
        {
        }

        public override string GetNotificationContent()
        {
            try
            {
                return _content[NotificationContentTags.Action] switch
                {
                    ContentAction.Commented => GetCommentContent(),
                    ContentAction.Approved => GetApprovedContent(),
                    ContentAction.Denied => GetApprovedContent(),
                    ContentAction.Recommended => GetApprovedContent(),
                    ContentAction.Withdrawn => GetApprovedContent(),
                    ContentAction.Sent => GetSentContent(),
                    ContentAction.Reminder => GetReminderContent(),
                    _ => throw new ArgumentException("Unable to build notification content." +
                            "Make sure the action specified is valid."),
                };
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Unable to build notification content." +
                            "Make sure all required notification content tags are specified.", ex.Message);
            }
        }

        private string GetReminderContent()
        {
            return _content[NotificationContentTags.ApplicationNotificationFor].ToLower() switch
            {
                ApplicationContentForOptions.Reviewer => $"{GetFirstName(_content[NotificationContentTags.ActionPerformer])}, you still have a pending {_content[NotificationContentTags.ContentTypeDisplayName]} from {_content[NotificationContentTags.ContentOriginOwner]} to review.",
                _ => throw new ArgumentException(Message, _content[NotificationContentTags.ApplicationNotificationFor])
            };
        }

        private string GetFirstName(string name) => name.Split(" ")[0];

        private string GetSentContent()
        {
            return _content[NotificationContentTags.ApplicationNotificationFor].ToLower() switch
            {
                ApplicationContentForOptions.Owner => $"Your {_content[NotificationContentTags.ContentTypeDisplayName]} has been sent to {_content[NotificationContentTags.ActionPerformer]} for review.",
                ApplicationContentForOptions.Reviewer => $"A {_content[NotificationContentTags.ContentTypeDisplayName]} from {_content[NotificationContentTags.ContentOriginOwner]} has been received for your review.",
                ApplicationContentForOptions.Watcher => $"A {_content[NotificationContentTags.ContentTypeDisplayName]} from {_content[NotificationContentTags.ContentOriginOwner]} has been sent to {_content[NotificationContentTags.ActionPerformer]} for review.",
                _ => throw new ArgumentException(Message, _content[NotificationContentTags.ApplicationNotificationFor])
            };
        }

        private string GetApprovedContent()
        {
            return _content[NotificationContentTags.ApplicationNotificationFor].ToLower() switch
            {
                ApplicationContentForOptions.Owner => $"Your {_content[NotificationContentTags.ContentTypeDisplayName]} has been {_content[NotificationContentTags.Action]} by {_content[NotificationContentTags.ActionPerformer]}.",
                ApplicationContentForOptions.Watcher => $"A {_content[NotificationContentTags.ContentTypeDisplayName]} from {_content[NotificationContentTags.ContentOriginOwner]} has been {_content[NotificationContentTags.Action]} by {_content[NotificationContentTags.ActionPerformer]}.",
                _ => throw new ArgumentException(Message, _content[NotificationContentTags.ApplicationNotificationFor])
            };
        }

        private string GetCommentContent()
        {
            return _content[NotificationContentTags.ApplicationNotificationFor] switch
            {
                ApplicationContentForOptions.Owner => $"Your {_content[NotificationContentTags.ContentTypeDisplayName].ToLower()} has received a new comment!",
                ApplicationContentForOptions.Reviewer => $"A {_content[NotificationContentTags.ContentTypeDisplayName]} you reviewed has received a new comment!",
                _ => throw new ArgumentException(Message, _content[NotificationContentTags.ApplicationNotificationFor])
            };
        }
    }
}