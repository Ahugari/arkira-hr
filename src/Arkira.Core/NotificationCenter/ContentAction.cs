﻿namespace Arkira.Core.NotificationCenter
{
    public static class ContentAction
    {
        public const string Sent = "sent";
        public const string Denied = "denied";
        public const string Recommended = "recommended";
        public const string Approved = "approved";
        public const string Commented = "comment";
        public const string Withdrawn = "withdrawn";
        public const string Reminder = "reminder";
    }
}