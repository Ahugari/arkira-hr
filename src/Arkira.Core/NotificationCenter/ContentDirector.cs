﻿using System;

namespace Arkira.Core.NotificationCenter
{
    internal class ContentDirector
    {
        private readonly NotificationContentBuilder _notificationContentBuilder;

        public ContentDirector(NotificationContentBuilder notificationContentBuilder)
        {
            _notificationContentBuilder = notificationContentBuilder;
        }

        public NotificationContentBuilder CreateApplicationContentForApplicant(string action, string contentTypeDisplayName, string contentActionPerformer)
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, ApplicationContentForOptions.Owner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, GetValidAction(action));
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, contentActionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, contentTypeDisplayName);

            return _notificationContentBuilder;
        }                                                       

        public NotificationContentBuilder CreateApplicationContentForReviewer(string action, string contentTypeDisplayName, string contentOriginOwnerName)
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, ApplicationContentForOptions.Reviewer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, GetValidAction(action));
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, contentOriginOwnerName);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, contentTypeDisplayName);

            return _notificationContentBuilder;
        }

        public NotificationContentBuilder CreateApplicationContentForWatcher(string action, string contentTypeDisplayName, string contentOriginOwnerName, string contentActionPerformer)
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, ApplicationContentForOptions.Watcher);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, GetValidAction(action));
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, contentOriginOwnerName);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, contentTypeDisplayName);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, contentActionPerformer);

            return _notificationContentBuilder;
        }

        private string GetValidAction(string action)
        {
            string _action;

            if (ContentAction.Approved.Equals(action, StringComparison.OrdinalIgnoreCase))
                _action = ContentAction.Approved;
            else if (ContentAction.Commented.Equals(action, StringComparison.OrdinalIgnoreCase))
                _action = ContentAction.Commented;
            else if (ContentAction.Reminder.Equals(action, StringComparison.OrdinalIgnoreCase))
                _action = ContentAction.Reminder;
            else if (ContentAction.Sent.Equals(action, StringComparison.OrdinalIgnoreCase))
                _action = ContentAction.Sent;
            else if (ContentAction.Withdrawn.Equals(action, StringComparison.OrdinalIgnoreCase))
                _action = ContentAction.Withdrawn;
            else throw new ArgumentException("Invalid action provided.", action);

            return _action;
        }
    }
}