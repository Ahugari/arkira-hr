﻿using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Repositories;
using Arkira.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Core.NotificationCenter.Handlers
{
    //TODO: Improve the way multi-reviewer notifications are sent.
    public class AddApplicationNotifications : IAppNotificationHandler<ApplicationSentToReviewer<Application>>
    {
        private readonly IGenericCreateUpdateDeleteRepository<Notification> _notificationRepository;

        public AddApplicationNotifications(IGenericCreateUpdateDeleteRepository<Notification> notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }

        public async Task Handle(ApplicationSentToReviewer<Application> notification, CancellationToken cancellationToken)
        {
            ValidateReviewers(notification.Reviewers);
            await CreateLeaveApplicationNotifications(notification);
        }

        private void ValidateReviewers(HashSet<Reviewer> reviewers)
        {
            if (reviewers == null || !reviewers.Any())
                throw new ApplicationNotificationException("Error while processing application notifications.", new ArgumentException($"Reviewers cannot nut be empty: {reviewers}"));
        }

        private async Task CreateLeaveApplicationNotifications(ApplicationSentToReviewer<Application> notification)
        {
            var notificationContentBuilder = new ApplicationNotificationContentBuilder();
            var contentDirector = new ContentDirector(notificationContentBuilder);

            await CreateApplicantNotification(notification, notificationContentBuilder, contentDirector);

            await CreateReviewerNotification(notification, notificationContentBuilder, contentDirector);

            await CreateSubscriberNotifications(notification, notificationContentBuilder, contentDirector);
        }

        private async Task CreateApplicantNotification(ApplicationSentToReviewer<Application> notification, ApplicationNotificationContentBuilder notificationContentBuilder, ContentDirector contentDirector)
        {
            foreach (var reviewer in notification.Reviewers)
            {
                _ = contentDirector.CreateApplicationContentForApplicant(ContentAction.Sent, notification.Application.GetName(), reviewer.FullName);
                var newNotification = new Notification(notification.Application.ApplicantId.ToString(), notificationContentBuilder.GetNotificationContent(), notification.Application.GetName());

                await _notificationRepository.SaveAsync(newNotification);
            }
        }

        private async Task CreateReviewerNotification(ApplicationSentToReviewer<Application> notification, ApplicationNotificationContentBuilder notificationContentBuilder, ContentDirector contentDirector)
        {
            foreach (var reviewer in notification.Reviewers)
            {
                _ = contentDirector.CreateApplicationContentForReviewer(ContentAction.Sent, notification.Application.GetName(), notification.ApplicantName);
                var newNotification = new Notification(reviewer.Id, notificationContentBuilder.GetNotificationContent(), notification.Application.GetName());
                await _notificationRepository.SaveAsync(newNotification);
            }
        }

        private async Task CreateSubscriberNotifications(ApplicationSentToReviewer<Application> notification, ApplicationNotificationContentBuilder notificationContentBuilder, ContentDirector contentDirector)
        {
            if (!notification.ApplicationWatchers.Any())
                return;

            foreach (var reviewer in notification.Reviewers)
            {
                _ = contentDirector.CreateApplicationContentForWatcher(ContentAction.Sent, notification.Application.GetName(), notification.ApplicantName, reviewer.FullName);

                foreach (var watcherId in notification.ApplicationWatchers)
                {
                    var newNotification = new Notification(watcherId, notificationContentBuilder.GetNotificationContent(), notification.Application.GetName());

                    await _notificationRepository.SaveAsync(newNotification);
                }
            }
        }
    }
}