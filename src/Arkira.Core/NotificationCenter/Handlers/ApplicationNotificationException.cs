﻿using System;

namespace Arkira.Core.NotificationCenter.Handlers
{
    public class ApplicationNotificationException : Exception
    {
        public ApplicationNotificationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}