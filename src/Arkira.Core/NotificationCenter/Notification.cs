﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.NotificationCenter
{
    public class Notification : Entity<Guid>
    {
        public string TargetId { get; set; } = default!;
        public string ContentOrigin { get; set; } = default!;
        public string Content { get; set; } = default!;
        public bool IsRead { get; set; }

        private Notification()
        {
        }

        public Notification(string targetId, string content, string contentOrigin)
        {
            TargetId = targetId.Trim();
            Content = content.Trim();
            ContentOrigin = contentOrigin.Trim();
        }

        public void Read()
        {
            IsRead = true;
        }

        public static readonly string EntityName = "Notification";

        public override string ToString() => EntityName;
    }
}