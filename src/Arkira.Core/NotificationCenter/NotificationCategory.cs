﻿namespace Arkira.Core.NotificationCenter
{
    public static class NotificationCategory
    {
        public const string SystemReminder = "System Reminder";
        public const string SystemAlert = "System Alert";
        public const string ApplicationReminder = "Application Reminder";
        public const string ForApplicationOwner = "Application Owner";
        public const string ForApplicationWatcher = "Application Watcher";
        public const string ForApplicationReviewer = "Application Reviewer";
        public const string TaskCompleted = "Task Completed";

    }
}