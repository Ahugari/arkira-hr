﻿using System.Collections.Generic;

namespace Arkira.Core.NotificationCenter
{
    public abstract class NotificationContentBuilder
    {
        protected Dictionary<string, string> _content;

        protected NotificationContentBuilder()
        {
            Reset();
        }

        public void Reset()
        {
            _content = new Dictionary<string, string>();
        }

        public abstract string GetNotificationContent();

        public NotificationContentBuilder AddContentValue(string tagName, string value)
        {
            _content[tagName] = value;

            return this;
        }
    }
}