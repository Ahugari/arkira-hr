﻿namespace Arkira.Core.NotificationCenter
{
    public static class NotificationContentTags
    {
        public const string Action = "action";
        public const string ApplicationNotificationFor = "applicationNotificationFor";
        public const string ActionPerformer = "actionPerformer";
        public const string ContentTypeDisplayName = "contentTypeDisplayName";
        public const string ContentOriginOwner = "contentOriginOwner";
    }
}