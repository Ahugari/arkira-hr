﻿namespace Arkira.Core.NotificationCenter
{
    public static class NotificationType
    {
        public const string System = "System";
        public const string Task = "Task";
        public const string Application = "Application";
    }
}