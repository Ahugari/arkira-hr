﻿using Arkira.Core.Repositories;

namespace Arkira.Core.PublicHolidays
{
    public interface IPublicHolidayRepository : IGenericRepository<PublicHoliday>
    {
    }
}