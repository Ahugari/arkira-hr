﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.PublicHolidays
{
    public class PublicHoliday : Entity<Guid>, IHasCreationDate, IHasModifiedDate
    {
        public DateTime Day { get; set; }

        public DateTime DateCreated { get; set; }
        public string Name { get; set; }

        public DateTime? DateModified { get; set; }

        private PublicHoliday()
        {
        }

        public PublicHoliday(DateTime day, string name)
        {
            Day = day;
            Name = name.Trim();
            DateCreated = DateTime.Now.Date;
        }

        public static readonly string EntityName = "Public Holiday";

        public override string ToString() => EntityName;
    }
}