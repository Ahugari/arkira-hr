﻿using Arkira.Core.Services;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.PublicHolidays
{
    public class PublicHolidayDomainService
    {
        private readonly IPublicHolidayRepository _publicHolidayRepository;

        public PublicHolidayDomainService(IPublicHolidayRepository publicHolidayRepository)
        {
            _publicHolidayRepository = publicHolidayRepository;
        }

        public async Task<PublicHoliday> CreateAsync(DateTime day, string name)
        {
            if (await PublicHolidayExistsAsync(day, name))
                throw new InvalidOperationException("Public Holiday already created. Please check the day or name.");

            var publicHoliday = new PublicHoliday(day.Date, name);

            return await _publicHolidayRepository.InsertAsync(publicHoliday);
        }

        private async Task<bool> PublicHolidayExistsAsync(DateTime day, string name)
        {
            return await _publicHolidayRepository.FindAsync(x => x.Name.ToLower().Equals(name.ToLower())
                                                                || x.Day.Date.Equals(day.Date)) != null;
        }

        public async Task<PublicHoliday> UpdateAsync(PublicHoliday oldHoliday, PublicHoliday newHoliday)
        {
            var existingPublicHoliday = await _publicHolidayRepository.FindAsync(oldHoliday.Id);
            if (existingPublicHoliday == null)
                throw new InvalidOperationException("Public Holiday is not valid.");

            if (await PublicHolidayExistsAsync(newHoliday.Day, newHoliday.Name))
                throw new InvalidOperationException("Public Holiday already created. Please check the day or name.");

            existingPublicHoliday.Name = newHoliday.Name;
            existingPublicHoliday.Day = newHoliday.Day.Date;
            existingPublicHoliday.DateModified = new SystemClock().UtcNow;

            return await _publicHolidayRepository.UpdateAsync(existingPublicHoliday);
        }
    }
}