﻿using Arkira.Core.Employees;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Arkira.Core
{
    public interface IArkiraDbContext : IUnitOfWork
    {
        public DbSet<Employee> EmployeeBasics { get; }
        public DbSet<LeaveType> LeaveTypes { get; }
        public DbSet<LeavePolicy> LeavePolicies { get; }
    }
}