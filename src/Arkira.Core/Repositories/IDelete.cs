﻿using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface IDelete<IEntity>
    {
        public Task<IEntity> RemoveAsync(IEntity entity);
    }
}