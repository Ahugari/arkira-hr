﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface IGenericCreateUpdateDeleteRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity<Guid>
    {
        IEnumerable<TEntity> ReadAll();

        Task<TEntity> ReadOneAsync(Guid id);

        Task<TEntity> ReadOneAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> RemoveAsync(TEntity entity);

        Task<TEntity> SaveAsync(TEntity entity);
    }
}