﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface IGenericRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity<Guid>
    {
        void Add(TEntity entity);

        Task<TEntity> FindAsync(Guid id);

        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate);

        Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate);

        Task<int> GetCountAsync();

        Task<TEntity> GetPagedAsync(int skipCount, int maxResultCount, int sorting);

        Task<TEntity> InsertAsync(TEntity entity);

        Task RemoveAsync(Guid id);

        Task<TEntity> UpdateAsync(TEntity entity);

        Task<IEnumerable<TEntity>> GetAllAsync();
    }
}