﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;

namespace Arkira.Core.Repositories
{
    public interface IPagedAndSorted<TEntity> where TEntity : IEntity<Guid>
    {
        IEnumerable<IPagedAndSorted<TEntity>> GetAllPagedAndSorted(int skipCount, int maxResultCount, int sorting);
    }
}