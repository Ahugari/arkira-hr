﻿using Arkira.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface IRead<TEntity> where TEntity : IEntity<Guid>
    {
        Task<TEntity> ReadOneAsync(Guid id);

        Task<TEntity> ReadOneAsync(Expression<Func<TEntity, bool>> predicate);

        IEnumerable<TEntity> ReadAll();

        Task<IEnumerable<TEntity>> ReadManyAsync(Expression<Func<TEntity, bool>> predicate);
    }
}