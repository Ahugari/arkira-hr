﻿using Arkira.Core.Entities;
using System;

namespace Arkira.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : IEntity<Guid>
    {
        public IUnitOfWork UnitOfWork { get; }
    }
}