﻿using Arkira.Core.Entities;
using System;
using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface ISave<TEntity> where TEntity : IEntity<Guid>
    {
        public Task<TEntity> SaveAsync(TEntity entity);
    }
}