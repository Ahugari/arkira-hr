﻿using System.Threading.Tasks;

namespace Arkira.Core.Repositories
{
    public interface IUnitOfWork
    {
        Task<bool> CompleteAsync();
    }
}