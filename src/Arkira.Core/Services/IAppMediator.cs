﻿using MediatR;

namespace Arkira.Core.Services
{
    public interface IAppMediator : IMediator
    {
    }

    public class AppMediator : Mediator, IAppMediator
    {
        public AppMediator(ServiceFactory serviceFactory) : base(serviceFactory)
        {
        }
    }
}