﻿using MediatR;

namespace Arkira.Core.Services
{
    public interface IAppNotification<T> : INotification where T : class
    {
    }
}