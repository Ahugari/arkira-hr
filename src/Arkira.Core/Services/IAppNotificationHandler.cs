﻿using MediatR;

namespace Arkira.Core.Services
{
    public interface IAppNotificationHandler<in T> : INotificationHandler<T> where T : INotification
    {
    }
}