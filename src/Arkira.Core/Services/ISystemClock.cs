﻿using System;

namespace Arkira.Core.Services
{
    public interface ISystemClock
    {
        DateTime UtcNow { get; }
    }
}