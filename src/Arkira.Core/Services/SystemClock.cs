﻿using System;

namespace Arkira.Core.Services
{
    public class SystemClock : ISystemClock
    {
        public DateTime UtcNow => DateTime.Now;
    }
}