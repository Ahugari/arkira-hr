﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.ApplicationActivities
{
    public class EfCoreApplicationActivityRepository : IApplicationActivityRepository
    {
        private readonly IRead<ApplicationActivity> _read;
        private readonly ISave<ApplicationActivity> _save;
        private readonly ArkiraDbContext _context;

        public EfCoreApplicationActivityRepository(IRead<ApplicationActivity> read, ISave<ApplicationActivity> save, ArkiraDbContext context)
        {
            _read = read;
            _save = save;
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IEnumerable<ApplicationActivity> ReadAll()
        {
            return _read.ReadAll();
        }

        public async Task<ApplicationActivity> ReadByActivityIdAsync(string activityId)
        {
            return await ReadOneAsync(x => x.ActivityId == activityId);
        }

        public async Task<IEnumerable<ApplicationActivity>> ReadManyAsync(Expression<Func<ApplicationActivity, bool>> predicate)
        {
            return await _read.ReadManyAsync(predicate);
        }

        public async Task<ApplicationActivity> ReadOneAsync(Guid id)
        {
            return await _read.ReadOneAsync(id);
        }

        public async Task<ApplicationActivity> ReadOneAsync(Expression<Func<ApplicationActivity, bool>> predicate)
        {
            return await _read.ReadOneAsync(predicate);
        }

        public async Task<ApplicationActivity> SaveAsync(ApplicationActivity entity)
        {
            return await _save.SaveAsync(entity);
        }
    }
}