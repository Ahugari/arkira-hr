﻿using Arkira.Core.ApplicationsHistory;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.ApplicationsHistory
{
    public class EfCoreApplicationHistoryRepository : IApplicationHistoryRepository
    {
        private readonly IRead<ApplicationHistory> _read;
        private readonly ISave<ApplicationHistory> _save;
        private readonly ArkiraDbContext _context;

        public EfCoreApplicationHistoryRepository(IRead<ApplicationHistory> read, ISave<ApplicationHistory> save, ArkiraDbContext context)
        {
            _read = read;
            _save = save;
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IEnumerable<ApplicationHistory> ReadAll()
        {
            return _read.ReadAll();
        }

        public async Task<IEnumerable<ApplicationHistory>> ReadManyAsync(Expression<Func<ApplicationHistory, bool>> predicate)
        {
            return await _read.ReadManyAsync(predicate);
        }

        public async Task<ApplicationHistory> ReadOneAsync(Guid id)
        {
            return await _read.ReadOneAsync(id);
        }

        public async Task<ApplicationHistory> ReadOneAsync(Expression<Func<ApplicationHistory, bool>> predicate)
        {
            return await _read.ReadOneAsync(predicate);
        }

        public async Task<ApplicationHistory> GetApplicationReviewerActivity(string applicationId, string activityId, string reviewerId)
        {
            return await _read.ReadOneAsync(x => x.ReviewActivityId == activityId
                                                    && x.ActionPerformerId == reviewerId
                                                    && x.ApplicationId == applicationId);
        }

        public async Task<IEnumerable<ApplicationHistory>> GetAllByActionAndApplicationActivityId(string activityId, string action)
        {
            var result = await _read.ReadManyAsync(x => x.Id.ToString().Equals(activityId, StringComparison.OrdinalIgnoreCase)
                                               && x.ActionPerformed.Equals(action, StringComparison.OrdinalIgnoreCase));

            return result ?? new HashSet<ApplicationHistory>();
        }

        public async Task<ApplicationHistory> SaveAsync(ApplicationHistory entity)
        {
            return await _save.SaveAsync(entity);
        }
    }
}