﻿using Arkira.Core;
using Arkira.Core.ApplicationActivities;
using Arkira.Core.ApplicationsHistory;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.EmployeeLeavePolicies;
using Arkira.Core.Employees;
using Arkira.Core.Inboxes;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.NotificationCenter;
using Arkira.Core.PublicHolidays;
using Arkira.Infrastructure.Configurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Arkira.Infrastructure
{
    public class ArkiraDbContext : IdentityDbContext, IArkiraDbContext

    {
        public DbSet<Employee> EmployeeBasics { get; set; }
        public DbSet<LeaveType> LeaveTypes { get; set; }
        public DbSet<LeavePolicy> LeavePolicies { get; set; }
        public DbSet<LeaveRestriction> LeaveRestrictions { get; set; }
        public DbSet<LeaveAttachment> LeaveAttachments { get; set; }
        public DbSet<LeaveApplication> LeaveApplications { get; set; }
        public DbSet<LeavePolicyAccount> LeavePolicyAccounts { get; set; }
        public DbSet<LeavePolicyAccountHistory> LeavePolicyAccountsHistory { get; set; }
        public DbSet<EmployeeLeavePolicy> EmployeeLeavePolicies { get; set; }
        public DbSet<PublicHoliday> PublicHolidays { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }
        public DbSet<EmployeeJobInfo> JobInformation { get; set; }
        public DbSet<Inbox> Inbox { get; set; }
        public DbSet<Notification> Notification { get; set; }
        public DbSet<ApplicationHistory> ApplicationsHistory { get; set; }
        public DbSet<ApplicationActivity> ApplicationActivities { get; set; }

        public ArkiraDbContext(DbContextOptions<ArkiraDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new EmployeeBasicsEntityConfiguration());
            builder.ApplyConfiguration(new LeaveTypesEntityConfiguration());
            builder.ApplyConfiguration(new LeavePoliciesEntityConfiguration());
            builder.ApplyConfiguration(new LeaveRestrictionsEntityConfiguration());
            builder.ApplyConfiguration(new LeaveApplicationEntityConfiguration());
            builder.ApplyConfiguration(new LeaveAttachmentsConfiguration());
            builder.ApplyConfiguration(new LeavePolicyAccountConfiguration());
            builder.ApplyConfiguration(new LeavePolicyAccountHistoryConfiguration());
            builder.ApplyConfiguration(new EmployeeLeavePolicyEntityConfiguration());
            builder.ApplyConfiguration(new PublicHolidayEntityConfiguration());
            builder.ApplyConfiguration(new JobTitlesEntityConfiguration());
            builder.ApplyConfiguration(new InboxEntityConfiguration());
            builder.ApplyConfiguration(new EmployeeJobInformationEntityConfiguration());
            builder.ApplyConfiguration(new NotificationEntityConfiguration());
            builder.ApplyConfiguration(new ApplicationHistoryConfiguration());
            builder.ApplyConfiguration(new ApplicationActivityEntityConfiguration());
            base.OnModelCreating(builder);
        }

        public async Task<bool> CompleteAsync()
        {
            bool result;
            try
            {
                result = await SaveChangesAsync() >= 1;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    entry.State = EntityState.Detached;
                }
                throw;
            }

            return result;
        }
    }
}