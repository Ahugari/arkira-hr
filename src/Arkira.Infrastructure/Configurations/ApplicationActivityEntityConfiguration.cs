﻿using Arkira.Core;
using Arkira.Core.ApplicationActivities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Arkira.Infrastructure.Configurations
{
    internal class ApplicationActivityEntityConfiguration : IEntityTypeConfiguration<ApplicationActivity>
    {
        public void Configure(EntityTypeBuilder<ApplicationActivity> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "ApplicationActivities");

            builder.Property(x => x.ActivityId).HasMaxLength(250);
            builder.Property(x => x.ApplicationId).IsRequired().HasMaxLength(250);
            builder.Property(x => x.Reviewers)
                   .IsRequired()
                   .HasConversion(x => JsonConvert.SerializeObject(x, Formatting.Indented), y => JsonConvert.DeserializeObject<HashSet<string>>(y));
            builder.Property(x => x.Actions)
                   .IsRequired()
                   .HasConversion(x => JsonConvert.SerializeObject(x, Formatting.Indented), y => JsonConvert.DeserializeObject<HashSet<string>>(y));
        }
    }
}