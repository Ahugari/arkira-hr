﻿using Arkira.Core;
using Arkira.Core.ApplicationsHistory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    public class ApplicationHistoryConfiguration : IEntityTypeConfiguration<ApplicationHistory>
    {
        public void Configure(EntityTypeBuilder<ApplicationHistory> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "ApplicationsHistory");
            builder.Property(x => x.ApplicationId).IsRequired();
            builder.Property(x => x.ApplicationType).IsRequired().HasMaxLength(128);
            builder.Property(x => x.ActionPerformed).IsRequired().HasMaxLength(128);
            builder.Property(x => x.ActionPerformer).IsRequired().HasMaxLength(128);
            builder.Property(x => x.ActionPerformerId).IsRequired();
            builder.Property(x => x.DateCreated).IsRequired();
        }
    }
}