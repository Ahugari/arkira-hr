﻿using Arkira.Core;
using Arkira.Core.Employees;
using Arkira.Core.LeavePolicyAccounts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class EmployeeBasicsEntityConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "EmployeeBasics");

            builder.Property(p => p.Version).HasDefaultValue(1);
            builder.HasOne(x => x.LeavePolicyAccount)
                .WithOne()
                .HasForeignKey<LeavePolicyAccount>(x => x.EmployeeId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(x => x.LeavePolicyEntitlements)
                .WithOne(y => y.Employee)
                .HasForeignKey(x => x.EmployeeId)
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}