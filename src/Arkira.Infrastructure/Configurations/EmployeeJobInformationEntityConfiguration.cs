﻿using Arkira.Core;
using Arkira.Core.EmployeeJobInformation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class EmployeeJobInformationEntityConfiguration : IEntityTypeConfiguration<EmployeeJobInfo>
    {
        public void Configure(EntityTypeBuilder<EmployeeJobInfo> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "EmployeeJobInfo");
            builder.Property(x => x.EmployeeId).IsRequired();
            builder.Property(x => x.JobTitleId).IsRequired();
            //TODO: add reference for jobtitle nav property
            builder.Property(x => x.ReportsTo).IsRequired();
            builder.HasOne(x => x.Employee)
                .WithMany(x => x.EmployeeJobInformation)
                .HasForeignKey(x => x.EmployeeId);
        }
    }
}