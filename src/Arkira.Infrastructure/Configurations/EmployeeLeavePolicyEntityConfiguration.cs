﻿using Arkira.Core;
using Arkira.Core.EmployeeLeavePolicies;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class EmployeeLeavePolicyEntityConfiguration : IEntityTypeConfiguration<EmployeeLeavePolicy>
    {
        public void Configure(EntityTypeBuilder<EmployeeLeavePolicy> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "EmployeeLeavePolicyEntitlements");
            builder.HasKey("EmployeeId", "LeavePolicyId");
            builder.HasOne(x => x.Employee)
                .WithMany(y => y.LeavePolicyEntitlements)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.LeavePolicy)
                .WithMany(y => y.EntitledEmployees)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}