﻿using Arkira.Core;
using Arkira.Core.Inboxes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Arkira.Infrastructure.Configurations
{
    internal class InboxEntityConfiguration : IEntityTypeConfiguration<Inbox>
    {
        public void Configure(EntityTypeBuilder<Inbox> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "Inbox");
            builder.Property(x => x.ItemName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.Section).HasMaxLength(30).IsRequired();
            builder.Property(x => x.Category).HasMaxLength(30).IsRequired();
            builder.Property(x => x.ItemId).HasMaxLength(120).IsRequired();
            builder.Property(x => x.TargetId).HasMaxLength(120).IsRequired();
            builder.Property(x => x.Actions).HasConversion(x => JsonConvert.SerializeObject(x, Formatting.Indented), y => JsonConvert.DeserializeObject<Dictionary<string, HashSet<string>>>(y));
        }
    }
}