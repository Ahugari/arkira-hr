﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class JobTitlesEntityConfiguration : IEntityTypeConfiguration<JobTitle>
    {
        public void Configure(EntityTypeBuilder<JobTitle> builder)
        {
            builder.ToTable($"{ArkiraConsts.DB_PREFIX}JobTitles");
            builder.Property(x => x.Name)
                .HasMaxLength(320);
        }
    }
}