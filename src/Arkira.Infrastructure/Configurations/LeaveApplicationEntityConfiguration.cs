﻿using Arkira.Core;
using Arkira.Core.LeaveApplications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeaveApplicationEntityConfiguration : IEntityTypeConfiguration<LeaveApplication>
    {
        public void Configure(EntityTypeBuilder<LeaveApplication> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeaveApplications");

            builder.HasMany(x => x.Attachments)
                .WithOne(y => y.LeaveApplication)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(x => x.Actions)
                .HasConversion(y => JsonConvert.SerializeObject(y, Formatting.Indented),
                z => JsonConvert.DeserializeObject<Dictionary<string, HashSet<string>>>(z));
        }
    }
}