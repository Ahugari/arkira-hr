﻿using Arkira.Core;
using Arkira.Core.LeaveApplications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeaveAttachmentsConfiguration : IEntityTypeConfiguration<LeaveAttachment>
    {
        public void Configure(EntityTypeBuilder<LeaveAttachment> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeaveAttachments");
            builder.HasOne(x => x.LeaveApplication)
                .WithMany(y => y.Attachments);
        }
    }
}