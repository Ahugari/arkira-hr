﻿using Arkira.Core;
using Arkira.Core.LeavePolicies;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeavePoliciesEntityConfiguration : IEntityTypeConfiguration<LeavePolicy>
    {
        public void Configure(EntityTypeBuilder<LeavePolicy> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeavePolicies");
            builder.Property(x => x.CorrelationId)
                .HasDefaultValue(Guid.Empty);
            builder.HasMany(x => x.EntitledEmployees)
                .WithOne(x => x.LeavePolicy)
                .HasForeignKey(x => x.LeavePolicyId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(p => p.LeaveRestrictions)
                   .WithOne(p => p.LeavePolicy)
                   .HasForeignKey(p => p.LeavePolicyId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}