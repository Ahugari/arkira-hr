﻿using Arkira.Core;
using Arkira.Core.LeavePolicyAccounts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeavePolicyAccountConfiguration : IEntityTypeConfiguration<LeavePolicyAccount>
    {
        public void Configure(EntityTypeBuilder<LeavePolicyAccount> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeavePolicyAccounts");
            builder.HasKey(x => new { x.EmployeeId, x.LeavePolicyId });
        }
    }
}