﻿using Arkira.Core;
using Arkira.Core.LeavePolicyAccountsHistory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeavePolicyAccountHistoryConfiguration : IEntityTypeConfiguration<LeavePolicyAccountHistory>
    {
        public void Configure(EntityTypeBuilder<LeavePolicyAccountHistory> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeavePolicyAccountsHistory");
        }
    }
}