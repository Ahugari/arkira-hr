﻿using Arkira.Core;
using Arkira.Core.LeaveRestrictions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure
{
    internal class LeaveRestrictionsEntityConfiguration : IEntityTypeConfiguration<LeaveRestriction>
    {
        public void Configure(EntityTypeBuilder<LeaveRestriction> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeaveRestrictions");

            builder.Property(p => p.LeaveRestrictionAction).IsRequired();
            builder.Property(p => p.LeaveRestrictionOption).IsRequired();
            builder.HasKey(p => new { p.LeaveRestrictionOption, p.LeavePolicyId} );

            builder.HasOne(p => p.LeavePolicy)
                .WithMany(p => p.LeaveRestrictions)
                .HasForeignKey(p => p.LeavePolicyId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}