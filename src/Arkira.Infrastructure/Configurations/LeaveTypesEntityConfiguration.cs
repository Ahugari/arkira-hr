﻿using Arkira.Core;
using Arkira.Core.LeaveTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Arkira.Infrastructure.Configurations
{
    internal class LeaveTypesEntityConfiguration : IEntityTypeConfiguration<LeaveType>
    {
        public void Configure(EntityTypeBuilder<LeaveType> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "LeaveTypes");
            builder.Property(p => p.Version).HasDefaultValue(1);
            builder.Property(x => x.CorrelationId).HasDefaultValue(Guid.Empty);

            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.EntitlementType).IsRequired();
            builder.Property(p => p.EffectiveDate).IsRequired();
            builder.Property(p => p.LeaveRequestLengthFactor).IsRequired();
            builder.Property(p => p.LeaveRequestWindow).IsRequired();

            builder.HasMany(p => p.LeavePolicies)
                .WithOne(x => x.LeaveType)
                .HasForeignKey(f => f.LeaveTypeId)
                .HasPrincipalKey(k => k.Id)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}