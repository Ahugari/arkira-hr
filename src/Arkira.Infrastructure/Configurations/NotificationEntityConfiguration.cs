﻿using Arkira.Core;
using Arkira.Core.NotificationCenter;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    public class NotificationEntityConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "Notification");
            builder.Property(x => x.TargetId).IsRequired();
            builder.Property(x => x.Content).IsRequired();
            builder.Property(x => x.ContentOrigin).IsRequired();
        }
    }
}