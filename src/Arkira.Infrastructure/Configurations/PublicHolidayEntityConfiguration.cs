﻿using Arkira.Core;
using Arkira.Core.PublicHolidays;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arkira.Infrastructure.Configurations
{
    public class PublicHolidayEntityConfiguration : IEntityTypeConfiguration<PublicHoliday>
    {
        public void Configure(EntityTypeBuilder<PublicHoliday> builder)
        {
            builder.ToTable(ArkiraConsts.DB_PREFIX + "PublicHolidays");
        }
    }
}