﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.EmployeeFields.JobTitles
{
    public class EfCoreJobTitleRepository : IJobTitleRepository
    {
        private readonly ISave<JobTitle> create;
        private readonly IRead<JobTitle> read;
        private readonly ArkiraDbContext _context;

        public EfCoreJobTitleRepository(ISave<JobTitle> create, IRead<JobTitle> read, ArkiraDbContext context)
        {
            this.create = create;
            this.read = read;
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IEnumerable<JobTitle> ReadAll()
        {
            return read.ReadAll();
        }

        public async Task<IEnumerable<JobTitle>> ReadManyAsync(Expression<Func<JobTitle, bool>> predicate)
        {
            return await read.ReadManyAsync(predicate);
        }

        public async Task<JobTitle> ReadOneAsync(Guid id)
        {
            return await read.ReadOneAsync(id);
        }

        public async Task<JobTitle> ReadOneAsync(Expression<Func<JobTitle, bool>> predicate)
        {
            return await read.ReadOneAsync(predicate);
        }

        public async Task<JobTitle> SaveAsync(JobTitle entity)
        {
            return await create.SaveAsync(entity);
        }
    }
}