﻿using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.Repositories;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.EmployeeJobInformation
{
    public class EfCoreEmployeeJobInfoRepository : GenericCreateUpdateDeleteRepository<EmployeeJobInfo>, IEmployeeJobInfoRepository
    {
        private readonly ArkiraDbContext _context;

        public EfCoreEmployeeJobInfoRepository(ISave<EmployeeJobInfo> create, IRead<EmployeeJobInfo> read, IDelete<EmployeeJobInfo> delete, ArkiraDbContext context) : base(create, read, delete, context)
        {
            _context = context;
        }

        public async Task<EmployeeJobInfo> ReadOneWithEmployeeAsync(Expression<Func<EmployeeJobInfo, bool>> predicate)
        {
            return await _context.JobInformation.Include(x => x.Employee).FirstOrDefaultAsync(predicate);
        }
    }
}