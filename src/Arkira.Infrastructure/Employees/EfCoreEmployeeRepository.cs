﻿using Arkira.Core.Employees;
using Arkira.Core.Helpers;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Employees
{
    public class EfCoreEmployeeRepository : Repository<ArkiraDbContext, Employee>, IEmployeeRepository
    {
        public EfCoreEmployeeRepository(ArkiraDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<Employee> FindAsync(Expression<Func<Employee, bool>> predicate)
        {
            return await _context.EmployeeBasics.FirstOrDefaultAsync(predicate);
        }

        public override Task<Employee> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }

        public override async Task<IEnumerable<Employee>> GetManyAsync(Expression<Func<Employee, bool>> predicate)
        {
            return await _context.EmployeeBasics
                .Where(predicate)
                .Select(x => x)
                .ToListAsync();
        }

        public async Task<Employee> GetWithLeavePolicyEntitlements(Expression<Func<Employee, bool>> predicate)
        {
            return await _context.EmployeeBasics
                .Include(x => x.LeavePolicyEntitlements)
                .FirstOrDefaultAsync(predicate);
        }

        public async Task<Employee> GetWithJobInformationAsync(Expression<Func<Employee, bool>> predicate, CancellationToken cancellationToken)
        {
            return await _context.EmployeeBasics
                .Include(x => x.EmployeeJobInformation)
                .FirstOrDefaultAsync(predicate, cancellationToken);
        }

        public async Task<IEnumerable<Employee>> GetAllByJobTitle(string jobTitleId, CancellationToken cancellationToken)
        {
            var jobId = Validate.AsGuid(jobTitleId);

            return await _context.EmployeeBasics
                .Include(x => x.EmployeeJobInformation)
                .SelectMany(x => x.EmployeeJobInformation
                .Where(x => x.JobTitleId == jobId)
                .Select(x => x.Employee))
                .ToListAsync(cancellationToken);
        }

        public async Task<Employee> GetByIdAsync(string id, CancellationToken cancellationToken)
        {
            var employeeId = Validate.AsGuid(id);

            return await _context.EmployeeBasics.SingleAsync(x => x.Id == employeeId, cancellationToken);
        }

        public async Task<IEnumerable<Employee>> GetAllWithJobInformationAsync(CancellationToken cancellationToken)
        {
            return await _context.EmployeeBasics
                .Include(x => x.EmployeeJobInformation)
                .Select(x => x)
                .ToListAsync(cancellationToken);
        }
    }
}