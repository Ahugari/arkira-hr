﻿using Arkira.Core;
using Arkira.Core.ApplicationActivities;
using Arkira.Core.Applications;
using Arkira.Core.ApplicationsHistory;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.Employees;
using Arkira.Core.Inboxes;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.NotificationCenter;
using Arkira.Core.PublicHolidays;
using Arkira.Core.Repositories;
using Arkira.Core.Services;
using Arkira.Infrastructure.ApplicationActivities;
using Arkira.Infrastructure.ApplicationsHistory;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.EmployeeJobInformation;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.Inboxes;
using Arkira.Infrastructure.LeaveApplications;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.LeaveRestrictions;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.PublicHolidays;
using Arkira.Infrastructure.Repositories;
using Arkira.Infrastructure.Services;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Arkira.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddArkiraInfrastructureServices(this IServiceCollection services, string connectionString)
        {
            string name = typeof(SqlServerArkiraDbContextFactory).Assembly.GetName().Name;
            services
                .AddDbContextPool<ArkiraDbContext>(options => options.UseSqlServer(connectionString, options => options.MigrationsAssembly(name)));
            services.AddSingleton<ISystemClock, SystemClock>();
            services.AddScoped<IArkiraDbContext, ArkiraDbContext>();
            services.AddTransient<ISave<JobTitle>, SaveTransactional<JobTitle>>();
            services.AddTransient<IRead<JobTitle>, ReadTransactional<JobTitle>>();
            services.AddTransient<IJobTitleRepository, EfCoreJobTitleRepository>();
            services.AddTransient<IEmployeeDomainService, EmployeeDomainService>();
            services.AddTransient<IEmployeeRepository, EfCoreEmployeeRepository>();
            services.AddTransient<ILeaveTypeRepository, EfCoreLeaveTypeRepository>();
            services.AddTransient<ILeaveTypeDomainService, LeaveTypeDomainService>();
            services.AddTransient<ILeavePolicyDomainService, LeavePolicyDomainService>();
            services.AddTransient<ILeavePolicyRepository, EfCoreLeavePolicyRepository>();
            services.AddTransient<ILeavePolicyAccountHistoryRepository, EfCoreLeavePolicyAccountHistoryRepository>();
            services.AddTransient<ILeaveRestrictionRepository, EfCoreLeaveRestrictionRepository>();
            services.AddTransient<ILeaveApplicationRepository, EfCoreLeaveApplicationRepository>();
            services.AddTransient<ILeaveApplicationDomainService, LeaveApplicationDomainService>();
            services.AddTransient<IPublicHolidayRepository, EfCorePublicHolidayRepository>();
            services.AddTransient<ISave<Inbox>, SaveTransactional<Inbox>>();
            services.AddTransient<IRead<Inbox>, ReadTransactional<Inbox>>();
            services.AddTransient<IInboxRepository, EfCoreInboxRepository>();
            services.AddTransient<LeavePolicyAccountHistoryDomainService, LeavePolicyAccountHistoryDomainService>();
            services.AddTransient<LeavePolicyDomainService, LeavePolicyDomainService>();
            services.AddTransient<IEmployeeJobInfoRepository, EfCoreEmployeeJobInfoRepository>();
            services.AddTransient<ISave<EmployeeJobInfo>, SaveTransactional<EmployeeJobInfo>>();
            services.AddTransient<IRead<EmployeeJobInfo>, ReadTransactional<EmployeeJobInfo>>();
            services.AddTransient<IDelete<EmployeeJobInfo>, RemoveTransactional<EmployeeJobInfo>>();
            services.AddTransient<IGenericCreateUpdateDeleteRepository<Notification>, GenericCreateUpdateDeleteRepository<Notification>>();
            services.AddTransient<ISave<Notification>, SaveTransactional<Notification>>();
            services.AddTransient<IRead<Notification>, ReadTransactional<Notification>>();
            services.AddTransient<IDelete<Notification>, RemoveTransactional<Notification>>();
            services.AddTransient<IApplicationHistoryRepository, EfCoreApplicationHistoryRepository>();
            services.AddTransient<IRead<ApplicationHistory>, ReadTransactional<ApplicationHistory>>();
            services.AddTransient<ISave<ApplicationHistory>, SaveTransactional<ApplicationHistory>>();
            services.AddTransient<IApplicationDomainManager, ApplicationDomainManager>();
            services.AddTransient<IApplicationActivityRepository, EfCoreApplicationActivityRepository>();
            services.AddTransient<IRead<ApplicationActivity>, ReadTransactional<ApplicationActivity>>();
            services.AddTransient<ISave<ApplicationActivity>, SaveTransactional<ApplicationActivity>>();
            services.AddTransient<IApplicationHistoryDomainService, ApplicationHistoryDomainService>();
            services.AddScoped<IMediator, Mediator>();
            services.AddScoped<IAppMediator, AppMediator>();

            services.AddHostedService<AddDbInitialData>();

            return services;
        }
    }
}