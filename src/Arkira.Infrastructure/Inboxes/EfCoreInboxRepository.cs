﻿using Arkira.Core.Inboxes;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Inboxes
{
    public class EfCoreInboxRepository : IInboxRepository
    {
        private readonly ISave<Inbox> _create;
        private readonly IRead<Inbox> _read;
        private readonly ArkiraDbContext _context;

        public EfCoreInboxRepository(ISave<Inbox> create, IRead<Inbox> read, ArkiraDbContext context)
        {
            _create = create;
            _read = read;
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IEnumerable<Inbox> ReadAll()
        {
            return _read.ReadAll();
        }

        public async Task<IEnumerable<Inbox>> ReadManyAsync(Expression<Func<Inbox, bool>> predicate)
        {
            return await _read.ReadManyAsync(predicate);
        }

        public async Task<Inbox> ReadOneAsync(Guid id)
        {
            return await _read.ReadOneAsync(id);
        }

        public async Task<Inbox> ReadOneAsync(Expression<Func<Inbox, bool>> predicate)
        {
            return await _read.ReadOneAsync(predicate);
        }

        public async Task<Inbox> SaveAsync(Inbox entity)
        {
            return await _create.SaveAsync(entity);
        }
    }
}