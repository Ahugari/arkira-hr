﻿using Arkira.Core.LeaveApplications;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeaveApplications
{
    public class EfCoreLeaveApplicationRepository : Repository<ArkiraDbContext, LeaveApplication>, ILeaveApplicationRepository
    {
        public EfCoreLeaveApplicationRepository(ArkiraDbContext context) : base(context)
        {
        }

        public override async Task<LeaveApplication> FindAsync(Expression<Func<LeaveApplication, bool>> predicate)
        {
            return await _context.LeaveApplications.FirstOrDefaultAsync(predicate);
        }

        public override async Task<IEnumerable<LeaveApplication>> GetManyAsync(Expression<Func<LeaveApplication, bool>> predicate)
        {
            return await _context.LeaveApplications.Where(predicate).Select(x => x).ToListAsync();
        }

        public override Task<LeaveApplication> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }
    }
}