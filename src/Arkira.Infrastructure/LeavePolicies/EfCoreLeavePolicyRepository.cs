﻿using Arkira.Core.LeavePolicies;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeavePolicies
{
    public class EfCoreLeavePolicyRepository : Repository<ArkiraDbContext, LeavePolicy>, ILeavePolicyRepository
    {
        public EfCoreLeavePolicyRepository(ArkiraDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<LeavePolicy> FindAsync(Expression<Func<LeavePolicy, bool>> predicate)
        {
            return await _context.LeavePolicies.FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<LeavePolicy>> GetAsync()
        {
            return await _context.LeavePolicies.ToListAsync();
        }

        public override Task<LeavePolicy> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }

        public async Task RemoveAsync(LeavePolicy leavePolicy)
        {
            _context.LeavePolicies.Remove(leavePolicy);
        }

        public override async Task<IEnumerable<LeavePolicy>> GetManyAsync(Expression<Func<LeavePolicy, bool>> predicate)
        {
            return _context.LeavePolicies.Where(predicate).Select(x => x);
        }

        public async Task<IEnumerable<LeavePolicy>> GetAllWithRestrictions()
        {
            return await _context.LeavePolicies.Include(x => x.LeaveRestrictions).ToListAsync();
        }

        public LeavePolicy GetOneWithRestrictions(Expression<Func<LeavePolicy, bool>> predicate)
        {
            return _context.LeavePolicies.Include(x => x.LeaveRestrictions).Where(predicate).FirstOrDefault();
        }
    }
}