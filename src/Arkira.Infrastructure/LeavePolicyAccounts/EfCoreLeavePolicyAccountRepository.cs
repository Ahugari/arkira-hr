﻿using Arkira.Core.LeavePolicyAccounts;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeavePolicyAccounts
{
    internal class EfCoreLeavePolicyAccountRepository : Repository<ArkiraDbContext, LeavePolicyAccount>, ILeavePolicyAccountRepository
    {
        public EfCoreLeavePolicyAccountRepository(ArkiraDbContext context) : base(context)
        {
        }

        public override async Task<LeavePolicyAccount> FindAsync(Expression<Func<LeavePolicyAccount, bool>> predicate)
        {
            return await _context.LeavePolicyAccounts.FirstOrDefaultAsync(predicate);
        }

        public override async Task<IEnumerable<LeavePolicyAccount>> GetManyAsync(Expression<Func<LeavePolicyAccount, bool>> predicate)
        {
            return _context.LeavePolicyAccounts.Where(predicate).Select(x => x).AsEnumerable();
        }

        public override Task<LeavePolicyAccount> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }
    }
}