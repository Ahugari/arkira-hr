﻿using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeavePolicyAccountsHistory
{
    public class EfCoreLeavePolicyAccountHistoryRepository : Repository<ArkiraDbContext, LeavePolicyAccountHistory>, ILeavePolicyAccountHistoryRepository
    {
        public EfCoreLeavePolicyAccountHistoryRepository(ArkiraDbContext context) : base(context)
        {
        }

        public override async Task<LeavePolicyAccountHistory> FindAsync(Expression<Func<LeavePolicyAccountHistory, bool>> predicate)
        {
            return await _context.LeavePolicyAccountsHistory.FirstOrDefaultAsync(predicate);
        }

        public override async Task<IEnumerable<LeavePolicyAccountHistory>> GetManyAsync(Expression<Func<LeavePolicyAccountHistory, bool>> predicate)
        {
            return await _context.LeavePolicyAccountsHistory.Where(predicate).Select(x => x).ToListAsync();
        }

        public override Task<LeavePolicyAccountHistory> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }
    }
}