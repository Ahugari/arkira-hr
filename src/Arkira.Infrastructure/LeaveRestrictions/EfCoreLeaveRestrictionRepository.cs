﻿using Arkira.Core.Enums;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeaveRestrictions
{
    public class EfCoreLeaveRestrictionRepository : ILeaveRestrictionRepository
    {
        private readonly ArkiraDbContext _context;

        public EfCoreLeaveRestrictionRepository(ArkiraDbContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<LeaveRestriction> CreateAsync(LeaveRestriction newLeaveRestriction)
        {
            LeaveRestriction existingRestriction = await LeaveRestrictionExistsAsync(newLeaveRestriction);

            if (existingRestriction != null)
                return existingRestriction;

            Add(newLeaveRestriction);

            return newLeaveRestriction;
        }

        public async Task<LeaveRestriction> FindAsync(LeaveRestrictionOption leaveRestrictionOption, Guid leavePolicyId)
        {
            return await _context.LeaveRestrictions.FirstOrDefaultAsync(restriction => restriction.LeaveRestrictionOption == leaveRestrictionOption
                                                                                    && restriction.LeavePolicyId == leavePolicyId);
        }

        public async Task<IEnumerable<LeaveRestriction>> GetAllAsync(LeaveRestrictionOption leaveRestrictionOption, Guid leavePolicyId)
        {
            return await _context.LeaveRestrictions.Where(restriction => restriction.LeaveRestrictionOption == leaveRestrictionOption
                                                                        && restriction.LeavePolicyId == leavePolicyId)
                                                    .Select(restriction => restriction)
                                                    .ToListAsync();
        }

        public async Task<int> GetCountAsync()
        {
            return await _context.LeaveRestrictions.CountAsync();
        }

        public async Task RemoveAsync(LeaveRestriction restriction)
        {
            var restrictionInDb = await LeaveRestrictionExistsAsync(restriction);
            if (restrictionInDb == null)
                return;

            _context.LeaveRestrictions.Remove(restrictionInDb);
        }

        public LeaveRestriction Update(LeaveRestriction entity)
        {
            _context.LeaveRestrictions.Update(entity);

            return entity;
        }

        private void Add(LeaveRestriction entity)
        {
            _context.LeaveRestrictions.Add(entity);
        }

        private async Task<LeaveRestriction> LeaveRestrictionExistsAsync(LeaveRestriction restriction)
        {
            return await _context.LeaveRestrictions.FirstOrDefaultAsync(x => x.LeavePolicyId == restriction.LeavePolicyId
                                                        && x.LeaveRestrictionOption == restriction.LeaveRestrictionOption);
        }
    }
}