﻿using Arkira.Core.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.LeaveTypes
{
    public class EfCoreLeaveTypeRepository : Repository<ArkiraDbContext, LeaveType>, ILeaveTypeRepository
    {
        public EfCoreLeaveTypeRepository(ArkiraDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<LeaveType> FindAsync(Expression<Func<LeaveType, bool>> predicate)
        {
            return await _context.LeaveTypes.FirstOrDefaultAsync(predicate);
        }

        public override async Task<IEnumerable<LeaveType>> GetManyAsync(Expression<Func<LeaveType, bool>> predicate)
        {
            return (IEnumerable<LeaveType>)await _context.LeaveTypes.Select(predicate).ToListAsync();
        }

        public async Task<List<LeaveType>> GetAsync()
        {
            return await _context.LeaveTypes.ToListAsync();
        }

        public override Task<LeaveType> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<LeaveType> InsertAsync(LeaveType entity)
        {
            try
            {
                _context.LeaveTypes.Add(entity);
                await _context.SaveChangesAsync();
                return entity;
            }
            catch (DbUpdateException)
            {
                throw new DbUpdateException($"An error occurred while updating {nameof(entity)}");
            }
        }

        public async Task RemoveAsync(LeaveType leaveType)
        {
            _context.LeaveTypes.Remove(leaveType);
        }

        public override async Task<LeaveType> UpdateAsync(LeaveType entity)
        {
            try
            {
                _context.LeaveTypes.Update(entity);
                await _context.SaveChangesAsync();
                return entity;
            }
            catch (DbUpdateException)
            {
                throw new DbUpdateException($"An error occurred while updating {nameof(entity)}");
            }
        }
    }
}