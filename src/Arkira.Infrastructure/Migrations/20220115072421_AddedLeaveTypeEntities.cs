﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedLeaveTypeEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ArkiraEmployeeBasics",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraEmployeeBasics",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EntitlementType = table.Column<int>(type: "int", nullable: false),
                    LeaveEntitlementReset = table.Column<int>(type: "int", nullable: false),
                    CarryOverUnusedEntitlement = table.Column<bool>(type: "bit", nullable: false),
                    MaxCarryOverDays = table.Column<int>(type: "int", nullable: true),
                    CarryOverNegativeEntitlement = table.Column<bool>(type: "bit", nullable: false),
                    ExpireCarriedOverDays = table.Column<bool>(type: "bit", nullable: false),
                    MonthsForCarryOverExpiry = table.Column<int>(type: "int", nullable: true),
                    CarryOverLeaveYearStarts = table.Column<int>(type: "int", nullable: true),
                    LeaveRequestLengthFactor = table.Column<int>(type: "int", nullable: false),
                    IncludePublicHolidays = table.Column<bool>(type: "bit", nullable: false),
                    AllowNegativeEntitlement = table.Column<bool>(type: "bit", nullable: false),
                    MaxNegativeEntitlementBelowZero = table.Column<int>(type: "int", nullable: true),
                    LeaveRequestWindow = table.Column<int>(type: "int", nullable: false),
                    NewYearActiveWindow = table.Column<int>(type: "int", nullable: true),
                    NewYearLeaveRequestDeductionOptions = table.Column<int>(type: "int", nullable: false),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArkiraLeavePolicies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeaveTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HasOwnEntitlement = table.Column<bool>(type: "bit", nullable: false),
                    PaidLeave = table.Column<bool>(type: "bit", nullable: false),
                    EntitlementFromTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    MinLeaveDurationInDay = table.Column<int>(type: "int", nullable: false),
                    EnableAttachments = table.Column<bool>(type: "bit", nullable: false),
                    EntitlementCap = table.Column<int>(type: "int", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeavePolicies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeavePolicies_ArkiraLeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "ArkiraLeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveRestrictionProfiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeaveRestrictionOptions = table.Column<int>(type: "int", nullable: false),
                    LeaveRestrictionAction = table.Column<int>(type: "int", nullable: false),
                    OptionsPrimaryValue = table.Column<int>(type: "int", nullable: false),
                    OptionsSecondaryValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeaveTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveRestrictionProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeaveRestrictionProfiles_ArkiraLeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "ArkiraLeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeavePolicies_LeaveTypeId",
                table: "ArkiraLeavePolicies",
                column: "LeaveTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeaveRestrictionProfiles_LeaveTypeId",
                table: "ArkiraLeaveRestrictionProfiles",
                column: "LeaveTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraLeavePolicies");

            migrationBuilder.DropTable(
                name: "ArkiraLeaveRestrictionProfiles");

            migrationBuilder.DropTable(
                name: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ArkiraEmployeeBasics");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraEmployeeBasics");
        }
    }
}
