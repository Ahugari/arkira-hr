﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class CleanedUpExistingEntitiesStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "NewYearLeaveRequestDeductionOptions",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<DateTime>(
                name: "CarryOverLeaveYearStartDate",
                table: "ArkiraLeaveTypes",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntitlementCap",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LeaveEntitlementResetScheduleDate",
                table: "ArkiraLeaveTypes",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxMonthsPeriodInFuture",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxNegativeCarryOverDays",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "ArkiraLeaveRestrictionProfiles",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "ArkiraLeavePolicies",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "ArkiraEmployeeBasics",
                type: "int",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CarryOverLeaveYearStartDate",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "EntitlementCap",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "LeaveEntitlementResetScheduleDate",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "MaxMonthsPeriodInFuture",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "MaxNegativeCarryOverDays",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "ArkiraLeaveRestrictionProfiles");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "ArkiraEmployeeBasics");

            migrationBuilder.AlterColumn<int>(
                name: "NewYearLeaveRequestDeductionOptions",
                table: "ArkiraLeaveTypes",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
