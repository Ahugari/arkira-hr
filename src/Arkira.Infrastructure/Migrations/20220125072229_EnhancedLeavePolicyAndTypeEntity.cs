﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class EnhancedLeavePolicyAndTypeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeaveTypes",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("22171d84-fcd4-400a-8fc3-87b08f602626"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeavePolicies",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("2b8dc16b-68c8-48d8-8ef5-7ba80e0417be"));

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "ArkiraLeavePolicies",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CorrelationId",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "ArkiraLeavePolicies");

            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeaveTypes",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("22171d84-fcd4-400a-8fc3-87b08f602626"));
        }
    }
}
