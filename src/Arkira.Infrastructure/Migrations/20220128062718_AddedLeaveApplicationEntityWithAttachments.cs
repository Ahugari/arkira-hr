﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedLeaveApplicationEntityWithAttachments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraLeaveTypes");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraLeaveRestrictionProfiles");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraEmployeeBasics");

            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeaveTypes",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("22171d84-fcd4-400a-8fc3-87b08f602626"));

            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeavePolicies",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("2b8dc16b-68c8-48d8-8ef5-7ba80e0417be"));

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveApplications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeaveTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StartsOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndsOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CoveringEmployee = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ApplicantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HrNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CorrelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Version = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveApplications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveAttachments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeaveApplicationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UploadedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeaveAttachments_ArkiraLeaveApplications_LeaveApplicationId",
                        column: x => x.LeaveApplicationId,
                        principalTable: "ArkiraLeaveApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeaveAttachments_LeaveApplicationId",
                table: "ArkiraLeaveAttachments",
                column: "LeaveApplicationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraLeaveAttachments");

            migrationBuilder.DropTable(
                name: "ArkiraLeaveApplications");

            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeaveTypes",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("22171d84-fcd4-400a-8fc3-87b08f602626"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraLeaveTypes",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraLeaveRestrictionProfiles",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CorrelationId",
                table: "ArkiraLeavePolicies",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("2b8dc16b-68c8-48d8-8ef5-7ba80e0417be"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldDefaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraLeavePolicies",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraEmployeeBasics",
                type: "datetime2",
                nullable: true);
        }
    }
}
