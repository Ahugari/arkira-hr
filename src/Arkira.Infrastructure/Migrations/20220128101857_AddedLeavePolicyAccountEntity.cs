﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedLeavePolicyAccountEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ArkiraLeaveAttachments");

            migrationBuilder.AddColumn<Guid>(
                name: "InitiatorId",
                table: "ArkiraLeaveApplications",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsInitiatedByHr",
                table: "ArkiraLeaveApplications",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ArkiraLeavePolicyAccounts",
                columns: table => new
                {
                    LeavePolicyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Balance = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeavePolicyAccounts", x => new { x.EmployeeId, x.LeavePolicyId });
                    table.ForeignKey(
                        name: "FK_ArkiraLeavePolicyAccounts_ArkiraEmployeeBasics_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "ArkiraEmployeeBasics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArkiraLeavePolicyAccountsHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeavePolicyAccountId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ActionTaken = table.Column<int>(type: "int", nullable: false),
                    ActionValue = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeavePolicyAccountsHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeavePolicyAccountsHistory_ArkiraLeavePolicyAccounts_LeavePolicyAccountId_EmployeeId",
                        columns: x => new { x.LeavePolicyAccountId, x.EmployeeId },
                        principalTable: "ArkiraLeavePolicyAccounts",
                        principalColumns: new[] { "EmployeeId", "LeavePolicyId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeavePolicyAccounts_EmployeeId",
                table: "ArkiraLeavePolicyAccounts",
                column: "EmployeeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeavePolicyAccountsHistory_LeavePolicyAccountId_EmployeeId",
                table: "ArkiraLeavePolicyAccountsHistory",
                columns: new[] { "LeavePolicyAccountId", "EmployeeId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropTable(
                name: "ArkiraLeavePolicyAccounts");

            migrationBuilder.DropColumn(
                name: "InitiatorId",
                table: "ArkiraLeaveApplications");

            migrationBuilder.DropColumn(
                name: "IsInitiatedByHr",
                table: "ArkiraLeaveApplications");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ArkiraLeaveAttachments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
