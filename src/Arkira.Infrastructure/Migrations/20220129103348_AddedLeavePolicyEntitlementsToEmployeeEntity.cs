﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedLeavePolicyEntitlementsToEmployeeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ArkiraEmployeeLeavePolicyEntitlements",
                columns: table => new
                {
                    EntitledEmployeesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeavePolicyEntitlementsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraEmployeeLeavePolicyEntitlements", x => new { x.EntitledEmployeesId, x.LeavePolicyEntitlementsId });
                    table.ForeignKey(
                        name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraEmployeeBasics_EntitledEmployeesId",
                        column: x => x.EntitledEmployeesId,
                        principalTable: "ArkiraEmployeeBasics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraLeavePolicies_LeavePolicyEntitlementsId",
                        column: x => x.LeavePolicyEntitlementsId,
                        principalTable: "ArkiraLeavePolicies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraEmployeeLeavePolicyEntitlements_LeavePolicyEntitlementsId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                column: "LeavePolicyEntitlementsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraEmployeeLeavePolicyEntitlements");
        }
    }
}
