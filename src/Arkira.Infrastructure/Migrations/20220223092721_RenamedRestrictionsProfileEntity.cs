﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class RenamedRestrictionsProfileEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraLeavePolicies_ArkiraLeaveTypes_LeaveTypeId",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraLeavePolicyAccountsHistory_ArkiraLeavePolicyAccounts_LeavePolicyAccountId_EmployeeId",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropTable(
                name: "ArkiraLeaveRestrictionProfiles");

            migrationBuilder.DropIndex(
                name: "IX_ArkiraLeavePolicyAccountsHistory_LeavePolicyAccountId_EmployeeId",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropColumn(
                name: "ActionValue",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.RenameColumn(
                name: "LeavePolicyAccountId",
                table: "ArkiraLeavePolicyAccountsHistory",
                newName: "LeavePolicyId");

            migrationBuilder.RenameColumn(
                name: "LeaveTypeId",
                table: "ArkiraLeaveApplications",
                newName: "LeavePolicyId");

            migrationBuilder.AddColumn<DateTime>(
                name: "ActionDate",
                table: "ArkiraLeavePolicyAccountsHistory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "ArkiraLeavePolicyAccountsHistory",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ArkiraLeavePolicyAccountsHistory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsUsable",
                table: "ArkiraLeavePolicyAccounts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "DaysToEnrollment",
                table: "ArkiraLeavePolicies",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "HireDate",
                table: "ArkiraEmployeeBasics",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveRestrictions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LeaveRestrictionOption = table.Column<int>(type: "int", nullable: false),
                    LeaveRestrictionAction = table.Column<int>(type: "int", nullable: false),
                    RestrictionPrimaryValue = table.Column<int>(type: "int", nullable: false),
                    RestrictionSecondaryValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeavePolicyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveRestrictions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeaveRestrictions_ArkiraLeavePolicies_LeavePolicyId",
                        column: x => x.LeavePolicyId,
                        principalTable: "ArkiraLeavePolicies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeavePolicyAccountsHistory_LeavePolicyId",
                table: "ArkiraLeavePolicyAccountsHistory",
                column: "LeavePolicyId");

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeaveRestrictions_LeavePolicyId",
                table: "ArkiraLeaveRestrictions",
                column: "LeavePolicyId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraLeavePolicies_ArkiraLeaveTypes_LeaveTypeId",
                table: "ArkiraLeavePolicies",
                column: "LeaveTypeId",
                principalTable: "ArkiraLeaveTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraLeavePolicyAccountsHistory_ArkiraLeavePolicies_LeavePolicyId",
                table: "ArkiraLeavePolicyAccountsHistory",
                column: "LeavePolicyId",
                principalTable: "ArkiraLeavePolicies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraLeavePolicies_ArkiraLeaveTypes_LeaveTypeId",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraLeavePolicyAccountsHistory_ArkiraLeavePolicies_LeavePolicyId",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropTable(
                name: "ArkiraLeaveRestrictions");

            migrationBuilder.DropIndex(
                name: "IX_ArkiraLeavePolicyAccountsHistory_LeavePolicyId",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropColumn(
                name: "ActionDate",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ArkiraLeavePolicyAccountsHistory");

            migrationBuilder.DropColumn(
                name: "IsUsable",
                table: "ArkiraLeavePolicyAccounts");

            migrationBuilder.DropColumn(
                name: "DaysToEnrollment",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropColumn(
                name: "HireDate",
                table: "ArkiraEmployeeBasics");

            migrationBuilder.RenameColumn(
                name: "LeavePolicyId",
                table: "ArkiraLeavePolicyAccountsHistory",
                newName: "LeavePolicyAccountId");

            migrationBuilder.RenameColumn(
                name: "LeavePolicyId",
                table: "ArkiraLeaveApplications",
                newName: "LeaveTypeId");

            migrationBuilder.AddColumn<int>(
                name: "ActionValue",
                table: "ArkiraLeavePolicyAccountsHistory",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ArkiraLeaveRestrictionProfiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LeaveRestrictionAction = table.Column<int>(type: "int", nullable: false),
                    LeaveRestrictionOptions = table.Column<int>(type: "int", nullable: false),
                    LeaveTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OptionsPrimaryValue = table.Column<int>(type: "int", nullable: false),
                    OptionsSecondaryValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraLeaveRestrictionProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArkiraLeaveRestrictionProfiles_ArkiraLeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "ArkiraLeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeavePolicyAccountsHistory_LeavePolicyAccountId_EmployeeId",
                table: "ArkiraLeavePolicyAccountsHistory",
                columns: new[] { "LeavePolicyAccountId", "EmployeeId" });

            migrationBuilder.CreateIndex(
                name: "IX_ArkiraLeaveRestrictionProfiles_LeaveTypeId",
                table: "ArkiraLeaveRestrictionProfiles",
                column: "LeaveTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraLeavePolicies_ArkiraLeaveTypes_LeaveTypeId",
                table: "ArkiraLeavePolicies",
                column: "LeaveTypeId",
                principalTable: "ArkiraLeaveTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraLeavePolicyAccountsHistory_ArkiraLeavePolicyAccounts_LeavePolicyAccountId_EmployeeId",
                table: "ArkiraLeavePolicyAccountsHistory",
                columns: new[] { "LeavePolicyAccountId", "EmployeeId" },
                principalTable: "ArkiraLeavePolicyAccounts",
                principalColumns: new[] { "EmployeeId", "LeavePolicyId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
