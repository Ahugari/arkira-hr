﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedEmployeeLeavePolicyModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraEmployeeBasics_EntitledEmployeesId",
                table: "ArkiraEmployeeLeavePolicyEntitlements");

            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraLeavePolicies_LeavePolicyEntitlementsId",
                table: "ArkiraEmployeeLeavePolicyEntitlements");

            migrationBuilder.RenameColumn(
                name: "LeavePolicyEntitlementsId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "LeavePolicyId");

            migrationBuilder.RenameColumn(
                name: "EntitledEmployeesId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "EmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_ArkiraEmployeeLeavePolicyEntitlements_LeavePolicyEntitlementsId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "IX_ArkiraEmployeeLeavePolicyEntitlements_LeavePolicyId");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraEmployeeBasics_EmployeeId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                column: "EmployeeId",
                principalTable: "ArkiraEmployeeBasics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraLeavePolicies_LeavePolicyId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                column: "LeavePolicyId",
                principalTable: "ArkiraLeavePolicies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraEmployeeBasics_EmployeeId",
                table: "ArkiraEmployeeLeavePolicyEntitlements");

            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraLeavePolicies_LeavePolicyId",
                table: "ArkiraEmployeeLeavePolicyEntitlements");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "ArkiraEmployeeLeavePolicyEntitlements");

            migrationBuilder.RenameColumn(
                name: "LeavePolicyId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "LeavePolicyEntitlementsId");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "EntitledEmployeesId");

            migrationBuilder.RenameIndex(
                name: "IX_ArkiraEmployeeLeavePolicyEntitlements_LeavePolicyId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                newName: "IX_ArkiraEmployeeLeavePolicyEntitlements_LeavePolicyEntitlementsId");

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraEmployeeBasics_EntitledEmployeesId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                column: "EntitledEmployeesId",
                principalTable: "ArkiraEmployeeBasics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraEmployeeLeavePolicyEntitlements_ArkiraLeavePolicies_LeavePolicyEntitlementsId",
                table: "ArkiraEmployeeLeavePolicyEntitlements",
                column: "LeavePolicyEntitlementsId",
                principalTable: "ArkiraLeavePolicies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
