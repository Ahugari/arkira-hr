﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedEffectiveDateAndIsLatestOnLeavePolicyEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EffectiveDate",
                table: "ArkiraLeavePolicies",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsLatest",
                table: "ArkiraLeavePolicies",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EffectiveDate",
                table: "ArkiraLeavePolicies");

            migrationBuilder.DropColumn(
                name: "IsLatest",
                table: "ArkiraLeavePolicies");
        }
    }
}
