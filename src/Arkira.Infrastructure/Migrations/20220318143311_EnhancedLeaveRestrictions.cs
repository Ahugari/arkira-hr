﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class EnhancedLeaveRestrictions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RestrictionSecondaryValue",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.RenameColumn(
                name: "RestrictionPrimaryValue",
                table: "ArkiraLeaveRestrictions",
                newName: "PeriodBeforeApplicationAcceptance");

            migrationBuilder.AddColumn<int>(
                name: "ApplicationLengthConstraint",
                table: "ArkiraLeaveRestrictions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DatePeriodFormat",
                table: "ArkiraLeaveRestrictions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DaysLimit",
                table: "ArkiraLeaveRestrictions",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationLengthConstraint",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.DropColumn(
                name: "DatePeriodFormat",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.DropColumn(
                name: "DaysLimit",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.RenameColumn(
                name: "PeriodBeforeApplicationAcceptance",
                table: "ArkiraLeaveRestrictions",
                newName: "RestrictionPrimaryValue");

            migrationBuilder.AddColumn<string>(
                name: "RestrictionSecondaryValue",
                table: "ArkiraLeaveRestrictions",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
