﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class MarkedCompositeKeysOnLeaveRestrictionsEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ArkiraLeaveRestrictions",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArkiraLeaveRestrictions",
                table: "ArkiraLeaveRestrictions",
                columns: new[] { "LeaveRestrictionOption", "LeavePolicyId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ArkiraLeaveRestrictions",
                table: "ArkiraLeaveRestrictions");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArkiraLeaveRestrictions",
                table: "ArkiraLeaveRestrictions",
                column: "Id");
        }
    }
}
