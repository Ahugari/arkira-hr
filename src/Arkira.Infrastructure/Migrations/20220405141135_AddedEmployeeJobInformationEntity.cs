﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedEmployeeJobInformationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "EmployeeJobInformation",
                table: "ArkiraEmployeeBasics",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "JobInformation",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    JobTitleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobInformation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobInformation_ArkiraEmployeeBasics_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "ArkiraEmployeeBasics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobInformation_ArkiraJobTitles_JobTitleId",
                        column: x => x.JobTitleId,
                        principalTable: "ArkiraJobTitles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobInformation_EmployeeId",
                table: "JobInformation",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobInformation_JobTitleId",
                table: "JobInformation",
                column: "JobTitleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobInformation");

            migrationBuilder.DropColumn(
                name: "EmployeeJobInformation",
                table: "ArkiraEmployeeBasics");
        }
    }
}
