﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class EnhancedLeaveApplicationToUseIApplicationProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobInformation_ArkiraJobTitles_JobTitleId",
                table: "JobInformation");

            migrationBuilder.DropIndex(
                name: "IX_JobInformation_JobTitleId",
                table: "JobInformation");

            migrationBuilder.AddColumn<string>(
                name: "Actions",
                table: "ArkiraLeaveApplications",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Actions",
                table: "ArkiraLeaveApplications");

            migrationBuilder.CreateIndex(
                name: "IX_JobInformation_JobTitleId",
                table: "JobInformation",
                column: "JobTitleId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobInformation_ArkiraJobTitles_JobTitleId",
                table: "JobInformation",
                column: "JobTitleId",
                principalTable: "ArkiraJobTitles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
