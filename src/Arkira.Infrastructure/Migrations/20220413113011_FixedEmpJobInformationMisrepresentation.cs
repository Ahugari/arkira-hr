﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class FixedEmpJobInformationMisrepresentation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobInformation_ArkiraEmployeeBasics_EmployeeId",
                table: "JobInformation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobInformation",
                table: "JobInformation");

            migrationBuilder.RenameTable(
                name: "JobInformation",
                newName: "ArkiraEmployeeJobInfo");

            migrationBuilder.RenameIndex(
                name: "IX_JobInformation_EmployeeId",
                table: "ArkiraEmployeeJobInfo",
                newName: "IX_ArkiraEmployeeJobInfo_EmployeeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArkiraEmployeeJobInfo",
                table: "ArkiraEmployeeJobInfo",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ArkiraEmployeeJobInfo_ArkiraEmployeeBasics_EmployeeId",
                table: "ArkiraEmployeeJobInfo",
                column: "EmployeeId",
                principalTable: "ArkiraEmployeeBasics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArkiraEmployeeJobInfo_ArkiraEmployeeBasics_EmployeeId",
                table: "ArkiraEmployeeJobInfo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArkiraEmployeeJobInfo",
                table: "ArkiraEmployeeJobInfo");

            migrationBuilder.RenameTable(
                name: "ArkiraEmployeeJobInfo",
                newName: "JobInformation");

            migrationBuilder.RenameIndex(
                name: "IX_ArkiraEmployeeJobInfo_EmployeeId",
                table: "JobInformation",
                newName: "IX_JobInformation_EmployeeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobInformation",
                table: "JobInformation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JobInformation_ArkiraEmployeeBasics_EmployeeId",
                table: "JobInformation",
                column: "EmployeeId",
                principalTable: "ArkiraEmployeeBasics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
