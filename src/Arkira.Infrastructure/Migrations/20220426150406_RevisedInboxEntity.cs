﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class RevisedInboxEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "WorkId",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "WorkType",
                table: "ArkiraInbox");

            migrationBuilder.AlterColumn<string>(
                name: "Section",
                table: "ArkiraInbox",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Actions",
                table: "ArkiraInbox",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Category",
                table: "ArkiraInbox",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ArkiraInbox",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemId",
                table: "ArkiraInbox",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "ArkiraInbox",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TargetId",
                table: "ArkiraInbox",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Actions",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "ArkiraInbox");

            migrationBuilder.DropColumn(
                name: "TargetId",
                table: "ArkiraInbox");

            migrationBuilder.AlterColumn<int>(
                name: "Section",
                table: "ArkiraInbox",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldMaxLength: 30);

            migrationBuilder.AddColumn<Guid>(
                name: "EmployeeId",
                table: "ArkiraInbox",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "WorkId",
                table: "ArkiraInbox",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "WorkType",
                table: "ArkiraInbox",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
