﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class RevisedNotificationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "From",
                table: "ArkiraNotification");

            migrationBuilder.DropColumn(
                name: "Target",
                table: "ArkiraNotification");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "ArkiraNotification",
                newName: "TargetId");

            migrationBuilder.RenameColumn(
                name: "Category",
                table: "ArkiraNotification",
                newName: "ContentOrigin");

            migrationBuilder.AddColumn<bool>(
                name: "IsRead",
                table: "ArkiraNotification",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRead",
                table: "ArkiraNotification");

            migrationBuilder.RenameColumn(
                name: "TargetId",
                table: "ArkiraNotification",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "ContentOrigin",
                table: "ArkiraNotification",
                newName: "Category");

            migrationBuilder.AddColumn<string>(
                name: "From",
                table: "ArkiraNotification",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Target",
                table: "ArkiraNotification",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
