﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedApplicationHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ArkiraApplicationsHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ApplicationId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ApplicationTypeName = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ActivityName = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ActivityPerformerId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ActivityPerformerName = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ActivityRequiresCompletion = table.Column<bool>(type: "bit", nullable: false),
                    ActivityCompleted = table.Column<bool>(type: "bit", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraApplicationsHistory", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraApplicationsHistory");
        }
    }
}
