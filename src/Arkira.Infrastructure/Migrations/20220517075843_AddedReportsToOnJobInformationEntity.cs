﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedReportsToOnJobInformationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmployeeJobInformation",
                table: "ArkiraEmployeeBasics");

            migrationBuilder.AddColumn<Guid>(
                name: "ReportsTo",
                table: "ArkiraEmployeeJobInfo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReportsTo",
                table: "ArkiraEmployeeJobInfo");

            migrationBuilder.AddColumn<Guid>(
                name: "EmployeeJobInformation",
                table: "ArkiraEmployeeBasics",
                type: "uniqueidentifier",
                nullable: true);
        }
    }
}
