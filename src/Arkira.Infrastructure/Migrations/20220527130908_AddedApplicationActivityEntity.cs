﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class AddedApplicationActivityEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ActivityRequiresCompletion",
                table: "ArkiraApplicationsHistory",
                newName: "UserActionsRequired");

            migrationBuilder.RenameColumn(
                name: "ActivityCompleted",
                table: "ArkiraApplicationsHistory",
                newName: "UserActionsCompleted");

            migrationBuilder.AddColumn<string>(
                name: "OngoingActivityId",
                table: "ArkiraLeaveApplications",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CorrelationId",
                table: "ArkiraApplicationsHistory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ArkiraApplicationActivities",
                columns: table => new
                {
                    ActivityId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Actions = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ApplicationId = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Reviewers = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArkiraApplicationActivities", x => x.ActivityId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArkiraApplicationActivities");

            migrationBuilder.DropColumn(
                name: "OngoingActivityId",
                table: "ArkiraLeaveApplications");

            migrationBuilder.DropColumn(
                name: "CorrelationId",
                table: "ArkiraApplicationsHistory");

            migrationBuilder.RenameColumn(
                name: "UserActionsRequired",
                table: "ArkiraApplicationsHistory",
                newName: "ActivityRequiresCompletion");

            migrationBuilder.RenameColumn(
                name: "UserActionsCompleted",
                table: "ArkiraApplicationsHistory",
                newName: "ActivityCompleted");
        }
    }
}
