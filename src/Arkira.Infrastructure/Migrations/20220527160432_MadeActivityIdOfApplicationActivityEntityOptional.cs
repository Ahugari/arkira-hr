﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class MadeActivityIdOfApplicationActivityEntityOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ArkiraApplicationActivities",
                table: "ArkiraApplicationActivities");

            migrationBuilder.AlterColumn<string>(
                name: "ActivityId",
                table: "ArkiraApplicationActivities",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArkiraApplicationActivities",
                table: "ArkiraApplicationActivities",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ArkiraApplicationActivities",
                table: "ArkiraApplicationActivities");

            migrationBuilder.AlterColumn<string>(
                name: "ActivityId",
                table: "ArkiraApplicationActivities",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArkiraApplicationActivities",
                table: "ArkiraApplicationActivities",
                column: "ActivityId");
        }
    }
}
