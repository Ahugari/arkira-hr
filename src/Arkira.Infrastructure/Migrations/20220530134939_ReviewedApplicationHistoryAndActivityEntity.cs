﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arkira.Infrastructure.Migrations
{
    public partial class ReviewedApplicationHistoryAndActivityEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "ArkiraApplicationsHistory");

            migrationBuilder.DropColumn(
                name: "UserActionsCompleted",
                table: "ArkiraApplicationsHistory");

            migrationBuilder.RenameColumn(
                name: "CorrelationId",
                table: "ArkiraApplicationsHistory",
                newName: "ReviewActivityId");

            migrationBuilder.RenameColumn(
                name: "ApplicationTypeName",
                table: "ArkiraApplicationsHistory",
                newName: "ApplicationType");

            migrationBuilder.RenameColumn(
                name: "ActivityPerformerName",
                table: "ArkiraApplicationsHistory",
                newName: "ActionPerformer");

            migrationBuilder.RenameColumn(
                name: "ActivityPerformerId",
                table: "ArkiraApplicationsHistory",
                newName: "ActionPerformerId");

            migrationBuilder.RenameColumn(
                name: "ActivityName",
                table: "ArkiraApplicationsHistory",
                newName: "ActionPerformed");

            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "ArkiraApplicationActivities",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Outcome",
                table: "ArkiraApplicationActivities",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "ArkiraApplicationActivities");

            migrationBuilder.DropColumn(
                name: "Outcome",
                table: "ArkiraApplicationActivities");

            migrationBuilder.RenameColumn(
                name: "ReviewActivityId",
                table: "ArkiraApplicationsHistory",
                newName: "CorrelationId");

            migrationBuilder.RenameColumn(
                name: "ApplicationType",
                table: "ArkiraApplicationsHistory",
                newName: "ApplicationTypeName");

            migrationBuilder.RenameColumn(
                name: "ActionPerformerId",
                table: "ArkiraApplicationsHistory",
                newName: "ActivityPerformerId");

            migrationBuilder.RenameColumn(
                name: "ActionPerformer",
                table: "ArkiraApplicationsHistory",
                newName: "ActivityPerformerName");

            migrationBuilder.RenameColumn(
                name: "ActionPerformed",
                table: "ArkiraApplicationsHistory",
                newName: "ActivityName");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "ArkiraApplicationsHistory",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "UserActionsCompleted",
                table: "ArkiraApplicationsHistory",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
