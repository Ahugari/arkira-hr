﻿using Arkira.Core.PublicHolidays;
using Arkira.Infrastructure.Repositories;
using System;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.PublicHolidays
{
    public class EfCorePublicHolidayRepository : Repository<ArkiraDbContext, PublicHoliday>, IPublicHolidayRepository
    {
        public EfCorePublicHolidayRepository(ArkiraDbContext context) : base(context)
        {
        }

        public override Task<PublicHoliday> GetPagedAsync(int skipCount, int maxResultCount, int sorting)
        {
            throw new NotImplementedException();
        }
    }
}