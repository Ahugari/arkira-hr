﻿using Arkira.Core;
using Arkira.Core.Entities;
using Arkira.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public class GenericCreateUpdateDeleteRepository<TEntity> : IGenericCreateUpdateDeleteRepository<TEntity>
        where TEntity : Entity<Guid>
    {
        private readonly ISave<TEntity> _create;
        private readonly IRead<TEntity> _read;
        private readonly IDelete<TEntity> _delete;
        private readonly IArkiraDbContext _context;

        public GenericCreateUpdateDeleteRepository(ISave<TEntity> create, IRead<TEntity> read, IDelete<TEntity> delete, IArkiraDbContext context)
        {
            _create = create;
            _read = read;
            _delete = delete;
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IEnumerable<TEntity> ReadAll()
        {
            return _read.ReadAll();
        }

        public async Task<TEntity> ReadOneAsync(Guid id)
        {
            return await _read.ReadOneAsync(id);
        }

        public async Task<TEntity> ReadOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _read.ReadOneAsync(predicate);
        }

        public async Task<TEntity> RemoveAsync(TEntity entity)
        {
            return await _delete.RemoveAsync(entity);
        }

        public async Task<TEntity> SaveAsync(TEntity entity)
        {
            return await _create.SaveAsync(entity);
        }
    }
}