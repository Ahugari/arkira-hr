﻿using Arkira.Core.Entities;
using Arkira.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public class ReadTransactional<TEntity> : IRead<TEntity> where TEntity : Entity<Guid>
    {
        private readonly ArkiraDbContext _context;

        public ReadTransactional(ArkiraDbContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> ReadAll()
        {
            return _context.Set<TEntity>().Select(x => x).AsEnumerable();
        }

        public async Task<TEntity> ReadOneAsync(Guid id)
        {
            var entityInDatabase = await _context.Set<TEntity>().FindAsync(id);
            if (entityInDatabase == null)
                throw new InvalidOperationException($"{typeof(TEntity).Name} doesn't exist;");
            return entityInDatabase;
        }

        public async Task<TEntity> ReadOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> ReadManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().Where(predicate).ToListAsync();
        }
    }
}