﻿using Arkira.Core.Entities;
using Arkira.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public class RemoveTransactional<TEntity> : IDelete<TEntity> where TEntity : Entity<Guid>
    {
        private readonly ArkiraDbContext _context;

        public RemoveTransactional(ArkiraDbContext context)
        {
            _context = context;
        }

        public async Task<TEntity> RemoveAsync(TEntity entity)
        {
            var entityInDatabase = await _context.FindAsync<TEntity>(entity.Id);

            if (entityInDatabase != null)
                _context.Remove(entityInDatabase);

            return entityInDatabase;
        }
    }
}