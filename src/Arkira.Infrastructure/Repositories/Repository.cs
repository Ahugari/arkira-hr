﻿using Arkira.Core.Entities;
using Arkira.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public abstract class Repository<KDbContext, TEntity> : IGenericRepository<TEntity> where TEntity : Entity<Guid> where KDbContext : ArkiraDbContext
    {
        protected readonly KDbContext _context;

        protected Repository(KDbContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public virtual void Add(TEntity entity)
        {
            _context.Add(entity);
        }

        public virtual async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> FindAsync(Guid id)
        {
            var entityInDatabase = await _context.Set<TEntity>().FindAsync(id);
            if (entityInDatabase == null)
                throw new InvalidOperationException($"{typeof(TEntity).Name} doesn't exist;");
            return entityInDatabase;
        }

        public virtual async Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().Where(predicate).Select(x => x).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync() => await _context.Set<TEntity>().ToListAsync();

        public virtual async Task<int> GetCountAsync()
        {
            return await _context.Set<TEntity>().CountAsync();
        }

        public abstract Task<TEntity> GetPagedAsync(int skipCount, int maxResultCount, int sorting);

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            var entityInDataBase = await _context.Set<TEntity>().FindAsync(entity.Id);

            if (entityInDataBase != null)
                throw new InvalidOperationException($"{typeof(TEntity).Name} already exists.");

            await _context.AddAsync(entity);

            return entity;
        }

        public virtual async Task RemoveAsync(Guid id)
        {
            var entityInDatabase = await _context.FindAsync<TEntity>(id);

            if (entityInDatabase == null)
                return;

            _context.Remove(entityInDatabase);
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            var entityInDatabase = await _context.FindAsync<TEntity>(entity.Id);

            if (entityInDatabase == null)
                return null;

            _context.Update(entityInDatabase);

            return entity;
        }
    }
}