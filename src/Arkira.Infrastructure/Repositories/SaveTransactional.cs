﻿using Arkira.Core.Entities;
using Arkira.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public class SaveTransactional<TEntity> : ISave<TEntity> where TEntity : Entity<Guid>
    {
        private readonly ArkiraDbContext _context;

        public SaveTransactional(ArkiraDbContext context)
        {
            _context = context;
        }

        public async Task<TEntity> SaveAsync(TEntity entity)
        {
            if (entity.IsTransient())
                return await CreateAsync(entity);

            _context.Update(entity);

            return entity;
        }

        private async Task<TEntity> CreateAsync(TEntity entity)
        {
            await _context.AddAsync(entity);

            return entity;
        }
    }
}