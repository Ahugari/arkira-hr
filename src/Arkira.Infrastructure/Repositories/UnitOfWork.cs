﻿using Arkira.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContextFactory<ArkiraDbContext> _contextFactory;
        private readonly ArkiraDbContext _dbContext;

        public UnitOfWork(IDbContextFactory<ArkiraDbContext> dbContextFactory)
        {
            _contextFactory = dbContextFactory;
        }

        private async Task<ArkiraDbContext> GetDbContextAsync()
        {
            await using ArkiraDbContext dbContext = _contextFactory.CreateDbContext();
            return _dbContext ?? dbContext;
        }

        public async Task<bool> CompleteAsync()
        {
            bool result;
            var dbcontext = await GetDbContextAsync();
            try
            {
                result = await dbcontext.SaveChangesAsync() >= 1;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    entry.State = EntityState.Detached;
                }
                throw;
            }

            return result;
        }
    }
}