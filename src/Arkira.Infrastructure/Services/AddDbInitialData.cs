﻿using Arkira.Core;
using Arkira.Core.ApplicationActivities;
using Arkira.Core.ApplicationsHistory;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveTypes;
using Arkira.Core.PublicHolidays;
using Arkira.Core.Services;
using Arkira.Infrastructure.ApplicationActivities;
using Arkira.Infrastructure.ApplicationsHistory;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.EmployeeJobInformation;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeaveApplications;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.PublicHolidays;
using Arkira.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Infrastructure.Services
{
    internal class AddDbInitialData : IHostedService
    {
        private IJobTitleRepository _jobTitleRepository;
        private IEmployeeRepository _employeeRepository;
        private IEmployeeDomainService _employeeDomainService;
        private EmployeeJobInfoManager _employeeJobInfoManager;
        private IEmployeeJobInfoRepository _empJobInfoRepository;
        private IPublicHolidayRepository _publicHolidayRepository;
        private ILeaveTypeRepository _leaveTypeRepository;
        private ILeavePolicyRepository _leavePolicyRepository;
        private ILeaveTypeDomainService _leaveTypeDomainService;
        private LeavePolicyDomainService _leavePolicyDomainService;
        private ILeavePolicyAccountHistoryRepository _leavePolicyAccountHistoryRepository;
        private LeavePolicyAccountHistoryDomainService _leaveAccountHistoryDomainService;
        private LeaveApplicationDomainService _leaveApplicationDomainService;
        private EfCoreApplicationActivityRepository _applicationActivityRepository;
        private EfCoreApplicationHistoryRepository _applicationHistoryRepository;
        private ILeaveApplicationRepository _leaveApplicationRepository;
        private readonly IServiceScopeFactory _serviceProvider;

        public AddDbInitialData(IServiceScopeFactory serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                SystemClock clock = new();
                var dbContext = scope.ServiceProvider.GetService<ArkiraDbContext>();
                var mediator = scope.ServiceProvider.GetService<IAppMediator>();
                _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(dbContext), new ReadTransactional<JobTitle>(dbContext), dbContext);
                _employeeRepository = new EfCoreEmployeeRepository(dbContext);
                _employeeDomainService = new EmployeeDomainService(clock, _employeeRepository, _jobTitleRepository);
                _empJobInfoRepository = new EfCoreEmployeeJobInfoRepository(new SaveTransactional<EmployeeJobInfo>(dbContext), new ReadTransactional<EmployeeJobInfo>(dbContext), new RemoveTransactional<EmployeeJobInfo>(dbContext), dbContext);
                _employeeJobInfoManager = new EmployeeJobInfoManager(_empJobInfoRepository, _jobTitleRepository, _employeeRepository);
                _publicHolidayRepository = new EfCorePublicHolidayRepository(dbContext);
                _leaveTypeRepository = new EfCoreLeaveTypeRepository(dbContext);
                _leavePolicyRepository = new EfCoreLeavePolicyRepository(dbContext);
                _leaveTypeDomainService = new LeaveTypeDomainService(_leaveTypeRepository, _leavePolicyRepository, _employeeRepository);
                _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository, _leaveTypeRepository, _employeeRepository, mediator);
                _leavePolicyAccountHistoryRepository = new EfCoreLeavePolicyAccountHistoryRepository(dbContext);
                _leaveAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(_leavePolicyAccountHistoryRepository, _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);
                _leaveApplicationRepository = new EfCoreLeaveApplicationRepository(dbContext);
                _leaveApplicationDomainService = new LeaveApplicationDomainService(_leavePolicyRepository, _leaveAccountHistoryDomainService, _employeeRepository, _leaveTypeRepository, _leaveApplicationRepository, _publicHolidayRepository, clock, mediator);
                _applicationActivityRepository = new EfCoreApplicationActivityRepository(new ReadTransactional<ApplicationActivity>(dbContext), new SaveTransactional<ApplicationActivity>(dbContext), dbContext);
                _applicationHistoryRepository = new EfCoreApplicationHistoryRepository(new ReadTransactional<ApplicationHistory>(dbContext), new SaveTransactional<ApplicationHistory>(dbContext), dbContext);

                Console.WriteLine($"Seeding initial data...");

                IEnumerable<JobTitle> jobTitles = _jobTitleRepository.ReadAll();
                if (!jobTitles.Any()) await PopulateJobTitleDbAsync();

                var employees = await _employeeRepository.GetAllAsync();
                if (!employees.Any()) await PopulateEmployeeDbAsync();

                var jobInfo = _empJobInfoRepository.ReadAll();
                if (!jobInfo.Any()) await PopulateEmployeeJobInfo();

                var leaveTypes = await _leaveTypeRepository.GetAllAsync();
                LeaveType leaveType = !leaveTypes.Any() ? await CreateLeaveType() : leaveTypes.FirstOrDefault();

                var leavePolicies = await _leavePolicyRepository.GetAllWithRestrictions();
                LeavePolicy leavePolicy = !leavePolicies.Any() ? await CreateLeavePolicy(clock, leaveType) : leavePolicies.FirstOrDefault();

                var leavePolicyHistory = await _leavePolicyAccountHistoryRepository.GetAllAsync();
                if (!leavePolicyHistory.Any()) await CreateLeavePolicyHistoryAsync(clock, employees, leavePolicy);

                var leaveApplication = await _leaveApplicationRepository.GetAllAsync();
                if (!leaveApplication.Any()) await CreateLeaveApplication(employees, leavePolicy);

                //var applicationActivity = _applicationActivityRepository.ReadAll();
                //if (!applicationActivity.Any()) await _applicationActivityRepository.SaveAsync(new ApplicationActivity(,));

                Console.WriteLine($"Successfuly seeded initial data.");
            }
        }

        private async Task CreateLeaveApplication(IEnumerable<Employee> employees, LeavePolicy leavePolicy)
        {
            var application = await _leaveApplicationDomainService.CreateAsync(leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), employees.LastOrDefault().Id, employees.FirstOrDefault().Id, null, null, null, null, ApplicationStatus.Submitted);
            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            //For Testing purposes...
            Console.WriteLine($"Seeded Application Id:{application.Application.Id}");
        }

        private async Task CreateLeavePolicyHistoryAsync(SystemClock clock, IEnumerable<Employee> employees, LeavePolicy leavePolicy)
        {
            _leaveAccountHistoryDomainService.Create(employees.FirstOrDefault().Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED, clock.UtcNow, leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();
        }

        private async Task<LeavePolicy> CreateLeavePolicy(SystemClock clock, LeaveType leaveType)
        {
            var leavePolicy = await _leavePolicyDomainService.CreateAsync("Vacation", leaveType.Id, false, true, leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, clock.UtcNow, null);

            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            return leavePolicy;
        }

        private async Task<LeaveType> CreateLeaveType()
        {
            var leaveType = await _leaveTypeDomainService.CreateAsync("Annual Leave", EntitlementType.Standard, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);

            await _leaveTypeRepository.UnitOfWork.CompleteAsync();
            return leaveType;
        }

        private async Task PopulateEmployeeJobInfo()
        {
            var employeeList = (await _employeeRepository.GetAllAsync()).Select(x => x.Id).ToList();
            var jobTitles = _jobTitleRepository.ReadAll().ToList();
            var numberGenerator = new Random();

            foreach (var employeeId in employeeList)
            {
                var jobIndex = numberGenerator.Next(0, jobTitles.Count - 1);
                var empIndex = numberGenerator.Next(0, employeeList.Count - 1);
                await CreateEmployeeJobInfoAsync(employeeId, jobTitles[jobIndex].Id, employeeList[empIndex]);
                jobTitles.RemoveAt(jobIndex);
                employeeList.RemoveAt(empIndex);
            }
        }

        private async Task CreateEmployeeJobInfoAsync(Guid employeeId, Guid jobTitleId, Guid reportsToId)
        {
            var empJobInfo = await _employeeJobInfoManager.CreateAsync(employeeId, jobTitleId, reportsToId);
            await _empJobInfoRepository.SaveAsync(empJobInfo);
            await _empJobInfoRepository.UnitOfWork.CompleteAsync();
        }

        private async Task PopulateEmployeeDbAsync()
        {
            await CreateEmployeeAsync("1", "John", null, "Doe", null, Gender.Male, MaritalStatus.Married, "123", null, null, null, DateTime.Now.AddYears(-20), DateTime.Now);
            await CreateEmployeeAsync("2", "Estelle", null, "Vida", null, Gender.Female, MaritalStatus.Single, "123", null, null, null, DateTime.Now.AddYears(-20), DateTime.Now);
            await CreateEmployeeAsync("3", "Van", null, "Hoffeinheim", null, Gender.Male, MaritalStatus.Single, "123", null, null, null, DateTime.Now.AddYears(-20), DateTime.Now);
            await CreateEmployeeAsync("4", "Loius", null, "C.K", null, Gender.Male, MaritalStatus.Single, "123", null, null, null, DateTime.Now.AddYears(-40), DateTime.Now);
            await CreateEmployeeAsync("5", "Kisha", null, "L.", null, Gender.Female, MaritalStatus.Married, "123", null, null, null, DateTime.Now.AddYears(-40), DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();
        }

        private async Task CreateEmployeeAsync(string employeeNumber, string firstName, string middleName, string lastName, string preferredName, Gender gender, MaritalStatus martitalStatus, string nIN, int? nSSF, int? tIN, ShirtSize? shirtSize, DateTime birthDate, DateTime hireDate)
        {
            await _employeeDomainService.CreateAsync(employeeNumber, firstName, middleName, lastName, preferredName, gender, martitalStatus, nIN, nSSF, tIN, shirtSize, birthDate, hireDate);
        }

        private async Task PopulateJobTitleDbAsync()
        {
            await CreateJobTitleAsync("Head Human Resource");
            await CreateJobTitleAsync("Receptionist");
            await CreateJobTitleAsync("Director Finance");
            await CreateJobTitleAsync("Director Planning & Administration");
            await CreateJobTitleAsync("Manager Operations");
            await CreateJobTitleAsync("Executive Director");
            await _jobTitleRepository.UnitOfWork.CompleteAsync();
        }

        private async Task CreateJobTitleAsync(string name)
        {
            var jobTitle = new JobTitle();
            jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = name, Description = "" });
            await _jobTitleRepository.SaveAsync(jobTitle);
        }
    }
}