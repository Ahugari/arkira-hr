﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Arkira.Infrastructure
{
    internal class SqlServerArkiraDbContextFactory : IDesignTimeDbContextFactory<ArkiraDbContext>
    {
        public ArkiraDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ArkiraDbContext>();
            const string ConnectionString = "Server=the-unruly\\sqlexpress;Database=Arkira-HR;Trusted_Connection=True;MultipleActiveResultSets=true;";
            builder.UseSqlServer(ConnectionString);

            return new ArkiraDbContext(builder.Options);
        }
    }
}