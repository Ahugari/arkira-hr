﻿using Elsa.Activities.Signaling.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Web.Endpoints.Applications
{
    [ApiController]
    [Route("reviews")]
    public class Post : Controller
    {
        private readonly ISignaler _reviewReceivedInvoker;

        public Post(ISignaler reviewReceivedInvoker)
        {
            _reviewReceivedInvoker = reviewReceivedInvoker;
        }

        [HttpPost("{signal}")]
        public async Task<ActionResult> Handle([FromRoute] string signal, [FromBody] SignalPayload payload, CancellationToken cancellationToken)
        {
            var collectedWorkflows = await _reviewReceivedInvoker.TriggerSignalAsync(signal, payload.UserId, payload.WorkflowInstanceId, cancellationToken: cancellationToken);
            return Ok(collectedWorkflows.ToList());
        }
    }

    public class SignalPayload
    {
        public string WorkflowInstanceId { get; set; }
        public string UserId { get; set; }
    }
}