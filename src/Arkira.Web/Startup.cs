using Arkira.Core.Inboxes.Handlers;
using Arkira.Infrastructure.Extensions;
using Arkira.Web.Data;
using Arkira.Web.Extensions;
using Arkira.Workflows.Extensions;
using Arkira.Workflows.Infrastructure.Extensions;
using Elsa;
using Elsa.Activities.Email.Options;
using Elsa.Activities.Http.Options;
using Elsa.Persistence.EntityFramework.SqlServer;
using Elsa.Server.Hangfire.Extensions;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NodaTime;
using NodaTime.Serialization.JsonNet;

namespace Arkira.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddArkiraInfrastructureServices(connectionString);
            services.AddApplicationInfrastructureServices(connectionString);
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();

            services.AddNotificationHandlersFrom<AddApplicationToInbox>();

            AddHangfire(services, connectionString);

            AddWorkflowServices(services, connectionString);

            AddWorkflowDashboardServices(services);
        }

        private void AddWorkflowDashboardServices(IServiceCollection services)
        {
            services.AddRazorPages();
        }

        private void AddWorkflowServices(IServiceCollection services, string connectionString)
        {
            services.AddWorkflowServices(dbcontext => dbcontext.UseSqlServer(connectionString));

            services.AddWorkflowInfrastructureServices(connectionString);

            services.Configure<SmtpOptions>(options => Configuration.GetSection("Elsa:Smtp").Bind(options));

            services.Configure<HttpActivityOptions>(options => Configuration.GetSection("Elas:Server").Bind(options));

            services.AddElsaApiEndpoints();
        }

        private void AddHangfire(IServiceCollection services, string connectionString)
        {
            services.AddHangfire(config => config
                                    .UseSqlServerStorage(connectionString)
                                    .UseSimpleAssemblyNameTypeSerializer()
                                    .UseRecommendedSerializerSettings(settings => settings.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb)))

                    .AddHangfireServer((serviceProvider, options) =>
                    {
                        Configuration.GetSection("Hangfire").Bind(options);
                        options.ConfigureForElsaDispatchers(serviceProvider);
                    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseHttpActivities();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}