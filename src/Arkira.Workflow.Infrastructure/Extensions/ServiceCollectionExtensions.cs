﻿using Elsa.Persistence.EntityFramework.Core;
using Elsa.Persistence.EntityFramework.SqlServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Arkira.Workflows.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddWorkflowInfrastructureServices(this IServiceCollection services, string connectionString)
        {
            var assembly = typeof(SqlServerElsaContextFactory).Assembly.GetName().Name;
            return services
                .AddPooledDbContextFactory<ElsaContext>(options => options.UseSqlServer(connectionString, options => options.MigrationsAssembly(assembly)));
        }
    }
}