﻿using Elsa.Persistence.EntityFramework.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Arkira.Workflows.EfMigrator
{
    public class SqlServerElsaContextFactory : IDesignTimeDbContextFactory<ElsaContext>
    {
        public ElsaContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ElsaContext>();
            const string ConnectionString = "Server=the-unruly\\sqlexpress;Database=Arkira-HR;Trusted_Connection=True;MultipleActiveResultSets=true;";

            builder.UseSqlServer(ConnectionString, options => options.MigrationsAssembly(typeof(Elsa.Persistence.EntityFramework.SqlServer.SqlServerElsaContextFactory).Assembly.FullName));

            return new ElsaContext(builder.Options);
        }
    }
}