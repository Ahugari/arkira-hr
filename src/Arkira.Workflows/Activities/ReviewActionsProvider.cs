﻿using Arkira.Core.Applications;
using Elsa.Design;
using Elsa.Metadata;
using System.Collections.Generic;
using System.Reflection;

namespace Arkira.Workflows.Activities
{
    public class ReviewActionsProvider : IActivityPropertyOptionsProvider
    {
        public object GetOptions(PropertyInfo property)
        {
            return new List<SelectListItem>()
            {
                new SelectListItem { Text = ApplicationReviewActions.Recommend, Value = ApplicationReviewActions.Recommend },
                new SelectListItem { Text = ApplicationReviewActions.Approve, Value = ApplicationReviewActions.Approve },
                new SelectListItem { Text = ApplicationReviewActions.Reject, Value = ApplicationReviewActions.Reject },
                new SelectListItem { Text = ApplicationReviewActions.Withdraw, Value = ApplicationReviewActions.Withdraw }
            };
        }
    }
}