﻿using Arkira.Core;
using Arkira.Core.ApplicationActivities;
using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Helpers;
using Arkira.Core.LeaveApplications;
using Arkira.Core.Services;
using Arkira.Workflows.Models;
using Elsa.ActivityResults;
using Elsa.Attributes;
using Elsa.Design;
using Elsa.Expressions;
using Elsa.Services;
using Elsa.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Activities
{
    [Trigger(Category = "Applications", Description = "Handle application review signals basing on the user.")]
    public class ReviewSignalReceived : Activity
    {
        [ActivityOutput(Hint = "The input that was recieved with the signal.")]
        public object Output { get; set; }

        [ActivityInput(Label = "Review Activity Identifier",
            Hint = "Specify the Output of the previous review activity. (Usually 'input.activityId')",
            DefaultSyntax = SyntaxNames.Liquid,
            SupportedSyntaxes = new[] { SyntaxNames.Liquid })]
        public string ReviewActivityId { get; set; } = default!;

        [ActivityInput(Hint = "The name of the Signal to listen for.")]
        public string Signal { get; set; } = default!;

        [ActivityInput(Label = "Type of review to handle",
            UIHint = ActivityInputUIHints.Dropdown,
            OptionsProvider = typeof(ReviewActionsProvider),
            Hint = "Select the kind of action to perform against the application.",
            IsDesignerCritical = true,
            DefaultSyntax = SyntaxNames.Literal,
            SupportedSyntaxes = new[] { SyntaxNames.Literal })]
        public string ReviewAction { get; set; } = default!;

        private readonly IApplicationDomainManager _applicationDomainManager;
        private readonly ILeaveApplicationRepository _leaveApplicationRepository;
        private readonly IApplicationActivityRepository _activityRepository;
        private readonly IAppMediator _mediator;

        public ReviewSignalReceived(ILeaveApplicationRepository leaveApplicationRepository, IApplicationActivityRepository activityRepository, IApplicationDomainManager applicationDomainManager, IAppMediator mediator)
        {
            _leaveApplicationRepository = leaveApplicationRepository;
            _activityRepository = activityRepository;
            _applicationDomainManager = applicationDomainManager;
            _mediator = mediator;
        }

        protected override async ValueTask<IActivityExecutionResult> OnExecuteAsync(ActivityExecutionContext context)
        {
            if (context.WorkflowExecutionContext.IsFirstPass)
                return OnResume(context);

            return Suspend();
        }

        protected override async ValueTask<IActivityExecutionResult> OnResumeAsync(ActivityExecutionContext context)
        {
            //handle scenarios where multiple reviewers exist and in that case pass it onto the processapplication method and depending on the response,
            //choose to complete this activity or continue suspension.
            var signal = context.GetInput<ReviewSignal>();

            var application = await GetApplicationAsync(context.GetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_TAG).ToString());
            if (context.GetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_REVIEWERS_LIST) is not HashSet<Reviewer> reviewers)
                throw new InvalidOperationException("No available reviewers found.");

            var result = await _applicationDomainManager.ProcessAsync(application, signal.Input.ToString(), reviewers, ReviewAction, ReviewActivityId);

            if (!result.ProcessCompleted)
                throw new ActivityException("Error while processing application.", result.Error);

            if (!result.ReviewCompleted)
                return Suspend();

            await _mediator.Publish(new ApplicationProcessed<LeaveApplication>(application.Id.ToString(), ReviewAction, signal.Input.ToString(), application.ToString(), ReviewActivityId), CancellationToken.None);

            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            Output = signal.Input;
            context.LogOutputProperty(this, nameof(Output), Output);
            return Done();
        }

        protected override async ValueTask<bool> OnCanExecuteAsync(ActivityExecutionContext context)
        {
            if (context.Input is ReviewSignal triggeredSignal)
            {
                var reviewerList = (HashSet<Reviewer>)context.GetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_REVIEWERS_LIST);

                return string.Equals(triggeredSignal.SignalName, Signal, StringComparison.OrdinalIgnoreCase) &&
                    reviewerList.Any(x => x.Id.Equals(triggeredSignal.Input.ToString(), System.StringComparison.OrdinalIgnoreCase));
            }

            return false;
        }

        private async Task<LeaveApplication> GetApplicationAsync(string id)
        {
            Guid applicationId = Validate.AsGuid(id);

            var existingApplication = await _leaveApplicationRepository.FindAsync(applicationId);
            if (existingApplication == null)
                throw new InvalidOperationException("No valid application found!");

            return existingApplication;
        }
    }
}