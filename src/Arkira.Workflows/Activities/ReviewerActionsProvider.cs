﻿using Arkira.Core.Enums;
using Elsa.Metadata;
using System.Collections.Generic;
using System.Reflection;

namespace Arkira.Workflows.Activities
{
    public class ReviewerActionsProvider : IActivityPropertyOptionsProvider
    {
        public object GetOptions(PropertyInfo property)
        {
            return new List<string>()
            {
                ApplicationActionOptions.Deny.ToString(),
                ApplicationActionOptions.Recommend.ToString(),
                ApplicationActionOptions.Approve.ToString()
            };
        }
    }
}