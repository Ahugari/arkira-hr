﻿using Arkira.Core.Applications;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Elsa.Design;
using Elsa.Metadata;
using Open.Linq.AsyncExtensions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Activities
{
    public class ReviewerOptionsProvider : IActivityPropertyOptionsProvider, IRuntimeSelectListProvider
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IJobTitleRepository _jobTitleRepository;

        public ReviewerOptionsProvider(IJobTitleRepository jobTitleRepository, IEmployeeRepository employeeRepository)
        {
            _jobTitleRepository = jobTitleRepository;
            _employeeRepository = employeeRepository;
        }

        public object GetOptions(PropertyInfo property) => new RuntimeSelectListProviderSettings(GetType());

        public async ValueTask<SelectList> GetSelectListAsync(object context = null, CancellationToken cancellationToken = default)
        {
            SelectList slist = new();
            List<SelectListItem> selectListItems = new();

            selectListItems = selectListItems.Union(await GetJobTitlesSelectListAsync(cancellationToken)).ToList();
            selectListItems = selectListItems.Union(GetDirectAndIndirectReportsList()).ToList();
            selectListItems = selectListItems.Union(await GetEmployeeListAsync(cancellationToken)).ToList();

            slist.Items = selectListItems;

            return slist;
        }

        private List<SelectListItem> GetDirectAndIndirectReportsList()
        {
            var supervisor = "Supervisor";
            var manager = "manager";
            return new List<SelectListItem>
            {
                new SelectListItem { Text = supervisor, Value = IdTagService.EncodeAsTag(supervisor.ToLower(), IdTagService.EncodeAsTag(supervisor.ToLower())) },
                new SelectListItem { Text = manager , Value = IdTagService.EncodeAsTag(manager.ToLower(), IdTagService.EncodeAsTag(manager.ToLower()))   }
            };
        }

        private async Task<List<SelectListItem>> GetEmployeeListAsync(CancellationToken cancellationToken)
        {
            return await _employeeRepository.GetAllAsync()
                                                .Select(x => new SelectListItem { Text = x.GetFullName(), Value = IdTagService.EncodeAsTag(x.ToString(), x.Id.ToString()) })
                                                .ToList();
        }

        private async Task<List<EmployeesAndJobtitles>> GetEmployeesAndJobtitlesListAsync(CancellationToken cancellationToken)
        {
            List<EmployeesAndJobtitles> employeesAndJobtitles = await _employeeRepository.GetAllWithJobInformationAsync(cancellationToken)
                                                                                            .Select(x => new EmployeesAndJobtitles { EmployeeId = x.Id.ToString(), JobTitleId = x.GetLatestJobTitleId() })
                                                                                            .ToList();

            return employeesAndJobtitles;
        }

        private IEnumerable<JobTitle> GetJobTitlesFromEmployeeList(List<EmployeesAndJobtitles> employeesAndJobtitles)
        {
            var jobtitles = _jobTitleRepository.ReadAll();

            return jobtitles.Where(x => employeesAndJobtitles
                            .Any(y => y.JobTitleId == x.Id.ToString()))
                            .Select(x => x)
                            .ToList();
        }

        private async Task<IEnumerable<SelectListItem>> GetJobTitlesSelectListAsync(CancellationToken cancellationToken)
        {
            var availableEmployeesAndJobTitles = await GetEmployeesAndJobtitlesListAsync(cancellationToken);
            var jobTitlesList = GetJobTitlesFromEmployeeList(availableEmployeesAndJobTitles);
            var jobTitlesSelectList = jobTitlesList.Select(x => new SelectListItem { Text = x.Name, Value = IdTagService.EncodeAsTag(x.ToString(), x.Id.ToString()) });

            return jobTitlesSelectList;
        }

        public class EmployeesAndJobtitles
        {
            public string EmployeeId { get; set; }
            public string JobTitleId { get; set; }
        }
    }
}