﻿using Arkira.Core;
using Arkira.Core.Applications;
using Arkira.Core.Applications.Events;
using Arkira.Core.Helpers;
using Arkira.Core.LeaveApplications;
using Arkira.Core.Services;
using Elsa.ActivityResults;
using Elsa.Attributes;
using Elsa.Design;
using Elsa.Expressions;
using Elsa.Providers.WorkflowStorage;
using Elsa.Services;
using Elsa.Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arkira.Workflows.Activities
{
    public record ReviewActivity(string Id);

    [Activity(Category = "Leave Applications",
        DisplayName = "Send Application for Review",
        Description = "Provide the next reviewer details and specify email subscribers.",
        Outcomes = new string[] { })]
    public class SendApplicationForReview : Activity
    {
        private readonly IAppMediator mediator;
        private readonly IApplicationDomainManager _applicationDomainManager;
        private readonly ILeaveApplicationDomainService _leaveApplicationDomainService;
        private readonly ILeaveApplicationRepository _leaveApplicationRepository;

        public SendApplicationForReview(IAppMediator mediator,
            IApplicationDomainManager applicationDomainManager,
            ILeaveApplicationDomainService leaveApplicationDomainService,
            ILeaveApplicationRepository leaveApplicationRepository)
        {
            this.mediator = mediator;
            _applicationDomainManager = applicationDomainManager;
            _leaveApplicationDomainService = leaveApplicationDomainService;
            _leaveApplicationRepository = leaveApplicationRepository;
        }

        [ActivityInput(
            Label = "Review By",
            UIHint = ActivityInputUIHints.Dropdown,
            OptionsProvider = typeof(ReviewerOptionsProvider),
            DefaultSyntax = SyntaxNames.Literal,
            SupportedSyntaxes = new[] { SyntaxNames.Literal },
            IsDesignerCritical = true)]
        public string ReviewBy { get; set; } = default!;

        [ActivityInput(
            Name = "ReviewActions",
            Label = "Reviewer Actions List",
            UIHint = ActivityInputUIHints.MultiText,
            SupportedSyntaxes = new[] { SyntaxNames.Json },
            DefaultSyntax = "Json",
            OptionsProvider = typeof(ReviewActionsProvider),
            IsDesignerCritical = true,
            ConsiderValuesAsOutcomes = true)]
        public HashSet<string> ReviewActions { get; set; }

        [ActivityInput(
            Label = "Subcriber list",
            UIHint = ActivityInputUIHints.MultiText,
            SupportedSyntaxes = new[] { SyntaxNames.Json },
            DefaultSyntax = "Json",
            OptionsProvider = typeof(SubscriberOptionsProvider),
            IsDesignerCritical = true)]
        public HashSet<string> NotificationSubscribers { get; set; }

        [ActivityOutput(
            Hint = "Output the Id of this activity.",
            DefaultWorkflowStorageProvider = TransientWorkflowStorageProvider.ProviderName,
            DisableWorkflowProviderSelection = true
            )]
        public ReviewActivity Output { get; set; }

        protected override async ValueTask<IActivityExecutionResult> OnExecuteAsync(ActivityExecutionContext context)
        {
            var applicationId = context.GetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_TAG).ToString();

            if (applicationId == null)
                throw new InvalidCastException("Leave Application is null.");

            if (string.IsNullOrWhiteSpace(ReviewBy.ToString()) || ReviewActions.Count < 1)
                throw new InvalidOperationException("Acitivity cannot contain empty values.");

            var applicantId = (await _leaveApplicationDomainService.GetApplicantByApplicationIdAsync(applicationId)).Id;

            var application = await _leaveApplicationRepository.FindAsync(Transform.ToGuid(applicationId));
            application.UpdateOngoingActivity(context.ActivityId);

            var reviewers = await _applicationDomainManager.GetReviewersByTagAsync(ReviewBy, applicantId.ToString());

            context.SetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_REVIEWERS_LIST, reviewers);
            context.SetVariable(ArkiraConsts.APPLICATIONS_WORKFLOW_REVIEWER_ACTIONS_LIST, ReviewActions);

            NotificationSubscribers.RemoveWhere(x => ReviewBy.Contains(x));

            await mediator.Publish(
                new ApplicationReadyForReviewSend<LeaveApplication>(
                    new WorkflowReviewApplication(applicationId,
                                                  reviewers,
                                                  ReviewActions,
                                                  NotificationSubscribers,
                                                  context.ActivityId)));

            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            Output = new ReviewActivity(context.ActivityId);
            context.LogOutputProperty(this, nameof(Output), Output.Id);
            return Outcomes(ReviewActions);
        }
    }
}