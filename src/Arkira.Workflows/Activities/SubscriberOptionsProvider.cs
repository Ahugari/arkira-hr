﻿using Arkira.Core.EmployeeFields.JobTitles;
using Elsa.Design;
using Elsa.Metadata;
using System.Collections.Generic;
using System.Reflection;

namespace Arkira.Workflows.Activities
{
    public class SubscriberOptionsProvider : IActivityPropertyOptionsProvider
    {
        private readonly IJobTitleRepository _jobTitleRepository;

        public SubscriberOptionsProvider(IJobTitleRepository jobTitleRepository)
        {
            _jobTitleRepository = jobTitleRepository;
        }

        public object GetOptions(PropertyInfo property)
        {
            var jobTitles = _jobTitleRepository.ReadAll();
            List<SelectListItem> selectList = new();
            foreach (var title in jobTitles)
                selectList.Add(new SelectListItem
                {
                    Text = title.Name,
                    Value = title.Id.ToString()
                });

            return selectList;
        }
    }
}