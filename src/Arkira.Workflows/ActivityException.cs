﻿using System;

namespace Arkira.Workflows
{
    internal class ActivityException : Exception
    {
        public ActivityException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}