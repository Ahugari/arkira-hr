﻿using Elsa.Services;

namespace Arkira.Workflows.Bookmarks
{
    public record ReviewSignalReceivedBookmark(string Signal) : IBookmark;
}