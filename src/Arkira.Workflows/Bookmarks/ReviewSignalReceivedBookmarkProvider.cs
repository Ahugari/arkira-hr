﻿using Arkira.Workflows.Activities;
using Elsa.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Bookmarks
{
    public class ReviewSignalReceivedBookmarkProvider : BookmarkProvider<ReviewSignalReceivedBookmark, ReviewSignalReceived>
    {
        public override async ValueTask<IEnumerable<BookmarkResult>> GetBookmarksAsync(BookmarkProviderContext<ReviewSignalReceived> context, CancellationToken cancellationToken)
        {
            var signalSpecified = await context.ReadActivityPropertyAsync<ReviewSignalReceived, string>(x => x.Signal, cancellationToken);
            var validReviewSignalName = !string.IsNullOrWhiteSpace(signalSpecified) ? signalSpecified.ToLower() : "";
            return new[] { Result(new ReviewSignalReceivedBookmark(validReviewSignalName)) };
        }
    }
}