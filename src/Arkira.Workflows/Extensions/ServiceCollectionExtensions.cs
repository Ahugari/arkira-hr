﻿using Arkira.Workflows.Activities;
using Arkira.Workflows.Bookmarks;
using Arkira.Workflows.Scripting;
using Arkira.Workflows.Services;
using Elsa;
using Elsa.Activities.Signaling.Services;
using Elsa.Persistence.EntityFramework.Core.Extensions;
using Elsa.Server.Hangfire.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Arkira.Workflows.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddWorkflowServices(this IServiceCollection services, Action<DbContextOptionsBuilder> configureDb)
        {
            return services.AddElsa(configureDb);
        }

        public static IServiceCollection AddElsa(this IServiceCollection services, Action<DbContextOptionsBuilder> configureDb)
        {
            services.AddElsa(elsa => elsa
                                    .UseEntityFrameworkPersistence(configureDb, false)
                                    .UseHangfireDispatchers()
                                    .AddConsoleActivities()
                                    .AddEmailActivities()
                                    .AddHttpActivities()
                                    .AddActivitiesFrom<SendApplicationForReview>()
                                    );
            services.AddBookmarkProvider<ReviewSignalReceivedBookmarkProvider>();
            services.AddTransient<ISignaler, ReviewSignalReceivedInvoker>();
            services.AddNotificationHandlersFrom<ConfigureLiquidEngine>();

            return services;
        }
    }
}