﻿using Arkira.Core;
using Arkira.Core.LeaveApplications.Events;
using Elsa.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Handlers
{
    internal class LeaveApplicationSubmissionComplete : INotificationHandler<LeaveApplicationPrepared>
    {
        private readonly IWorkflowRegistry _workflowRegistry;
        private readonly IWorkflowDefinitionDispatcher _workflowDispatcher;

        public LeaveApplicationSubmissionComplete(IWorkflowRegistry workflowRegistry, IWorkflowDefinitionDispatcher workflowDispatcher)
        {
            _workflowRegistry = workflowRegistry;
            _workflowDispatcher = workflowDispatcher;
        }

        public async Task Handle(LeaveApplicationPrepared notification, CancellationToken cancellationToken)
        {
            var workflows = await _workflowRegistry.FindManyByTagAsync(ArkiraConsts.LEAVE_APPLICATIONS_WORKFLOW_TAG, Elsa.Models.VersionOptions.Published, null, cancellationToken);

            if (workflows == null)
                return;

            foreach (var workflow in workflows)
            {
                await _workflowDispatcher.DispatchAsync(new ExecuteWorkflowDefinitionRequest(workflow!.Id, Input: new Elsa.Models.WorkflowInput(notification.LeaveApplication)), cancellationToken);
            }
        }
    }
}