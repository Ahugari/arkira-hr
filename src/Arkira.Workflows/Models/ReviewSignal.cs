﻿namespace Arkira.Workflows.Models
{
    public class ReviewSignal
    {
        public ReviewSignal(string signalName, object signalInput)
        {
            SignalName = signalName;
            Input = signalInput;
        }

        public string SignalName { get; set; }
        public object Input { get; set; }
    }

    public class ReviewSignalModel
    {
        public ReviewSignalModel(string signalName, string workflowInstanceId, string reviewerId)
        {
            Name = signalName;
            WorkflowInstanceId = workflowInstanceId;
            ReviewerId = reviewerId;
        }

        public string Name { get; set; }
        public string WorkflowInstanceId { get; set; }
        public string ReviewerId { get; set; }
    }
}