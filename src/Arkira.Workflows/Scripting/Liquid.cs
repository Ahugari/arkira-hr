﻿using Arkira.Workflows.Activities;
using Elsa.Scripting.Liquid.Messages;
using Fluid;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Scripting
{
    public class ConfigureLiquidEngine : INotificationHandler<EvaluatingLiquidExpression>
    {
        public Task Handle(EvaluatingLiquidExpression notification, CancellationToken cancellationToken)
        {
            var memberAccessStrategy = notification.TemplateContext.Options.MemberAccessStrategy;

            memberAccessStrategy.Register<ReviewActivity>();

            return Task.CompletedTask;
        }
    }
}