﻿using Arkira.Workflows.Activities;
using Arkira.Workflows.Bookmarks;
using Arkira.Workflows.Models;
using Elsa.Activities.Signaling.Services;
using Elsa.Models;
using Elsa.Services;
using Elsa.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Workflows.Services
{
    public class ReviewSignalReceivedInvoker : ISignaler
    {
        private const string? TenantId = default;
        private readonly IWorkflowLaunchpad _workflowLaunchpad;
        private readonly ITokenService _tokenService;

        public ReviewSignalReceivedInvoker(IWorkflowLaunchpad workflowLaunchpad, ITokenService tokenService)
        {
            _workflowLaunchpad = workflowLaunchpad;
            _tokenService = tokenService;
        }

        public async Task<IEnumerable<CollectedWorkflow>> DispatchSignalAsync(string signal, object input = null, string workflowInstanceId = null, string correlationId = null, CancellationToken cancellationToken = default)
        {
            var normalizedSignal = signal.ToLowerInvariant();

            return await _workflowLaunchpad.CollectAndDispatchWorkflowsAsync(new WorkflowsQuery(
                nameof(ReviewSignalReceived),
                new ReviewSignalReceivedBookmark(signal),
                correlationId,
                workflowInstanceId,
                default,
                TenantId
            ), new WorkflowInput(new ReviewSignal(normalizedSignal, input)), cancellationToken);
        }

        public async Task<IEnumerable<CollectedWorkflow>> DispatchSignalTokenAsync(string token, object input = null, CancellationToken cancellationToken = default)
        {
            if (!_tokenService.TryDecryptToken(token, out ReviewSignalModel signal))
                return Enumerable.Empty<CollectedWorkflow>();

            return await DispatchSignalAsync(signal.Name, input, signal.WorkflowInstanceId, cancellationToken: cancellationToken);
        }

        public async Task<IEnumerable<CollectedWorkflow>> TriggerSignalAsync(string signal, object input = null, string workflowInstanceId = null, string correlationId = null, CancellationToken cancellationToken = default)
        {
            var normalizedSignal = signal.ToLowerInvariant();

            return await _workflowLaunchpad.CollectAndExecuteWorkflowsAsync(new WorkflowsQuery(
                nameof(ReviewSignalReceived),
                new ReviewSignalReceivedBookmark(signal),
                correlationId,
                workflowInstanceId,
                default,
                TenantId
            ), new WorkflowInput(new ReviewSignal(normalizedSignal, input)), cancellationToken);
        }

        public async Task<IEnumerable<CollectedWorkflow>> TriggerSignalTokenAsync(string signalToken, object input = null, CancellationToken cancellationToken = default)
        {
            if (!_tokenService.TryDecryptToken(signalToken, out ReviewSignalModel signal))
                return Enumerable.Empty<CollectedWorkflow>();

            return await TriggerSignalAsync(signal.Name, input, signal.WorkflowInstanceId, cancellationToken: cancellationToken);
        }
    }
}