﻿using Arkira.Core.Models;
using Arkira.Infrastructure.Repositories;
using NUnit.Framework;

namespace Arkira.Infrastructure.Tests.Repositories
{
    public class EfCoreEmployee
    {
        [Test]
        public void CreateAsync_WhenCalled_ReturnsTrue()
        {
            var repository = new EfCoreEmployeeRepository();
            var result = repository.CreateAsync(new Employee());
            Assert.That(result, Is.True);
        }
    }
}