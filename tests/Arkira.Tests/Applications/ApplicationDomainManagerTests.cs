﻿using Arkira.Core.ApplicationActivities;
using Arkira.Core.Applications;
using Arkira.Core.ApplicationsHistory;
using Arkira.Core.Employees;
using Arkira.Core.Helpers;
using Arkira.Core.LeaveApplications;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Tests.Applications
{
    internal class ApplicationDomainManagerTests
    {
        private ApplicationDomainManager _applicationDomainManager;
        private Mock<IApplicationActivityRepository> _applicationActivityRepository;
        private Mock<IApplicationHistoryRepository> _applicationHistoryRepository;
        private Mock<IApplicationHistoryDomainService> _applicationHistoryDomainService;
        private Mock<ApplicationHistory> _applicationHistory;
        private Mock<ApplicationActivity> _applicationActivity;
        private Mock<LeaveApplication> _fakeApplication;
        private Mock<IEmployeeDomainService> _employeeDomainService;
        private Mock<Employee> _currentReviewer;
        private Mock<Employee> _fakeEmp;
        private HashSet<Reviewer> _reviewers;
        private string fakeReviewActivityId;

        [SetUp]
        public void Initialize()
        {
            var fakeId = Guid.NewGuid();
            fakeReviewActivityId = Guid.NewGuid().ToString();
            var fakeApplicantId = Guid.NewGuid();

            _fakeApplication = new Mock<LeaveApplication>();
            _fakeApplication.Setup(x => x.Id)
                           .Returns(fakeId);
            _fakeApplication.Setup(x => x.ApplicantId)
                            .Returns(fakeApplicantId);
            _fakeApplication.Setup(x => x.OngoingActivityId)
                           .Returns(fakeReviewActivityId);
            _fakeApplication.Setup(x => x.ApplicantId)
                           .Returns(fakeApplicantId);

            _fakeEmp = new Mock<Employee>();
            _fakeEmp.Setup(x => x.Id).Returns(Guid.NewGuid());
            _fakeEmp.Setup(x => x.GetFullName()).Returns("John");

            _currentReviewer = new Mock<Employee>();
            _currentReviewer.Setup(x => x.Id).Returns(Guid.NewGuid());
            _currentReviewer.Setup(x => x.GetFullName()).Returns("Mary");

            _applicationActivity = new Mock<ApplicationActivity>();
            _applicationActivity.Setup(x => x.Actions).Returns(new HashSet<string> { ApplicationReviewActions.Recommend, ApplicationReviewActions.Reject, ApplicationReviewActions.Withdraw, ApplicationReviewActions.Approve });
            _applicationActivityRepository = new Mock<IApplicationActivityRepository>();
            _applicationActivityRepository.Setup(y => y.ReadAll()).Returns(new HashSet<ApplicationActivity> { _applicationActivity.Object }.AsEnumerable());
            _applicationActivityRepository.Setup(y => y.ReadByActivityIdAsync(_fakeApplication.Object.OngoingActivityId))
                              .ReturnsAsync(_applicationActivity.Object);
            _applicationActivityRepository.Setup(y => y.ReadOneAsync(x => x.ActivityId == _fakeApplication.Object.OngoingActivityId))
                                          .ReturnsAsync(_applicationActivity.Object);
            _applicationActivityRepository.Setup(z => z.ReadOneAsync(Transform.ToGuid(_fakeApplication.Object.OngoingActivityId)))
                              .ReturnsAsync(_applicationActivity.Object);

            _applicationHistory = new Mock<ApplicationHistory>();
            _applicationHistory.Setup(x => x.ApplicationId).Returns(fakeId.ToString());
            _applicationHistoryRepository = new Mock<IApplicationHistoryRepository>();
            _applicationHistoryRepository.Setup(x => x.ReadOneAsync(x => x.ApplicationId == _fakeApplication.Object.Id.ToString())).Returns(Task.FromResult(_applicationHistory.Object));
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                                         .ReturnsAsync(_applicationHistory.Object);
            _applicationHistoryRepository.Setup(x => x.SaveAsync(_applicationHistory.Object))
                                         .ReturnsAsync(_applicationHistory.Object);

            _reviewers = new()
            {
                new Reviewer{Id = _currentReviewer.Object.Id.ToString(), FullName = _currentReviewer.Object.GetFullName() },
                new Reviewer {Id = _fakeEmp.Object.Id.ToString(), FullName = _fakeEmp.Object.GetFullName() }
            };

            CancellationToken token = default;
            _employeeDomainService = new Mock<IEmployeeDomainService>();
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_fakeApplication.Object.ApplicantId.ToString(), token))
                                  .Returns(Task.FromResult(_fakeEmp.Object));

            _applicationHistoryDomainService = new Mock<IApplicationHistoryDomainService>();
            _applicationHistoryDomainService.Setup(x => x.AddAsync(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString(), _currentReviewer.Object.GetFullName(), _currentReviewer.Object.Id.ToString()));

            _applicationDomainManager = new ApplicationDomainManager(_employeeDomainService.Object, _applicationHistoryRepository.Object, _applicationActivityRepository.Object, _applicationHistoryDomainService.Object);
            _applicationDomainManager.Should().NotBeNull();
        }

        [Test]
        public async Task GetApplicantAsync_WhenCalled_ShouldReturnEmployee()
        {
            var result = await _applicationDomainManager.GetApplicantAsync(_fakeApplication.Object, CancellationToken.None);

            result.Should().NotBeNull();
        }

        [Test]
        public async Task GetReviewersByTagAsync_WhenCalledWithJobTitleTag_ShouldReturnReviewers()
        {
            var tagName = "jbt";
            var tagId = Guid.NewGuid();
            var emp1 = new Mock<Employee>();
            var employeesWithJobTitles = new HashSet<Employee> { emp1.Object };
            _employeeDomainService.Setup(x => x.GetEmployeesByJobTitleAsync(tagId.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(employeesWithJobTitles.AsEnumerable()));

            var result = await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", _fakeApplication.Object.ApplicantId.ToString());

            result.Should().NotBeNull();
            result.Count.Should().Be(1);
        }

        [Test]
        public async Task GetReviewersByTagAsync_WhenCalledWithEmployeeTag_ShouldReturnReviewer()
        {
            var tagName = "emp";
            var tagId = Guid.NewGuid();
            var emp1 = new Mock<Employee>();
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(tagId.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(emp1.Object));

            var result = await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", _fakeApplication.Object.ApplicantId.ToString());

            result.Should().NotBeNull();
            result.Count.Should().Be(1);
        }

        [Test]
        public async Task GetReviewersByTagAsync_WhenCalledWithManagerTag_ShouldReturnReviewer()
        {
            var tagName = "mgr";
            var tagId = "mgr";
            var emp1 = new Mock<Employee>();
            var applicantIdAsString = _fakeApplication.Object.ApplicantId.ToString();
            _employeeDomainService.Setup(x => x.GetManagerByIdAsync(applicantIdAsString, CancellationToken.None))
                                  .Returns(Task.FromResult(emp1.Object));

            var result = await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", applicantIdAsString);

            result.Should().NotBeNull();
            result.Count.Should().Be(1);
        }

        [Test]
        public async Task GetReviewersByTagAsync_WhenCalledWithSupervisorTag_ShouldReturnReviewer()
        {
            var tagName = "spr";
            var tagId = "spr";
            var emp1 = new Mock<Employee>();
            var applicantIdAsString = _fakeApplication.Object.ApplicantId.ToString();
            _employeeDomainService.Setup(x => x.GetSupervisorByIdAsync(applicantIdAsString, CancellationToken.None))
                                  .Returns(Task.FromResult(emp1.Object));

            var result = await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", applicantIdAsString);

            result.Should().NotBeNull();
            result.Count.Should().Be(1);
        }

        [Test]
        public void GetReviewersByTagAsync_WhenCalledWithEmptyTag_ShouldThrowApplicationFormException()
        {
            var emp1 = new Mock<Employee>();
            var applicantIdAsString = _fakeApplication.Object.ApplicantId.ToString();
            _employeeDomainService.Setup(x => x.GetManagerByIdAsync(applicantIdAsString, CancellationToken.None))
                                  .Returns(Task.FromResult(emp1.Object));

            Assert.ThrowsAsync<ApplicationFormException>(async () => await _applicationDomainManager.GetReviewersByTagAsync(string.Empty, applicantIdAsString));
        }

        [Test]
        public void GetReviewersByTagAsync_WhenCalledWithInvalidTag_ShouldThrowApplicationFormException()
        {
            var tagName = "abc";
            var tagId = "abc";
            var emp1 = new Mock<Employee>();
            var applicantIdAsString = _fakeApplication.Object.ApplicantId.ToString();
            _employeeDomainService.Setup(x => x.GetSupervisorByIdAsync(applicantIdAsString, CancellationToken.None))
                                  .Returns(Task.FromResult(emp1.Object));

            Assert.ThrowsAsync<ApplicationFormException>(async () => await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", applicantIdAsString));
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void GetReviewersByTagAsync_WhenCalledWithInvalidApplicantId_ShouldThrowApplicationFormException(string applicantId)
        {
            var tagName = "jbt";
            var tagId = Guid.NewGuid();
            var emp1 = new Mock<Employee>();
            var employeesWithJobTitles = new HashSet<Employee> { emp1.Object };
            _employeeDomainService.Setup(x => x.GetEmployeesByJobTitleAsync(tagId.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(employeesWithJobTitles.AsEnumerable()));

            Assert.ThrowsAsync<ApplicationFormException>(async () => await _applicationDomainManager.GetReviewersByTagAsync($"{tagName}:{tagId}", applicantId));
        }

        [TestCase(ApplicationReviewActions.Recommend)]
        [TestCase(ApplicationReviewActions.Approve)]
        [TestCase(ApplicationReviewActions.Reject)]
        [TestCase(ApplicationReviewActions.Withdraw)]
        public async Task ProcessAsync_WhenApplicationRequiresSingleReview_ShouldCompleteReview(string reviewAction)
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _reviewers.Clear();
            _reviewers.Add(new Reviewer { Id = _currentReviewer.Object.Id.ToString(), FullName = _currentReviewer.Object.GetFullName() });
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                             .Returns(Task.FromResult<ApplicationHistory>(null));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, reviewAction, fakeReviewActivityId);

            _applicationHistoryDomainService.Verify(x => x.AddAsync(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.ToString(), fakeReviewActivityId, reviewAction, _currentReviewer.Object.GetFullName(), _currentReviewer.Object.Id.ToString()), Times.Once);
            result.Should().NotBeNull();
            result.Should().BeOfType<ApplicationProcessResult>();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeTrue();
            result.Error.Should().BeNull();
        }

        [Test]
        public async Task ProcessAsync_WhenApplicationIsInvalid_ThrowApplicationFormException()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            var result = await _applicationDomainManager.ProcessAsync(null, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Reject, fakeReviewActivityId);

            result.Should().NotBeNull();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeFalse();
            result.Error.Should().BeOfType<ApplicationFormException>();
        }

        [TestCase("abc")]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task ProcessAsync_WhenReviewerIdIsInvalid_ThrowApplicationFormException(string reviewerId)
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, reviewerId, _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            result.Should().NotBeNull();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeFalse();
            result.Error.Should().BeOfType<ApplicationFormException>();
        }

        [TestCase(null)]
        public async Task ProcessAsync_WhenReviewersListIsInvalid_ThrowApplicationFormException(ICollection<Reviewer> reviewers)
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            result.Should().NotBeNull();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeFalse();
            result.Error.Should().BeOfType<ApplicationFormException>();
        }

        [Test]
        public async Task ProcessAsync_WhenReviewersListIsEmpty_ThrowApplicationFormException()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            _reviewers.Clear();

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            result.Should().NotBeNull();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeFalse();
            result.Error.Should().BeOfType<ApplicationFormException>();
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("abc")]
        public async Task ProcessAsync_WhenReviewerActionIsInvalid_ThrowApplicationFormException(string reviewerAction)
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, reviewerAction, fakeReviewActivityId);

            result.Should().NotBeNull();
            result.ProcessCompleted.Should().BeTrue();
            result.ReviewCompleted.Should().BeFalse();
            result.Error.Should().BeOfType<ApplicationFormException>();
        }

        [Test]
        public async Task ProcessAsync_WhenReviewerAlreadyCompletedAction_ShouldNotSaveApplicationHistory()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            _applicationHistoryDomainService.Verify(x => x.AddAsync(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.ToString(), _fakeApplication.Object.OngoingActivityId.ToString(), ApplicationReviewActions.Recommend, _currentReviewer.Object.GetFullName(), _currentReviewer.Object.Id.ToString()), Times.Never);

            _applicationActivityRepository.Verify(x => x.ReadOneAsync(Transform.ToGuid(_fakeApplication.Object.OngoingActivityId)), Times.Never);
        }

        [Test]
        public async Task ProcessAsync_WhenApplicationRequiresMultireview_ShouldNotCompleteReview()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                             .Returns(Task.FromResult<ApplicationHistory>(null));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeNull();
            result.ReviewCompleted.Should().BeFalse();
        }

        [Test]
        public async Task ProcessAsync_WhenApplicationRequiresMultiReviewAndTheFinalReviewIsReceivedOfTheSameType_ShouldCompleteReview()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            var review1 = new Mock<ApplicationHistory>();
            var reviewHistory = new HashSet<ApplicationHistory> { review1.Object }.AsEnumerable();
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                             .Returns(Task.FromResult<ApplicationHistory>(null));
            _applicationHistoryRepository.Setup(x => x.GetAllByActionAndApplicationActivityId(_fakeApplication.Object.OngoingActivityId, ApplicationReviewActions.Recommend))
                 .Returns(Task.FromResult(reviewHistory));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            _applicationHistoryRepository.Verify(x => x.GetAllByActionAndApplicationActivityId(_fakeApplication.Object.OngoingActivityId, ApplicationReviewActions.Recommend), Times.Once);
            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeNull();
            result.ReviewCompleted.Should().BeTrue();
        }

        [Test]
        public async Task ProcessAsync_WhenApplicationRequiresMultiReviewAndFinalReviewIsOfDifferentType_ShouldNotCompleteReview()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                             .Returns(Task.FromResult<ApplicationHistory>(null));
            _applicationHistoryRepository.Setup(x => x.GetAllByActionAndApplicationActivityId(_fakeApplication.Object.OngoingActivityId, ApplicationReviewActions.Reject))
                 .Returns(Task.FromResult<IEnumerable<ApplicationHistory>>(new HashSet<ApplicationHistory>()));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Reject, fakeReviewActivityId);

            _applicationHistoryRepository.Verify(x => x.GetAllByActionAndApplicationActivityId(_fakeApplication.Object.OngoingActivityId, ApplicationReviewActions.Reject), Times.Once);
            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeNull();
            result.ReviewCompleted.Should().BeFalse();
        }

        [Test]
        public async Task ProcessAsync_WhenApplicationRequiresSingleReview_ShouldAcceptReviewAndCompleteOngoingActivityReview()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                          .Returns(Task.FromResult(_currentReviewer.Object));
            _reviewers.Clear();
            _reviewers.Add(new Reviewer { Id = _currentReviewer.Object.Id.ToString(), FullName = _currentReviewer.Object.GetFullName() });
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            _applicationHistoryRepository.Setup(x => x.GetApplicationReviewerActivity(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.OngoingActivityId, _currentReviewer.Object.Id.ToString()))
                                         .Returns(Task.FromResult<ApplicationHistory>(null));

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Recommend, fakeReviewActivityId);

            result.ReviewCompleted.Should().BeTrue();

            _applicationHistoryDomainService.Verify(x => x.AddAsync(_fakeApplication.Object.Id.ToString(), _fakeApplication.Object.ToString(), _fakeApplication.Object.OngoingActivityId.ToString(), ApplicationReviewActions.Recommend, _currentReviewer.Object.GetFullName(), _currentReviewer.Object.Id.ToString()), Times.Once);
        }

        [Test]
        public async Task ProcessAsync_WhenReviewerIsntAValidReviewer_ThrowApplicationFormException()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                      .Returns(Task.FromResult(_currentReviewer.Object));
            _reviewers.Clear();
            _reviewers.Add(new Reviewer { Id = _fakeEmp.Object.Id.ToString(), FullName = _fakeEmp.Object.GetFullName() });
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());
            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Reject, fakeReviewActivityId);

            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeOfType<ApplicationFormException>();
            result.ReviewCompleted.Should().BeFalse();
        }

        [Test]
        public async Task ProcessAsync_WhenActionIsNotValidForApplication_ThrowApplicationFormException()
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, ApplicationReviewActions.Archive, fakeReviewActivityId);

            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeOfType<ApplicationFormException>();
            result.ReviewCompleted.Should().BeFalse();
        }

        [Test]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("abc")]
        [TestCase(null)]
        public async Task ProcessAsync_WhenActionIsInvalid_ThrowApplicationFormException(string action)
        {
            _employeeDomainService.Setup(x => x.GetEmployeeByIdAsync(_currentReviewer.Object.Id.ToString(), CancellationToken.None))
                                  .Returns(Task.FromResult(_currentReviewer.Object));
            _applicationActivity.Setup(x => x.Reviewers)
                                .Returns(_reviewers.Select(x => x.Id).ToList());

            var result = await _applicationDomainManager.ProcessAsync(_fakeApplication.Object, _currentReviewer.Object.Id.ToString(), _reviewers, action, fakeReviewActivityId);

            result.ProcessCompleted.Should().BeTrue();
            result.Error.Should().BeOfType<ApplicationFormException>();
            result.ReviewCompleted.Should().BeFalse();
        }
    }
}