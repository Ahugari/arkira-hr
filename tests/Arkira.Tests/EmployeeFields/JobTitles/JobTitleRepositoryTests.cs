﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Repositories;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.EmployeeFields.JobTitles
{
    [TestFixture]
    internal class JobTitleRepositoryTests
    {
        private IGenericCreateUpdateDeleteRepository<JobTitle> _jobTitleRepository;

        [SetUp]
        public void Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            fakeContext.GetType();
            _jobTitleRepository = new GenericCreateUpdateDeleteRepository<JobTitle>(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), new RemoveTransactional<JobTitle>(fakeContext), fakeContext);
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ShouldReturnJobTitleAsync()
        {
            var jobTitle = new JobTitle();
            jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = "Abc", Description = "def" });
            var result = await _jobTitleRepository.SaveAsync(jobTitle);

            result.Should().NotBeNull();
            result.Id.Should().NotBe(Guid.Empty);
            result.DateModified.Should().BeNull();
            result.DateCreated.Should().Be(DateTime.UtcNow.Date);
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ShouldReturnUpdatedJobTitleAsync()
        {
            var jobTitle = new JobTitle();
            jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = "Abc", Description = "def" });
            await _jobTitleRepository.SaveAsync(jobTitle);
            await _jobTitleRepository.UnitOfWork.CompleteAsync();

            var existingJobTitle = await _jobTitleRepository.ReadOneAsync(jobTitle.Id);
            existingJobTitle.FullUpdate(new JobTitleFullUpdateDto { Id = jobTitle.Id, Name = "xyz", Description = "uvw" });
            var result = await _jobTitleRepository.SaveAsync(existingJobTitle);
            var isSuccessfull = await _jobTitleRepository.UnitOfWork.CompleteAsync();

            isSuccessfull.Should().BeTrue();
            result.Should().NotBeNull();
            result.Id.Should().Be(jobTitle.Id);
            result.Name.Should().Be("xyz");
            result.Description.Should().Be("uvw");
            result.DateModified.Should().Be(DateTime.UtcNow.Date);
        }

        [Test]
        public async Task RemoveAsync_WhenCalled_ShouldReturnDeletedEntity()
        {
            var jobTitle = new JobTitle();
            jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = "Abc", Description = "def" });
            await _jobTitleRepository.SaveAsync(jobTitle);
            await _jobTitleRepository.UnitOfWork.CompleteAsync();

            var existingJobTitle = await _jobTitleRepository.ReadOneAsync(jobTitle.Id);
            var result = await _jobTitleRepository.RemoveAsync(existingJobTitle);
            var isSuccessfull = await _jobTitleRepository.UnitOfWork.CompleteAsync();

            isSuccessfull.Should().BeTrue();
            result.Should().NotBeNull();
            result.Id.Should().Be(jobTitle.Id);
            result.Name.Should().Be("Abc");
            result.Description.Should().Be("def");
        }
    }
}