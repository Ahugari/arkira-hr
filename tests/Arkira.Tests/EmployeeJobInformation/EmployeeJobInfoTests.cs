﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.EmployeeJobInformation;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.Services;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.EmployeeJobInformation;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.EmployeeJobInformation
{
    public class EmployeeJobInfoTests
    {
        private EmployeeJobInfoManager _infoManager;
        private IEmployeeJobInfoRepository _employeeJobInfoRepository;
        private IJobTitleRepository _jobTitleRepository;
        private EfCoreEmployeeRepository _employeeRepository;
        private Employee _employee;
        private Employee _employeeReporting;
        private JobTitle _jobTitle;

        [SetUp]
        public async Task InitializeAsync()
        {
            const int AGE = 20;

            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            fakeContext.GetType();

            _employeeJobInfoRepository = new EfCoreEmployeeJobInfoRepository(new SaveTransactional<EmployeeJobInfo>(fakeContext), new ReadTransactional<EmployeeJobInfo>(fakeContext), new RemoveTransactional<EmployeeJobInfo>(fakeContext), fakeContext);
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), fakeContext);
            _employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            _infoManager = new EmployeeJobInfoManager(_employeeJobInfoRepository, _jobTitleRepository, _employeeRepository);
            var _employeeDomainService = new EmployeeDomainService(new SystemClock(), _employeeRepository, _jobTitleRepository);

            _jobTitle = new JobTitle();
            _jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = "abc", Description = "def" });
            _jobTitle = await _jobTitleRepository.SaveAsync(_jobTitle);
            await _jobTitleRepository.UnitOfWork.CompleteAsync();
            _employee = await _employeeDomainService.CreateAsync("abc", "abc", null, "def", null, Gender.Male, MaritalStatus.Married,
                 "123", null, null, null, DateTime.Now.AddYears(-AGE), DateTime.Now);
            _employeeReporting = await _employeeDomainService.CreateAsync("abcd", "abcd", null, "defg", null, Gender.Female, MaritalStatus.Single,
                "123", null, null, null, DateTime.Now.AddYears(-AGE), DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            _jobTitle.Should().NotBeNull();
            _employee.Should().NotBeNull();
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ShouldReturnEmployeeJobInformation()
        {
            var jobInfo = await _infoManager.CreateAsync(_employee.Id, _jobTitle.Id, _employeeReporting.Id);

            jobInfo = await _employeeJobInfoRepository.SaveAsync(jobInfo);

            jobInfo.Id.Should().NotBe(Guid.Empty);
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ShouldReturnUpdatedEmployeeJobInformation()
        {
            var jobTitle = new JobTitle();
            jobTitle.FullUpdate(new JobTitleFullUpdateDto { Name = "abc", Description = "def" });
            jobTitle = await _jobTitleRepository.SaveAsync(jobTitle);
            await _jobTitleRepository.UnitOfWork.CompleteAsync();
            var jobInfo = await _infoManager.CreateAsync(_employee.Id, _jobTitle.Id, _employeeReporting.Id);
            await _employeeJobInfoRepository.SaveAsync(jobInfo);
            var existingJobInfo = await _employeeJobInfoRepository.ReadOneAsync(jobInfo.Id);

            var updatedJobInfo = await _infoManager.UpdateAsync(existingJobInfo.Id, jobTitle.Id);

            updatedJobInfo.JobTitleId.Should().Be(jobTitle.Id);
        }

        [Test]
        public async Task RemoveAsync_WhenCalled_ShouldThrowEmployeeJobInfoStateException()
        {
            var jobInfo = await _infoManager.CreateAsync(_employee.Id, _jobTitle.Id, _employeeReporting.Id);
            await _employeeJobInfoRepository.SaveAsync(jobInfo);

            await _employeeJobInfoRepository.RemoveAsync(jobInfo);

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _employeeJobInfoRepository.ReadOneAsync(jobInfo.Id));
        }
    }
}