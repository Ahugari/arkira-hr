﻿using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.Services;
using Arkira.Infrastructure;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.Employees
{
    public class EmployeeDomainServiceTests
    {
        private EmployeeDomainService _employeeDomainService;
        private ArkiraDbContext _dbContext;
        private const int AGE = 20;
        private EfCoreEmployeeRepository _employeeRepository;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public void Initialize()
        {
            _dbContext = MockDbContextFactory.Create().CreateDbContext();
            _employeeRepository = new EfCoreEmployeeRepository(_dbContext);
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(_dbContext), new ReadTransactional<JobTitle>(_dbContext), _dbContext);
            _employeeDomainService = new EmployeeDomainService(new SystemClock(), _employeeRepository, _jobTitleRepository);

            _employeeDomainService.Should().NotBeNull();
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnsEmployee()
        {
            var result = await _employeeDomainService.CreateAsync(
                "abc",
                "abc",
                null,
                "def",
                null,
                Gender.Male,
                MaritalStatus.Married,
                "123",
                null,
                null,
                null,
                DateTime.Now.AddYears(-AGE),
                DateTime.Now);

            result.Should().NotBeNull();
            result.Should().BeOfType<Employee>();
            result.Id.Should().NotBeEmpty();
            result.FirstName.Should().Be("abc");
        }

        [Test]
        public async Task CreateAsync_WhenNameAlreadyExists_ThrowInvalidOperationExceptionAsync()
        {
            await _employeeDomainService.CreateAsync("abc", "abc", null, "def", null, Gender.Male, MaritalStatus.Married,
                "123", null, null, null, DateTime.Now.AddYears(-AGE), DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(
                async () =>
                {
                    await _employeeDomainService.CreateAsync("aBc ",
                    "abc", null, "def", null, Gender.Male, MaritalStatus.Married, "123", null, null, null,
                    DateTime.Now.AddYears(-AGE), DateTime.Now);
                    await _employeeRepository.UnitOfWork.CompleteAsync();
                });
        }

        [Test]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(20)]
        [TestCase(300)]
        public void CreateAsync_WhenDateIsGreaterThanSystemDate_ThrowInvalidBirthDateException(int years)
        {
            Assert.ThrowsAsync<InvalidBirthDateException>(async () =>
                 await _employeeDomainService.CreateAsync(
                "abc",
                "abc",
                null,
                "def",
                null,
                Gender.Male,
                MaritalStatus.Married,
                "123",
                null,
                null,
                null,
                DateTime.Now.AddYears(years),
                DateTime.Now));
        }

        [Test]
        [TestCase(95)]
        [TestCase(96)]
        [TestCase(100)]
        [TestCase(200)]
        [TestCase(300)]
        public void CreateAsync_WhenDateIsGreaterThan95YearsInThePast_ThrowInvalidBirthDateException(int years)
        {
            Assert.ThrowsAsync<InvalidBirthDateException>(async () =>
                await _employeeDomainService.CreateAsync(
                "abc",
                "abc",
                null,
                "def",
                null,
                Gender.Male,
                MaritalStatus.Married,
                "123",
                null,
                null,
                null,
                DateTime.Now.AddYears(-years),
                DateTime.Now));
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(15)]
        [TestCase(16)]
        public void CreateAsync_WhenDateIsLessThan16YearsInThePast_ThrowInvalidBirthDateException(int years)
        {
            Assert.ThrowsAsync<InvalidBirthDateException>(async () =>
                await _employeeDomainService.CreateAsync(
                "abc",
                "abc",
                null,
                "def",
                null,
                Gender.Male,
                MaritalStatus.Married,
                "123",
                null,
                null,
                null,
                DateTime.Now.AddYears(-years),
                DateTime.Now));
        }
    }
}