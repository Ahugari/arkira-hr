﻿using Arkira.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Arkira.Tests.Helpers
{
    public static class MockDbContextFactory
    {
        public static IDbContextFactory<ArkiraDbContext> Create()
        {
            var dbContextFactory = new Moq.Mock<IDbContextFactory<ArkiraDbContext>>();
            var dbOptions = new DbContextOptionsBuilder<ArkiraDbContext>();
            dbOptions.UseInMemoryDatabase("arkira-hr");

            var dbContext = new ArkiraDbContext(dbOptions.Options);
            dbContext.Database.EnsureDeleted();
            dbContextFactory.Setup(x => x.CreateDbContext()).Returns(dbContext);

            return dbContextFactory.Object;
        }
    }
}