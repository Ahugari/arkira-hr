﻿using Arkira.Core.Inboxes;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace Arkira.Tests.Inboxes
{
    internal class InboxServiceTests
    {
        private InboxBuilder _inboxService;
        private Dictionary<string, HashSet<string>> _actions;

        [SetUp]
        public void Initialize()
        {
            _inboxService = new ApprovalItem();
            _actions = new();
            _actions.Add("abc", new HashSet<string> { "abc" });
        }

        [Test]
        public void Create_WhenCalled_ShouldReturnNewInbox()
        {
            var result = _inboxService.PrepareInbox(InboxSections.Inbox)
                .AddTarget("abc")
                .AddItemDetails("abc", "abc")
                .AddItemActions(_actions)
                .Build();

            result.Should().NotBeNull();
            result.Should().BeOfType<Inbox>();
            result.TargetId.Should().Be("abc");
            result.Category.Should().Be(InboxItemTypes.Approvals);
            result.Section.Should().Be(InboxSections.Inbox);
        }

        [Test]
        public void Create_WhenInvalidSectionIsProvided_ThrowInboxException([Values(InboxSections.Completed, "abc", " ")] string section)
        {
            Assert.Throws<InboxException>(() => _inboxService.PrepareInbox(section)
                .AddTarget("abc")
                .AddItemDetails("abc", "abc")
                .AddItemActions(_actions)
                .Build());
        }

        [Test]
        public void Create_WhenNoActionsAreProvided_ThrowInboxException()
        {
            Assert.Throws<InboxException>(() => _inboxService.PrepareInbox(InboxSections.Inbox)
                .AddTarget("abc")
                .AddItemDetails("abc", "abc")
                .Build());

            Assert.Throws<InboxException>(() => _inboxService.PrepareInbox(InboxSections.Inbox)
                .AddTarget("abc")
                .AddItemDetails("abc", "abc")
                .AddItemActions(null)
                .Build());
        }
    }
}