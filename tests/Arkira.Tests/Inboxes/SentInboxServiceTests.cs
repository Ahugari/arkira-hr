﻿using Arkira.Core.Inboxes;
using FluentAssertions;
using NUnit.Framework;

namespace Arkira.Tests.Inboxes
{
    internal class SentInboxServiceTests
    {
        private InboxBuilder _inboxBuilder;

        [SetUp]
        public void Initialize()
        {
            _inboxBuilder = new ApprovalItem();
        }

        [Test]
        public void Create_WhenCalled_ShouldReturnNewInbox([Values(InboxSections.Sent, "sent")] string section)
        {
            var result = _inboxBuilder.PrepareInbox(section)
                                    .AddTarget("abc")
                                    .AddItemDetails("abc", "abc")
                                    .Build();

            result.Should().NotBeNull();
            result.Should().BeOfType<Inbox>();
            result.Section.Trim().ToLower().Should().Be(InboxSections.Sent.ToLower());
            result.Category.Should().Be(InboxItemTypes.Approvals);
            result.TargetId.Should().Be("abc");
            result.ItemId.Should().Be("abc");
        }

        [Test]
        public void Create_WhenInvalidSectionIsProvided_ThrowInboxException([Values(" ", "abc")] string section)
        {
            Assert.Throws<InboxException>(() => _inboxBuilder.PrepareInbox(section)
                                    .AddTarget("abc")
                                    .AddItemDetails("abc", "abc")
                                    .Build());
        }
    }
}