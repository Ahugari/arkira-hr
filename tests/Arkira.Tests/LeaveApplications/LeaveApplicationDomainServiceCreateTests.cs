﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeaveApplications.Events;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.PublicHolidays;
using Arkira.Core.Services;
using Arkira.Infrastructure;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeaveApplications;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.LeaveRestrictions;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.PublicHolidays;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Tests.LeaveApplications
{
    internal class LeaveApplicationDomainServiceCreateTests
    {
        private LeaveApplicationDomainService _leaveApplicationDomainService;
        private LeavePolicyDomainService _leavePolicyDomainService;
        private LeaveType _leaveType;
        private Employee _employee;
        private Employee _coveringEmployee;
        private LeavePolicy _leavePolicy;
        private LeavePolicyAccountHistoryDomainService _leavePolicyAccountHistoryDomainService;
        private const int AGE = 20;
        private EmployeeDomainService _employeeDomainService;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private SystemClock _systemClock;
        private ArkiraDbContext _context;
        private ILeavePolicyRepository _leavePolicyRepository;
        private IPublicHolidayRepository _publicHolidayRepository;
        private IEmployeeRepository _employeeRepository;
        private ILeaveTypeRepository _leaveTypeRepository;
        private ILeaveApplicationRepository _leaveApplicationRepository;
        private Mock<IAppMediator> _mediator;
        private ILeaveRestrictionRepository _leaveRestrictionProfileRepository;
        private ILeavePolicyAccountHistoryRepository _leavePolicyAccountHistoryRepository;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task InitializeAsync()
        {
            _systemClock = new SystemClock();
            _context = MockDbContextFactory.Create().CreateDbContext();
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(_context), new ReadTransactional<JobTitle>(_context), _context);
            _employeeRepository = new EfCoreEmployeeRepository(_context);
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(_context);
            _leaveRestrictionProfileRepository = new EfCoreLeaveRestrictionRepository(_context);
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(_context);
            _leaveApplicationRepository = new EfCoreLeaveApplicationRepository(_context);
            _publicHolidayRepository = new EfCorePublicHolidayRepository(_context);
            _leavePolicyAccountHistoryRepository = new EfCoreLeavePolicyAccountHistoryRepository(_context);
            _mediator = new Mock<IAppMediator>();

            _employeeDomainService = new EmployeeDomainService(_systemClock,
                                                                  _employeeRepository,
                                                                  _jobTitleRepository);
            _leaveTypeDomainService = new LeaveTypeDomainService(_leaveTypeRepository,
                                                                    _leavePolicyRepository,
                                                                    _employeeRepository);
            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository,
                                                                       _leaveTypeRepository,
                                                                       _employeeRepository, _mediator.Object);
            var leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context),
                                                                                                    _leavePolicyRepository,
                                                                                                    _leaveTypeDomainService,
                                                                                                    _leavePolicyDomainService);

            _employee = await _employeeDomainService.CreateAsync("abc",
                                                                "abc",
                                                                null,
                                                                "def",
                                                                null,
                                                                Gender.Male,
                                                                MaritalStatus.Married,
                                                                "123",
                                                                null,
                                                                null,
                                                                null,
                                                                DateTime.Now.AddYears(-AGE),
                                                                DateTime.Now);
            _coveringEmployee = await _employeeDomainService.CreateAsync("abcd",
                                                                "abcd",
                                                                null,
                                                                "defg",
                                                                null,
                                                                Gender.Male,
                                                                MaritalStatus.Married,
                                                                "123",
                                                                null,
                                                                null,
                                                                null,
                                                                DateTime.Now.AddYears(-AGE),
                                                                DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            _leaveType = await _leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                  ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                                                                  LeaveEntitlementReset.ResetNever, null, false, null,
                                                                  false, null, false, null, null, null,
                                                                  LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                                  null, LeaveRequestWindow.OnlyInCurrentYear, null, null,
                                                                  null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(_leavePolicyAccountHistoryRepository, _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);

            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);

            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            _leaveApplicationDomainService = new LeaveApplicationDomainService(_leavePolicyRepository,
                                                                    leavePolicyAccountHistoryDomainService,
                                                                    _employeeRepository,
                                                                    _leaveTypeRepository,
                                                                    _leaveApplicationRepository,
                                                                    _publicHolidayRepository,
                                                                    _systemClock,
                                                                    _mediator.Object);
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnsLeaveApplicationAsync()
        {
            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            result.Application.Should().NotBeNull();
            result.Application.Id.Should().NotBeEmpty();
            result.Application.StartsOn.Should().Be(DateTime.Now.AddDays(1).Date);
            result.Application.EndsOn.Should().Be(DateTime.Now.AddDays(2).Date);
            result.Application.ApplicantId.Should().Be(_employee.Id);
        }

        //Input Validation
        [Test]
        public void CreateAsync_WhenLeavePolicyIsInvalid_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(Guid.NewGuid(), DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenApplicantDoesntExist_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, Guid.NewGuid(), null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenCoveringEmployeeDoesntExist_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), Guid.NewGuid(), _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenAttachmentsArentProvidedButRequired_ThrowInvalidOperationExceptionAsync()
        {
            var updatedLeavePolicy = await _leavePolicyDomainService.UpdatePolicy(_leavePolicy.Id, "abc", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true, null);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(updatedLeavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenAttachmentsAreProvidedButNotRequired_ThrowInvalidOperationExceptionAsync()
        {
            var leaveAttachments = new List<LeaveAttachment>();
            leaveAttachments.Add(new LeaveAttachment("abc", _employee.Id));

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, leaveAttachments, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenStartDateIsLaterThanEndDate_ThrowInvalidOperationExceptionAsync()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(2), DateTime.Now.AddDays(1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenEmployeeIsNotEntitled_ThrowInvalidOperationExceptionAsync()
        {
            var employee = await _employeeDomainService.CreateAsync("abcde", "abcde", null, "def", null, Gender.Male, MaritalStatus.Married,
                "123", null, null, null, DateTime.Now.AddYears(-AGE), DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();
            var leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true, _leaveType.Id,
                MinLeaveDurationInDay.WholeDay, ArkiraConsts.MAX_DAYS_TO_ENROLLMENT, false, _systemClock.UtcNow, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenSameEmployeeIsCovering_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _coveringEmployee.Id, null, null, null, null, ApplicationStatus.Submitted));

            //TODO:to add creation test by hr
        }

        [Test]
        public async Task CreateAsync_WhenApplicationPeriodConflictsWithAnother_ThrowInvalidOperationExceptionAsync()
        {
            await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(3), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(2), DateTime.Now.AddDays(4), _coveringEmployee.Id, _employee.Id, null, null, null, null));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(2), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(4), _coveringEmployee.Id, _employee.Id, null, null, null, null));
        }

        [Test]
        public async Task CreateAsync_WhenApplicationExistsWithTheSamePeriod_ThrowInvalidOperationExceptionAsync()
        {
            await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null));
        }

        //Business logic validation
        [Test]
        public async Task CreateAsync_WhenCalled_ShouldHaveAStatusOfCreatedAsync()
        {
            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            result.Application.Status.Should().Be(ApplicationStatus.Submitted);
            result.Application.Id.Should().NotBeEmpty();
        }

        [Test]
        public async Task CreateAsync_WhenDatesDontFallInRequestWindow_ThrowInvalidOperationExceptionAsync()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddYears(1),
                                                                    DateTime.Now.AddYears(1).AddDays(1),
                                                                    _coveringEmployee.Id, _employee.Id, null, null, null,
                                                                    null, ApplicationStatus.Submitted));

            var leaveTypeWithCurrentAndFollowingYearSet = await _leaveTypeDomainService.CreateAsync("abcd",
                EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever,
                null, false, null, false, null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true,
                false, null, LeaveRequestWindow.CurrentAndFollowingYear, null,
                ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);
            var leavePolicy1 = await _leavePolicyDomainService.CreateAsync("abcd", leaveTypeWithCurrentAndFollowingYearSet.Id, false, true,
                                                                         leaveTypeWithCurrentAndFollowingYearSet.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _leaveApplicationDomainService.CreateAsync(leavePolicy1.Id, DateTime.Now.AddYears(2),
                    DateTime.Now.AddYears(2).AddDays(1), _coveringEmployee.Id, _employee.Id, null, null, null, null));

            var leaveTypeWithSomePeriodInFutureSet = await _leaveTypeDomainService.CreateAsync("abcde",
                EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever,
                null, false, null, false, null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true,
                false, null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE,
                ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);
            var leavePolicy2 = await _leavePolicyDomainService.CreateAsync("abcde", leaveTypeWithSomePeriodInFutureSet.Id, false, true,
                                                                         leaveTypeWithSomePeriodInFutureSet.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _leaveApplicationDomainService.CreateAsync(leavePolicy2.Id, DateTime.Now.AddYears(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1),
                    DateTime.Now.AddYears(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1).AddDays(1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted));
        }

        [Test]
        public void CreateAsync_WhenRequestedDaysExceedEntitlementCap_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED), DateTime.Now.AddDays(ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED + 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenRequestedDaysIs1AndItsAPublicHolidayAgainstLeavePolicy_ThrowInvalidOperationException()
        {
            var publicHolidayRepository = new EfCorePublicHolidayRepository(_context);
            var publicHolidayDomainService = new PublicHolidayDomainService(publicHolidayRepository);
            var today = DateTime.Today.Date;
            await publicHolidayDomainService.CreateAsync(today, "abc");

            await _publicHolidayRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, today, today, _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public void CreateAsync_WhenRequestedDaysAreSatAndSunWhilePolicyIsWorkingDays_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, GetNextWeekend(skipDays: 0), GetNextWeekend(skipDays: 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenLeavePolicyIsUnlimitedButDaysExceedSomePeriodInFutureLimit_ThrowInvalidOperationExceptionAsync()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Unlimited,
                                                      null,
                                                      LeaveEntitlementReset.ResetNever, null, false, null,
                                                      false, null, false, null, null, null,
                                                      LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                      null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, null,
                                                      null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);
            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddMonths(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenLeavePolicyIsUnlimitedButDaysExceedsCurrentAndFollowingYearWindowLimit_ThrowInvalidOperationExceptionAsync()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Unlimited,
                                                      null,
                                                      LeaveEntitlementReset.ResetNever, null, false, null,
                                                      false, null, false, null, null, null,
                                                      LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                      null, LeaveRequestWindow.CurrentAndFollowingYear, null, null,
                                                      null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);
            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddMonths(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenLeavePolicyIsUnlimitedButDaysExceedsSomePeriodInFutureWindowLimit_ThrowInvalidOperationExceptionAsync()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Unlimited,
                                                      null,
                                                      LeaveEntitlementReset.ResetNever, null, false, null,
                                                      false, null, false, null, null, null,
                                                      LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                      null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                                                      NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);
            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddMonths(ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenLeavePolicyIsUnlimitedButDaysExceedOnlyInCurrentYearLimit_ThrowInvalidOperationExceptionAsync()
        {
            const int monthsInAYear = 365;
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Unlimited,
                                                      null,
                                                      LeaveEntitlementReset.ResetNever, null, false, null,
                                                      false, null, false, null, null, null,
                                                      LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                      null, LeaveRequestWindow.OnlyInCurrentYear, null, null,
                                                      null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);
            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);
            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddMonths(monthsInAYear + 1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);
            });
        }

        [Test]
        public async Task CreateAsync_WhenCalledAndLeavePolicyAcceptsNegativeEntitlement_ReturnsLeaveApplicationAsync()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Standard,
                                                      ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                                                      LeaveEntitlementReset.ResetNever, null, false, null,
                                                      false, null, false, null, null, null,
                                                      LeaveRequestLengthFactor.EveryWorkingDay, false, true,
                                                      ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE,
                                                      ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION, NewYearLeaveRequestDeductionOptions.ProportionalFromEachYear,
                                                      DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, true, true,
                                                                         null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);

            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(60), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            result.Application.Should().NotBeNull();
            result.Application.Id.Should().NotBeEmpty();
        }

        [Test]
        public async Task CreateAsync_WhenCalledAndNewYearDeducutionIsAtFirstCurrentYear_ReturnsLeaveApplicationAsync()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Standard,
                                          ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                                          LeaveEntitlementReset.ResetNever, null, false, null,
                                          false, null, false, null, null, null,
                                          LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                          null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                                          NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, true, true,
                                                                         null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);

            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();
            var dateMock = new Mock<ISystemClock>();
            dateMock.Setup(x => x.UtcNow).Returns(DateTime.Now.AddMonths(_leavePolicy.LeaveType.NewYearActiveWindow.Value));
            var leaveApplicationDomainService = new LeaveApplicationDomainService(_leavePolicyRepository,
                                            _leavePolicyAccountHistoryDomainService,
                                            _employeeRepository,
                                            _leaveTypeRepository,
                                            _leaveApplicationRepository,
                                            _publicHolidayRepository,
                                            dateMock.Object,
                                            _mediator.Object);

            var result = await leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(60), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            result.Application.Should().NotBeNull();
            result.Application.Id.Should().NotBeEmpty();
        }

        [Test]
        public async Task CreateAsync_WhenNewYearActiveWindowIsNotActiveYet_ThrowInvalidOperationException()
        {
            _leaveType = await _leaveTypeDomainService.CreateAsync("abcd", EntitlementType.Standard,
                              ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                              LeaveEntitlementReset.ResetNever, null, false, null,
                              false, null, false, null, null, null,
                              LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                              null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                              NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, true, true,
                                                                         null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);

            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            await _leavePolicyAccountHistoryRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(60), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted));
        }

        [Test]
        public async Task CreateAsync_WhenCalled_PublishNotification()
        {
            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            _mediator.Verify(x => x.Publish(new LeaveApplicationPrepared(result.Application, 2), It.IsAny<CancellationToken>()), Times.Exactly(1));
        }

        [Test]
        public async Task CreateAsync_WhenRestrictionsRequireBlockingOfRequest_ThrowInvalidOperationException()
        {
            _ = await _leaveRestrictionProfileRepository.CreateAsync(new LeaveMustNotBeShorterThanNDays(2, LeaveRestrictionAction.BlockRequest, _leavePolicy));
            await _leaveRestrictionProfileRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(1), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted));
        }

        [Test]
        public async Task CreateAsync_WhenRestrictionsRequireDisplayOfWarning_ReturnWarnings()
        {
            _ = await _leaveRestrictionProfileRepository.CreateAsync(new LeaveMustNotBeLongerThanNDays(1, LeaveRestrictionAction.DisplayWarning, _leavePolicy));
            _ = await _leaveRestrictionProfileRepository.CreateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(1, DatePeriodFormatOption.Days, 1, LeaveRestrictionAction.DisplayWarning, _leavePolicy));
            await _leaveRestrictionProfileRepository.UnitOfWork.CompleteAsync();

            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(4), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            result.ApplicationWarnings.Count.Should().Be(2);
        }

        [Test]
        public async Task CreateAsync_WhenApplicationIsSubmittedInLineWithRestriction_ReturnWarning()
        {
            _ = await _leaveRestrictionProfileRepository.CreateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(2, DatePeriodFormatOption.Months, 1, LeaveRestrictionAction.DisplayWarning, _leavePolicy));
            await _leaveRestrictionProfileRepository.UnitOfWork.CompleteAsync();

            var result = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddMonths(1), DateTime.Now.AddMonths(1).AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null, ApplicationStatus.Submitted);

            result.ApplicationWarnings.Count.Should().Be(1);
        }

        private DateTime GetNextWeekend(int skipDays)
        {
            int maxNumberOfDaysStored = 8;

            if (skipDays > maxNumberOfDaysStored)
                throw new InvalidOperationException("Number of days to skip should not be more than " + maxNumberOfDaysStored);

            List<DateTime> weekendList = new List<DateTime>();

            for (int i = 0; weekendList.Count < maxNumberOfDaysStored; i++)
            {
                DateTime currentDate = DateTime.Now.AddDays(i);
                if (currentDate.DayOfWeek is DayOfWeek.Saturday ^ currentDate.DayOfWeek is DayOfWeek.Sunday)
                    weekendList.Add(currentDate);
            }

            return weekendList[skipDays].Date;
        }
    }
}