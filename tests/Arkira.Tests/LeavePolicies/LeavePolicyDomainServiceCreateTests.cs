﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicies.Exceptions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveRestrictions;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeavePolicies
{
    internal class LeavePolicyDomainServiceCreateTests
    {
        private LeavePolicyDomainService _leavePolicyDomainService;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private ILeavePolicyRepository _leavePolicyRepository;
        private LeaveType _leaveType;
        private Employee _employee;
        private const int AGE = 20;
        private DateTime _date = new SystemClock().UtcNow;
        private IEmployeeRepository _employeeRepository;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            var leaveTypeRepository = new EfCoreLeaveTypeRepository(fakeContext);
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(fakeContext);
            var employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            var leaveRestrictionRepository = new EfCoreLeaveRestrictionRepository(fakeContext);
            _employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), fakeContext);
            var employeeDomainService = new EmployeeDomainService(new SystemClock(), employeeRepository, _jobTitleRepository);

            _employee = await employeeDomainService.CreateAsync("abc",
                                        "abc",
                                        null,
                                        "def",
                                        null,
                                        Gender.Male,
                                        MaritalStatus.Married,
                                        "123",
                                        null,
                                        null,
                                        null,
                                        DateTime.Now.AddYears(-AGE),
                                        DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            _leaveTypeDomainService = new LeaveTypeDomainService(leaveTypeRepository,
                                                                 _leavePolicyRepository,
                                                                 employeeRepository);
            _leaveType = await _leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                   ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                                                   LeaveEntitlementReset.ResetNever, null, false, null,
                                                                   false, null, false, null, null, null,
                                                                   LeaveRequestLengthFactor.EveryWorkingDay, true, false,
                                                                   null, LeaveRequestWindow.OnlyInCurrentYear, null,
                                                                   null, null, DateTime.Now.AddDays(1), null);
            await leaveTypeRepository.UnitOfWork.CompleteAsync();

            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository,
                                                                     leaveTypeRepository,
                                                                     employeeRepository,
                                                                     new Mock<IMediator>().Object);
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnLeavePolicy()
        {
            var result1 = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);
            var result2 = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, true, false, null, MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true, _date, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);

            result1.Should().NotBeNull();
            result2.Should().NotBeNull();
        }

        [Test]
        public async Task CreateAsync_WhenNameAlreadyUsed_ThrowInvalidOperationException()
        {
            _ = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, true, false, null, MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true, _date, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);
                await _leavePolicyRepository.UnitOfWork.CompleteAsync();
            });
        }

        [Test]
        public void CreateAsync_WhenEntitlementFromTypeIsNotProvidedButRequired_ThrowsEntitlementFromTypeArgumentException()
        {
            Assert.ThrowsAsync<EntitlementFromTypeArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, false, true, null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementFromTypeIsProvidedButNotRequired_ThrowsEntitlementFromTypeArgumentException()
        {
            Assert.ThrowsAsync<EntitlementFromTypeArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, true, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementFromTypeIsInvalid_ThrowsEntitlementFromTypeArgumentException()
        {
            Assert.ThrowsAsync<EntitlementFromTypeArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, false, true, Guid.NewGuid(), MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsNotProvidedButRequired_ThrowsEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, true, true, null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsProvidedButNotRequired_ThrowsEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsHigherThanLimit_ThrowsEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, true, true, null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED + 1));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsLowerThanLimit_ThrowsEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _leavePolicyDomainService.CreateAsync("abc",
                _leaveType.Id, true, true, null, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED - 1));
        }

        [Test]
        public async Task CreateAsync_WhenCalled_EntitledEmployeesShouldGetLeavePolicy()
        {
            var leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            leavePolicy.EntitledEmployees.Count.Should().Be(1);
            _employee.LeavePolicyEntitlements.Count.Should().Be(1);
        }

        [Test]
        public void CreateAsync_WhenEffectiveIsNotProvided_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, DateTime.MinValue, null));
        }
    }
}