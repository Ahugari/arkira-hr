﻿using Arkira.Core;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeavePolicies
{
    internal class LeavePolicyDomainServiceRemoveTests
    {
        private LeavePolicyDomainService _leavePolicyDomainService;
        private UnitOfWork _unitOfWork;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private Core.LeaveTypes.LeaveType _leaveType;
        private Core.LeavePolicies.LeavePolicy _leavePolicy;

        [SetUp]
        public async Task Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            var leaveTypeRepository = new EfCoreLeaveTypeRepository(fakeContext);
            var leavePolicyRepository = new EfCoreLeavePolicyRepository(fakeContext);
            var employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            _leaveTypeDomainService = new LeaveTypeDomainService(leaveTypeRepository, leavePolicyRepository,
                                                                 employeeRepository);
            var mediator = new Mock<IMediator>();
            _leavePolicyDomainService = new LeavePolicyDomainService(leavePolicyRepository, leaveTypeRepository,
                                                                     employeeRepository,
                                                                     mediator.Object);

            _leaveType = await _leaveTypeDomainService.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id,
                MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, new SystemClock().UtcNow, null);
            await leavePolicyRepository.UnitOfWork.CompleteAsync();

            _leavePolicyDomainService = new LeavePolicyDomainService(leavePolicyRepository, leaveTypeRepository, employeeRepository, mediator.Object);
        }

        [Test]
        public void RemoveAsync_WhenCalled_Success()
        {
            Assert.DoesNotThrowAsync(async () => await _leavePolicyDomainService.RemoveAsync(_leavePolicy.Id));
        }

        [Test]
        public void RemoveAsync_WhenIdIsInvalid_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leavePolicyDomainService.RemoveAsync(Guid.NewGuid()));
        }
    }
}