﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeavePolicies
{
    internal class LeavePolicyDomainServiceUpdateTests
    {
        private LeavePolicyDomainService _leavePolicyDomainService;
        private ILeavePolicyRepository _leavePolicyRepository;
        private IEmployeeRepository _employeeRepository;
        private UnitOfWork _unitOfWork;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private LeaveType _leaveType;
        private LeavePolicy _leavePolicy;
        private EmployeeDomainService _employeeDomainService;
        private DateTime _date = new SystemClock().UtcNow;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            var leaveTypeRepository = new EfCoreLeaveTypeRepository(fakeContext);
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(fakeContext);
            _employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            var mediator = new Mock<IMediator>();
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), fakeContext);
            _employeeDomainService = new EmployeeDomainService(new SystemClock(), _employeeRepository, _jobTitleRepository);
            _leaveTypeDomainService = new LeaveTypeDomainService(leaveTypeRepository, _leavePolicyRepository, _employeeRepository);
            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository, leaveTypeRepository, _employeeRepository, mediator.Object);

            _leaveType = await _leaveTypeDomainService.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id,
                MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository, leaveTypeRepository, _employeeRepository, mediator.Object);
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ReturnUpdatedLeavePolicy()
        {
            var result = await _leavePolicyDomainService.UpdatePolicy(_leavePolicy.Id, "abc", _leaveType.Id, true, false,
                null, MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);

            result.Should().NotBeNull();
            result.Version.Should().Be(2);
            result.CorrelationId.Should().Be(_leavePolicy.CorrelationId);
        }

        [Test]
        public async Task UpdateAsync_WhenNameIsAlreadyUsed_ThrowInvalidOperationException()
        {
            await _leavePolicyDomainService.CreateAsync("abcd", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leavePolicyDomainService.UpdatePolicy(_leavePolicy.Id,
                "abcd ", _leaveType.Id, true, false, null, MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true,
                ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED));
        }

        [Test]
        public void UpdateAsync_WhenIdIsInvalid_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leavePolicyDomainService.UpdatePolicy(Guid.NewGuid(),
                "abcd", _leaveType.Id, true, false, null, MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true,
                ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED));
        }

        [Test]
        public async Task UpdateAsync_WhenExistingEmployeesAreNolongerEligible_RemoveEntitlementFromAllPreviouslyEntitledEmployeesAsync()
        {
            const int AGE = 20;
            var employee = await _employeeDomainService.CreateAsync(
                "abcd", "abc", null, "def", null, Gender.Male, MaritalStatus.Married, "123", null, null, null,
                DateTime.Now.AddYears(-AGE), DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            var leaveType = await _leaveTypeDomainService.CreateAsync(
                "abcd", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);
            var leavePolicy = await _leavePolicyDomainService.CreateAsync("abcd", leaveType.Id, true, false, null,
                MinLeaveDurationInDay.HalfDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, true, _date,
                ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            employee.LeavePolicyEntitlements.Count.Should().Be(1);

            await _leavePolicyDomainService.UpdatePolicy(leavePolicy.Id, "abcde", leaveType.Id, true, false, null,
                MinLeaveDurationInDay.HalfDay, ArkiraConsts.MAX_DAYS_TO_ENROLLMENT, true,
                ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            employee.LeavePolicyEntitlements.Count.Should().Be(1);
        }
    }
}