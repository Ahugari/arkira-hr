﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeavePolicyAccountHistory
{
    [TestFixture]
    public class LeavePolicyAccountHistoryDomainServiceTests
    {
        private Employee _employee;
        private LeavePolicy _leavePolicy;
        private LeavePolicyAccountHistoryDomainService _leavePolicyAccountHistoryDomainService;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task InitializeAsync()
        {
            const int AGE = 20;
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), fakeContext);
            var employeeDomainService = new EmployeeDomainService(new SystemClock(), new EfCoreEmployeeRepository(fakeContext), _jobTitleRepository);
            var mediator = new Mock<IMediator>();
            var employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            var leaveTypeRepository = new EfCoreLeaveTypeRepository(fakeContext);
            var leavePolicyRepository = new EfCoreLeavePolicyRepository(fakeContext);
            var leaveTypeDomainService = new LeaveTypeDomainService(leaveTypeRepository,
                                                                    leavePolicyRepository,
                                                                    employeeRepository
                                                                    );
            var leavePolicyDomainService = new LeavePolicyDomainService(leavePolicyRepository,
                                                                       leaveTypeRepository,
                                                                       employeeRepository,
                                                                       mediator.Object);

            _employee = await employeeDomainService.CreateAsync("abc",
                                                    "abc",
                                                    null,
                                                    "def",
                                                    null,
                                                    Gender.Male,
                                                    MaritalStatus.Married,
                                                    "123",
                                                    null,
                                                    null,
                                                    null,
                                                    DateTime.Now.AddYears(-AGE),
                                                    DateTime.Now);

            var leaveType = await leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                  ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                                                  LeaveEntitlementReset.ResetNever, null, false, null,
                                                                  false, null, false, null, null, null,
                                                                  LeaveRequestLengthFactor.EveryWorkingDay, true, false,
                                                                  null, LeaveRequestWindow.OnlyInCurrentYear, null, null,
                                                                  null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await leavePolicyDomainService.CreateAsync("abc", leaveType.Id, false, true,
                                                                         leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, new SystemClock().UtcNow, null);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(fakeContext), leavePolicyRepository, leaveTypeDomainService, leavePolicyDomainService);
        }

        [Test]
        public void Create_WhenCalled_ReturnsLeavePolicyAccountHistory()
        {
            var result = _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Add, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, new SystemClock().UtcNow, _leavePolicy.Id);

            result.Id.Should().NotBeEmpty();
            result.Should().NotBeNull();
        }
    }
}