﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeaveApplications;
using Arkira.Core.LeaveApplications.Events;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeavePolicyAccounts;
using Arkira.Core.LeavePolicyAccountsHistory;
using Arkira.Core.LeavePolicyAccountsHistory.Handlers;
using Arkira.Core.LeaveTypes;
using Arkira.Core.PublicHolidays;
using Arkira.Core.Services;
using Arkira.Infrastructure;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeaveApplications;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeavePolicyAccountsHistory;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.PublicHolidays;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Arkira.Tests.LeavePolicyAccountHistory
{
    public class LeavePolicyAccountHistoryHandlerTests
    {
        private LeavePolicyAccountHistoryDomainService _leavePolicyAccountHistoryDomainService;
        private AddLeaveDaysFromLeaveApplication _addLeaveDaysFromLeaveApplication;
        private LeaveApplicationDomainService _leaveApplicationDomainService;
        private LeavePolicyDomainService _leavePolicyDomainService;
        private LeaveType _leaveType;
        private Employee _employee;
        private Employee _coveringEmployee;
        private LeavePolicy _leavePolicy;
        private const int AGE = 20;
        private EmployeeDomainService _employeeDomainService;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private SystemClock _systemClock;
        private ArkiraDbContext _context;
        private ILeavePolicyRepository _leavePolicyRepository;
        private IPublicHolidayRepository _publicHolidayRepository;
        private IEmployeeRepository _employeeRepository;
        private ILeaveTypeRepository _leaveTypeRepository;
        private ILeaveApplicationRepository _leaveApplicationRepository;
        private Mock<IAppMediator> _mediator;
        private ILeavePolicyAccountHistoryRepository _leavePolicyAcccountHistoryRepository;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task InitializeAsync()
        {
            _systemClock = new SystemClock();
            _context = MockDbContextFactory.Create().CreateDbContext();
            _employeeRepository = new EfCoreEmployeeRepository(_context);
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(_context);
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(_context);
            _leaveApplicationRepository = new EfCoreLeaveApplicationRepository(_context);
            _publicHolidayRepository = new EfCorePublicHolidayRepository(_context);
            _mediator = new Mock<IAppMediator>();

            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(_context), new ReadTransactional<JobTitle>(_context), _context);
            _employeeDomainService = new EmployeeDomainService(_systemClock,
                                                                  _employeeRepository, _jobTitleRepository);
            _leaveTypeDomainService = new LeaveTypeDomainService(_leaveTypeRepository,
                                                                    _leavePolicyRepository,
                                                                    _employeeRepository);
            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository,
                                                                       _leaveTypeRepository,
                                                                       _employeeRepository, _mediator.Object);
            var leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context),
                                                                                                    _leavePolicyRepository,
                                                                                                    _leaveTypeDomainService,
                                                                                                    _leavePolicyDomainService);

            _employee = await _employeeDomainService.CreateAsync("abc",
                                                                "abc",
                                                                null,
                                                                "def",
                                                                null,
                                                                Gender.Male,
                                                                MaritalStatus.Married,
                                                                "123",
                                                                null,
                                                                null,
                                                                null,
                                                                DateTime.Now.AddYears(-AGE),
                                                                DateTime.Now);
            _coveringEmployee = await _employeeDomainService.CreateAsync("abcd",
                                                                "abcd",
                                                                null,
                                                                "defg",
                                                                null,
                                                                Gender.Male,
                                                                MaritalStatus.Married,
                                                                "123",
                                                                null,
                                                                null,
                                                                null,
                                                                DateTime.Now.AddYears(-AGE),
                                                                DateTime.Now);
            await _employeeRepository.UnitOfWork.CompleteAsync();

            _leaveType = await _leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                  ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                                                                  LeaveEntitlementReset.ResetNever, null, false, null,
                                                                  false, null, false, null, null, null,
                                                                  LeaveRequestLengthFactor.EveryWorkingDay, false, false,
                                                                  null, LeaveRequestWindow.OnlyInCurrentYear, null, null,
                                                                  null, DateTime.Now.AddDays(1), null);
            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true,
                                                                         _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT,
                                                                         false, _systemClock.UtcNow, null);

            _leavePolicyAccountHistoryDomainService = new LeavePolicyAccountHistoryDomainService(new EfCoreLeavePolicyAccountHistoryRepository(_context), _leavePolicyRepository, _leaveTypeDomainService, _leavePolicyDomainService);

            _leavePolicyAccountHistoryDomainService.Create(_employee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);
            _leavePolicyAccountHistoryDomainService.Create(_coveringEmployee.Id, LeavePolicyAccountAction.Enrolled, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, _systemClock.UtcNow, _leavePolicy.Id);

            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            _leaveApplicationDomainService = new LeaveApplicationDomainService(_leavePolicyRepository,
                                                                    leavePolicyAccountHistoryDomainService,
                                                                    _employeeRepository,
                                                                    _leaveTypeRepository,
                                                                    _leaveApplicationRepository,
                                                                    _publicHolidayRepository,
                                                                    _systemClock,
                                                                    _mediator.Object);

            _addLeaveDaysFromLeaveApplication = new AddLeaveDaysFromLeaveApplication(leavePolicyAccountHistoryDomainService);
            _leavePolicyAcccountHistoryRepository = new EfCoreLeavePolicyAccountHistoryRepository(_context);
        }

        [Test]
        public async Task AddLeaveDaysFromLeaveApplication_WhenCalled_LeaveDaysAreAddedToLeavePolicyHistoryAsync()
        {
            var leaveApplication = await _leaveApplicationDomainService.CreateAsync(_leavePolicy.Id, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), _coveringEmployee.Id, _employee.Id, null, null, null, null);

            await _addLeaveDaysFromLeaveApplication.Handle(new LeaveApplicationPrepared(leaveApplication.Application, 2), CancellationToken.None);
            await _leaveApplicationRepository.UnitOfWork.CompleteAsync();

            Assert.IsTrue(_addLeaveDaysFromLeaveApplication.Handle(new LeaveApplicationPrepared(leaveApplication.Application, 2), CancellationToken.None).IsCompletedSuccessfully);
            var history = await _leavePolicyAcccountHistoryRepository.FindAsync(x => x.Amount == 2);
            history.Should().NotBeNull();
        }
    }
}