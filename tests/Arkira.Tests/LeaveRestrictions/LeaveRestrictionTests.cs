﻿using Arkira.Core;
using Arkira.Core.EmployeeFields.JobTitles;
using Arkira.Core.Employees;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveRestrictions;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.EmployeeFields.JobTitles;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveRestrictions;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeaveRestrictions
{
    internal class LeaveRestrictionTests
    {
        private LeavePolicyDomainService _leavePolicyDomainService;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private IEmployeeRepository _employeeRepository;
        private ILeavePolicyRepository _leavePolicyRepository;
        private ILeaveTypeRepository _leaveTypeRepository;
        private LeaveType _leaveType;
        private Employee _employee;
        private const int AGE = 20;
        private DateTime _date = new SystemClock().UtcNow;
        private LeavePolicy _leavePolicy;
        private ILeaveRestrictionRepository _restrictionRepository;
        private LeaveRestrictionDomainService _restrictionDomainService;
        private IJobTitleRepository _jobTitleRepository;

        [SetUp]
        public async Task Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(fakeContext);
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(fakeContext);
            _employeeRepository = new EfCoreEmployeeRepository(fakeContext);
            _jobTitleRepository = new EfCoreJobTitleRepository(new SaveTransactional<JobTitle>(fakeContext), new ReadTransactional<JobTitle>(fakeContext), fakeContext);
            var employeeDomainService = new EmployeeDomainService(new SystemClock(), _employeeRepository, _jobTitleRepository);
            _restrictionRepository = new EfCoreLeaveRestrictionRepository(fakeContext);
            _restrictionDomainService = new LeaveRestrictionDomainService(_restrictionRepository);

            _employee = await employeeDomainService.CreateAsync("abc",
                                        "abc",
                                        null,
                                        "def",
                                        null,
                                        Gender.Male,
                                        MaritalStatus.Married,
                                        "123",
                                        null,
                                        null,
                                        null,
                                        DateTime.Now.AddYears(-AGE),
                                        DateTime.Now);
            await this._employeeRepository.UnitOfWork.CompleteAsync();

            _leaveTypeDomainService = new LeaveTypeDomainService(_leaveTypeRepository,
                                                                 _leavePolicyRepository,
                                                                 _employeeRepository);
            _leaveType = await _leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                   ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                                                   LeaveEntitlementReset.ResetNever, null, false, null,
                                                                   false, null, false, null, null, null,
                                                                   LeaveRequestLengthFactor.EveryWorkingDay, true, false,
                                                                   null, LeaveRequestWindow.OnlyInCurrentYear, null,
                                                                   null, null, DateTime.Now.AddDays(1), null);
            await this._leaveTypeRepository.UnitOfWork.CompleteAsync();

            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository,
                                                                     _leaveTypeRepository,
                                                                     _employeeRepository,
                                                                     new Mock<IMediator>().Object);

            _leavePolicy = await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, _date, null);

            await this._leavePolicyRepository.UnitOfWork.CompleteAsync();
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnLeaveRestriction()
        {
            var result = await _restrictionRepository.CreateAsync(new LeaveMustNotBeLongerThanNDays(1, LeaveRestrictionAction.BlockRequest, _leavePolicy));
            var result2 = await _restrictionRepository.CreateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(1, DatePeriodFormatOption.Days, 1, LeaveRestrictionAction.BlockRequest, _leavePolicy));

            result.Should().NotBeNull();
            result2.Should().NotBeNull();
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ReturnUpdatedLeaveRestriction()
        {
            var savedRestriction = await _restrictionRepository.CreateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(1, DatePeriodFormatOption.Days, 1, LeaveRestrictionAction.BlockRequest, _leavePolicy));
            await _restrictionRepository.UnitOfWork.CompleteAsync();

            var result = await _restrictionDomainService.UpdateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(2, DatePeriodFormatOption.Days, 2, LeaveRestrictionAction.BlockRequest, _leavePolicy), savedRestriction);

            result.Should().NotBeNull();
            result.PeriodBeforeApplicationAcceptance.Should().Be(2);
        }

        [Test]
        public async Task UpdateAsync_WhenRestrictionOptionIsDifferent_ThrowInvalidOperationException()
        {
            var savedRestriction = await _restrictionRepository.CreateAsync(new LeaveMustBeRequestedNDaysBeforeIfLongerThanNDays(1, DatePeriodFormatOption.Days, 1, LeaveRestrictionAction.BlockRequest, _leavePolicy));
            await _restrictionRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _restrictionDomainService.UpdateAsync(new LeaveMustNotBeLongerThanNDays(1, LeaveRestrictionAction.BlockRequest, _leavePolicy), savedRestriction));
        }
    }
}