﻿using Arkira.Core;
using Arkira.Core.Enums;
using Arkira.Core.LeaveTypes;
using Arkira.Core.LeaveTypes.Exceptions;
using Arkira.Infrastructure;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeaveTypes
{
    internal class LeaveTypeDomainServiceCreateTests
    {
        private ArkiraDbContext _context;
        private LeaveTypeDomainService _service;
        private ILeaveTypeRepository _leaveTypeRepository;

        [SetUp]
        public void Initialize()
        {
            _context = MockDbContextFactory.Create().CreateDbContext();
            _service = new LeaveTypeDomainService(
                new EfCoreLeaveTypeRepository(_context), new EfCoreLeavePolicyRepository(_context),
                new EfCoreEmployeeRepository(_context));
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(_context);
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnsLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public async Task CreateAsync_WhenNameAlreadyExists_ThrowInvalidOperationException()
        {
            await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);
            await _leaveTypeRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(
                async () =>
                {
                    await _service.CreateAsync(
                    "abC ", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null,
                    false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                    LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);
                    await _leaveTypeRepository.UnitOfWork.CompleteAsync();
                });
        }

        [Test]
        public void CreateAsync_WhenInvalidNamePassed_ThrowArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.CreateAsync(
                "", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<ArgumentException>(async () => await _service.CreateAsync(
                string.Empty, EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false,
                null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<ArgumentException>(async () => await _service.CreateAsync(
                "  ", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsNullAndEntitlementIsStandard_ThrowEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, null, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapLimitIsExceededAndEntitlementIsStandard_ThrowEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED + 1, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenEntitlementCapIsBelowMinimumLimitAndEntitlementIsStandard_ThrowEntitlementCapArgumentException()
        {
            Assert.ThrowsAsync<EntitlementCapArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED - 1, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenEntitlementCapIsWithinLimitAndEntitlementIsStandard_ReturnLeaveType()
        {
            var result1 = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED, LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);

            var result2 = await _service.CreateAsync(
                "abcd", EntitlementType.Standard, ArkiraConsts.MAX_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);

            result1.Should().NotBeNull();
            result1.Should().BeOfType<LeaveType>();
            result2.Should().NotBeNull();
            result2.Should().BeOfType<LeaveType>();
        }

        [Test]
        public void CreateAsync_WhenInvalidEffectiveDateIsProvided_ThrowArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(-1), null));

            Assert.ThrowsAsync<ArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddYears(ArkiraConsts.FUTURE_EFFECTIVE_DATE_YEARS_LIMIT + 1), null));
        }

        [Test]
        public void CreateAsync_WhenScheduleDateIsProvidedButNotRequired_ThrowLeaveEntitlementResetScheduleDateArgumentException()
        {
            Assert.ThrowsAsync<LeaveEntitlementResetScheduleDateArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, DateTime.Now, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<LeaveEntitlementResetScheduleDateArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetOnWorkAnniversary, DateTime.Now, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenScheduleDateIsNotProvided_ThrowLeaveEntitlementResetScheduleDateArgumentException()
        {
            Assert.ThrowsAsync<LeaveEntitlementResetScheduleDateArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetOnSchedule, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenScheduleDateIsProvided_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetOnSchedule, DateTime.Now, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenMaxCarryOverDaysIsSetButNotRequired_ThrowMaxCarryOverDaysArgumentException()
        {
            Assert.ThrowsAsync<MaxCarryOverDaysArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxCarryOverDaysIsNullButRequired_ThrowMaxCarryOverDaysArgumentException()
        {
            Assert.ThrowsAsync<MaxCarryOverDaysArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxCarryOverDaysIsRequiredButLowerThanMinimum_ThrowMaxCarryOverDaysArgumentException()
        {
            Assert.ThrowsAsync<MaxCarryOverDaysArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED - 1, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxCarryOverDaysIsRequiredButHigherThanMaximum_ThrowMaxCarryOverDaysArgumentException()
        {
            Assert.ThrowsAsync<MaxCarryOverDaysArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MAX_CARRY_OVER_DAYS_ALLOWED + 1, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenMaxCarryOverDaysIsRequiredAndValid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MAX_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenCarryOverNegativeEntitlementIsSetWhileAllowNegativeEntitlementIsFalse_ThrowCarryOverNegativeEntitlementArgumentException()
        {
            Assert.ThrowsAsync<CarryOverNegativeEntitlementArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenCarryOverNegativeEntitlementIsTrueWhileMaxNegativeDaysIsNotProvided_ThrowMaxNegativeCarryOverDaysException()
        {
            Assert.ThrowsAsync<MaxNegativeCarryOverDaysException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeCarryOverDaysIsProvidedButNotRequired_ThrowMaxNegativeCarryOverDaysException()
        {
            Assert.ThrowsAsync<MaxNegativeCarryOverDaysException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeCarryOverDaysIsLessThanMinimum_ThrowMaxNegativeCarryOverDaysException()
        {
            Assert.ThrowsAsync<MaxNegativeCarryOverDaysException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED - 1, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeCarryOverDaysIsMoreThanMaximum_ThrowMaxNegativeCarryOverDaysException()
        {
            Assert.ThrowsAsync<MaxNegativeCarryOverDaysException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MAX_NEGATIVE_CARRY_OVER_DAYS_ALLOWED + 1, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeCarryOverDaysIsNullButRequired_ThrowMaxNegativeCarryOverDaysException()
        {
            Assert.ThrowsAsync<MaxNegativeCarryOverDaysException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeEntitlementBelowZeroIsNullButRequired_ThrowInvalidNegativeEntitlementBelowZeroArgumentException()
        {
            Assert.ThrowsAsync<InvalidNegativeEntitlementBelowZeroArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeEntitlementBelowZeroIsProvidedButNotRequired_ThrowInvalidNegativeEntitlementBelowZeroArgumentException()
        {
            Assert.ThrowsAsync<InvalidNegativeEntitlementBelowZeroArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeEntitlementBelowZeroIsBelowMinimum_ThrowInvalidNegativeEntitlementBelowZeroArgumentException()
        {
            Assert.ThrowsAsync<InvalidNegativeEntitlementBelowZeroArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT - 1,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxNegativeEntitlementBelowZeroIsAboveMaximum_ThrowInvalidNegativeEntitlementBelowZeroArgumentException()
        {
            Assert.ThrowsAsync<InvalidNegativeEntitlementBelowZeroArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MAX_NEGATIVE_ENTITLEMENT + 1,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenMaxNegativeEntitlementBelowZeroIsVAlid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MAX_NEGATIVE_ENTITLEMENT,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenExpireCarryOverDaysIsTrueWhileCarryOverNegativeEntitlementIsFalse_ThrowExpireCarriedOverDaysArgumentException()
        {
            Assert.ThrowsAsync<ExpireCarriedOverDaysArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, true, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMonthsForCarryOverExpiryIsProvidedButNotRequired_ThrowMonthsForCarryOverExpiryArgumentException()
        {
            Assert.ThrowsAsync<MonthsForCarryOverExpiryArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false,
                ArkiraConsts.MIN_MONTHS_FOR_CARRY_OVER_EXPIRY, null, null, LeaveRequestLengthFactor.EveryWorkingDay,
                true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMonthsForCarryOverExpiryIsNullButRequired_ThrowMonthsForCarryOverExpiryArgumentException()
        {
            Assert.ThrowsAsync<MonthsForCarryOverExpiryArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED, true, null, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMonthsForCarryOverExpiryIsBelowMinimum_ThrowMonthsForCarryOverExpiryArgumentException()
        {
            Assert.ThrowsAsync<MonthsForCarryOverExpiryArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED, true, ArkiraConsts.MIN_MONTHS_FOR_CARRY_OVER_EXPIRY - 1, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMonthsForCarryOverExpiryIsAboveMaximum_ThrowMonthsForCarryOverExpiryArgumentException()
        {
            Assert.ThrowsAsync<MonthsForCarryOverExpiryArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED, true, ArkiraConsts.MAX_MONTHS_FOR_CARRY_OVER_EXPIRY + 1, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenMonthsForCarryOverExpiryIsValid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, true,
                ArkiraConsts.MIN_NEGATIVE_CARRY_OVER_DAYS_ALLOWED, true, ArkiraConsts.MAX_MONTHS_FOR_CARRY_OVER_EXPIRY, null, null,
                LeaveRequestLengthFactor.EveryWorkingDay, true, true, ArkiraConsts.MIN_NEGATIVE_ENTITLEMENT, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenCarryOverLeaveYearStartsIsProvidedButNotRequired_ThrowCarryOverLeaveYearStartsArgumentException()
        {
            Assert.ThrowsAsync<CarryOverLeaveYearStartsArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null,
                CarryOverLeaveYearStarts.AnniversaryOfHireDate, null, LeaveRequestLengthFactor.EveryWorkingDay, false,
                false, null, LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenCarryOverLeaveYearStartsIsNullButRequired_ThrowCarryOverLeaveYearStartsArgumentException()
        {
            Assert.ThrowsAsync<CarryOverLeaveYearStartsArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, false, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenCarryOverLeaveYearStartsIsValid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.OnlyInCurrentYear, null,
                null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenCarryOverLeaveYearStartDateIsProvidedButNotRequired_ThrowCarryOverLeaveYearStartDateArgumentException()
        {
            Assert.ThrowsAsync<CarryOverLeaveYearStartDateArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, DateTime.Now, LeaveRequestLengthFactor.EveryWorkingDay, false, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenCarryOverLeaveYearStartDateIsNullButRequired_ThrowCarryOverLeaveYearStartDateArgumentException()
        {
            Assert.ThrowsAsync<CarryOverLeaveYearStartDateArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.GivenDayAndMonth, null, LeaveRequestLengthFactor.EveryWorkingDay, false, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenCarryOverLeaveYearStartDateIsValid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.GivenDayAndMonth, DateTime.Now, LeaveRequestLengthFactor.EveryWorkingDay, false, false, null,
                LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenMaxPeriodInFutureIsSetButNotRequired_ThrowMaxMonthsPeriodInFutureArgumentException()
        {
            Assert.ThrowsAsync<MaxMonthsPeriodInFutureArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null, LeaveRequestLengthFactor.EveryWorkingDay,
                false, false, null, LeaveRequestWindow.OnlyInCurrentYear, ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxPeriodInFutureIsNullButRequired_ThrowMaxMonthsPeriodInFutureArgumentException()
        {
            Assert.ThrowsAsync<MaxMonthsPeriodInFutureArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null, LeaveRequestLengthFactor.EveryWorkingDay,
                false, false, null, LeaveRequestWindow.SomePeriodInFuture, null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxPeriodInFutureIsBelowMinimum_ThrowMaxMonthsPeriodInFutureArgumentException()
        {
            Assert.ThrowsAsync<MaxMonthsPeriodInFutureArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null, LeaveRequestLengthFactor.EveryWorkingDay,
                false, false, null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE - 1, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenMaxPeriodInFutureIsAboveMaximum_ThrowMaxMonthsPeriodInFutureArgumentException()
        {
            Assert.ThrowsAsync<MaxMonthsPeriodInFutureArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null, LeaveRequestLengthFactor.EveryWorkingDay,
                false, false, null, LeaveRequestWindow.SomePeriodInFuture, ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE + 1, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenMaxPeriodInFutureIsValid_ReturnLeaveType()
        {
            var result = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.SomePeriodInFuture,
                ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION,
                NewYearLeaveRequestDeductionOptions.ProportionalFromEachYear, DateTime.Now.AddDays(1), null);

            result.Should().NotBeNull();
            result.Should().BeOfType<LeaveType>();
            result.Id.Should().NotBeEmpty();
            result.Name.Should().Be("abc");
        }

        [Test]
        public void CreateAsync_WhenNewYearActiveWindowIsProvidedButNotRequired_ThrowNewYearActiveWindowArgumentException()
        {
            Assert.ThrowsAsync<NewYearActiveWindowArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.OnlyInCurrentYear,
                null, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenNewYearActiveWindowIsNullButRequired_ThrowNewYearActiveWindowArgumentException()
        {
            Assert.ThrowsAsync<NewYearActiveWindowArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.CurrentAndFollowingYear,
                null, null, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenNewYearActiveWindowIsBelowMinimum_ThrowNewYearActiveWindowArgumentException()
        {
            Assert.ThrowsAsync<NewYearActiveWindowArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.CurrentAndFollowingYear,
                null, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION - 1, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenNewYearActiveWindowIsAboveMaximum_ThrowNewYearActiveWindowArgumentException()
        {
            Assert.ThrowsAsync<NewYearActiveWindowArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.CurrentAndFollowingYear,
                null, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION + 1, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<NewYearActiveWindowArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.SomePeriodInFuture,
                ArkiraConsts.MAX_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION + 1, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public async Task CreateAsync_WhenNewYearActiveWindowIsValid_ReturnLeaveType()
        {
            var result1 = await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.SomePeriodInFuture,
                ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION, NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);

            var result2 = await _service.CreateAsync(
                "abcd", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.CurrentAndFollowingYear,
                null, ArkiraConsts.MAX_MONTHS_FOR_NEW_YEAR_ACTIVATION, NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null);

            result1.Should().NotBeNull();
            result1.Should().BeOfType<LeaveType>();
            result2.Should().NotBeNull();
            result2.Should().BeOfType<LeaveType>();
        }

        [Test]
        public void CreateAsync_WhenNewYearRequestDeductionOptionsIsProvidedButNotRequired_ThrowNewYearDeductionOptionsArgumentException()
        {
            Assert.ThrowsAsync<NewYearDeductionOptionsArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.OnlyInCurrentYear,
                null, null, NewYearLeaveRequestDeductionOptions.AtFirstFromTheCurrentYear, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenNewYearRequestDeductionOptionsIsNullButRequired_ThrowNewYearDeductionOptionsArgumentException()
        {
            Assert.ThrowsAsync<NewYearDeductionOptionsArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.SomePeriodInFuture,
                ArkiraConsts.MIN_MONTHS_PERIOD_IN_FUTURE, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<NewYearDeductionOptionsArgumentException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                LeaveEntitlementReset.ResetNever, null, true, ArkiraConsts.MIN_CARRY_OVER_DAYS_ALLOWED, false, null,
                false, null, CarryOverLeaveYearStarts.AnniversaryOfHireDate, null,
                LeaveRequestLengthFactor.EveryWorkingDay, false, false, null, LeaveRequestWindow.CurrentAndFollowingYear,
                null, ArkiraConsts.MIN_MONTHS_FOR_NEW_YEAR_ACTIVATION, null, DateTime.Now.AddDays(1), null));
        }

        [Test]
        public void CreateAsync_WhenUnlimitedAndRequestWindowIsSomePeriodInFutureButEntitlementResetIsInvalid_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Unlimited, null, LeaveEntitlementReset.ResetOnSchedule, DateTime.Now, false, null,
                false, null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.SomePeriodInFuture, null, null, null, DateTime.Now.AddDays(1), null));

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.CreateAsync(
                "abc", EntitlementType.Unlimited, null, LeaveEntitlementReset.ResetOnWorkAnniversary, null, false, null,
                false, null, false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                LeaveRequestWindow.SomePeriodInFuture, null, null, null, DateTime.Now.AddDays(1), null));
        }
    }
}