﻿using Arkira.Core;
using Arkira.Core.Enums;
using Arkira.Core.LeavePolicies;
using Arkira.Core.LeaveTypes;
using Arkira.Core.Services;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Tests.Helpers;
using FluentAssertions;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeaveTypes
{
    internal class LeaveTypeDomainServiceRemoveTests
    {
        private LeaveTypeDomainService _leaveTypeDomainService;
        private LeaveType _leaveType;
        private LeavePolicyDomainService _leavePolicyDomainService;
        private ILeaveTypeRepository _leaveTypeRepository;
        private ILeavePolicyRepository _leavePolicyRepository;

        [SetUp]
        public async Task Initialize()
        {
            var context = MockDbContextFactory.Create().CreateDbContext();
            _leavePolicyRepository = new EfCoreLeavePolicyRepository(context);
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(context);
            EfCoreEmployeeRepository employeeRepository = new EfCoreEmployeeRepository(context);
            _leaveTypeDomainService = new LeaveTypeDomainService(_leaveTypeRepository, _leavePolicyRepository,
                                                                 employeeRepository);
            _leavePolicyDomainService = new LeavePolicyDomainService(_leavePolicyRepository, _leaveTypeRepository, employeeRepository,
                                                                      new Mock<IMediator>().Object);

            _leaveType = await _leaveTypeDomainService.CreateAsync("abc", EntitlementType.Standard,
                                                                   ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                                                   LeaveEntitlementReset.ResetNever, null, false, null,
                                                                   false, null, false, null, null, null,
                                                                   LeaveRequestLengthFactor.EveryWorkingDay, true, false,
                                                                   null, LeaveRequestWindow.OnlyInCurrentYear, null,
                                                                   null, null, DateTime.Now.AddDays(1), null);
            var result = await _leaveTypeRepository.UnitOfWork.CompleteAsync();

            result.Should().BeTrue();
        }

        [Test]
        public void RemoveAsync_WhenCalled_Success()
        {
            Assert.DoesNotThrowAsync(async () => await _leaveTypeDomainService.RemoveAsync(_leaveType.Id));
        }

        [Test]
        public void RemoveAsync_WhenIdIsInvalid_ThrowInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _leaveTypeDomainService.RemoveAsync(Guid.NewGuid()));
        }

        [Test]
        public async Task RemoveAsync_WhenLeaveTypeIsUsed_ThrowInvalidOperationException()
        {
            await _leavePolicyDomainService.CreateAsync("abc", _leaveType.Id, false, true, _leaveType.Id, MinLeaveDurationInDay.WholeDay, ArkiraConsts.MIN_DAYS_TO_ENROLLMENT, false, new SystemClock().UtcNow, null);
            await _leavePolicyRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(
                async () =>
                {
                    await _leaveTypeDomainService.RemoveAsync(_leaveType.Id);
                    await _leaveTypeRepository.UnitOfWork.CompleteAsync();
                });
        }
    }
}