﻿using Arkira.Core;
using Arkira.Core.Enums;
using Arkira.Core.LeaveTypes;
using Arkira.Infrastructure;
using Arkira.Infrastructure.Employees;
using Arkira.Infrastructure.LeavePolicies;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.LeaveTypes
{
    internal class LeaveTypeDomainServiceUpdateTests
    {
        private LeaveTypeDomainService _service;
        private LeaveType _leaveType;
        private ArkiraDbContext _context;
        private ILeaveTypeRepository _repository;

        [SetUp]
        public async Task Initialize()
        {
            _context = MockDbContextFactory.Create().CreateDbContext();
            _service = new LeaveTypeDomainService(new EfCoreLeaveTypeRepository(_context),
                                                  new EfCoreLeavePolicyRepository(_context),
                                                  new EfCoreEmployeeRepository(_context));
            _leaveType = await _service.CreateAsync("abc", EntitlementType.Standard,
                                                    ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                                    LeaveEntitlementReset.ResetNever, null, false, null, false, null,
                                                    false, null, null, null, LeaveRequestLengthFactor.EveryWorkingDay,
                                                    true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null, null,
                                                    null, DateTime.Now.AddDays(1), null);
            _repository = new EfCoreLeaveTypeRepository(_context);
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ReturnsLeaveType()
        {
            var result = await _service.UpdateAsync(_leaveType.Id, "abCD ", EntitlementType.Unlimited, null,
                                                    LeaveEntitlementReset.ResetNever, null, false, null,
                                                    false, null, false, null, null, null,
                                                    LeaveRequestLengthFactor.EveryCalendarDay, true, false, null,
                                                    LeaveRequestWindow.OnlyInCurrentYear, null, null, null,
                                                    DateTime.Now.AddDays(5), _leaveType.CorrelationId);

            result.Name.Should().BeEquivalentTo("abCD");
            result.Version.Should().Be(2);
            result.CorrelationId.Should().NotBeEmpty();
            result.CorrelationId.Should().Be(_leaveType.CorrelationId);
        }

        [Test]
        public async Task UpdateAsync_WhenLeaveTypeNameAlreadyExists_ThrowsInvalidOperationException()
        {
            await _service.CreateAsync("abcd", EntitlementType.Standard, ArkiraConsts.MIN_ENTITLEMENT_CAP_ALLOWED,
                                       LeaveEntitlementReset.ResetNever, null, false, null, false, null, false, null,
                                       null, null, LeaveRequestLengthFactor.EveryWorkingDay, true, false, null,
                                       LeaveRequestWindow.OnlyInCurrentYear, null, null, null, DateTime.Now.AddDays(1),
                                       null);
            await _repository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(
                async () =>
                {
                    await _service.UpdateAsync(_leaveType.Id, "abcD ", EntitlementType.Unlimited, null,
                                               LeaveEntitlementReset.ResetOnWorkAnniversary, null, false, null, false,
                                               null, false, null, null, null, LeaveRequestLengthFactor.EveryCalendarDay,
                                               true, false, null, LeaveRequestWindow.OnlyInCurrentYear, null, null, null,
                                               DateTime.Now.AddDays(5), _leaveType.CorrelationId);
                    await _repository.UnitOfWork.CompleteAsync();
                });
        }

        [Test]
        public async Task UpdateAsync_WhenSameNameIsUsed_ReturnsLeaveType()
        {
            var result = await _service.UpdateAsync(_leaveType.Id, "abc", EntitlementType.Unlimited, null,
                                                    LeaveEntitlementReset.ResetNever, null, false, null,
                                                    false, null, false, null, null, null,
                                                    LeaveRequestLengthFactor.EveryCalendarDay, true, false, null,
                                                    LeaveRequestWindow.OnlyInCurrentYear, null, null, null,
                                                    DateTime.Now.AddDays(5), _leaveType.CorrelationId);

            result.Should().NotBeNull();
        }

        [Test]
        public void UpdateAsync_WhenIdIsInvalid_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAsync<InvalidOperationException>(async () =>
                                                            await _service.UpdateAsync(Guid.NewGuid(), "abcD ",
                                                                                       EntitlementType.Unlimited, null,
                                                                                       LeaveEntitlementReset.ResetOnWorkAnniversary,
                                                                                       null, false, null, false, null,
                                                                                       false, null, null, null,
                                                                                       LeaveRequestLengthFactor.EveryCalendarDay,
                                                                                       true, false, null,
                                                                                       LeaveRequestWindow.OnlyInCurrentYear,
                                                                                       null, null, null,
                                                                                       DateTime.Now.AddDays(5),
                                                                                       _leaveType.CorrelationId));
        }
    }
}