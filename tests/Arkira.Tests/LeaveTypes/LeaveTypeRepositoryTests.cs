﻿using Arkira.Core.LeaveTypes;
using Arkira.Infrastructure.LeaveTypes;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace Arkira.Tests.LeaveTypes
{
    public class LeaveTypeRepositoryTests
    {
        private ILeaveTypeRepository _leaveTypeRepository;
        private LeaveTypeDomainService _leaveTypeDomainService;
        private LeaveType _leaveType;

        [SetUp]
        public void Initialize()
        {
            _leaveTypeRepository = new EfCoreLeaveTypeRepository(MockDbContextFactory.Create().CreateDbContext());
            _leaveTypeRepository.Should().NotBeNull();
        }
    }
}