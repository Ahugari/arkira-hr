﻿using Arkira.Core.NotificationCenter;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace Arkira.Tests.NotificationCenter
{
    internal class ApplicationCommentNotificationTests
    {
        private ApplicationNotificationContentBuilder _notificationContentBuilder;
        private string _actionPerformer;
        private string _contentOriginOwner;
        private string _applicationNotificationFor;
        private string _contentTypeDisplayName;

        [SetUp]
        public void Initialize()
        {
            _notificationContentBuilder = new();
            _actionPerformer = "John Doe";
            _contentOriginOwner = "Mary Doe";
            _applicationNotificationFor = "owner";
            _contentTypeDisplayName = "test application";
        }

        [TearDown]
        public void Clear()
        {
            _notificationContentBuilder.Reset();
        }

        [Test]
        public void GetNotificationContent_WhenApplicationHasCommentAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Commented);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Commented, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationHasCommentAndContentIsValid_ReturnReviewerContent()
        {
            _applicationNotificationFor = "reviewer";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Commented);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Commented, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }
    }
}