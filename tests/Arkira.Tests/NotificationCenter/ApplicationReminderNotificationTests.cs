﻿using Arkira.Core.NotificationCenter;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace Arkira.Tests.NotificationCenter
{
    internal class ApplicationReminderNotificationTests
    {
        private ApplicationNotificationContentBuilder _notificationContentBuilder;
        private string _actionPerformer;
        private string _contentOriginOwner;
        private string _applicationNotificationFor;
        private string _contentTypeDisplayName;

        [SetUp]
        public void Initialize()
        {
            _notificationContentBuilder = new();
            _actionPerformer = "John Doe";
            _contentOriginOwner = "Mary Doe";
            _applicationNotificationFor = "reviewer";
            _contentTypeDisplayName = "test application";
        }

        [TearDown]
        public void Clear()
        {
            _notificationContentBuilder.Reset();
        }

        [Test]
        public void GetNotificationContent_WhenApplicationHasCommentAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Reminder);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { _actionPerformer.Split(" ")[0], _contentTypeDisplayName, _contentOriginOwner }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }
    }
}