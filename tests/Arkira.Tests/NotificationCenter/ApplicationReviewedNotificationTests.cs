﻿using Arkira.Core.NotificationCenter;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace Arkira.Tests.NotificationCenter
{
    internal class ApplicationReviewedNotificationTests
    {
        private ApplicationNotificationContentBuilder _notificationContentBuilder;
        private string _actionPerformer;
        private string _contentOriginOwner;
        private string _applicationNotificationFor;
        private string _contentTypeDisplayName;

        [SetUp]
        public void Initialize()
        {
            _notificationContentBuilder = new();
            _actionPerformer = "John Doe";
            _contentOriginOwner = "Mary Doe";
            _applicationNotificationFor = "owner";
            _contentTypeDisplayName = "test application";
        }

        [TearDown]
        public void Clear()
        {
            _notificationContentBuilder.Reset();
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsApprovedAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Approved);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Approved, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsApprovedAndContentIsValid_ReturnWatcherContent()
        {
            _applicationNotificationFor = "watcher";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Approved);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Approved, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsRecommendedAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Recommended);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Recommended, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsRecommendedAndContentIsValid_ReturnWatcherContent()
        {
            _applicationNotificationFor = "watcher";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Recommended);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Recommended, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsDeniedAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Denied);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Denied, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsDeniedAndContentIsValid_ReturnWatcherContent()
        {
            _applicationNotificationFor = "watcher";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Denied);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Denied, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsWithdrawnAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Withdrawn);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Withdrawn, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsWithdrawnAndContentIsValid_ReturnWatcherContent()
        {
            _applicationNotificationFor = "watcher";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Withdrawn);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Withdrawn, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }
    }
}