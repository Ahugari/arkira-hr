﻿using Arkira.Core.NotificationCenter;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Arkira.Tests.NotificationCenter
{
    internal class ApplicationSentNotificationTests
    {
        private ApplicationNotificationContentBuilder _notificationContentBuilder;
        private string _actionPerformer;
        private string _contentOriginOwner;
        private string _applicationNotificationFor;
        private string _contentTypeDisplayName;

        [SetUp]
        public void Initialize()
        {
            _notificationContentBuilder = new();
            _actionPerformer = "John Doe";
            _contentOriginOwner = "Mary Doe";
            _applicationNotificationFor = "owner";
            _contentTypeDisplayName = "test application";
        }

        [TearDown]
        public void Clear()
        {
            _notificationContentBuilder.Reset();
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsSentAndContentIsValid_ReturnApplicantContent()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { ContentAction.Sent, _actionPerformer, _contentTypeDisplayName }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsSentAndContentIsInvalid_ThrowArgumentException()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("notification content tags");

            _notificationContentBuilder.Reset();
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("notification content tags");

            _notificationContentBuilder.Reset();
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("notification content tags");

            _notificationContentBuilder.Reset();
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("notification content tags");

            _notificationContentBuilder.Reset();
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("notification content tags");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsSentAndContentActionIsInvalid_ThrowArgumentException()
        {
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, "abc");
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent());
            Assert.Throws<ArgumentException>(() => _notificationContentBuilder.GetNotificationContent()).Message.Should().Contain("action");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsSentAndContentIsValid_ReturnReviewerContent()
        {
            _applicationNotificationFor = "reviewer";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { _contentTypeDisplayName, _contentOriginOwner }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }

        [Test]
        public void GetNotificationContent_WhenApplicationIsSentAndContentIsValid_ReturnWatcherContent()
        {
            _applicationNotificationFor = "watcher";
            _notificationContentBuilder.AddContentValue(NotificationContentTags.Action, ContentAction.Sent);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ActionPerformer, _actionPerformer);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentOriginOwner, _contentOriginOwner);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ApplicationNotificationFor, _applicationNotificationFor);
            _notificationContentBuilder.AddContentValue(NotificationContentTags.ContentTypeDisplayName, _contentTypeDisplayName);

            var result = _notificationContentBuilder.GetNotificationContent();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().ContainAll(new HashSet<string> { _contentTypeDisplayName, _contentOriginOwner, _actionPerformer, ContentAction.Sent }, "because all values indicated are required to qualify as valid application notification content for applicant.");
        }
    }
}