﻿using Arkira.Core.PublicHolidays;
using Arkira.Infrastructure.PublicHolidays;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Arkira.Tests.PublicHolidays
{
    [TestFixture]
    public class PublicHolidayDomainServiceTests
    {
        private PublicHolidayDomainService _publicHolidayDomainService;
        private IPublicHolidayRepository _publicHolidayRepository;

        [SetUp]
        public void Initialize()
        {
            var fakeContext = MockDbContextFactory.Create().CreateDbContext();
            var publicHolidayRepository = new EfCorePublicHolidayRepository(fakeContext);
            _publicHolidayDomainService = new PublicHolidayDomainService(publicHolidayRepository);
            _publicHolidayRepository = new EfCorePublicHolidayRepository(fakeContext);
        }

        [Test]
        public async Task CreateAsync_WhenCalled_ReturnPublicHolidayAsync()
        {
            var result = await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abc");

            result.Id.Should().NotBeEmpty();
        }

        [Test]
        public async Task UpdateAsync_WhenCalled_ReturnsUpdatedPublicHolidayAsync()
        {
            var publicHoliday = await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abc");
            await _publicHolidayRepository.UnitOfWork.CompleteAsync();

            PublicHoliday holidayUpdate = new PublicHoliday(DateTime.Now.AddDays(1), "abcd");
            var result = await _publicHolidayDomainService.UpdateAsync(publicHoliday, holidayUpdate);

            result.Day.Should().Be(DateTime.Now.AddDays(1).Date);
            result.Name.Should().Be("abcd");
            result.DateModified.Should().NotBe(DateTime.MinValue);
        }

        [Test]
        public async Task CreateAsync_WhenPublicHolidayAlreadyExists_ThrowInvalidOperationException()
        {
            await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abc");
            await _publicHolidayRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abc"));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.CreateAsync(DateTime.Now.AddDays(1), "abc"));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abcd"));
        }

        [Test]
        public async Task UpdateAsync_WhenPublicHolidayAlreadyExists_ThrowInvalidOperationException()
        {
            var publicHoliday = await _publicHolidayDomainService.CreateAsync(DateTime.Now, "abc");
            await _publicHolidayRepository.UnitOfWork.CompleteAsync();

            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.UpdateAsync(publicHoliday, new PublicHoliday(DateTime.Now, "abc")));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.UpdateAsync(publicHoliday, new PublicHoliday(DateTime.Now.AddDays(1), "abc")));
            Assert.ThrowsAsync<InvalidOperationException>(async () => await _publicHolidayDomainService.UpdateAsync(publicHoliday, new PublicHoliday(DateTime.Now, "abcd")));
        }
    }
}