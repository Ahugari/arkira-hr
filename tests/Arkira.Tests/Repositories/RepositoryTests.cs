﻿using Arkira.Core.Inboxes;
using Arkira.Core.Repositories;
using Arkira.Infrastructure.Repositories;
using Arkira.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arkira.Tests.Repositories
{
    internal class RepositoryTests
    {
        private IGenericCreateUpdateDeleteRepository<Inbox> _genericRepository;
        private Dictionary<string, HashSet<string>> _actions;

        [SetUp]
        public async Task Initialize()
        {
            var context = MockDbContextFactory.Create().CreateDbContext();
            _genericRepository = new GenericCreateUpdateDeleteRepository<Inbox>(new SaveTransactional<Inbox>(context), new ReadTransactional<Inbox>(context), new RemoveTransactional<Inbox>(context), context);
            _actions = new();
            _actions.Add("abc", new HashSet<string> { "abc" });
        }

        [Test]
        public async Task SaveAsync_WhenCalledWithApprovalItem_ReturnsNewInbox()
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddItemActions(_actions)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();
            var sentInbox = inboxBuilder.PrepareInbox(InboxSections.Sent)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();

            await _genericRepository.SaveAsync(inbox);
            await _genericRepository.SaveAsync(sentInbox);

            inbox.Id.Should().NotBe(Guid.Empty);

            inbox.Actions.Should().NotBeEmpty();
            inbox.Actions.Should().Equal(_actions);
            inbox.Actions.Should().Contain(_actions);
            inbox.TargetId.Should().Be("abc");
            inbox.ItemName.Should().Be("abc");
            inbox.ItemId.Should().Be("abc");

            sentInbox.TargetId.Should().Be("abc");
            sentInbox.ItemName.Should().Be("abc");
            sentInbox.ItemId.Should().Be("abc");
        }

        [Test]
        public async Task ReadOneAsync_WhenCalled_ReturnsExistingInbox()
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddItemActions(_actions)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();
            await _genericRepository.SaveAsync(inbox);
            await _genericRepository.UnitOfWork.CompleteAsync();

            var result = await _genericRepository.ReadOneAsync(inbox.Id);

            result.Id.Should().NotBe(Guid.Empty);
            result.Id.Should().Be(inbox.Id);
            result.Actions.Should().Equal(_actions);
            result.TargetId.Should().Be("abc");
            result.ItemName.Should().Be("abc");
            result.ItemId.Should().Be("abc");
        }

        [Test]
        public async Task ReadOnePredicateAsync_WhenCalled_ReturnsExistingInbox()
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddItemActions(_actions)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();
            await _genericRepository.SaveAsync(inbox);
            await _genericRepository.UnitOfWork.CompleteAsync();

            var result = await _genericRepository.ReadOneAsync(x => x.Id == inbox.Id);

            result.Id.Should().NotBe(Guid.Empty);
            result.Id.Should().Be(inbox.Id);
            result.Actions.Should().Equal(_actions);
            result.TargetId.Should().Be("abc");
            result.ItemName.Should().Be("abc");
            result.ItemId.Should().Be("abc");
        }

        [Test]
        public async Task ReadAllAsync_WhenCalled_ReturnsExistingInboxes()
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddItemActions(_actions)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();
            var sentInbox = inboxBuilder.PrepareInbox(InboxSections.Sent)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();

            await _genericRepository.SaveAsync(inbox);
            await _genericRepository.SaveAsync(sentInbox);
            await _genericRepository.UnitOfWork.CompleteAsync();

            var result = _genericRepository.ReadAll();

            result.Should().Contain(inbox);
            result.Should().Contain(sentInbox);
        }

        [Test]
        public async Task SaveAsync_WhenCalledWithUpdate_ReturnsUpdatedInbox()
        {
            var inboxBuilder = new ApprovalItem();
            var inbox = inboxBuilder.PrepareInbox(InboxSections.Inbox)
                .AddItemActions(_actions)
                .AddItemDetails("abc", "abc")
                .AddTarget("abc")
                .Build();
            await _genericRepository.SaveAsync(inbox);
            await _genericRepository.UnitOfWork.CompleteAsync();
            var existingInbox = await _genericRepository.ReadOneAsync(inbox.Id);

            existingInbox.CompleteItem();
            var result = await _genericRepository.SaveAsync(existingInbox);
            await _genericRepository.UnitOfWork.CompleteAsync();

            result.Id.Should().NotBe(Guid.Empty);
            result.Id.Should().Be(inbox.Id);
            result.Section.Should().Be(InboxSections.Completed);
        }
    }
}